////////////////////////////////////////////////////////////////////////////
//            SSSSSS  FFFFFF  EEEEEE  MMM   MMM        ++       ++        //
//           SS       FF      EE      MM M M MM       ++       ++         //
//           SSSSSSS  FFFF    EEEE    MM  M  MM   ++++++++ ++++++++       //
//                SS  FF      EE      MM     MM     ++       ++           //
//                SS  FF      EE      MM     MM    ++       ++            //
//           SSSSSS   FF      EEEEEE  MM     MM   ++       ++             //
////////////////////////////////////////////////////////////////////////////
#include "solver.hpp"
#include "Toolbox.hpp"

CSolver::CSolver()
{
    //ctor
}

CSolver::CSolver(CConfig *config, CGeometry *geometry)
{
    //ctor
}

CSolver::~CSolver()
{
    //dtor
}

vector<double> CSolver::GetNodalElemSize(CConfig *config, CGeometry *geometry){
    unsigned long nele = geometry->GetnElem();
    unsigned long nnode = geometry->GetnNode();

    vector<double> h_node_all(nnode);
    for(unsigned long inode = 0; inode < nnode; inode++){
        vector<double> h_inode;
        vector<unsigned long> ele_inode;
        ele_inode = geometry->GetNode(inode)->GetElems();
        for(unsigned iele=0; iele < ele_inode.size(); iele++){
            unsigned long ieleID = ele_inode[iele];
            h_inode.push_back(geometry->GetElem(ieleID)->Geth());
        }
        h_node_all[inode] = *min_element(h_inode.begin(), h_inode.end());
    }

    return h_node_all;
}

void CSolver::GetGPVelocity(vector<vector<double> > &gp_v_n,CPreprocessor *Prep_val,
                            vector<double> &v_n_val, unsigned short nDim){
    unsigned long ngp = Prep_val->GetnGP();
    vector<double> igp_v_n;
    double vx, vy, vz;
    unsigned short nspnode;
//    vector<unsigned long> igp_spnodes;
//    vector<double> igp_sf;

    for(unsigned long igp = 0; igp < ngp; igp++){
        nspnode = Prep_val->GetGP(igp)->GetnSupportNodes();
//        igp_spnodes = Prep_val->GetGP(igp)->GetSupportNodes();
//        igp_sf = Prep_val->GetGPsShapeFunc(igp);

        vx = 0.0;
        vy = 0.0;
        vz = 0.0;

        for(unsigned short i = 0; i < nspnode; i++){
//            unsigned long I = igp_spnodes[i];
//            double sf = igp_sf[i];
            unsigned long I = Prep_val->GetGP(igp)->GetSupportNode(i);
            double sf = Prep_val->GetGPsShapeFunc(igp,i);
            double vx_I, vy_I, vz_I;

            if(nDim == 2){
                vx_I = v_n_val[2*I];
                vy_I = v_n_val[2*I+1];
            }
            else{
                vx_I = v_n_val[3*I];
                vy_I = v_n_val[3*I+1];
                vz_I = v_n_val[3*I+2];
            }

            vx = vx + sf*vx_I;
            vy = vy + sf*vy_I;
            if(nDim == 3) vz = vz + sf*vz_I;
        }

//        igp_v_n.push_back(vx);
//        igp_v_n.push_back(vy);
//        if(nDim == 3) igp_v_n.push_back(vz);

        gp_v_n[igp][0] = vx;
        gp_v_n[igp][1] = vy;
        if(nDim == 3) gp_v_n[igp][2] = vz;

    }
}

void  CSolver::GetGPVelocityGrad(vector<vector<double> > &gp_gradv_n, CPreprocessor *Prep_val,
                                 vector<double> &v_n_val, unsigned short nDim){
    unsigned long ngp = Prep_val->GetnGP();
    vector<double> igp_gradv_n;
    double vxdx, vxdy, vxdz;
    double vydx, vydy, vydz;
    double vzdx, vzdy, vzdz;
    unsigned short nspnode;
//    vector<unsigned long> igp_spnodes;
//    vector<double> igp_sfdx, igp_sfdy, igp_sfdz;

    for(unsigned long igp = 0; igp < ngp; igp++){
        nspnode = Prep_val->GetGP(igp)->GetnSupportNodes();
//        igp_spnodes = Prep_val->GetGP(igp)->GetSupportNodes();
//        igp_sfdx = Prep_val->GetGPsShapeFuncDX(igp);
//        igp_sfdy = Prep_val->GetGPsShapeFuncDY(igp);
//        if(nDim == 3) igp_sfdz = Prep_val->GetGPsShapeFuncDZ(igp);
        vxdx = 0.0; vxdy = 0.0; vxdz = 0.0;
        vydx = 0.0; vydy = 0.0; vydz = 0.0;
        vzdx = 0.0; vzdy = 0.0; vzdz = 0.0;

        igp_gradv_n.clear();
        for(unsigned short i = 0; i < nspnode; i++){
//            unsigned long I = igp_spnodes[i];
//            double sfdx = igp_sfdx[i];
//            double sfdy = igp_sfdy[i];
//            double sfdz;
//            if(nDim == 3)  sfdz = igp_sfdz[i];
            unsigned long I = Prep_val->GetGP(igp)->GetSupportNode(i);
            double sfdx = Prep_val->GetGPsShapeFuncDX(igp,i);
            double sfdy = Prep_val->GetGPsShapeFuncDY(igp,i);
            double sfdz = 0.0;
            if(nDim == 3)  sfdz = Prep_val->GetGPsShapeFuncDZ(igp,i);

            double vx_I, vy_I, vz_I;

            if(nDim == 2){
                vx_I = v_n_val[2*I];
                vy_I = v_n_val[2*I+1];
            }
            else{
                vx_I = v_n_val[3*I];
                vy_I = v_n_val[3*I+1];
                vz_I = v_n_val[3*I+2];
            }

            vxdx = vxdx + sfdx*vx_I;
            vxdy = vxdy + sfdy*vx_I;
            if(nDim == 3) vxdz = vxdz + sfdz*vx_I;
            vydx = vydx + sfdx*vy_I;
            vydy = vydy + sfdy*vy_I;
            if(nDim == 3) vydz = vydz + sfdz*vy_I;
            if(nDim == 3){
                vzdx = vzdx + sfdx*vz_I;
                vzdy = vzdy + sfdy*vz_I;
                vzdz = vzdz + sfdz*vz_I;
            }

        }

//        igp_gradv_n.push_back(vxdx);
//        igp_gradv_n.push_back(vxdy);
//        if(nDim == 3) igp_gradv_n.push_back(vxdz);
//        igp_gradv_n.push_back(vydx);
//        igp_gradv_n.push_back(vydy);
//        if(nDim == 3) igp_gradv_n.push_back(vydz);
//        if(nDim == 3){
//            igp_gradv_n.push_back(vzdx);
//            igp_gradv_n.push_back(vzdy);
//            igp_gradv_n.push_back(vzdz);
//        }
        if(nDim == 2){
            gp_gradv_n[igp][0] = vxdx;
            gp_gradv_n[igp][1] = vxdy;
            gp_gradv_n[igp][2] = vydx;
            gp_gradv_n[igp][3] = vydy;
        }
        else{
            gp_gradv_n[igp][0] = vxdx;
            gp_gradv_n[igp][1] = vxdy;
            gp_gradv_n[igp][2] = vxdz;
            gp_gradv_n[igp][3] = vydx;
            gp_gradv_n[igp][4] = vydy;
            gp_gradv_n[igp][5] = vydz;
            gp_gradv_n[igp][6] = vzdx;
            gp_gradv_n[igp][7] = vzdy;
            gp_gradv_n[igp][8] = vzdz;
        }

    }

}

void CSolver::GetGPBML(vector<vector<double> >& gp_BML, CPreprocessor* Prep_val, unsigned short nDim){

    unsigned short igp_nrow;
    unsigned long ngp = Prep_val->GetnGP();
    if(nDim == 2){
        gp_BML.resize(3*ngp);
        igp_nrow = 3;
    }
    else{
        gp_BML.resize(6*ngp);
        igp_nrow = 6;
    }
    vector<vector<double> > igp_BML;
    //if ndim == 2, igp_BML is 3 rows x 2*nspnode cols
    //else if ndim == 3, igp_BML is 6 rows x 3*nspnode cols

    for(unsigned long igp = 0; igp < ngp; igp++){
        unsigned short nspnode = Prep_val->GetGP(igp)->GetnSupportNodes();

        if(nDim == 2){
            igp_BML.resize(3, vector<double>(2*nspnode, 0.0));
        }
        else{
            igp_BML.resize(6, vector<double>(3*nspnode, 0.0));
        }

        for(unsigned short ispnode = 0; ispnode < nspnode; ispnode++){
            double b1I = Prep_val->GetGPsShapeFuncDX(igp, ispnode);
            double b2I = Prep_val->GetGPsShapeFuncDY(igp, ispnode);
            double b3I;
            if(nDim == 3) b3I = Prep_val->GetGPsShapeFuncDZ(igp, ispnode);

            if(nDim == 2){
                igp_BML[0][2*ispnode] = b1I;

                igp_BML[1][2*ispnode+1] = b2I;

                igp_BML[2][2*ispnode] = b2I;
                igp_BML[2][2*ispnode+1] = b1I;

            }
            else{
                igp_BML[0][3*ispnode] = b1I;
                igp_BML[3][3*ispnode] = b2I;
                igp_BML[5][3*ispnode] = b3I;

                igp_BML[1][3*ispnode+1] = b2I;
                igp_BML[3][3*ispnode+1] = b1I;
                igp_BML[4][3*ispnode+1] = b3I;

                igp_BML[2][3*ispnode+2] = b3I;
                igp_BML[4][3*ispnode+2] = b2I;
                igp_BML[5][3*ispnode+2] = b1I;
            }

        }

        for(unsigned short igp_irow = 0; igp_irow < igp_nrow; igp_irow++){
            gp_BML[igp_nrow*igp + igp_irow] = igp_BML[igp_irow];
        }


    }
}

void CSolver::GetGPDeformGrad(vector<vector<double> >& gp_F, vector<double> &u, CPreprocessor *Prep_val,
                     unsigned short nDim, bool &flag_distorted){
    unsigned long ngp = Prep_val->GetnGP();
    gp_F.resize(ngp);


    for (unsigned long igp = 0; igp < ngp; igp++){
        unsigned short nspnode = Prep_val->GetGP(igp)->GetnSupportNodes();
        vector<double> igp_F;
        double detF;

        double L[3][3];
        double bI[3];
        double uI[3];

        L[0][0] = 0.0; L[0][1] = 0.0; L[0][2] = 0.0;
        L[1][0] = 0.0; L[1][1] = 0.0; L[1][2] = 0.0;
        L[2][0] = 0.0; L[2][1] = 0.0; L[2][2] = 0.0;

        for (unsigned short ispnode = 0; ispnode < nspnode; ispnode++)
        {
            unsigned long I = Prep_val->GetGP(igp)->GetSupportNode(ispnode);

            bI[0] = Prep_val->GetGPsShapeFuncDX(igp, ispnode);
            bI[1] = Prep_val->GetGPsShapeFuncDY(igp, ispnode);
            if(nDim == 3) bI[2] = Prep_val->GetGPsShapeFuncDZ(igp, ispnode);

            uI[0] = u[nDim*I];
            uI[1] = u[nDim*I+1];
            if(nDim == 3) uI[2] = u[nDim*I+2];

            //TODO: implicitly memeory leaking when nDim==3 (JC)
            L[0][0] = L[0][0] + bI[0]*uI[0];
            L[0][1] = L[0][1] + bI[1]*uI[0];
            L[0][2] = L[0][2] + bI[2]*uI[0];

            L[1][0] = L[1][0] + bI[0]*uI[1];
            L[1][1] = L[1][1] + bI[1]*uI[1];
            L[1][2] = L[1][2] + bI[2]*uI[1];
            if(nDim == 3){
                L[2][0] = L[2][0] + bI[0]*uI[2];
                L[2][1] = L[2][1] + bI[1]*uI[2];
                L[2][2] = L[2][2] + bI[2]*uI[2];
            }

        }

        L[0][0] = L[0][0] + 1.0;
        L[1][1] = L[1][1] + 1.0;
        L[2][2] = L[2][2] + 1.0;

        if(nDim == 2){
            igp_F.push_back(L[0][0]);
            igp_F.push_back(L[0][1]);
            igp_F.push_back(L[1][0]);
            igp_F.push_back(L[1][1]);

            detF = L[0][0]*L[1][1] - L[1][0]*L[0][1];
        }
        else{
            igp_F.push_back(L[0][0]);
            igp_F.push_back(L[0][1]);
            igp_F.push_back(L[0][2]);
            igp_F.push_back(L[1][0]);
            igp_F.push_back(L[1][1]);
            igp_F.push_back(L[1][2]);
            igp_F.push_back(L[2][0]);
            igp_F.push_back(L[2][1]);
            igp_F.push_back(L[2][2]);

            detF = L[0][0]*L[1][1]*L[2][2] - L[0][0]*L[1][2]*L[2][1] - L[0][1]*L[1][0]*L[2][2] +
                    L[0][1]*L[1][2]*L[2][0] + L[0][2]*L[1][0]*L[2][1] - L[0][2]*L[1][1]*L[2][0];
        }

        gp_F[igp] = igp_F;

        if(detF < 0.0){
            flag_distorted = true;
            break;
        }

        //debug
//        cout << "print gp_F:" << endl;
//        Print2DVecDouble(gp_F);


    }
}


void CSolver::GetGPB(vector<vector<double> > &gp_B, vector<vector<double> > &gp_F, CPreprocessor *Prep_val, unsigned short nDim){
    unsigned short igp_nrow;
    unsigned long ngp = Prep_val->GetnGP();

    vector<vector<double> > igp_B;

    for(unsigned long igp = 0; igp < ngp; igp++){
        unsigned short nspnode = Prep_val->GetGP(igp)->GetnSupportNodes();
        double L[3][3];
        if(nDim == 2){
              igp_nrow = 3;
              igp_B.clear();
              igp_B.resize(3, vector<double>(2*nspnode, 0.0));
              L[0][0] = gp_F[igp][0];
              L[0][1] = gp_F[igp][1];
              L[1][0] = gp_F[igp][2];
              L[1][1] = gp_F[igp][3];
        }
        else{
            igp_nrow = 6;
            igp_B.clear();
            igp_B.resize(6, vector<double>(3*nspnode, 0.0));
            L[0][0] = gp_F[igp][0];
            L[0][1] = gp_F[igp][1];
            L[0][2] = gp_F[igp][2];

            L[1][0] = gp_F[igp][3];
            L[1][1] = gp_F[igp][4];
            L[1][2] = gp_F[igp][5];

            L[2][0] = gp_F[igp][6];
            L[2][1] = gp_F[igp][7];
            L[2][2] = gp_F[igp][8];
        }

        double bI[3];
        for(unsigned short ispnode = 0; ispnode < nspnode; ispnode++){
            bI[0] = Prep_val->GetGPsShapeFuncDX(igp,ispnode);
            bI[1] = Prep_val->GetGPsShapeFuncDY(igp,ispnode);
            if(nDim == 3){
                bI[2] = Prep_val->GetGPsShapeFuncDZ(igp,ispnode);
            }

            if(nDim == 2){
                igp_B[0][2*ispnode] = L[0][0]*bI[0];
                igp_B[1][2*ispnode] = L[0][1]*bI[1];
                igp_B[2][2*ispnode] = L[0][0]*bI[1] + L[0][1]*bI[0];

                igp_B[0][2*ispnode+1] = L[1][0]*bI[0];
                igp_B[1][2*ispnode+1] = L[1][1]*bI[1];
                igp_B[2][2*ispnode+1] = L[1][0]*bI[1] + L[1][1]*bI[0];
            }
            else{
                igp_B[0][3*ispnode] = L[0][0]*bI[0];
                igp_B[1][3*ispnode] = L[0][1]*bI[1];
                igp_B[2][3*ispnode] = L[0][2]*bI[2];
                igp_B[3][3*ispnode] = L[0][0]*bI[1] + L[0][1]*bI[0];
                igp_B[4][3*ispnode] = L[0][1]*bI[2] + L[0][2]*bI[1];
                igp_B[5][3*ispnode] = L[0][0]*bI[2] + L[0][2]*bI[0];

                igp_B[0][3*ispnode+1] = L[1][0]*bI[0];
                igp_B[1][3*ispnode+1] = L[1][1]*bI[1];
                igp_B[2][3*ispnode+1] = L[1][2]*bI[2];
                igp_B[3][3*ispnode+1] = L[1][0]*bI[1] + L[1][1]*bI[0];
                igp_B[4][3*ispnode+1] = L[1][1]*bI[2] + L[1][2]*bI[1];
                igp_B[5][3*ispnode+1] = L[1][0]*bI[2] + L[1][2]*bI[0];

                igp_B[0][3*ispnode+2] = L[2][0]*bI[0];
                igp_B[1][3*ispnode+2] = L[2][1]*bI[1];
                igp_B[2][3*ispnode+2] = L[2][2]*bI[2];
                igp_B[3][3*ispnode+2] = L[2][0]*bI[1] + L[2][1]*bI[0];
                igp_B[4][3*ispnode+2] = L[2][1]*bI[2] + L[2][2]*bI[1];
                igp_B[5][3*ispnode+2] = L[2][0]*bI[2] + L[2][2]*bI[0];
            }

        }

        //debug
//        cout << "igp_B of gp " << igp << " = :" << endl;
//        Print2DVecDouble(igp_B);

        for(unsigned short igp_irow = 0; igp_irow < igp_nrow; igp_irow++){
            gp_B[igp_nrow*igp + igp_irow] = igp_B[igp_irow];
        }
    }
    //debug
//    cout << "gp_B  = :" << endl;
//    Print2DVecDouble(gp_B);
}
