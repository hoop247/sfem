////////////////////////////////////////////////////////////////////////////
//            SSSSSS  FFFFFF  EEEEEE  MMM   MMM        ++       ++        //
//           SS       FF      EE      MM M M MM       ++       ++         //
//           SSSSSSS  FFFF    EEEE    MM  M  MM   ++++++++ ++++++++       //
//                SS  FF      EE      MM     MM     ++       ++           //
//                SS  FF      EE      MM     MM    ++       ++            //
//           SSSSSS   FF      EEEEEE  MM     MM   ++       ++             //
////////////////////////////////////////////////////////////////////////////
#include "solver.hpp"

//+++++++++++++++++++++++++++++++++
// CSolverSolidTL
//+++++++++++++++++++++++++++++++++
CSolverSolidTL::CSolverSolidTL(){
    //ctor
    TimeIntegrationScheme = NULL;
    SpaceIntegration = NULL;
    Prep = NULL;
    Output = new COutputSolid;
    Loads = NULL;
    BCs = NULL;
    Materials = NULL;
    flag_solid_fail = false;
}

CSolverSolidTL::CSolverSolidTL(CConfig *config, CGeometry *geometry){
    //ctor
    TimeIntegrationScheme = NULL;
    SpaceIntegration = NULL;
    Prep = NULL;
    Output = new COutputSolid;
    Loads = NULL;
    BCs = NULL;
    Materials = NULL;
    flag_solid_fail = false;

    //prep the GPs or SDs
    Preprocessing(config, geometry);
    cout << "SFEM CSD: -----------  Preprocessing is done -----------" << endl;

    //Get DOFs and VALs of ndoes on BCs
    GetBCs(config, geometry);
    cout << "SFEM CSD: -----------  BCs are gotten -----------" << endl;

    //Get loads
    GetLoads(config, geometry);
    cout << "SFEM CSD: -----------  Loads are gotten -----------" << endl;

    //Get Material
    GetMaterial(config, geometry);
    cout << "SFEM CSD: -----------  Materials are gotten -----------" << endl;

    //Solve
    Solve(config, geometry);

}

void CSolverSolidTL::Preprocessing(CConfig *config, CGeometry *geometry){
    nDim = geometry->GetnDim();
    nnode = geometry->GetnNode();
    ndof = nDim*nnode;
    nele = geometry->GetnElem();
    unsigned short Kind_Elem = config->GetKind_Elem();
    gravity = config->GetGravity();
    unsigned short method = config->GetKind_Method();

    if(method == FEM){
        if(nDim == 2){
            if(Kind_Elem == T3){ //For Solid, FEM-T3
                //Get SF and SFdx info of different discretiztion methods.
                cout << "SFEM : FEM-T3 for 2D solid mechanics is used!" << endl;
                Prep = new CPreprocessorFEMTRI3(config, geometry);
            }
            else{ //For other 2D solver, like FEM-Q4 etc.
                cout << "SFEM : Error, Only FEM-T3 element for 2D solid mechanics is possible right now!" << endl;
                cout << "SFEM : Press any key to exit!!!" << endl;
                cin.get();
                exit(1);
            }
        }
        else{
            if(Kind_Elem == T4){
                cout << "SFEM : FEM-T4 for 3D solid mechanics is used!" << endl;
                Prep = new CPreprocessorFEMTET4(config, geometry);
            }
            else{ //For other 3D solver, like CS-FEM-T4 etc.
                cout << "SFEM : Error, Only FEM-T4 element for 3D solid mechanics is possible right now!" << endl;
                cout << "SFEM : Press any key to exit!!!" << endl;
                cin.get();
                exit(1);
            }

        }
    }
    else if(method == ESFEM){
        if(nDim == 2){//For CBS/ESFEM-T3
            if(Kind_Elem == T3){
                cout << "SFEM : ES-FEM-T3 for 2D solid mechanics is used!" << endl;
                Prep = new CPreprocessorESFEMTRI3(config, geometry);
            }
        }
        else{
            //For other 3D solver, like CBS/ES-FEM-T4 etc.
            cout << "SFEM : Error, ES-FEM-T4 element for 3D solid mechanics is not available right now!" << endl;
            cout << "SFEM : Press any key to exit!!!" << endl;
            cin.get();
            exit(1);
        }
    }
    else if(method  == FSFEM){
        if(Kind_Elem == T4){
            cout << "SFEM : FS-FEM-T4 for 3D solid mechanics is used!" << endl;
            Prep = new CPreprocessorFSFEMTET4(config, geometry);
        }
    }
    else{
        cout << "SFEM : Error, Only FEM, ES-FEM and FS-FEM are available right now!" << endl;
        cout << "SFEM : Press any key to exit!!!" << endl;
        cin.get();
        exit(1);
    }

    h_node = CSolver::GetNodalElemSize(config, geometry);

    // Get mass matrix (lumped)
    mass = Prep->GetLumpedMassAll();
    mass_inv.resize(mass.size());
    for(unsigned long i = 0; i < mass.size(); i++)
        mass_inv[i] = 1.0/mass[i];

}

void CSolverSolidTL::GetMaterial(CConfig *config, CGeometry *geometry){

    //For now only one material can be set to the whole solid
    mat_type = config->GetKind_Material();
    mat_para = config->GetMat_Para();
    Materials = new CMaterialSolid();
}

void CSolverSolidTL::GetBCs(CConfig *config, CGeometry *geometry){
    if(nDim == 2){
        BCs = new CBoundaryConditionCSD2D(config, geometry);
        cout << "SFEM : 2D Solid BCs are processed!" << endl;
    }
    else{
        BCs = new CBoundaryConditionCSD3D(config, geometry);
        cout << "SFEM : 3D Solid BCs are processed!" << endl;
    }
}

void CSolverSolidTL::GetLoads(CConfig *config, CGeometry *geometry){
    if(nDim == 2){
        Loads = new CLoad2D(config, geometry);
        cout << "SFEM : 2D Solid Loads are processed!" << endl;
    }
    else{
        Loads = new CLoad3D(config, geometry);
        cout << "SFEM : 3D Solid Loads are processed!" << endl;
    }

    F_ext = Loads->GetExtFAll();
}

void CSolverSolidTL::Solve(CConfig *config, CGeometry *geometry){
    //initialization
    u_n.resize(nDim*nnode,0.0);
    u_n_one.resize(nDim*nnode,0.0);
    v_n.resize(nDim*nnode, 0.0);
    v_n_one.resize(nDim*nnode, 0.0);
    a_n.resize(nDim*nnode, 0.0);
    a_n_one.resize(nDim*nnode, 0.0);

    node_coord_n = geometry->GetNode_coord();
    node_coord_n_one = node_coord_n;

    unsigned long ngp = Prep->GetnGP();
    gp_BML.resize((nDim-1)*3*ngp); //2D : igp_BML[3][2*nspnode], 3D: igp_BML[6][3*nspnode]
    gp_B.resize((nDim-1)*3*ngp); //2D : igp_B[3][2*nspnode], 3D: igp_B[6][3*nspnode]
    gp_F.resize(ngp, vector<double>(nDim*nDim)); //2D : igp_F[4], 3D: igp_F[9]
    gp_S.resize(ngp, vector<double>((nDim-1)*3)); //2D : igp_S[3], 3D: igp_S[6]

    Nodal_S_n.resize(nnode, vector<double>((nDim-1)*3));
    F_int.resize(nDim*nnode,0.0);
    F_gravity.resize(nDim*nnode,0.0);
    F_ext.resize(nDim*nnode,0.0);
    F_damp.resize(nDim*nnode,0.0);

    t_n = 0.0;
    t_n_one = 0.0;
    dt = config->GetDeltaTime();
    t_tot = config->GetTotalTime();

    unsigned long n = 0;
    unsigned long tot_n = config->GetnExtIter();

    unsigned long steps_con = config->GetWrt_Con_Freq();
    unsigned long steps_sol = config->GetWrt_Sol_Freq();

    //temporal loop
    double r_v = 1.0; //velocity residual
    double r_u = 1.0; //pressure residual
    double r_min = config->GetResidualMin();
    cout << "SFEM : -------------- Temporal loop begin (Solid Explicit T.L.) ---------------" << endl;
    for (n = 0; n < tot_n; n++){

        SingleIteration(config, geometry);

        if(flag_solid_fail){
            r_v = GetResidual(v_n_one, v_n);
            r_u = GetResidual(u_n_one, u_n);
            Output->WriteConvergeHistorySolid(config, r_u, r_v, n);
            cout << "SFEM : Solid in Explicit T.L. solver is failed!!!" << endl;
            cout << "SFEM : Press any key to exit!!!" << endl;
            cin.get();
            exit(1);
        }

        // check if need to print on screen
        if(n%steps_con == 0 & n>0){
            r_v = GetResidual(v_n_one, v_n);
            r_u = GetResidual(u_n_one, u_n);
            Output->WriteConvergeHistorySolid(config, r_u, r_v, n);
        }

        //update
        t_n_one = t_n + dt;
        t_n = t_n_one;
        u_n = u_n_one;
        v_n = v_n_one;
        a_n = a_n_one;
        node_coord_n = node_coord_n_one;

        // check if analysis finish or not.
        if(t_n >= t_tot){
            Output->WriteConvergeHistorySolid(config, r_u, r_v, n);
            Output->WriteSolutionSolid(config, geometry, u_n, v_n, Nodal_S_n, n, t_n);
            cout << "SFEM : -------------- Temporal loop Ends (XplT.L.) ---------------" << endl;
            break;
        }
        if(r_v < r_min){
            Output->WriteConvergeHistorySolid(config, r_u, r_v, n);
            Output->WriteSolutionSolid(config, geometry, u_n, v_n, Nodal_S_n, n, t_n);
            cout << "SFEM : -------------- Fluid is steady (XplT.L.) ---------------" << endl;
            break;
        }

        // check if need to write output solution files.
        if(n%steps_sol == 0){
            Output->WriteSolutionSolid(config, geometry, u_n, v_n, Nodal_S_n, n, t_n);
            cout << "SFEM : solution at time = " << t_n << " is saved."  << endl;
        }


    }


}

void CSolverSolidTL::SingleIteration(CConfig *config, CGeometry *geometry){
    unsigned short Kind_Solver = config->GetKind_Solver();
    if(Kind_Solver == EXPLICIT_DYNA){
        SingleIterXplTL(config, geometry);
    }
    else{
        cout << "SFEM : Error, implicit dynamic solver are not available right now!" << endl;
        cout << "SFEM : Press any key to exit!!!" << endl;
        cin.get();
        exit(1);
    }
}

void CSolverSolidTL::SingleIterXplTL(CConfig *config, CGeometry *geometry){
    double dt_n_half = dt;
    double t_n_half;

    //time update
    t_n_one = t_n + dt_n_half;
    t_n_half = 0.5*(t_n + t_n_one);

    //update the velocity at n+1/2 time step
    vector<double> v_n_half(nDim*nnode);
    for (unsigned long idof = 0; idof < ndof; idof++){
        v_n_half[idof] = v_n[idof] + (t_n_half - t_n)*a_n[idof];
    }

    ApplyVBC(v_n_half);

    //update the displacement and new node_coord at n+1 time step
    for (unsigned long idof = 0; idof < ndof; idof++){
        u_n_one[idof] = u_n[idof] + dt_n_half*v_n_half[idof];
    }

    ApplyUBC(u_n_one);

    for (unsigned long inode = 0; inode < nnode; inode++){
        for (unsigned short i = 0; i < nDim; i++){
            node_coord_n_one[inode][i] = node_coord_n[inode][i] +
                                        u_n_one[nDim*inode+i];
        }
    }

    //Get internal force
    GetInternalForce(F_int);
    //Get gravity force
    GetGravityForce(F_gravity);

    //Get external force
    GetExternalForce(F_ext);

    //Get damping force
    GetDampingForce(F_damp);

    //Get load amp
//    if(t_n_one < (t_tot/2.0)){
//        amp = t_n_one/(t_tot/2.0);
//    }
//    else{
//        amp = 1.0;
//    }
    amp = 1.0;

    //Get a_n_one
    for (unsigned long idof = 0; idof < ndof; idof++){
        a_n_one[idof] = mass_inv[idof]*(F_ext[idof] + F_gravity[idof] - F_int[idof] - F_damp[idof])*amp;
    }

    //update v_n_one
    for (unsigned long idof = 0; idof < ndof; idof++){
        v_n_one[idof] = v_n_half[idof] + (t_n_one - t_n_half)*a_n_one[idof];
    }
    ApplyVBC(v_n_one);
}

void CSolverSolidTL::GetInternalForce(vector<double> &F){
    flag_solid_fail = false;

    // get deformation gradient
    CSolver::GetGPDeformGrad(gp_F, u_n_one, Prep, nDim, flag_solid_fail);
    flag_solid_fail = CheckSolution();
    //debug
//    cout << "gp_F =" << endl;
//    Print2DVecDouble(gp_F);

    // get B strain-displace relation
    CSolver::GetGPB(gp_B, gp_F, Prep, nDim);
    //debug
//    cout << "gp_B" << endl;
//    Print2DVecDouble(gp_B);

    Materials->GetGPsPK2Stress(gp_S, gp_F, mat_type, mat_para, nDim);
    //debug
//    cout << "gp_S" << endl;
//    Print2DVecDouble(gp_S);

    // get nodal internal forces from PK2 stress
    std::fill(F.begin(), F.end(), 0.0);

    unsigned long ngp = Prep->GetnGP();
    for (unsigned long igp = 0; igp < ngp; igp++){
        unsigned short nspnode = Prep->GetGP(igp)->GetnSupportNodes();
        double weight = Prep->GetGP(igp)->GetWeight();
        unsigned long irow = (nDim-1)*3*igp;
        //loop spnodes of igp
        for (unsigned short ispnode = 0; ispnode < nspnode; ispnode++){
            unsigned long I = Prep->GetGP(igp)->GetSupportNode(ispnode);
            for (unsigned short i = 0; i < nDim; i++){
                for (unsigned short j = 0; j < (nDim-1)*3; j++){
                   F[nDim*I+i] =  F[nDim*I+i] +
                            gp_B[irow+j][nDim*ispnode+i]*gp_S[igp][j]*weight;
                }
            }
        }
    }


}


void CSolverSolidTL::GetExternalForce(vector<double> &F){
    F = Loads->GetExtFAll();
}

void CSolverSolidTL::GetGravityForce(vector<double> &F){
    //TODO (JC)
    F.resize(ndof,0.0);
}

void CSolverSolidTL::GetDampingForce(vector<double> &F){
    //TODO (JC)
    F.resize(ndof,0.0);
}




bool CSolverSolidTL::CheckSolution(){
    bool solid_fail = false;
    if(flag_solid_fail == true){
        solid_fail = true;

    }

    return solid_fail;
}

void CSolverSolidTL::ApplyVBC(vector<double> &v_val){
    unsigned long vbc_ndof = BCs->GetVBC_nDOF();
    for(unsigned long i = 0; i < vbc_ndof; i++){
        unsigned long I = BCs->GetVBC_DOF(i);
        v_val[I] = BCs->GetVBC_VAL(i);
    }
}

void CSolverSolidTL::ApplyUBC(vector<double> &u_val){
    unsigned long ubc_ndof = BCs->GetUBC_nDOF();
    for(unsigned long i = 0; i < ubc_ndof; i++){
        unsigned long I = BCs->GetUBC_DOF(i);
        u_val[I] = BCs->GetUBC_VAL(i);
    }
}

CSolverSolidTL::~CSolverSolidTL(){
    //dctor
}
