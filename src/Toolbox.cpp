#include "Toolbox.hpp"
double GetLength(vector<double> &p0_coord, vector<double> &p1_coord){
    double L;
    double dx,dy,dz;
    unsigned short nDim = p0_coord.size();

    dx = p1_coord[0] - p0_coord[0];
    dy = p1_coord[1] - p0_coord[1];
    if(nDim == 2){
        L = sqrt(dx*dx+dy*dy);
    }
    else if(nDim == 3){
        dz = p1_coord[2] - p0_coord[2];
        L = sqrt(dx*dx+dy*dy+dz*dz);
    }

    return L;
}

double GetAreaT3(vector<double> &p0_coord, vector<double> &p1_coord, vector<double> &p2_coord){
    double area, I, I0, I1, I2;
    int ndim;
    ndim = p0_coord.size();

    double x0, y0, z0;
    double x1, y1, z1;
    double x2, y2, z2;
    x0 = p0_coord[0];
    y0 = p0_coord[1];
    x1 = p1_coord[0];
    y1 = p1_coord[1];
    x2 = p2_coord[0];
    y2 = p2_coord[1];

    if(ndim == 2)
    {
        area = 0.5*fabs((x0-x2)*(y1-y0) - (x0-x1)*(y2-y0));
    }
    else if(ndim == 3)
    {
        z0 = p0_coord[2];
        z1 = p1_coord[2];
        z2 = p2_coord[2];
        I0 = sqrt((x0-x1)*(x0-x1) + (y0-y1)*(y0-y1) + (z0-z1)*(z0-z1));
        I1 = sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) + (z2-z1)*(z2-z1));
        I2 = sqrt((x0-x2)*(x0-x2) + (y0-y2)*(y0-y2) + (z0-z2)*(z0-z2));
        I = (I0+I1+I2)*0.5;
        area = sqrt(I*(I-I0)*(I-I1)*(I-I2));
    }

    return area;
}

double GetVolT4(vector<double> &p0_coord, vector<double> &p1_coord, vector<double> &p2_coord,
                vector<double> &p3_coord){
    double m[3][3];
    double detm;
    m[0][0] = p0_coord[0] - p1_coord[0];
    m[0][1] = p0_coord[1] - p1_coord[1]; //y[0] - y[1];
    m[0][2] = p0_coord[2] - p1_coord[2]; //z[0] - z[1];

    m[1][0] = p0_coord[0] - p2_coord[0]; //x[0] - x[2];
    m[1][1] = p0_coord[1] - p2_coord[1]; //y[0] - y[2];
    m[1][2] = p0_coord[2] - p2_coord[2]; //z[0] - z[2];

    m[2][2] = p0_coord[2] - p3_coord[2]; //z[0] - z[3];
    m[2][0] = p0_coord[0] - p3_coord[0]; //x[0] - x[3];
    m[2][1] = p0_coord[1] - p3_coord[1]; //y[0] - y[3];

    detm = m[0][0]*m[1][1]*m[2][2]+m[0][1]*m[1][2]*m[2][0]+m[0][2]*m[1][0]*m[2][1]
            -m[2][0]*m[1][1]*m[0][2]-m[1][0]*m[0][1]*m[2][2]-m[0][0]*m[2][1]*m[1][2];

    return detm*1.666666667; //detm/6
}



template <typename t>
std::vector<std::vector<t> > GetUniqueRows(std::vector<std::vector<t> > &input)
{
    std::sort(input.begin(), input.end());
    input.erase(std::unique(input.begin(), input.end()), input.end());
    return input;
}

std::vector<unsigned long> GetIntersection(std::vector<unsigned long> &input0, std::vector<unsigned long> &input1)
{
    std::vector<unsigned long> output;
    std::vector<unsigned long>::iterator it;
    std::sort(input0.begin(), input0.end());
    std::sort(input1.begin(), input1.end());
    it = std::set_intersection(input0.begin(), input0.end(),
                               input1.begin(), input1.end(),
                               output.begin());
    output.resize(it - output.begin());
    return output;
}

// several faster functions than built-in functions.
inline double fastInvSqrt(double x)
{
    double xhalf = 0.5f*x;
    int i = *(int*)&x;
    i = 0x5f3759df - (i>>1);
    x = *(double*)&i;
    x = x*(1.5f - xhalf*x*x);
    return x;
}

inline double fastSqrt(double x)
{
    double xhalf = 0.5f*x;
    int i = *(int*)&x;
    i = 0x5f3759df - (i>>1);
    x = *(double*)&i;
    x = x*(1.5f - xhalf*x*x);
    return 1.0/x;
}

inline double fastPow(const double a, const double b) {
  // be carefull about the accuracy, error about 1%
  union {
    double d;
    int x[2];
  } u = { a };
  u.x[1] = (int)(b * (u.x[1] - 1072632447) + 1072632447);
  u.x[0] = 0;
  return u.d;
}

double L2norm(vector<double> &a){
    double norm = 0.0;
    unsigned long n = a.size();

    for(unsigned long i = 0; i < n; i++){
        norm = norm + a[i]*a[i];
    }

    return fastPow(norm, 0.5);
}

double GetResidual(vector<double> &a_n_one, vector<double> &a_n){
    unsigned long n = a_n_one.size();
    vector<double> da(n);

    if(a_n_one.size() != a_n.size()){
        cout << "SFEM : Input vectors for GetResidual must have same size!" << endl;
        cout << "SFEM : Press any key to exit!!!" << endl;
        cin.get();
        exit(1);
    }

    for(unsigned long i=0; i < n; i++){
        da[i] = a_n_one[i] - a_n[i];
    }

    return L2norm(da)/L2norm(a_n);

}

void Print2DVecDouble(vector<vector<double> > &A){
    unsigned long nrow = A.size();
    for(unsigned long i = 0; i < nrow; i++){
        unsigned long ncol = A[i].size();
        for(unsigned long j = 0; j < ncol; j++){
            cout << A[i][j] << " ";
        }
        cout << endl;
    }
    cout << endl;
}
