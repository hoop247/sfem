////////////////////////////////////////////////////////////////////////////
//            SSSSSS  FFFFFF  EEEEEE  MMM   MMM        ++       ++        //
//           SS       FF      EE      MM M M MM       ++       ++         //
//           SSSSSSS  FFFF    EEEE    MM  M  MM   ++++++++ ++++++++       //
//                SS  FF      EE      MM     MM     ++       ++           //
//                SS  FF      EE      MM     MM    ++       ++            //
//           SSSSSS   FF      EEEEEE  MM     MM   ++       ++             //
////////////////////////////////////////////////////////////////////////////
#include "load.hpp"

CLoad::CLoad()
{
    //ctor
    nNode = 0;
    nDim = 0;
}

CLoad::CLoad(CConfig *config, CGeometry *geometry)
{
    nNode = geometry->GetnNode();
    nDim = geometry->GetnDim();

}

void CLoad::Display(){
    cout << "Load display:" << endl;
    cout << "  nNode: " << nNode << endl;
    cout << "  nDim: " << nDim << endl;

    for (unsigned long i = 0; i < nNode*nDim; i++){
        cout << "F_ext[" << i << "] = " << F_ext[i] << endl;
    }

}


CLoad::~CLoad()
{
    //dtor
}

//////////////////////////////////////////
// CLoad2D
//////////////////////////////////////////

CLoad2D::CLoad2D(){
    //ctor
    nNode = 0;
    nDim = 0;
}

CLoad2D::CLoad2D(CConfig* config, CGeometry* geometry){
    nNode = geometry->GetnNode();
    nDim = geometry->GetnDim();

    //initilazation
    F_ext.resize(nNode*nDim,0.0);
    F_ext_CF.resize(nNode*nDim,0.0);
    F_ext_pressure.resize(nNode*nDim,0.0);
    F_ext_traction.resize(nNode*nDim,0.0);

    //
    SetExtF_CF(config, geometry);
    SetExtF_Pressure(config, geometry);
    SetExtF_Traction(config, geometry);

    //
    for (unsigned long idof = 0; idof < nNode*nDim; idof++){
        F_ext[idof] = F_ext_CF[idof] + F_ext_pressure[idof] + F_ext_traction[idof];
    }


}

void CLoad2D::SetExtF_CF(CConfig* config, CGeometry* geometry){
    //TODO:
}

void CLoad2D::SetExtF_Pressure(CConfig* config, CGeometry* geometry){
    vector<string> marker_tags;
    unsigned short nmarker;
    string imarker_tag;
    double pressure;
    double normal_x, normal_y;
    double area;
    double f_x, f_y;

    //loop all markers
    nmarker = config->GetnMarker_Pressure();
    for(unsigned short imarker=0; imarker < nmarker; imarker++){
        // find pressure makers
        imarker_tag = config->GetMarkerTag_Pressure(imarker);
        CBoundMarker *marker = geometry->GetMarker(imarker_tag);
        pressure = config->GetPressure(imarker_tag);
        cout << "SFEM : ----------------------  Prepare Pressure BC (CSD) --------------------" << endl;
        cout << "SFEM : Pressure Load for marker = " << imarker_tag << endl;
        unsigned long nbe = marker->GetnBoundElem();
        for(unsigned long ibe=0; ibe < nbe; ibe++){
            //get i-th bound elements area
            area = marker->GetBoundElemVol(ibe);
            //get i-th bound elements normal
            normal_x = marker->GetBoundElemNormal(ibe)[0];
            normal_y = marker->GetBoundElemNormal(ibe)[1];
            //get element's pressure component
            f_x = normal_x*area*pressure;
            f_y = normal_y*area*pressure;
            //set nodal forces to nodes of ibe element
            CElem* be = marker->GetBoundElem(ibe);
            unsigned short nnode = be->GetnNodes();
            for(unsigned short inode =0; inode < nnode; inode++){
                unsigned long node_id = be->GetNode(inode);
                F_ext_pressure[node_id*nDim] = F_ext_pressure[node_id*nDim] + f_x*0.5*-1.0;
                F_ext_pressure[node_id*nDim+1] = F_ext_pressure[node_id*nDim+1] + f_y*0.5*-1.0;
            }

        }
        cout << "SFEM : " << nbe << " Pressure bele, with P = " << pressure  << endl;
        cout << endl;
    }


}

void CLoad2D::SetExtF_Traction(CConfig* config, CGeometry* geometry){
    //TODO:
}


CLoad2D::~CLoad2D(){
    //dctor
}


//////////////////////////////////////////
// CLoad3D
//////////////////////////////////////////

CLoad3D::CLoad3D(){
    //ctor
}

CLoad3D::CLoad3D(CConfig *config, CGeometry *geometry){
    //ctor
    nNode = geometry->GetnNode();
    nDim = geometry->GetnDim();

    //initilazation
    F_ext.resize(nNode*nDim,0.0);
    F_ext_CF.resize(nNode*nDim,0.0);
    F_ext_pressure.resize(nNode*nDim,0.0);
    F_ext_traction.resize(nNode*nDim,0.0);

    //
    SetExtF_CF(config, geometry);
    SetExtF_Pressure(config, geometry);
    SetExtF_Traction(config, geometry);

    for (unsigned long idof = 0; idof < nNode*nDim; idof++){
        F_ext[idof] = F_ext_CF[idof] + F_ext_pressure[idof] + F_ext_traction[idof];
    }
}

void CLoad3D::SetExtF_CF(CConfig* config, CGeometry* geometry){
    //TODO:
}

void CLoad3D::SetExtF_Pressure(CConfig* config, CGeometry* geometry){
    vector<string> marker_tags;
    unsigned short nmarker;
    string imarker_tag;
    double pressure;
    double normal_x, normal_y, normal_z;
    double area;
    double f_x, f_y, f_z;

    //loop all markers
    nmarker = config->GetnMarker_Pressure();
    for(unsigned short imarker=0; imarker < nmarker; imarker++){
        // find pressure makers
        imarker_tag = config->GetMarkerTag_Pressure(imarker);
        CBoundMarker *marker = geometry->GetMarker(imarker_tag);
        pressure = config->GetPressure(imarker_tag);
        cout << "SFEM : ----------------------  Prepare Pressure BC (CSD) --------------------" << endl;
        cout << "SFEM : Pressure Load for marker = " << imarker_tag << endl;
        unsigned long nbe = marker->GetnBoundElem();
        for(unsigned long ibe=0; ibe < nbe; ibe++){
            //get i-th bound elements area
            area = marker->GetBoundElemVol(ibe);
            //get i-th bound elements normal
            normal_x = marker->GetBoundElemNormal(ibe)[0];
            normal_y = marker->GetBoundElemNormal(ibe)[1];
            normal_z = marker->GetBoundElemNormal(ibe)[2];
            //get element's pressure component
            f_x = normal_x*area*pressure;
            f_y = normal_y*area*pressure;
            f_z = normal_z*area*pressure;
            //set nodal forces to nodes of ibe element
            CElem* be = marker->GetBoundElem(ibe);
            unsigned short nnode = be->GetnNodes();
            for(unsigned short inode =0; inode < nnode; inode++){
                unsigned long node_id = be->GetNode(inode);
                F_ext_pressure[node_id*nDim] = F_ext_pressure[node_id*nDim]
                                            + f_x*0.33333*-1.0;
                F_ext_pressure[node_id*nDim+1] = F_ext_pressure[node_id*nDim+1]
                                            + f_y*0.33333*-1.0;
                F_ext_pressure[node_id*nDim+2] = F_ext_pressure[node_id*nDim+2]
                                            + f_z*0.33333*-1.0;
            }

        }
        cout << "SFEM : " << nbe << " Pressure bele, with P = " << pressure  << endl;
        cout << endl;
    }


}

void CLoad3D::SetExtF_Traction(CConfig* config, CGeometry* geometry){
    //TODO:
}


CLoad3D::~CLoad3D(){
    //dctor
}
