////////////////////////////////////////////////////////////////////////////
//            SSSSSS  FFFFFF  EEEEEE  MMM   MMM        ++       ++        //
//           SS       FF      EE      MM M M MM       ++       ++         //
//           SSSSSSS  FFFF    EEEE    MM  M  MM   ++++++++ ++++++++       //
//                SS  FF      EE      MM     MM     ++       ++           //
//                SS  FF      EE      MM     MM    ++       ++            //
//           SSSSSS   FF      EEEEEE  MM     MM   ++       ++             //
////////////////////////////////////////////////////////////////////////////
#include "solver.hpp"
#include "Toolbox.hpp"


//+++++++++++++++++++++++++++++++++
// CSolverFluidFractalStep_Laminar
//+++++++++++++++++++++++++++++++++
CSolverSemiImpCBSLaminar::CSolverSemiImpCBSLaminar()
{
    TimeIntegrationScheme = NULL;
    SpaceIntegration = NULL;
    Prep = NULL;
    Output = new COutputFluid;
    Loads = NULL;
    BCs = NULL;
    Materials = NULL;
    flag_fluid_fail = false;

}

CSolverSemiImpCBSLaminar::CSolverSemiImpCBSLaminar(CConfig *config, CGeometry *geometry)
{
    TimeIntegrationScheme = NULL;
    SpaceIntegration = NULL;
    Prep = NULL;
    Output = new COutputFluid;
    Loads = NULL;
    BCs = NULL;
    Materials = NULL;
    flag_fluid_fail = false;

    //prep the GPs or SDs
    Preprocessing(config, geometry);
    cout << "SFEM CFD: ----------- Preprocessing is done -----------" << endl;

    //Get DOFs and VALs of nodes on BCs
    GetBCs(config, geometry);
    cout << "SFEM CFD: ----------- BCs are gotten -----------" << endl;

    //Get material parameters
    GetMaterial(config, geometry);
    cout << "SFEM CFD: ----------- Material para are gotten -----------" << endl;
    //Solve
    Solve(config, geometry);

}

void CSolverSemiImpCBSLaminar::Preprocessing(CConfig* config, CGeometry* geometry){
    nDim = geometry->GetnDim();
    nnode = geometry->GetnNode();
    nele = geometry->GetnElem();
    unsigned short Kind_Elem = config->GetKind_Elem();
    gravity = config->GetGravity();
    unsigned short method = config->GetKind_Method();

    if(method == FEM){
        if(nDim == 2){
            if(Kind_Elem == T3){ //For CBS/FEM-T3
                //Get SF and SFdx info of different discretiztion methods.
                Prep = new CPreprocessorFEMTRI3(config, geometry);
            }
            else{ //For other 2D solver, like CBS/CS-FEM-Q4 etc.
                cout << "SFEM : Error, Only FEM-T3 element for 2D semi-implicit CBS is possible right now!" << endl;
                cout << "SFEM : Press any key to exit!!!" << endl;
                cin.get();
                exit(1);
            }
        }
        else{
            if(Kind_Elem == T4){
                Prep = new CPreprocessorFEMTET4(config, geometry);
            }
            else{ //For other 3D solver, like CBS/CS-FEM-Q4 etc.
                cout << "SFEM : Error, Only FEM-T4 element for 3D semi-implicit CBS is possible right now!" << endl;
                cout << "SFEM : Press any key to exit!!!" << endl;
                cin.get();
                exit(1);
            }

        }
    }
    else if(method == ESFEM){
        if(nDim == 2){//For CBS/ESFEM-T3
            if(Kind_Elem == T3){
                cout << "SFEM : ES-FEM-T3 for 2D semi-implicit CBS is used!" << endl;
                Prep = new CPreprocessorESFEMTRI3(config, geometry);
            }
        }
        else{
            //For other 3D solver, like CBS/ES-FEM-T4 etc.
            cout << "SFEM : Error, ES-FEM-T4 element for 3D semi-implicit CBS is not available right now!" << endl;
            cout << "SFEM : Press any key to exit!!!" << endl;
            cin.get();
            exit(1);
        }
    }
    else if(method  == FSFEM){
        if(Kind_Elem == T4){
            cout << "SFEM : FS-FEM-T4 for 3D semi-implicit CBS is used!" << endl;
            Prep = new CPreprocessorFSFEMTET4(config, geometry);
        }
    }
    else{
        cout << "SFEM : Error, Only FEM, ES-FEM and FS-FEm are available right now!" << endl;
        cout << "SFEM : Press any key to exit!!!" << endl;
        cin.get();
        exit(1);
    }

    h_node = CSolver::GetNodalElemSize(config, geometry);

    SetPressureStiffness(H_eigen);
    cout << "SFEM : ----------- Get PressureStiffness -----------" << endl;

    // Get mass matrix (lumped)
    mass = Prep->GetLumpedMassAll();
    mass_inv.resize(mass.size());
    for(unsigned long i = 0; i < mass.size(); i++)
        mass_inv[i] = 1.0/mass[i];
    cout << "SFEM : ----------- Get Lumped Mass -----------" << endl;
}

void CSolverSemiImpCBSLaminar::GetMaterial(CConfig *config, CGeometry *geometry){
    double *mat_para;
    mat_para = config->GetMat_Para_Fluid();
    this->density = mat_para[0];
    this->mu = mat_para[1];
}

void CSolverSemiImpCBSLaminar::GetBCs(CConfig *config, CGeometry *geometry){
    nDim = geometry->GetnDim();

    if(nDim == 2){
        BCs = new CBoundaryConditionCFD2D(config, geometry);
    }
    else{
        BCs = new CBoundaryConditionCFD3D(config, geometry);
    }

    //Apply PBC to H
    ApplyPBC_H(H_eigen);

}

void CSolverSemiImpCBSLaminar::Solve(CConfig *config, CGeometry *geometry){
    v_n.resize(nDim*nnode);
    v_n_one.resize(nDim*nnode);
    p_n.resize(nnode);
    p_n_one.resize(nnode);
    v_m.resize(nDim*nnode);
    rhs_m.resize(nnode);
    rhs_n_one.resize(nDim*nnode);
    F.resize(nDim*nnode);

    //intial condition
    /// TODO (JC#1#): Add codes for initial conditons
    std::fill(v_n.begin(), v_n.end(), 0.0);
    std::fill(v_n_one.begin(), v_n_one.end(), 0.0);
    std::fill(p_n.begin(), p_n.end(), 0.0);
    std::fill(p_n_one.begin(), p_n_one.end(), 0.0);

    unsigned long ngp = Prep->GetnGP();
    gp_v_n.resize(ngp, vector<double>(nDim));
    gp_gradv_n.resize(ngp, vector<double>(nDim*nDim));

    t_n = 0.0;
    t_n_one = 0.0;
    dt = config->GetDeltaTime(); //Todo: can add adaptive time step in future (JC)
    double t_tot = config->GetTotalTime();

    unsigned long n = 0;
    unsigned long tot_n = config->GetnExtIter();

    unsigned long steps_con = config->GetWrt_Con_Freq();
    unsigned long steps_sol = config->GetWrt_Sol_Freq();

    double r_v = 1.0; //velocity residual
    double r_p = 1.0; //pressure residual
    double r_min = config->GetResidualMin();

    //temporal loop
    cout << "SFEM : -------------- Temporal loop begin (SemiImpCBS) ---------------" << endl;
    for(n = 0; n < tot_n; n++){

        /** **/
        SingleIteration(config, geometry);

        if(flag_fluid_fail){
            r_v = GetResidual(v_n_one, v_n);
            r_p = GetResidual(p_n_one, p_n);
            Output->WriteConvergeHistoryFluid(config, r_v, r_p, n);
            cout << "SFEM : Fluid in Semi-implicit CBS Laminar solver is failed!!!" << endl;
            cout << "SFEM : Press any key to exit!!!" << endl;
            cin.get();
            exit(1);
        }

        // check if need to print on screen

        if(n%steps_con == 0 & n>0){
            r_v = GetResidual(v_n_one, v_n);
            r_p = GetResidual(p_n_one, p_n);
            Output->WriteConvergeHistoryFluid(config, r_v, r_p, n);
        }


        //update
        t_n_one = t_n + dt;
        t_n = t_n_one;
        v_n = v_n_one;
        p_n = p_n_one;

        // check if analysis finish or not.
        if(t_n >= t_tot){
            Output->WriteConvergeHistoryFluid(config, r_v, r_p, n);
            Output->WriteSolutionFluid(config, geometry, v_n, p_n, n, t_n);
            cout << "SFEM : -------------- Temporal loop Ends (SemiImpCBS) ---------------" << endl;
            break;
        }
        if(r_v < r_min){
            Output->WriteConvergeHistoryFluid(config, r_v, r_p, n);
            Output->WriteSolutionFluid(config, geometry, v_n, p_n, n, t_n);
            cout << "SFEM : -------------- Fluid is steady  (SemiImpCBS) ---------------" << endl;
            break;
        }

        // check if need to write output solution files.
        if(n%steps_sol == 0){
            Output->WriteSolutionFluid(config, geometry, v_n, p_n, n, t_n);
            cout << "SFEM : solution at time = " << t_n << " is saved."  << endl;
        }


    }//end of temporal loop

}

void CSolverSemiImpCBSLaminar::SingleIteration(CConfig *config, CGeometry *geometry){


    //Get GPs' velocity and velocity gradient
    CSolver::GetGPVelocity(gp_v_n, Prep, v_n, nDim);
    CSolver::GetGPVelocityGrad(gp_gradv_n, Prep, v_n, nDim);

    //debug
//    for(unsigned long igp = 0; igp < gp_v_n.size(); igp++){
//        cout << "gp_v_n[" << igp << "] = " << gp_v_n[igp][0] << "," << gp_v_n[igp][1] << endl;
//    }
//
//    for(unsigned long igp = 0; igp < gp_gradv_n.size(); igp++){
//        cout << "gp_gradv_n[" << igp << "] = "
//        << gp_gradv_n[igp][0] << "," << gp_gradv_n[igp][1] << ","
//        << gp_gradv_n[igp][2] << "," << gp_gradv_n[igp][3]<< endl;
//    }

    //Step1: get the intermidiate velcoity
    Step1(gp_v_n, gp_gradv_n);
    ApplyVBC(v_m);
    //debug
//    for(unsigned long i = 0; i < v_m.size(); i++ ){
//        cout << "v_m[" << i << "] = "<< v_m[i] << endl;
//    }

    //Step2: get pressure
    Step2(v_m);

    //Step3: get corrected velocity
    Step3(1.0);
    ApplyVBC(v_n_one);

    //Get forces at all fluid nodes.
    GetForce(F);

    //Check if solver is failed.
    flag_fluid_fail = CheckSolution();

}

void CSolverSemiImpCBSLaminar::Step1(vector<vector<double> > &gp_v_n, vector<vector<double> > &gp_grad_v_n){
    unsigned long ndof = nnode*nDim;
    unsigned long ngp = Prep->GetnGP();

    vector<double> RHS1(ndof,0.0); //convective term
    vector<double> RHS2(ndof,0.0); //viscous term
    vector<double> RHS3(ndof,0.0); //CBS stablization term
    vector<double> RHS4(ndof,0.0); //gravity term
    vector<double> RHS5(ndof,0.0); //boundary terms are ignored here which is OK.

    unsigned short nspnode;
//    vector<unsigned long> igp_spnodes;
//    vector<double> igp_sf;
//    vector<double> igp_sfdx, igp_sfdy, igp_sfdz;
    double weight;
    double gradv_n_igp[3][3];
    double v_n_igp[3];
    double S[3][3];
    double gradv_n_igp_kk;
    double SFI;
    double SFDI[3];
    unsigned long I, J;

    for(unsigned short i = 0; i < 3; i++){
        for(unsigned short j = 0; j < 3; j++){
            gradv_n_igp[i][j] = 0.0;
            S[i][j] = 0.0;
        }
    }


     for(unsigned long igp = 0; igp < ngp; igp++){
        weight = Prep->GetGP(igp)->GetWeight();
        nspnode = Prep->GetGP(igp)->GetnSupportNodes();
//        igp_spnodes = Prep->GetGP(igp)->GetSupportNodes();
//        igp_sf = Prep->GetGPsShapeFunc(igp);
//        igp_sfdx = Prep->GetGPsShapeFuncDX(igp);
//        igp_sfdy = Prep->GetGPsShapeFuncDY(igp);
//        if(nDim == 3) igp_sfdz = Prep->GetGPsShapeFuncDZ(igp);

        v_n_igp[0] = gp_v_n[igp][0];
        v_n_igp[1] = gp_v_n[igp][1];
        if(nDim == 3) v_n_igp[2] = gp_v_n[igp][2];

        //debug
//        cout << "v_n_igp[" << igp << "] = "<< v_n_igp[0] << "," << v_n_igp[1] << endl;
//        cout << "gradv_n_igp1[" << igp << "] = "<< gradv_n_igp1[0] << "," << gradv_n_igp1[1]
//             << "," << gradv_n_igp1[2] << "," << gradv_n_igp1[3] << endl;

        if(nDim == 2){
            gradv_n_igp[0][0] = gp_grad_v_n[igp][0]; //vxdx
            gradv_n_igp[0][1] = gp_grad_v_n[igp][1]; //vxdy
            gradv_n_igp[1][0] = gp_grad_v_n[igp][2]; //vydx
            gradv_n_igp[1][1] = gp_grad_v_n[igp][3]; //vydy

            gradv_n_igp_kk = 2./3.*(gradv_n_igp[0][0]+gradv_n_igp[1][1]);

            S[0][0] = mu*(gradv_n_igp[0][0]+gradv_n_igp[0][0] - gradv_n_igp_kk);
            S[1][1] = mu*(gradv_n_igp[1][1]+gradv_n_igp[1][1] - gradv_n_igp_kk);
            S[0][1] = mu*(gradv_n_igp[0][1]+gradv_n_igp[1][0]);
            S[1][0] = S[0][1];
        }
        else{
            gradv_n_igp[0][0] = gp_grad_v_n[igp][0]; //vxdx
            gradv_n_igp[0][1] = gp_grad_v_n[igp][1]; //vxdy
            gradv_n_igp[0][2] = gp_grad_v_n[igp][2]; //vxdz
            gradv_n_igp[1][0] = gp_grad_v_n[igp][3]; //vydx
            gradv_n_igp[1][1] = gp_grad_v_n[igp][4]; //vydy
            gradv_n_igp[1][2] = gp_grad_v_n[igp][5]; //vydz
            gradv_n_igp[2][0] = gp_grad_v_n[igp][6]; //vzdx
            gradv_n_igp[2][1] = gp_grad_v_n[igp][7]; //vzdy
            gradv_n_igp[2][2] = gp_grad_v_n[igp][8]; //vzdx

            gradv_n_igp_kk = 2./3.*(gradv_n_igp[0][0]+gradv_n_igp[1][1]+gradv_n_igp[2][2]);

            S[0][0] = mu*(gradv_n_igp[0][0]+gradv_n_igp[0][0] - gradv_n_igp_kk);
            S[0][1] = mu*(gradv_n_igp[0][1]+gradv_n_igp[1][0]);
            S[0][2] = mu*(gradv_n_igp[0][2]+gradv_n_igp[2][0]);

            S[1][0] = S[0][1];
            S[1][1] = mu*(gradv_n_igp[1][1]+gradv_n_igp[1][1] - gradv_n_igp_kk);
            S[1][2] = mu*(gradv_n_igp[1][2]+gradv_n_igp[2][1]);

            S[2][0] = S[0][2];
            S[2][1] = S[1][2];
            S[2][2] = mu*(gradv_n_igp[2][2]+gradv_n_igp[2][2] - gradv_n_igp_kk);
        }

        for(unsigned short ispnode = 0; ispnode < nspnode; ispnode++){
//            I = igp_spnodes[ispnode];
//            SFI = igp_sf[ispnode];
//            SFDI[0] = igp_sfdx[ispnode];
//            SFDI[1] = igp_sfdy[ispnode];
//            if(nDim == 3) SFDI[2] = igp_sfdz[ispnode];

            I = Prep->GetGP(igp)->GetSupportNode(ispnode);
            SFI = Prep->GetGPsShapeFunc(igp, ispnode);
            SFDI[0] = Prep->GetGPsShapeFuncDX(igp, ispnode);
            SFDI[1] = Prep->GetGPsShapeFuncDY(igp, ispnode);
            if(nDim == 3) SFDI[2] = Prep->GetGPsShapeFuncDZ(igp, ispnode);

            for(unsigned short i = 0; i < nDim; i++){
                for(unsigned short j = 0; j < nDim; j++){
                    RHS1[nDim*I+i] = RHS1[nDim*I+i] - density*SFI*
                                    (v_n_igp[j]*gradv_n_igp[i][j])*weight;

                    RHS2[nDim*I+i] = RHS2[nDim*I+i] - SFDI[j]*S[i][j]*weight;

                    RHS3[nDim*I+i] = RHS3[nDim*I+i] - 0.5*dt*density*
                                    (SFDI[i]*v_n_igp[i])*(v_n_igp[j]*gradv_n_igp[i][j])*weight;
                }

                if(gravity[i] != 0.0){
                    RHS4[nDim*I+i] = RHS4[nDim*I+i] + density*weight*SFI*gravity[i];
                }
            }
        } //end of ispnode


     } //end of igp loop

     //Assign vals to variables.
     for(unsigned long i = 0; i < ndof; i++){
        rhs_m[i] = RHS1[i] + RHS2[i] + RHS3[i] +RHS4[i] + RHS5[i];
        v_m[i] = v_n[i] + mass_inv[i]*dt*rhs_m[i];
        //debug
//        cout << "v_m[" << i << "] = "<< v_m[i] << endl;
     }

}

void CSolverSemiImpCBSLaminar::Step2(vector<double> &v_m_val){
    unsigned long ngp = Prep->GetnGP();
    unsigned long ndof = nnode*nDim;
    vector<double> rhs(nnode,0.0);
    //Get RHS for step2
    unsigned short nspnode;
//    vector<unsigned long> igp_spnodes;
//    vector<double> igp_sf;
//    vector<double> igp_sfdx, igp_sfdy, igp_sfdz;
    double weight;
    double SFI, SFJ;
    double SFDI[3], SFDJ[3];
    double v_m_J[3];
    double dt_inv = 1.0/dt;

    unsigned long I, J;

    for(unsigned long igp = 0; igp < ngp; igp++){
        weight = Prep->GetGP(igp)->GetWeight();
        nspnode = Prep->GetGP(igp)->GetnSupportNodes();
//        igp_spnodes = Prep->GetGP(igp)->GetSupportNodes();
//        igp_sf = Prep->GetGPsShapeFunc(igp);
//        igp_sfdx = Prep->GetGPsShapeFuncDX(igp);
//        igp_sfdy = Prep->GetGPsShapeFuncDY(igp);
//        if(nDim == 3) igp_sfdz = Prep->GetGPsShapeFuncDZ(igp);

        for(unsigned short ispnode = 0; ispnode < nspnode; ispnode++){
//            I = igp_spnodes[ispnode];
//            SFI = igp_sf[ispnode];
            I = Prep->GetGP(igp)->GetSupportNode(ispnode);
            SFI = Prep->GetGPsShapeFunc(igp, ispnode);
            for(unsigned short jspnode = 0; jspnode < nspnode; jspnode++){
//                J = igp_spnodes[jspnode];
//                SFDJ[0] = igp_sfdx[jspnode];
//                SFDJ[1] = igp_sfdy[jspnode];
//                if(nDim == 3) SFDJ[2] = igp_sfdz[jspnode];

                J = Prep->GetGP(igp)->GetSupportNode(jspnode);
                SFDJ[0] = Prep->GetGPsShapeFuncDX(igp, jspnode);
                SFDJ[1] = Prep->GetGPsShapeFuncDY(igp, jspnode);
                if(nDim == 3) SFDJ[2] = Prep->GetGPsShapeFuncDZ(igp, jspnode);

                v_m_J[0] = v_m_val[nDim*J];
                v_m_J[1] = v_m_val[nDim*J+1];
                if(nDim == 3) v_m_J[2] = v_m_val[nDim*J+2];

                if(nDim == 2){
                    rhs[I] = rhs[I] - dt_inv*density*SFI*(SFDJ[0]*v_m_J[0] + SFDJ[1]*v_m_J[1])*weight;
                }
                else{
                    rhs[I] = rhs[I] - dt_inv*density*SFI*(SFDJ[0]*v_m_J[0] + SFDJ[1]*v_m_J[1] + SFDJ[2]*v_m_J[2])*weight;
                }



            }
        }
    }

    //
    ApplyPBC_rhs(rhs, H_eigen);

    //debug
//    for(unsigned long i = 0; i < rhs.size(); i++){
//        cout << "rhs[" << i << "] = "<< rhs[i] << endl;
//    }


    //Solve Pressure Poisson equation (use Eigen to solve this equation)
    ///TODO 2016-01-28-16.49: Add a new class to wrap Eigen/MKL as the linear equation solver
    Eigen::Map<VectorXd> rhs_eigen(rhs.data(), this->nnode); //copy std::vector rhs to Eigen::vector rhs_Eigen
    //temp debug code
//    cout << "Print rhs_eigen = \n" << rhs_eigen << endl;

    //Here first use direct solver suggested by Eigen tutorials.
    SimplicialLDLT<SparseMatrix<double>> solver;
//    ConjugateGradient<SparseMatrix<double>> solver;
    VectorXd p_n_one_eigen = solver.compute(H_eigen).solve(rhs_eigen);

    //temp debug code
//    cout << "Print p_n_one_eigen = \n" << p_n_one_eigen << endl;

    //convert to std::vector
    //p_n_one(p_n_one_eigen.data(), p_n_one_eigen.data()+p_n_one_eigen.size());
    p_n_one.clear();
    p_n_one.resize(p_n_one_eigen.size());
    VectorXd::Map(&p_n_one[0], p_n_one_eigen.size()) = p_n_one_eigen;
//    for(unsigned long i = 0; i < p_n_one.size(); i++){
//        cout << "p_n_one[" << i << "] = "<< p_n_one[i] << endl;
//    }
}

void CSolverSemiImpCBSLaminar::Step3(double theta){
    std::fill(rhs_n_one.begin(), rhs_n_one.end(), 0.0);

    //correct the intermediate velocity
    unsigned long ngp = Prep->GetnGP();
    unsigned long ndof = nnode*nDim;
    unsigned short nspnode;
    vector<unsigned long> igp_spnodes;
    vector<double> igp_sf;
    vector<double> igp_sfdx, igp_sfdy, igp_sfdz;
    double weight;
    double SFI, SFJ;
    double SFDI[3], SFDJ[3];
    double p_n_J;

    unsigned long I, J;
    for(unsigned long igp = 0; igp < ngp; igp++){
        weight = Prep->GetGP(igp)->GetWeight();
        nspnode = Prep->GetGP(igp)->GetnSupportNodes();
//        igp_spnodes = Prep->GetGP(igp)->GetSupportNodes();
//        igp_sf = Prep->GetGPsShapeFunc(igp);
//        igp_sfdx = Prep->GetGPsShapeFuncDX(igp);
//        igp_sfdy = Prep->GetGPsShapeFuncDY(igp);
//        if(nDim == 3) igp_sfdz = Prep->GetGPsShapeFuncDZ(igp);

        for(unsigned short ispnode = 0; ispnode < nspnode; ispnode++){
//            I = igp_spnodes[ispnode];
//            SFI = igp_sf[ispnode];
            I = Prep->GetGP(igp)->GetSupportNode(ispnode);
            SFI = Prep->GetGPsShapeFunc(igp, ispnode);
            for(unsigned short jspnode = 0; jspnode < nspnode; jspnode++){
//                J = igp_spnodes[jspnode];
//                SFDJ[0] = igp_sfdx[jspnode];
//                SFDJ[1] = igp_sfdy[jspnode];
//                if(nDim == 3) SFDJ[2] = igp_sfdz[jspnode];
                J = Prep->GetGP(igp)->GetSupportNode(jspnode);
                SFDJ[0] = Prep->GetGPsShapeFuncDX(igp, jspnode);
                SFDJ[1] = Prep->GetGPsShapeFuncDY(igp, jspnode);
                if(nDim == 3) SFDJ[2] = Prep->GetGPsShapeFuncDZ(igp, jspnode);

                p_n_J = p_n_one[J];

                for(unsigned short i = 0; i < nDim; i++)
                    rhs_n_one[nDim*I+i] = rhs_n_one[nDim*I+i] - SFI*SFDJ[i]*p_n_J*weight;

            }
        }
    }

    for(unsigned long i = 0; i < ndof; i++){
        this->v_n_one[i] = this->v_m[i] +  mass_inv[i]*dt*rhs_n_one[i];
        //debug
//        cout << "v_n_one[" << i << "] = " << v_n_one[i] << endl;
    }

}

void CSolverSemiImpCBSLaminar::SetPressureStiffness(Eigen::SparseMatrix<double> &H){
    unsigned long ngp = Prep->GetnGP();

    H.resize(nnode, nnode);
    typedef Eigen::Triplet<double> T;
    std::vector<T> tripletlist;

    vector<unsigned long> spnodes;
    unsigned short nspnode;
    CGaussPoint *iGP;

    vector<double> igp_sfdx, igp_sfdy, igp_sfdz;
    double weight;
    double sfIdx, sfIdy, sfIdz;
    double sfJdx, sfJdy, sfJdz;
    double HIJ;

    unsigned long I, J, K;
    unsigned short i,j,k,l;

    for(unsigned long igp = 0; igp < ngp; igp++){
        iGP = Prep->GetGP(igp);
        igp_sfdx = Prep->GetGPsShapeFuncDX(igp);
        igp_sfdy = Prep->GetGPsShapeFuncDY(igp);
        if(nDim == 3) igp_sfdz = Prep->GetGPsShapeFuncDZ(igp);
        nspnode = iGP->GetnSupportNodes();
        spnodes = iGP->GetSupportNodes();
        weight = iGP->GetWeight();
        for(i = 0; i < nspnode; i++){
            I = spnodes[i];
            sfIdx = igp_sfdx[i];
            sfIdy = igp_sfdy[i];
            if(nDim == 3) sfIdz = igp_sfdz[i];

            for(j = 0; j < nspnode; j++){
                J = spnodes[j];
                sfJdx = igp_sfdx[j];
                sfJdy = igp_sfdy[j];
                if(nDim == 2)
                    HIJ = (sfIdx*sfJdx + sfIdy*sfJdy)*weight;
                if(nDim == 3) {
                    sfJdz = igp_sfdz[j];
                    HIJ = (sfIdx*sfJdx + sfIdy*sfJdy + sfIdz*sfJdz)*weight;
                }
                tripletlist.push_back(T(I,J, HIJ));
            }
        }
    }

    H.setFromTriplets(tripletlist.begin(), tripletlist.end());
    //debug
//    cout << "H = \n" << H << endl;

}

void CSolverSemiImpCBSLaminar::GetForce(vector<double> &F_fluid){
    unsigned long ndof = nnode*nDim;
    double dt_inv = 1.0/dt;
    for(unsigned long idof = 0; idof < ndof; idof++){
        F_fluid[idof] = mass[idof]*(v_n_one[idof] - v_n[idof])*dt_inv
                    - rhs_m[idof] - rhs_n_one[idof];
    }


}

bool CSolverSemiImpCBSLaminar::CheckSolution(){
    unsigned long ndof = nnode*nDim;
    bool fluid_fail = false;
    for(unsigned long idof = 0; idof < ndof; idof++){
        double v_idof = v_n_one[idof];
        if (std::isnan(v_idof)){
            fluid_fail = true;
        }

    }

    return fluid_fail;
}

void CSolverSemiImpCBSLaminar::ApplyPBC_H(vector<unsigned long> &ir, vector<unsigned long> &jc, vector<double> &val){
    unsigned long pbc_ndof = BCs->GetPBC_nDOF();
    unsigned long tot_nnz = H_val.size();

    for(unsigned long i = 0; i < pbc_ndof; i++){
        unsigned long I = BCs->GetPBC_DOF(i);
        for(unsigned long j = 0; j < tot_nnz; j++){
            if(ir[j] == I && jc[j] == I) val[j] = 1.0e8*val[j];
        }
    }
    ///TODO JC: This will let more elements in H to be zeros.
    /// Can we just ignore them, just use old one.
}

void CSolverSemiImpCBSLaminar::ApplyPBC_H(Eigen::SparseMatrix<double> &H){
    unsigned long pbc_ndof = BCs->GetPBC_nDOF();

    for(unsigned long i = 0; i < pbc_ndof; i++){
        unsigned long I = BCs->GetPBC_DOF(i);
        H.coeffRef(I,I) = 1.0e8*H.coeffRef(I,I);
    }
}

void CSolverSemiImpCBSLaminar::ApplyPBC_rhs(vector<double> &p_rhs,
                                            Eigen::SparseMatrix<double> &H){
    unsigned long pbc_ndof = BCs->GetPBC_nDOF();
    for(unsigned long i = 0; i < pbc_ndof; i++){
        unsigned long I = BCs->GetPBC_DOF(i);
        p_rhs[I] = (BCs->GetPBC_VAL(i))*H.coeff(I,I);
        //debug
//        cout << "p_rhs_" << I << " = " << p_rhs[I] << endl;
    }
}

void CSolverSemiImpCBSLaminar::ApplyVBC(vector<double> &v_val){
    unsigned long vbc_ndof = BCs->GetVBC_nDOF();
    for(unsigned long i = 0; i < vbc_ndof; i++){
        unsigned long I = BCs->GetVBC_DOF(i);
        v_val[I] = BCs->GetVBC_VAL(i);
        //debug
//        cout << "VBC_val[" << I << "] = " << v_val[I] <<endl;
    }
}

CSolverSemiImpCBSLaminar::~CSolverSemiImpCBSLaminar()
{
    //dtor
}
