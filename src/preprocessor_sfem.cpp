////////////////////////////////////////////////////////////////////////////
//            SSSSSS  FFFFFF  EEEEEE  MMM   MMM        ++       ++        //
//           SS       FF      EE      MM M M MM       ++       ++         //
//           SSSSSSS  FFFF    EEEE    MM  M  MM   ++++++++ ++++++++       //
//                SS  FF      EE      MM     MM     ++       ++           //
//                SS  FF      EE      MM     MM    ++       ++            //
//           SSSSSS   FF      EEEEEE  MM     MM   ++       ++             //
////////////////////////////////////////////////////////////////////////////
#include "preprocessor.hpp"

//Be careful about this template. Each element of vector<t> should be sorted also.
template <typename t>
std::vector<std::vector<t> > GetUniqueRows(std::vector<std::vector<t> > &input)
{
    std::sort(input.begin(), input.end());
    input.erase(std::unique(input.begin(), input.end()), input.end());
    return input;
}

//+++++++++++++++++++++++++
// CPreprocessorESFEMTRI3
//+++++++++++++++++++++++++
CPreprocessorESFEMTRI3::CPreprocessorESFEMTRI3(){
    //ctor
}

CPreprocessorESFEMTRI3::CPreprocessorESFEMTRI3(CConfig *config, CGeometry *geometry){
    //ctor
    cout << "SFEM : -------------------  Prep ES-FEMT3 begin  ---------------" << endl;
    // -------------------------------
    // Set the GPs for FEM_TRI3 first
    // -------------------------------
    cout << "SFEM : ----------------  Prep FEMT3 first ------------" << endl;
    CPreprocessorFEMTRI3::SetElemsCGAndVol(config, geometry);
    cout << "SFEM : ----------------  Get CG and Vol of FEMT3 ------------" << endl;

    CPreprocessorFEMTRI3::SetElemsSize(config, geometry);
    cout << "SFEM : ---------------- Get Elem size of FEMT3 ---------------" << endl;

    CPreprocessorFEMTRI3::SetLumpedMassAll(config, geometry);
    cout << "SFEM : ---------------- Get Lumped Mass of FEMT3 -------------" << endl;

    CPreprocessorFEMTRI3::SetGPs(config, geometry);
    cout << "SFEM : -------------------  Get GPs of FEMT3 ---------------" << endl;

    CPreprocessorFEMTRI3::SetGPsShapeFunc(config, geometry);
    cout << "SFEM : -----------  Get Shape Func of GPs of FEMT3 --------" << endl;

    CPreprocessorFEMTRI3::SetGPsShapeFuncDX(config, geometry);
    cout << "SFEM : -----------  Get Shape Func DX of GPs of FEMT3 --------" << endl;

    CPreprocessorFEMTRI3::SetGPsShapeFuncDY(config, geometry);
    cout << "SFEM : -----------  Get Shape Func DY of GPs of FEMT3 --------" << endl;

    if(geometry->GetnDim() == 3){
        CPreprocessorFEMTRI3::SetGPsShapeFuncDZ(config, geometry);
        cout << "SFEM : -----------  Get Shape func DZ of GPs of FEMT3 --------" << endl;
    }
    cout << "SFEM : ----------------  Prep FEMT3 End ------------" << endl;

    // ------------------------------------
    // Copy GPs in base-class to this class
    // These GPs will be replaced by ESDs.
    // ------------------------------------
    CopyGPsToGPs_FEM();


    // ------------------------------------
    // Set the SF and SFDs of SD to the GPs
    // ------------------------------------
    cout << "SFEM : ----------------  Prep ES-FEMT3 begin ------------" << endl;
    this->GetESD(config, geometry);
    cout << "SFEM : ----------------  Get Edge-based SD ------------" << endl;

    this->SetGPs(config, geometry);
    cout << "SFEM : -------  Get Weigh and Spnodes of ES-FEMT3 ---------" << endl;

    this->SetGPsShapeFunc(config,geometry);
    cout << "SFEM : ----------------  Get Shape Func of ES-FEMT3 ------------" << endl;

    this->SetGPsShapeFuncDX(config, geometry);
    cout << "SFEM : ----------------  Get Shape Func DX of ES-FEMT3 ------------" << endl;

    this->SetGPsShapeFuncDY(config, geometry);
    cout << "SFEM : ----------------  Get Shape Func DY of ES-FEMT3 ------------" << endl;

    if(geometry->GetnDim() == 3){
        this->SetGPsShapeFuncDZ(config, geometry);
        cout << "SFEM : ----------------  Get Shape Func DZ of ES-FEMT3 ------------" << endl;
    }

    cout << "SFEM : -------------------  Prep ES-FEMT3 End  ---------------" << endl;

    cout << "SFEM : Number of ESD is " << GPsShapeFuncDX.size() << endl;
}

void CPreprocessorESFEMTRI3::SetGPs(CConfig *config, CGeometry *geometry){
    //loop each ESD
    unsigned long nsd = ESDs.size();

    for (unsigned long isd = 0; isd < nsd; isd++){
        CSmoothingDomain *SD = ESDs[isd];

        CGaussPoint *gp = new CGaussPoint;
        gp->SetWeight(SD->GetVol());
        gp->SetSupportNodes(SD->GetSupportNodes());
        gp->SetSupportElems(SD->GetElems());
        gp->Setx(0.0); //Coords of GP is useless for ES-FEM.
        gp->Sety(0.0);
        gp->Setz(0.0);
        gp->SetnDim(geometry->GetnDim());
        this->GPs.push_back(gp);
    }

    this->nGP = GPs.size();
}

void CPreprocessorESFEMTRI3::SetGPsShapeFunc(CConfig *config, CGeometry *geometry){
    // loop each ESD
    unsigned long nsd = ESDs.size();
    CSmoothingDomain *ESD;
    vector<double> gp_sf;
    unsigned short nspnodes;
    for (unsigned long isd = 0; isd < nsd; isd++){
        ESD = ESDs[isd];
        nspnodes = ESD->GetnSupportNodes();
        gp_sf.resize(nspnodes);

        unsigned long n0, n1;
        n0 = ESD->GetNode(0);  n1 = ESD->GetNode(1);
        for(unsigned short ispnode = 0; ispnode < nspnodes; ispnode++){
            unsigned long spnode = ESD->GetSupportNode(ispnode);
            if(spnode == n0 || spnode == n1){
                gp_sf[ispnode] = 0.5; // We only consider one GP for one ESD located at the CG of ESD.
            }
            else{
                gp_sf[ispnode] = 0.0;
            }
        }

        this->GPsShapeFunc.push_back(gp_sf);
    }
}

void CPreprocessorESFEMTRI3::SetGPsShapeFuncDX(CConfig *config, CGeometry *geometry){
    // loop each ESD
    unsigned long nsd = ESDs.size();
    CSmoothingDomain *ESD;
    vector<double> ESD_sfdx;
    unsigned short nspele;
    unsigned short nspnodes;
    bool BounESD = false;
    for (unsigned long isd = 0; isd < nsd; isd++){
        ESD = ESDs[isd];
        nspnodes = ESD->GetnSupportNodes();
        ESD_sfdx.resize(nspnodes);
        BounESD = ESD->GetisBoundSD();

        unsigned long ele0, ele1;
        unsigned long n0, n1;
        n0 = ESD->GetNode(0);  n1 = ESD->GetNode(1);
        if(BounESD){
            ele0 = ESD->GetElem(0);
            vector<double> gp_sfdx = GPsShapeFuncDX_FEM[ele0];
            for(unsigned short ispn = 0; ispn < nspnodes; ispn++)
                ESD_sfdx[ispn] = gp_sfdx[ispn];
        }
        else{
            //if not boundary ESD, use area-weighted average strain.
            std::fill(ESD_sfdx.begin(), ESD_sfdx.end(),0.0);
            ele0 = ESD->GetElem(0); ele1 = ESD->GetElem(1);
            vector<double> gp0_sfdx = GPsShapeFuncDX_FEM[ele0];
            vector<double> gp1_sfdx = GPsShapeFuncDX_FEM[ele1];
            double area_ESD = ESD->GetVol();
            double area_ele0 = geometry->GetElem(ele0)->GetVol();
            double area_ele1 = geometry->GetElem(ele1)->GetVol();
            for(unsigned short ispn = 0; ispn < nspnodes; ispn++){
                unsigned long spnode = ESD->GetSupportNode(ispn); //bug
                for(unsigned short i = 0; i < 3; i++){
                    unsigned long spnode_ele0 = GPs_FEM[ele0]->GetSupportNode(i);
                    unsigned long spnode_ele1 = GPs_FEM[ele1]->GetSupportNode(i);
                    if( spnode_ele0 == spnode ){
                        ESD_sfdx[ispn] = ESD_sfdx[ispn] + 0.33333*gp0_sfdx[i]*area_ele0/area_ESD;
                    }
                    if( spnode_ele1 == spnode ){
                        ESD_sfdx[ispn] = ESD_sfdx[ispn] + 0.33333*gp1_sfdx[i]*area_ele1/area_ESD;
                    }

                    //TODO: here should have a better strategy (JC)

                }
            }

        }


        this->GPsShapeFuncDX.push_back(ESD_sfdx);
    }
}

void CPreprocessorESFEMTRI3::SetGPsShapeFuncDY(CConfig *config, CGeometry *geometry){
    // loop each ESD
    unsigned long nsd = ESDs.size();
    CSmoothingDomain *ESD;
    vector<double> ESD_sfdy;
    unsigned short nspele;
    unsigned short nspnodes;
    bool BounESD = false;
    for (unsigned long isd = 0; isd < nsd; isd++){
        ESD = ESDs[isd];
        nspnodes = ESD->GetnSupportNodes();
        ESD_sfdy.resize(nspnodes);
        BounESD = ESD->GetisBoundSD();

        unsigned long ele0, ele1;
        unsigned long n0, n1;
        n0 = ESD->GetNode(0);  n1 = ESD->GetNode(1);
        if(BounESD){
            ele0 = ESD->GetElem(0);
            vector<double> gp_sfdy = GPsShapeFuncDY_FEM[ele0];
            for(unsigned short ispn = 0; ispn < nspnodes; ispn++)
                ESD_sfdy[ispn] = gp_sfdy[ispn];
        }
        else{
            //if not boundary ESD, use area-weighted average strain.
            std::fill(ESD_sfdy.begin(), ESD_sfdy.end(),0.0);
            ele0 = ESD->GetElem(0); ele1 = ESD->GetElem(1);
            vector<double> gp0_sfdy = GPsShapeFuncDY_FEM[ele0];
            vector<double> gp1_sfdy = GPsShapeFuncDY_FEM[ele1];
            double area_ESD = ESD->GetVol();
            double area_ele0 = geometry->GetElem(ele0)->GetVol();
            double area_ele1 = geometry->GetElem(ele1)->GetVol();
            for(unsigned short ispn = 0; ispn < nspnodes; ispn++){
                unsigned long spnode = ESD->GetSupportNode(ispn);
                for(unsigned short i = 0; i < 3; i++){
                    unsigned long spnode_ele0 = GPs_FEM[ele0]->GetSupportNode(i);
                    unsigned long spnode_ele1 = GPs_FEM[ele1]->GetSupportNode(i);
                    if( spnode_ele0 == spnode ){
                        ESD_sfdy[ispn] = ESD_sfdy[ispn] + 0.33333*gp0_sfdy[i]*area_ele0/area_ESD;
                    }
                    if( spnode_ele1 == spnode ){
                        ESD_sfdy[ispn] = ESD_sfdy[ispn] + 0.33333*gp1_sfdy[i]*area_ele1/area_ESD;
                    }

                }
            }

        }


        this->GPsShapeFuncDY.push_back(ESD_sfdy);
    }
}

void CPreprocessorESFEMTRI3::SetGPsShapeFuncDZ(CConfig *config, CGeometry *geometry){
    // loop each ESD
    unsigned long nsd = ESDs.size();
    CSmoothingDomain *ESD;
    vector<double> ESD_sfdz;
    unsigned short nspele;
    unsigned short nspnodes;
    bool BounESD = false;
    for (unsigned long isd = 0; isd < nsd; isd++){
        ESD = ESDs[isd];
        nspnodes = ESD->GetnSupportNodes();
        ESD_sfdz.resize(nspnodes);
        BounESD = ESD->GetisBoundSD();

        unsigned long ele0, ele1;
        unsigned long n0, n1;
        n0 = ESD->GetNode(0);  n1 = ESD->GetNode(1);
        if(BounESD){
            ele0 = ESD->GetElem(0);
            vector<double> gp_sfdz = GPsShapeFuncDZ_FEM[ele0];
            for(unsigned short ispn = 0; ispn < nspnodes; ispn++)
                ESD_sfdz[ispn] = gp_sfdz[ispn];
        }
        else{
            //if not boundary ESD, use area-weighted average strain.
            std::fill(ESD_sfdz.begin(), ESD_sfdz.end(),0.0);
            ele0 = ESD->GetElem(0); ele1 = ESD->GetElem(1);
            vector<double> gp0_sfdz = GPsShapeFuncDZ_FEM[ele0];
            vector<double> gp1_sfdz = GPsShapeFuncDZ_FEM[ele1];
            double area_ESD = ESD->GetVol();
            double area_ele0 = geometry->GetElem(ele0)->GetVol();
            double area_ele1 = geometry->GetElem(ele1)->GetVol();
            for(unsigned short ispn = 0; ispn < nspnodes; ispn++){
                unsigned long spnode = ESD->GetSupportNode(ispn);
                for(unsigned short i = 0; i < 3; i++){
                    unsigned long spnode_ele0 = GPs_FEM[ele0]->GetSupportNode(i);
                    unsigned long spnode_ele1 = GPs_FEM[ele1]->GetSupportNode(i);
                    if( spnode_ele0 == spnode ){
                        ESD_sfdz[ispn] = ESD_sfdz[ispn] + 0.33333*gp0_sfdz[i]*area_ele0/area_ESD;
                    }
                    if( spnode_ele1 == spnode ){
                        ESD_sfdz[ispn] = ESD_sfdz[ispn] + 0.33333*gp1_sfdz[i]*area_ele1/area_ESD;
                    }

                }
            }

        }


        this->GPsShapeFuncDZ.push_back(ESD_sfdz);
    }
}

void CPreprocessorESFEMTRI3::GetESD(CConfig *config, CGeometry *geometry){

    // Get all duplicated Edges from Elems
    unsigned long nele = geometry->GetnElem();
    unsigned long nnode = geometry->GetnNode();
    unsigned short ndim = geometry->GetnDim();
    vector<vector<unsigned long> > Edges;
    for (unsigned long iele = 0; iele < nele; iele++){
        unsigned short nface = geometry->GetElem(iele)->GetnFaces();
        for(unsigned short iface = 0; iface < nface; iface++){
            vector<unsigned long> iEdge;
            iEdge = geometry->GetElem(iele)->GetFace(iface);
            std::sort(iEdge.begin(), iEdge.end());
            Edges.push_back(iEdge);
        }

    }

    // Unique Edges
    vector<vector<unsigned long> > Edges_uni;
    Edges_uni = GetUniqueRows(Edges);

    // Find same ele which shared by nodes of edges;
    unsigned long nedge = Edges_uni.size();
    vector<vector<unsigned long> > Edges_elem(nedge);

    for (unsigned long iedge = 0; iedge < nedge; iedge++){
        unsigned long node0 = Edges_uni[iedge][0];
        vector<unsigned long> ele_node0 = geometry->GetNode(node0)->GetElems();
        unsigned long node1 = Edges_uni[iedge][1];
        vector<unsigned long> ele_node1 = geometry->GetNode(node1)->GetElems();
        //
        vector<unsigned long> ele_iEdge(30);
        vector<unsigned long>::iterator it;
        std::sort(ele_node0.begin(), ele_node0.end());
        std::sort(ele_node1.begin(), ele_node1.end());
        it = std::set_intersection(ele_node0.begin(), ele_node0.end(),
                                   ele_node1.begin(), ele_node1.end(),
                                   ele_iEdge.begin());

        ele_iEdge.resize(it - ele_iEdge.begin());

        CEdgeBasedSD_T3 *ESD = NULL;
        if(ele_iEdge.size() == 1){
            ESD = new CEdgeBasedSD_T3(node0, node1,
                                      geometry->GetElem(ele_iEdge[0]),
                                      ndim, true);
            ESDs.push_back(ESD);
        }
        else{
            ESD = new CEdgeBasedSD_T3(node0, node1,
                                      geometry->GetElem(ele_iEdge[0]),
                                      geometry->GetElem(ele_iEdge[1]),
                                      ndim, false);
            ESDs.push_back(ESD);
        }
    }

}

void CPreprocessorESFEMTRI3::CopyGPsToGPs_FEM(){
    //
    this->nGP_FEM = CPreprocessor::GetnGP();
    //TODO by JC: could this will refer to other place after I change GPs in base-class.
    this->GPs_FEM = CPreprocessor::GPs;
    GPs.clear();

    this->GPsShapeFunc_FEM = CPreprocessor::GPsShapeFunc;
    GPsShapeFunc.clear();

    this->GPsShapeFuncDX_FEM = CPreprocessor::GPsShapeFuncDX;
    GPsShapeFuncDX.clear();

    this->GPsShapeFuncDY_FEM = CPreprocessor::GPsShapeFuncDY;
    GPsShapeFuncDY.clear();

    this->GPsShapeFuncDZ_FEM = CPreprocessor::GPsShapeFuncDZ;
    GPsShapeFuncDZ.clear();
}


CPreprocessorESFEMTRI3::~CPreprocessorESFEMTRI3(){
    //dtor
}

//+++++++++++++++++++++++++
// CPreprocessorNSFEMTRI3
//+++++++++++++++++++++++++
CPreprocessorNSFEMTRI3::CPreprocessorNSFEMTRI3(){
    //ctor
}

CPreprocessorNSFEMTRI3::CPreprocessorNSFEMTRI3(CConfig *config, CGeometry *geometry){
    //ctor
    //ctor
    cout << "SFEM : -------------------  Prep NS-FEMT3 begin  ---------------" << endl;
    // -------------------------------
    // Set the GPs for FEM_TRI3 first
    // -------------------------------
    cout << "SFEM : ----------------  Prep FEMT3 first ------------" << endl;
    CPreprocessorFEMTRI3::SetElemsCGAndVol(config, geometry);
    cout << "SFEM : ----------------  Get CG and Vol of FEMT3 ------------" << endl;

    CPreprocessorFEMTRI3::SetElemsSize(config, geometry);
    cout << "SFEM : ---------------- Get Elem size of FEMT3 ---------------" << endl;

    CPreprocessorFEMTRI3::SetLumpedMassAll(config, geometry);
    cout << "SFEM : ---------------- Get Lumped Mass of FEMT3 -------------" << endl;

    CPreprocessorFEMTRI3::SetGPs(config, geometry);
    cout << "SFEM : -------------------  Get GPs of FEMT3 ---------------" << endl;

    CPreprocessorFEMTRI3::SetGPsShapeFunc(config, geometry);
    cout << "SFEM : -----------  Get Shape Func of GPs of FEMT3 --------" << endl;

    CPreprocessorFEMTRI3::SetGPsShapeFuncDX(config, geometry);
    cout << "SFEM : -----------  Get Shape Func DX of GPs of FEMT3 --------" << endl;

    CPreprocessorFEMTRI3::SetGPsShapeFuncDY(config, geometry);
    cout << "SFEM : -----------  Get Shape Func DY of GPs of FEMT3 --------" << endl;

    if(geometry->GetnDim() == 3){
        CPreprocessorFEMTRI3::SetGPsShapeFuncDZ(config, geometry);
        cout << "SFEM : -----------  Get Shape func DZ of GPs of FEMT3 --------" << endl;
    }
    cout << "SFEM : ----------------  Prep FEMT3 End ------------" << endl;

    // ------------------------------------
    // Copy GPs in base-class to this class
    // These GPs will be replaced by ESDs.
    // ------------------------------------
    this->CopyGPsToGPs_FEM();



    // ------------------------------------
    // Set the SF and SFDs of SD to the GPs
    // ------------------------------------
    cout << "SFEM : ----------------  Prep NS-FEMT3 begin ------------" << endl;
    this->GetNSD(config, geometry);
    cout << "SFEM : ----------------  Get Node-based SD ------------" << endl;

    this->SetGPs(config, geometry);
    cout << "SFEM : -------  Get Weigh and Spnodes of NS-FEMT3 ---------" << endl;

    this->SetGPsShapeFunc(config,geometry);
    cout << "SFEM : ----------------  Get Shape Func of NS-FEMT3 ------------" << endl;

    this->SetGPsShapeFuncD(config, geometry);
    cout << "SFEM : ----------------  Get Shape Func DXYZ of NS-FEMT3 ------------" << endl;

    cout << "SFEM : -------------------  Prep NS-FEMT3 End  ---------------" << endl;

    cout << "SFEM : Number of NSD is " << GPsShapeFuncDX.size() << endl;
}

void CPreprocessorNSFEMTRI3::GetNSD(CConfig *config, CGeometry *geometry){
    //directly get node-basd SD from Elems associated with node.
    //Number of NSD is same with number of nodes.
    unsigned short ndim = geometry->GetnDim();
    unsigned long nnode = geometry->GetnNode();
    for (unsigned long inode = 0; inode < nnode; inode++){
        vector<CElem*> spele_inode_obj;
        unsigned short nspele_inode = geometry->GetNode(inode)->GetnElems();
        for (unsigned short ispele_inode = 0; ispele_inode < nspele_inode; ispele_inode++)
        {
            unsigned long ele_id = geometry->GetNode(inode)->GetElem(ispele_inode);
            spele_inode_obj.push_back(geometry->GetElem(ele_id));
        }

        CNodeBasedSD_T3 *NSD = NULL;
        NSD = new CNodeBasedSD_T3(inode, spele_inode_obj, ndim);

        NSDs.push_back(NSD);
    }
}

void CPreprocessorNSFEMTRI3::SetGPs(CConfig *config, CGeometry *geometry){
    unsigned long nsd = NSDs.size();
    for (unsigned long isd = 0; isd < nsd; isd++){
        CSmoothingDomain *SD = NSDs[isd];

        CGaussPoint *gp = new CGaussPoint;
        gp->SetWeight(SD->GetVol());
        gp->SetSupportNodes(SD->GetSupportNodes());
        gp->SetSupportElems(SD->GetElems());
        gp->Setx(0.0); //Coords of GP is useless for ES-FEM.
        gp->Sety(0.0);
        gp->Setz(0.0);
        gp->SetnDim(geometry->GetnDim());
        this->GPs.push_back(gp);
    }
    this->nGP = GPs.size();
}

void CPreprocessorNSFEMTRI3::SetGPsShapeFunc(CConfig *config, CGeometry *geometry){
    unsigned long nsd = NSDs.size();
    CSmoothingDomain *NSD;
    vector<double> gp_sf;
    unsigned short nspnodes;
    for (unsigned long isd = 0; isd < nsd; isd++){
        NSD = NSDs[isd];
        nspnodes = NSD->GetnSupportNodes();
        gp_sf.resize(nspnodes);
        std::fill(gp_sf.begin(), gp_sf.end(), 0.0);

        unsigned long n = NSD->GetNode(0);
        for (unsigned short ispnode = 0; ispnode < nspnodes; ispnode++){
            unsigned long spnode = NSD->GetSupportNode(ispnode);
            if(spnode == n){
                gp_sf[ispnode] = 1.0; // We only consider one GP for one NSD located at the CG of ESD.
            }
        }
        this->GPsShapeFunc.push_back(gp_sf);
    }
}

void CPreprocessorNSFEMTRI3::SetGPsShapeFuncD(CConfig *config, CGeometry *geometry){
    unsigned short ndim = geometry->GetnDim();
    unsigned long nsd = NSDs.size();
    CSmoothingDomain *NSD;
    vector<double> NSD_sfdx, NSD_sfdy, NSD_sfdz;
    unsigned short nspele;
    unsigned short nspnodes;

    for (unsigned long isd = 0; isd < nsd; isd++){
        NSD = NSDs[isd];
        nspnodes = NSD->GetnSupportNodes();
        NSD_sfdx.resize(nspnodes);
        NSD_sfdy.resize(nspnodes);
        NSD_sfdz.resize(nspnodes);
        std::fill(NSD_sfdx.begin(), NSD_sfdx.end(), 0.0);
        std::fill(NSD_sfdy.begin(), NSD_sfdy.end(), 0.0);
        std::fill(NSD_sfdz.begin(), NSD_sfdz.end(), 0.0);

        nspele = NSD->GetnElem();
        //average strain of elem of NSD
        double vol_NSD = NSD->GetVol();
        for (unsigned short ispnode = 0; ispnode < nspnodes; ispnode++){
            unsigned long spnode = NSD->GetSupportNode(ispnode);

            for (unsigned short ispele = 0; ispele < nspele; ispele++){
                unsigned long ele = NSD->GetElem(ispele);
                vector<double> gp_sfdx = GPsShapeFuncDX_FEM[ele];
                vector<double> gp_sfdy = GPsShapeFuncDY_FEM[ele];
                vector<double> gp_sfdz;
                if(ndim == 3) gp_sfdz = GPsShapeFuncDZ_FEM[ele];
                double vol_ele = geometry->GetElem(ele)->GetVol();

                for (unsigned short i = 0; i < 3; i++){
                    unsigned long spnode_ele = GPs_FEM[ele]->GetSupportNode(i);
                    if(spnode_ele == spnode){
                        NSD_sfdx[ispnode] = NSD_sfdx[ispnode] + 0.33333*gp_sfdx[i]*vol_ele/vol_NSD;
                        NSD_sfdy[ispnode] = NSD_sfdy[ispnode] + 0.33333*gp_sfdy[i]*vol_ele/vol_NSD;
                        if(ndim == 3) NSD_sfdz[ispnode] = NSD_sfdz[ispnode] + 0.33333*gp_sfdz[i]*vol_ele/vol_NSD;
                    }
                }
            }
        }

        this->GPsShapeFuncDX.push_back(NSD_sfdx);
        this->GPsShapeFuncDY.push_back(NSD_sfdy);
        if(ndim == 3) this->GPsShapeFuncDZ.push_back(NSD_sfdz);


    }
}

void CPreprocessorNSFEMTRI3::CopyGPsToGPs_FEM(){
    //
    this->nGP_FEM = CPreprocessor::GetnGP();
    //TODO by JC: could this will refer to other place after I change GPs in base-class.
    this->GPs_FEM = CPreprocessor::GPs;
    GPs.clear();

    this->GPsShapeFunc_FEM = CPreprocessor::GPsShapeFunc;
    GPsShapeFunc.clear();

    this->GPsShapeFuncDX_FEM = CPreprocessor::GPsShapeFuncDX;
    GPsShapeFuncDX.clear();

    this->GPsShapeFuncDY_FEM = CPreprocessor::GPsShapeFuncDY;
    GPsShapeFuncDY.clear();

    this->GPsShapeFuncDZ_FEM = CPreprocessor::GPsShapeFuncDZ;
    GPsShapeFuncDZ.clear();
}

CPreprocessorNSFEMTRI3::~CPreprocessorNSFEMTRI3(){

}

//+++++++++++++++++++++++++
// CPreprocessorFSFEMTET4
//+++++++++++++++++++++++++
CPreprocessorFSFEMTET4::CPreprocessorFSFEMTET4(){
    //ctor

}

CPreprocessorFSFEMTET4::CPreprocessorFSFEMTET4(CConfig *config, CGeometry *geometry){
    //ctor
    cout << "SFEM : -------------------  Prep FS-FEMT4 begin  ---------------" << endl;
    // -------------------------------
    // Set the GPs for FEM_TET4 first
    // -------------------------------
    cout << "SFEM : ----------------  Prep FEMT4 first ------------" << endl;
    CPreprocessorFEMTET4::SetElemsCGAndVol(config, geometry);
    cout << "SFEM : ----------- Get CG and Vol of FEMT4 -----------" << endl;

    CPreprocessorFEMTET4::SetElemsSize(config, geometry);
    cout << "SFEM : ----------- Get Elem Size of FEMT4 -----------" << endl;

    CPreprocessorFEMTET4::SetLumpedMassAll(config, geometry);
    cout << "SFEM : ----------- Get Lumped Mass of FEMT4 -----------" << endl;

    CPreprocessorFEMTET4::SetGPs(config, geometry);
    cout << "SFEM : ----------- Get GPs of FEMT4 -----------" << endl;

    CPreprocessorFEMTET4::SetGPsShapeFunc(config, geometry);
    cout << "SFEM : ----------- Get Shape Func of FEMT4 -----------" << endl;

    CPreprocessorFEMTET4::SetGPsShapeFuncDX(config, geometry);
    cout << "SFEM : ----------- Get Shape FuncDX of FEMT4 -----------" << endl;

    CPreprocessorFEMTET4::SetGPsShapeFuncDY(config, geometry);
    cout << "SFEM : ----------- Get Shape FuncDY of FEMT4 -----------" << endl;

    CPreprocessorFEMTET4::SetGPsShapeFuncDZ(config, geometry);
    cout << "SFEM : ----------- Get Shape FuncDZ of FEMT4 -----------" << endl;

    cout << "SFEM : ----------------  Prep FEMT4 end ------------" << endl;

    // ------------------------------------
    // Copy GPs in base-class to this class
    // These GPs will be replaced by FSDs.
    // ------------------------------------
    CopyGPsToGPs_FEM();

    // ------------------------------------
    // Set the SF and SFDs of SD to the GPs
    // ------------------------------------
    Timer t1;
    t1.start();
    this->GetFSD(config, geometry);
    t1.stop();
    cout << "SFEM : Face-based SD construction uses " << t1.getElapsedTimeInSec() << "s" << endl;
    cout << "SFEM : ----------- Get Face-based SD of T4 -----------" << endl;

    this->SetGPs(config, geometry);
    cout << "SFEM : ------ Get Weight and Spnodes of FS-FEMT4 -------" << endl;

    this->SetGPsShapeFunc(config,geometry);
    cout << "SFEM : ------ Get Shape Func of FS-FEMT4 -------" << endl;

    this->SetGPsShapeFuncD(config,geometry);
    cout << "SFEM : ------ Get Shape Func DX DY DZ of FS-FEMT4 -------" << endl;

    cout << "SFEM : -------------------  Prep FS-FEMT4 End  ---------------" << endl;

    cout << "SFEM : Number of FSD is " << GPsShapeFuncDX.size() << endl;
}

void CPreprocessorFSFEMTET4::SetGPs(CConfig *config, CGeometry *geometry){
    unsigned long nsd = FSDs.size();
    for (unsigned long isd = 0; isd < nsd; isd++){
        CSmoothingDomain *SD = FSDs[isd];

        CGaussPoint *gp = new CGaussPoint;
        gp->SetWeight(SD->GetVol());
        gp->SetSupportNodes(SD->GetSupportNodes());
        gp->SetSupportElems(SD->GetElems());
        gp->Setx(0.0); //Coords of GP is useless for ES-FEM.
        gp->Sety(0.0);
        gp->Setz(0.0);
        gp->SetnDim(geometry->GetnDim());
        this->GPs.push_back(gp);
    }

    this->nGP = GPs.size();
}

void CPreprocessorFSFEMTET4::SetGPsShapeFunc(CConfig *config, CGeometry *geometry){
    // loop each FSD
    unsigned long nsd = FSDs.size();
    CSmoothingDomain *FSD;
    vector<double> gp_sf;
    unsigned short nspnodes;
    for (unsigned long isd = 0; isd < nsd; isd++){
        FSD = FSDs[isd];
        nspnodes = FSD->GetnSupportNodes();
        gp_sf.resize(nspnodes);

        unsigned long n0, n1, n2;
        n0 = FSD->GetNode(0); n1 = FSD->GetNode(1); n2 = FSD->GetNode(2);
        for(unsigned short ispnode = 0; ispnode < nspnodes; ispnode++){
            unsigned long spnode = FSD->GetSupportNode(ispnode);
            if(spnode == n0 || spnode == n1 || spnode == n2){
                gp_sf[ispnode] = 0.333333333;
                // We only consider one GP for one FSD located at the CG of FSD.
            }
            else{
                gp_sf[ispnode] = 0.0;
            }
        }

        this->GPsShapeFunc.push_back(gp_sf);
    }
}

void CPreprocessorFSFEMTET4::SetGPsShapeFuncD(CConfig *config, CGeometry *geometry){
    // loop each FSD
    unsigned long nsd = FSDs.size();
    CSmoothingDomain *FSD;
    vector<double> FSD_sfdx, FSD_sfdy, FSD_sfdz;
    unsigned short nspele;
    unsigned short nspnodes;
    bool BounFSD = false;
    for (unsigned long isd = 0; isd < nsd; isd++){
        FSD = FSDs[isd];
        nspnodes = FSD->GetnSupportNodes();
        FSD_sfdx.resize(nspnodes);
        FSD_sfdy.resize(nspnodes);
        FSD_sfdz.resize(nspnodes);
        BounFSD = FSD->GetisBoundSD();

        unsigned long ele0, ele1;
        unsigned long n0, n1, n2;
        n0 = FSD->GetNode(0);  n1 = FSD->GetNode(1);  n2 = FSD->GetNode(2);
        if(BounFSD){
            ele0 = FSD->GetElem(0);
            vector<double> gp_sfdx = GPsShapeFuncDX_FEM[ele0];
            vector<double> gp_sfdy = GPsShapeFuncDY_FEM[ele0];
            vector<double> gp_sfdz = GPsShapeFuncDZ_FEM[ele0];
            for(unsigned short ispn = 0; ispn < nspnodes; ispn++){
                FSD_sfdx[ispn] = gp_sfdx[ispn];
                FSD_sfdy[ispn] = gp_sfdy[ispn];
                FSD_sfdz[ispn] = gp_sfdz[ispn];
            }
        }
        else{
            //if not boundary FSD, use area-weighted average strain.
            std::fill(FSD_sfdx.begin(), FSD_sfdx.end(),0.0);
            std::fill(FSD_sfdy.begin(), FSD_sfdy.end(),0.0);
            std::fill(FSD_sfdz.begin(), FSD_sfdz.end(),0.0);

            ele0 = FSD->GetElem(0); ele1 = FSD->GetElem(1);
            vector<double> gp0_sfdx = GPsShapeFuncDX_FEM[ele0];
            vector<double> gp1_sfdx = GPsShapeFuncDX_FEM[ele1];
            vector<double> gp0_sfdy = GPsShapeFuncDY_FEM[ele0];
            vector<double> gp1_sfdy = GPsShapeFuncDY_FEM[ele1];
            vector<double> gp0_sfdz = GPsShapeFuncDZ_FEM[ele0];
            vector<double> gp1_sfdz = GPsShapeFuncDZ_FEM[ele1];

            double vol_FSD = FSD->GetVol();
            double vol_ele0 = geometry->GetElem(ele0)->GetVol();
            double vol_ele1 = geometry->GetElem(ele1)->GetVol();
            for(unsigned short ispn = 0; ispn < nspnodes; ispn++){
                unsigned long spnode = FSD->GetSupportNode(ispn);
                for(unsigned short i = 0; i < 4; i++){
                    unsigned long spnode_ele0 = GPs_FEM[ele0]->GetSupportNode(i);
                    unsigned long spnode_ele1 = GPs_FEM[ele1]->GetSupportNode(i);
                    if( spnode_ele0 == spnode ){
                        FSD_sfdx[ispn] = FSD_sfdx[ispn] + 0.25*gp0_sfdx[i]*vol_ele0/vol_FSD;
                        FSD_sfdy[ispn] = FSD_sfdy[ispn] + 0.25*gp0_sfdy[i]*vol_ele0/vol_FSD;
                        FSD_sfdz[ispn] = FSD_sfdz[ispn] + 0.25*gp0_sfdz[i]*vol_ele0/vol_FSD;
                    }
                    if( spnode_ele1 == spnode ){
                        FSD_sfdx[ispn] = FSD_sfdx[ispn] + 0.25*gp1_sfdx[i]*vol_ele1/vol_FSD;
                        FSD_sfdy[ispn] = FSD_sfdy[ispn] + 0.25*gp1_sfdy[i]*vol_ele1/vol_FSD;
                        FSD_sfdz[ispn] = FSD_sfdz[ispn] + 0.25*gp1_sfdz[i]*vol_ele1/vol_FSD;
                    }

                }
            }

        }


        this->GPsShapeFuncDX.push_back(FSD_sfdx);
        this->GPsShapeFuncDY.push_back(FSD_sfdy);
        this->GPsShapeFuncDZ.push_back(FSD_sfdz);
    }
}

void CPreprocessorFSFEMTET4::GetFSD(CConfig *config, CGeometry *geometry){

    //Get all duplicated Faces from Elems
    unsigned long nele = geometry->GetnElem();
    unsigned long nnode = geometry->GetnNode();
    unsigned short ndim = geometry->GetnDim();
    vector<vector<unsigned long> > Faces;
    for (unsigned long iele = 0; iele < nele; iele++){
        unsigned short nface = geometry->GetElem(iele)->GetnFaces();
        for(unsigned short iface = 0; iface < nface; iface++){
            vector<unsigned long> iFace;
            iFace = geometry->GetElem(iele)->GetFace(iface);
            std::sort(iFace.begin(), iFace.end());
            Faces.push_back(iFace);
        }

    }

    // Unique Faces
    vector<vector<unsigned long> > Faces_uni;
    Faces_uni = GetUniqueRows(Faces);

    // Find same ele which shared by nodes of faces;
    unsigned long nfaces = Faces_uni.size();
    vector<vector<unsigned long> > Faces_elem(nfaces);

    for (unsigned long iface = 0; iface < nfaces; iface++){
        unsigned long node0 = Faces_uni[iface][0];
        vector<unsigned long> ele_node0 = geometry->GetNode(node0)->GetElems();
        unsigned long node1 = Faces_uni[iface][1];
        vector<unsigned long> ele_node1 = geometry->GetNode(node1)->GetElems();
        unsigned long node2 = Faces_uni[iface][2];
        vector<unsigned long> ele_node2 = geometry->GetNode(node2)->GetElems();
        //
        vector<unsigned long> ele_iFaceTemp(30);
        vector<unsigned long>::iterator itTemp;
        std::sort(ele_node0.begin(), ele_node0.end());
        std::sort(ele_node1.begin(), ele_node1.end());
        itTemp = std::set_intersection(ele_node0.begin(), ele_node0.end(),
                                   ele_node1.begin(), ele_node1.end(),
                                   ele_iFaceTemp.begin());
        ele_iFaceTemp.resize(itTemp - ele_iFaceTemp.begin());

        vector<unsigned long> ele_iFace(30);
        vector<unsigned long>::iterator it;
        std::sort(ele_node2.begin(), ele_node2.end());
        it = std::set_intersection(ele_node2.begin(), ele_node2.end(),
                                   ele_iFaceTemp.begin(), ele_iFaceTemp.end(),
                                   ele_iFace.begin());
        ele_iFace.resize(it - ele_iFace.begin());

        CFaceBasedSD_T4 *FSD = NULL;
        if(ele_iFace.size() == 1){
            FSD = new CFaceBasedSD_T4(node0, node1, node2,
                                      geometry->GetElem(ele_iFace[0]),
                                      true);
            FSDs.push_back(FSD);
        }
        else{
            FSD = new CFaceBasedSD_T4(node0, node1, node2,
                                      geometry->GetElem(ele_iFace[0]),
                                      geometry->GetElem(ele_iFace[1]),
                                      false);
            FSDs.push_back(FSD);
        }

    }
}

void CPreprocessorFSFEMTET4::CopyGPsToGPs_FEM(){
    this->nGP_FEM = CPreprocessor::GetnGP();
    //TODO by JC: could this will refer to other place after I change GPs in base-class.
    this->GPs_FEM = CPreprocessor::GPs;
    GPs.clear();

    this->GPsShapeFunc_FEM = CPreprocessor::GPsShapeFunc;
    GPsShapeFunc.clear();

    this->GPsShapeFuncDX_FEM = CPreprocessor::GPsShapeFuncDX;
    GPsShapeFuncDX.clear();

    this->GPsShapeFuncDY_FEM = CPreprocessor::GPsShapeFuncDY;
    GPsShapeFuncDY.clear();

    this->GPsShapeFuncDZ_FEM = CPreprocessor::GPsShapeFuncDZ;
    GPsShapeFuncDZ.clear();
}


CPreprocessorFSFEMTET4::~CPreprocessorFSFEMTET4(){
    //dtor
}
