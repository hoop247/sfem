////////////////////////////////////////////////////////////////////////////
//            SSSSSS  FFFFFF  EEEEEE  MMM   MMM        ++       ++        //
//           SS       FF      EE      MM M M MM       ++       ++         //
//           SSSSSSS  FFFF    EEEE    MM  M  MM   ++++++++ ++++++++       //
//                SS  FF      EE      MM     MM     ++       ++           //
//                SS  FF      EE      MM     MM    ++       ++            //
//           SSSSSS   FF      EEEEEE  MM     MM   ++       ++             //
////////////////////////////////////////////////////////////////////////////
#include "preprocessor.hpp"
#include "Toolbox.hpp"
CPreprocessor::CPreprocessor()
{
    //ctor
    nGP = 0;
}

CPreprocessor::~CPreprocessor()
{
    //dtor
    GPs.clear();
    GPsShapeFunc.clear();
    GPsShapeFuncDX.clear();
    GPsShapeFuncDY.clear();
    GPsShapeFuncDZ.clear();
}

void CPreprocessor::Display(){
    cout << "nGP: " << nGP << endl;

    for(int i=0; i<nGP; i++){
        cout << "GPsShapeFunc ["<< i <<"]: ";
        for(int j=0; j<GPsShapeFunc[i].size(); j++){
            cout << GPsShapeFunc[i][j] <<", ";
        }
        cout << endl;
    }

    for(int i=0; i<nGP; i++){
        cout << "GPsShapeFuncDX ["<< i <<"]: ";
        for(int j=0; j<GPsShapeFuncDX[i].size(); j++){
             cout << GPsShapeFuncDX[i][j]<<", ";
        }
        cout << endl;
    }

    for(int i=0; i<nGP; i++){
        cout << "GPsShapeFuncDY ["<< i <<"]: ";
        for(int j=0; j<GPsShapeFuncDY[i].size(); j++){
            cout << GPsShapeFuncDY[i][j]<<", ";
        }
        cout << endl;
    }

    if(GPsShapeFuncDZ.empty() == false){
        for(int i=0; i<nGP; i++){
            cout << "GPsShapeFuncDZ ["<< i <<"]: ";
            for(int j=0; j<GPsShapeFuncDZ[i].size(); j++){
                cout  << GPsShapeFuncDZ[i][j] << ", ";
            }
            cout << endl;
        }
    }

}

//+++++++++++++++++++++++++
// CPreprocessorFEMTRI3
//+++++++++++++++++++++++++
CPreprocessorFEMTRI3::CPreprocessorFEMTRI3(){
    //ctor

}

CPreprocessorFEMTRI3::CPreprocessorFEMTRI3(CConfig* config, CGeometry* geometry){

    cout << "SFEM : -------------------  Prep FEMT3 begin  ---------------" << endl;

    SetElemsCGAndVol(config, geometry);
    cout << "SFEM : ----------------  Get CG and Vol of FEMT3 ------------" << endl;

    SetElemsSize(config, geometry);
    cout << "SFEM : ---------------- Get Elem size of FEMT3 ---------------" << endl;

    SetLumpedMassAll(config, geometry);
    cout << "SFEM : ---------------- Get Lumped Mass of FEMT3 -------------" << endl;

    SetGPs(config, geometry);
    cout << "SFEM : -------------------  Get GPs of FEMT3 ---------------" << endl;

    SetGPsShapeFunc(config, geometry);
    cout << "SFEM : -----------  Get Shape func of GPs of FEMT3 --------" << endl;

    SetGPsShapeFuncDX(config, geometry);
    cout << "SFEM : -----------  Get Shape func DX of GPs of FEMT3 --------" << endl;

    SetGPsShapeFuncDY(config, geometry);
    cout << "SFEM : -----------  Get Shape func DY of GPs of FEMT3 --------" << endl;

    if(geometry->GetnDim() == 3){
        SetGPsShapeFuncDZ(config, geometry);
        cout << "SFEM : -----------  Get Shape func DZ of GPs of FEMT3 --------" << endl;
    }

    cout << "SFEM : -------------------  Prep FEMT3 End  ---------------" << endl;


    #ifdef DEBUG
    this->Display();
    #endif // DEBUG
}

void CPreprocessorFEMTRI3::SetElemsCGAndVol(CConfig* config, CGeometry* geometry){
    unsigned long nele = geometry->GetnElem();

    for(unsigned long iele=0; iele < nele; iele++){
        vector<double> p0_xyz, p1_xyz, p2_xyz;
        unsigned long p0_ID, p1_ID, p2_ID;
        p0_ID = geometry->GetElem(iele)->GetNode(0);
        p1_ID = geometry->GetElem(iele)->GetNode(1);
        p2_ID = geometry->GetElem(iele)->GetNode(2);

        p0_xyz = geometry->GetNode(p0_ID)->GetCoords();
        p1_xyz = geometry->GetNode(p1_ID)->GetCoords();
        p2_xyz = geometry->GetNode(p2_ID)->GetCoords();

        geometry->GetElem(iele)->SetCG(p0_xyz, p1_xyz, p2_xyz);
        geometry->GetElem(iele)->SetVol(p0_xyz, p1_xyz, p2_xyz);

        #ifdef DEBUG
        geometry->GetElem(iele)->Display();
        #endif // DEBUG

    }
}

void CPreprocessorFEMTRI3::SetElemsSize(CConfig *config, CGeometry *geometry){
    unsigned long nele = geometry->GetnElem();

    for (unsigned long iele=0; iele < nele; iele++){
        vector<double> p0_xyz, p1_xyz, p2_xyz;
        unsigned long p0_ID, p1_ID, p2_ID;
        p0_ID = geometry->GetElem(iele)->GetNode(0);
        p1_ID = geometry->GetElem(iele)->GetNode(1);
        p2_ID = geometry->GetElem(iele)->GetNode(2);

        p0_xyz = geometry->GetNode(p0_ID)->GetCoords();
        p1_xyz = geometry->GetNode(p1_ID)->GetCoords();
        p2_xyz = geometry->GetNode(p2_ID)->GetCoords();

        double area = geometry->GetElem(iele)->GetVol();
        vector<double> h(3);
        double h_ele;

        h[0] = 2.0*area/GetLength(p1_xyz,p2_xyz);//2*area/dist(p1,p2)
        h[1] = 2.0*area/GetLength(p0_xyz,p2_xyz);//2*area/dist(p0,p2)
        h[2] = 2.0*area/GetLength(p0_xyz,p1_xyz);//2*area/dist(p0,p1)

        h_ele = *min_element(h.begin(), h.end());
        geometry->GetElem(iele)->Seth(h_ele);
    }
}

void CPreprocessorFEMTRI3::SetGPs(CConfig* config, CGeometry* geometry){

    //loop each element
    unsigned long nele = geometry->GetnElem();
    unsigned long ele_id;
    vector<double> iele_CG;
    vector<unsigned long> iele_nodes;
    double iele_vol;
    for(unsigned long iele = 0; iele < nele; iele++){
        ele_id = geometry->GetElem(iele)->GetGlobalID();
        geometry->GetElem(iele)->SetCG();
        iele_CG = geometry->GetElem(iele)->GetCG_coord();
        iele_nodes = geometry->GetElem(iele)->GetNodes();

        iele_vol = geometry->GetElem(iele)->GetVol();

        CGaussPoint *gp = new CGaussPoint;
        gp->SetWeight(iele_vol);
        gp->SetSupportNodes(iele_nodes);
        gp->AddSupportElems(ele_id);
        gp->Setx(iele_CG[0]);  //for TRI3 element, the CG is GP.
        gp->Sety(iele_CG[1]);
        gp->Setz(iele_CG[2]);
        gp->SetnDim(geometry->GetnDim());
        this->GPs.push_back(gp);

    }

    this->nGP = GPs.size();
}

void CPreprocessorFEMTRI3::SetGPsShapeFunc(CConfig* config, CGeometry* geometry){
    //loop all GPs
    unsigned long ngp = GPs.size();
    vector<unsigned long> speles;
    unsigned long iele;
    CGaussPoint *gp;
    CElem *ele;
    vector<double> gp_sf(3);
    vector<double> gp_coords;
    vector<vector<double> > node_coord = geometry->GetNode_coord();
    for(unsigned long igp = 0; igp < ngp; igp++){
        gp = GPs[igp];
        gp_coords = gp->GetCoords();
        //for FEM T3, only one SPele for each GP
        speles = gp->GetSupportElems();
        iele = speles[0];
        ele = geometry->GetElem(iele);
        //get shapefunction of this gp respect to each node of support element
        gp_sf = ele->GetShapeFuncAt(gp_coords, node_coord);
        this->GPsShapeFunc.push_back(gp_sf);
    }
}

void CPreprocessorFEMTRI3::SetGPsShapeFuncDX(CConfig* config, CGeometry* geometry){
    unsigned long ngp = GPs.size();
    vector<unsigned long> speles;
    unsigned long iele;
    CGaussPoint *gp;
    CElem *ele;
    vector<double> gp_coords;
    vector<double> gp_sfdx(3);
    vector<vector<double> > node_coord = geometry->GetNode_coord();
    for(unsigned long igp = 0; igp < ngp; igp++){
        gp = GPs[igp];
        gp_coords = gp->GetCoords();
        //for FEM T3, only one SPele for each GP
        speles = gp->GetSupportElems();
        iele = speles[0];
        ele = geometry->GetElem(iele);
        //get shapefunction of this gp respect to each node of support element
        gp_sfdx = ele->GetShapeFuncDXAt(gp_coords, node_coord);
        this->GPsShapeFuncDX.push_back(gp_sfdx);
    }
}

void CPreprocessorFEMTRI3::SetGPsShapeFuncDY(CConfig* config, CGeometry* geometry){
    unsigned long ngp = GPs.size();
    vector<unsigned long> speles;
    unsigned long iele;
    CGaussPoint *gp;
    CElem *ele;
    vector<double> gp_coords;
    vector<double> gp_sfdy(3);
    vector<vector<double> > node_coord = geometry->GetNode_coord();
    for(unsigned long igp = 0; igp < ngp; igp++){
        gp = GPs[igp];
        gp_coords = gp->GetCoords();
        //for FEM T3, only one SPele for each GP
        speles = gp->GetSupportElems();
        iele = speles[0];
        ele = geometry->GetElem(iele);
        //get shapefunction of this gp respect to each node of support element
        gp_sfdy = ele->GetShapeFuncDYAt(gp_coords, node_coord);
        this->GPsShapeFuncDY.push_back(gp_sfdy);
    }
}

void CPreprocessorFEMTRI3::SetGPsShapeFuncDZ(CConfig* config, CGeometry* geometry){
    unsigned long ngp = GPs.size();
    vector<unsigned long> speles;
    unsigned long iele;
    CGaussPoint *gp;
    CElem *ele;
    vector<double> gp_coords;
    vector<double> gp_sfdz(3);
    vector<vector<double> > node_coord = geometry->GetNode_coord();
    for(unsigned long igp = 0; igp < ngp; igp++){
        gp = GPs[igp];
        gp_coords = gp->GetCoords();
        //for FEM T3, only one SPele for each GP
        speles = gp->GetSupportElems();
        iele = speles[0];
        ele = geometry->GetElem(iele);
        //get shapefunction of this gp respect to each node of support element
        gp_sfdz = ele->GetShapeFuncDZAt(gp_coords, node_coord);
        this->GPsShapeFuncDZ.push_back(gp_sfdz);
    }
}

void CPreprocessorFEMTRI3::SetLumpedMassAll(CConfig* config, CGeometry* geometry){
    unsigned short ndim = geometry->GetnDim();
    double density = config->GetDensity();
    vector<double> m(geometry->GetnNode()*ndim,0.0);

    unsigned long nele = geometry->GetnElem();
    unsigned long nele_tri = geometry->GetnElem_Tri();


    //check nele = nele_tri
    if(nele != nele_tri){
        cout << "SFEM : Error @ CPreprocessorFEMTRI3::SetLumpedMassTRI3All: nElem not equal to nElem_Tri!!!"<<endl;
        cout << "SFEM : Press any key to exit!!!" << endl;
        cin.get();
        exit(1);
    }

    //loop all elements to get nodal volume
    unsigned short nnode;
    unsigned long node_id;
    double ele_vol;
    for(unsigned long iele=0; iele<nele; iele++){
        nnode = geometry->GetElem(iele)->GetnNodes();
        ele_vol = geometry->GetElem(iele)->GetVol();
        for(unsigned short jnode = 0; jnode<nnode; jnode++){
            node_id = geometry->GetElem(iele)->GetNode(jnode);
            m[ndim*node_id] = m[ndim*node_id] + 0.3333333*ele_vol*density;
            m[ndim*node_id + 1] = m[ndim*node_id];
            if(ndim == 3) m[ndim*node_id + 2] = m[ndim*node_id];
        }
    }
    this->LumpedMass = m;
}

void CPreprocessorFEMTRI3::Display(){
    CPreprocessor::Display();
    cout << "LumpedMass:" << endl;
    for (int i=0; i<LumpedMass.size(); i++)
      cout << LumpedMass[i] << endl;
}

CPreprocessorFEMTRI3::~CPreprocessorFEMTRI3(){
    //dctor
}

//+++++++++++++++++++++++++
// CPreprocessorFEMTET4
//+++++++++++++++++++++++++
CPreprocessorFEMTET4::CPreprocessorFEMTET4(){
    //Ctor
}

CPreprocessorFEMTET4::CPreprocessorFEMTET4(CConfig *config, CGeometry *geometry){
    //Ctor
    SetElemsCGAndVol(config, geometry);

    SetElemsSize(config, geometry);

    SetLumpedMassAll(config, geometry);

    SetGPs(config, geometry);

    SetGPsShapeFunc(config, geometry);

    SetGPsShapeFuncDX(config, geometry);

    SetGPsShapeFuncDY(config, geometry);

    SetGPsShapeFuncDZ(config, geometry);
}

void CPreprocessorFEMTET4::SetElemsCGAndVol(CConfig* config, CGeometry* geometry){
    unsigned long nele = geometry->GetnElem();

    for(unsigned long iele=0; iele < nele; iele++){
        vector<double> p0_xyz, p1_xyz, p2_xyz, p3_xyz;
        unsigned long p0_ID, p1_ID, p2_ID, p3_ID;
        p0_ID = geometry->GetElem(iele)->GetNode(0);
        p1_ID = geometry->GetElem(iele)->GetNode(1);
        p2_ID = geometry->GetElem(iele)->GetNode(2);
        p3_ID = geometry->GetElem(iele)->GetNode(3);

        p0_xyz = geometry->GetNode(p0_ID)->GetCoords();
        p1_xyz = geometry->GetNode(p1_ID)->GetCoords();
        p2_xyz = geometry->GetNode(p2_ID)->GetCoords();
        p3_xyz = geometry->GetNode(p3_ID)->GetCoords();

        //use overloaded
        geometry->GetElem(iele)->SetCG(p0_xyz, p1_xyz, p2_xyz, p3_xyz);
        geometry->GetElem(iele)->SetVol(p0_xyz, p1_xyz, p2_xyz, p3_xyz);

        #ifdef DEBUG
        geometry->GetElem(iele)->Display();
        #endif // DEBUG

    }
}

void CPreprocessorFEMTET4::SetElemsSize(CConfig *config, CGeometry *geometry){
    unsigned long nele = geometry->GetnElem();

    for (unsigned long iele=0; iele < nele; iele++){
        vector<double> p0_xyz, p1_xyz, p2_xyz, p3_xyz;
        unsigned long p0_ID, p1_ID, p2_ID, p3_ID;
        p0_ID = geometry->GetElem(iele)->GetNode(0);
        p1_ID = geometry->GetElem(iele)->GetNode(1);
        p2_ID = geometry->GetElem(iele)->GetNode(2);
        p3_ID = geometry->GetElem(iele)->GetNode(3);

        p0_xyz = geometry->GetNode(p0_ID)->GetCoords();
        p1_xyz = geometry->GetNode(p1_ID)->GetCoords();
        p2_xyz = geometry->GetNode(p2_ID)->GetCoords();
        p3_xyz = geometry->GetNode(p3_ID)->GetCoords();

        double vol = geometry->GetElem(iele)->GetVol();
        vector<double> h(4);
        double h_ele;

        h[0] = 3.0*vol/GetAreaT3(p1_xyz, p2_xyz, p3_xyz);
        h[1] = 3.0*vol/GetAreaT3(p0_xyz, p2_xyz, p3_xyz);
        h[2] = 3.0*vol/GetAreaT3(p0_xyz, p1_xyz, p3_xyz);
        h[3] = 3.0*vol/GetAreaT3(p0_xyz, p2_xyz, p1_xyz);

        h_ele = *min_element(h.begin(), h.end());
        geometry->GetElem(iele)->Seth(h_ele);
    }
}

void CPreprocessorFEMTET4::SetGPs(CConfig* config, CGeometry* geometry){

    //loop each element
    unsigned long nele = geometry->GetnElem();
    unsigned long ele_id;
    vector<double> iele_CG;
    vector<unsigned long> iele_nodes;
    double iele_vol;
    for(unsigned long iele = 0; iele < nele; iele++){
        ele_id = geometry->GetElem(iele)->GetGlobalID();
        geometry->GetElem(iele)->SetCG();
        iele_CG = geometry->GetElem(iele)->GetCG_coord();
        iele_nodes = geometry->GetElem(iele)->GetNodes();

        iele_vol = geometry->GetElem(iele)->GetVol();

        CGaussPoint *gp = new CGaussPoint;
        gp->SetWeight(iele_vol);
        gp->SetSupportNodes(iele_nodes);
        gp->AddSupportElems(ele_id);
        gp->Setx(iele_CG[0]);  //for TET4 element, the CG is GP.
        gp->Sety(iele_CG[1]);
        gp->Setz(iele_CG[2]);
        gp->SetnDim(geometry->GetnDim());
        this->GPs.push_back(gp);

    }

    this->nGP = GPs.size();
}

void CPreprocessorFEMTET4::SetGPsShapeFunc(CConfig* config, CGeometry* geometry){
    //loop all GPs
    unsigned long ngp = GPs.size();
    vector<unsigned long> speles;
    unsigned long iele;
    CGaussPoint *gp;
    CElem *ele;
    vector<double> gp_sf(4);
    vector<double> gp_coords;
    vector<vector<double> > node_coord = geometry->GetNode_coord();
    for(unsigned long igp = 0; igp < ngp; igp++){
        gp = GPs[igp];
        gp_coords = gp->GetCoords();
        //for FEM T4, only one SPele for each GP
        speles = gp->GetSupportElems();
        iele = speles[0];
        ele = geometry->GetElem(iele);
        //get shapefunction of this gp respect to each node of support element
        gp_sf = ele->GetShapeFuncAt(gp_coords, node_coord);
        this->GPsShapeFunc.push_back(gp_sf);
    }
}

void CPreprocessorFEMTET4::SetGPsShapeFuncDX(CConfig* config, CGeometry* geometry){
    unsigned long ngp = GPs.size();
    vector<unsigned long> speles;
    unsigned long iele;
    CGaussPoint *gp;
    CElem *ele;
    vector<double> gp_coords;
    vector<double> gp_sfdx(4);
    vector<vector<double> > node_coord = geometry->GetNode_coord();
    for(unsigned long igp = 0; igp < ngp; igp++){
        gp = GPs[igp];
        gp_coords = gp->GetCoords();
        //for FEM T3, only one SPele for each GP
        speles = gp->GetSupportElems();
        iele = speles[0];
        ele = geometry->GetElem(iele);
        //get shapefunction of this gp respect to each node of support element
        gp_sfdx = ele->GetShapeFuncDXAt(gp_coords, node_coord);
        this->GPsShapeFuncDX.push_back(gp_sfdx);
    }
}

void CPreprocessorFEMTET4::SetGPsShapeFuncDY(CConfig* config, CGeometry* geometry){
    unsigned long ngp = GPs.size();
    vector<unsigned long> speles;
    unsigned long iele;
    CGaussPoint *gp;
    CElem *ele;
    vector<double> gp_coords;
    vector<double> gp_sfdy(4);
    vector<vector<double> > node_coord = geometry->GetNode_coord();
    for(unsigned long igp = 0; igp < ngp; igp++){
        gp = GPs[igp];
        gp_coords = gp->GetCoords();
        //for FEM T3, only one SPele for each GP
        speles = gp->GetSupportElems();
        iele = speles[0];
        ele = geometry->GetElem(iele);
        //get shapefunction of this gp respect to each node of support element
        gp_sfdy = ele->GetShapeFuncDYAt(gp_coords, node_coord);
        this->GPsShapeFuncDY.push_back(gp_sfdy);
    }
}

void CPreprocessorFEMTET4::SetGPsShapeFuncDZ(CConfig* config, CGeometry* geometry){
    unsigned long ngp = GPs.size();
    vector<unsigned long> speles;
    unsigned long iele;
    CGaussPoint *gp;
    CElem *ele;
    vector<double> gp_coords;
    vector<double> gp_sfdz(4);
    vector<vector<double> > node_coord = geometry->GetNode_coord();
    for(unsigned long igp = 0; igp < ngp; igp++){
        gp = GPs[igp];
        gp_coords = gp->GetCoords();
        //for FEM T3, only one SPele for each GP
        speles = gp->GetSupportElems();
        iele = speles[0];
        ele = geometry->GetElem(iele);
        //get shapefunction of this gp respect to each node of support element
        gp_sfdz = ele->GetShapeFuncDZAt(gp_coords, node_coord);
        this->GPsShapeFuncDZ.push_back(gp_sfdz);
    }
}

void CPreprocessorFEMTET4::SetLumpedMassAll(CConfig* config, CGeometry* geometry){
    unsigned short ndim = geometry->GetnDim();
    double density = config->GetDensity();
    vector<double> m(geometry->GetnNode()*ndim,0.0);

    unsigned long nele = geometry->GetnElem();
    unsigned long nele_tet = geometry->GetnElem_Tet();


    //check nele = nele_tri
    if(nele != nele_tet){
        cout << "SFEM : Error @ CPreprocessorFEMTET4::SetLumpedMassTET4All: nElem not equal to nElem_Tet!!!"<<endl;
        cout << "SFEM : Press any key to exit!!!" << endl;
        cin.get();
        exit(1);
    }

    //loop all elements to get nodal volume
    unsigned short nnode;
    unsigned long node_id;
    double ele_vol;
    for(unsigned long iele=0; iele<nele; iele++){
        nnode = geometry->GetElem(iele)->GetnNodes();
        ele_vol = geometry->GetElem(iele)->GetVol();
        for(unsigned short jnode = 0; jnode<nnode; jnode++){
            node_id = geometry->GetElem(iele)->GetNode(jnode);
            m[ndim*node_id] = m[ndim*node_id] + 0.25*ele_vol*density;
            m[ndim*node_id + 1] = m[ndim*node_id];
            if(ndim == 3) m[ndim*node_id + 2] = m[ndim*node_id];
        }
    }
    this->LumpedMass = m;
}

void CPreprocessorFEMTET4::Display(){
    CPreprocessor::Display();
    cout << "LumpedMass:" << endl;
    for (int i=0; i<LumpedMass.size(); i++)
      cout << LumpedMass[i] << endl;
}

CPreprocessorFEMTET4::~CPreprocessorFEMTET4(){
    //dctor
}

