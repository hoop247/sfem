////////////////////////////////////////////////////////////////////////////
//            SSSSSS  FFFFFF  EEEEEE  MMM   MMM        ++       ++        //
//           SS       FF      EE      MM M M MM       ++       ++         //
//           SSSSSSS  FFFF    EEEE    MM  M  MM   ++++++++ ++++++++       //
//                SS  FF      EE      MM     MM     ++       ++           //
//                SS  FF      EE      MM     MM    ++       ++            //
//           SSSSSS   FF      EEEEEE  MM     MM   ++       ++             //
////////////////////////////////////////////////////////////////////////////
#include "gausspoint.hpp"

CGaussPoint::CGaussPoint()
{
    //ctor
    x=0.0; y=0.0; z=0.0;
    SDidx = 0;

}

CGaussPoint::CGaussPoint(vector<double> &val_coord, double val_weight,
                        vector<unsigned long> &val_spnodes, vector<unsigned long> &val_speles,
                         vector<double> &val_normal, unsigned long val_SDidx){

    nDim = val_coord.size();
    x = val_coord[0];
    y = val_coord[1];
    z = 0.0;
    if(nDim == 3) z = val_coord[2];

    Weight = val_weight;
    SupportNodes = val_spnodes;
    SupportElems = val_speles;
    Normal = val_normal;
    SDidx = val_SDidx;

}

CGaussPoint::~CGaussPoint()
{
    //dtor
}
