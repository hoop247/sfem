////////////////////////////////////////////////////////////////////////////
//            SSSSSS  FFFFFF  EEEEEE  MMM   MMM        ++       ++        //
//           SS       FF      EE      MM M M MM       ++       ++         //
//           SSSSSSS  FFFF    EEEE    MM  M  MM   ++++++++ ++++++++       //
//                SS  FF      EE      MM     MM     ++       ++           //
//                SS  FF      EE      MM     MM    ++       ++            //
//           SSSSSS   FF      EEEEEE  MM     MM   ++       ++             //
////////////////////////////////////////////////////////////////////////////
#include "../include/geometry.hpp"

CGeometry::CGeometry(){
    //ctor
}

CGeometry::~CGeometry(){
    //dtor
}


/** \brief  constructor to read in the SU2 format meshfile
 *
 * \param
 * \param
 * \return
 *
 */
CGeometry::CGeometry(CConfig *config){
    //ctor
    nElem = 0;
    nElem_Tet = 0;
    nElem_Tri = 0;
    nMarker = 0;
    nElem_Tri_Bound = 0;
    nElem_Line_Bound = 0;
    nElem_Vertex_Bound = 0;


    // Todo: 2015-05-26-00.49
    cout << "SFEM : only TET4 and TRI3 elements is availiable now!"<< endl;
    mesh_filename = config->GetMesh_FileName();
    Read_SU2_format(config);
    AssignElemstoNodes();

}


/** \brief  function to read in the SU2 format meshfile

 */
void CGeometry::Read_SU2_format(CConfig *config){
    string text_line, Marker_Tag;
    ifstream mesh_file;
    unsigned short VTK_Type, iMarker, iChar, iCount=0;
    unsigned long iElem, iPoint;
    char cstr[200];
    vector<double> Coord;
    unsigned long vnodes_triangle[3], vnodes_tetrahedron[4], vnodes_line[2];
    string::size_type position;
    umat marker_elem;

    iElem = 0;
    iPoint = 0;
    nElem_Tet = 0, nElem_Tri = 0;

    /* ------------   Open grid file  --------------*/
   strcpy (cstr, mesh_filename.c_str());
   mesh_file.open(cstr, ios::in);
   cout << "SFEM : Mesh file name : " << mesh_filename << "." << endl;

   /*------------  Check the grid file ------------*/
   if( mesh_file.fail() ){
        cout << "SFEM : There is no geometry file!!! " << endl;
        cout << "SFEM : Press any key to exit!!!" << endl;
        cin.get();
        exit(1);
   }

   /*---------- Read meshfile line by line ------------*/
   while ( getline(mesh_file, text_line)){

        /*--------   Get the dimension  ------- */
        position = text_line.find("NDIME=",0);
        if (position != string::npos) {
            text_line.erase (0,6);
            nDim= atoi(text_line.c_str());
            if ( nDim == 2) cout << "SFEM : Two dimensional problem." << endl;
            if ( nDim == 3) cout << "SFEM : Three dimensional problem." << endl;
        }

         /*--------   Read information about the elements  ------- */
         position = text_line.find("NELEM=", 0);
         if (position != string::npos ){
            text_line.erase (0,6);
            nElem = atoi(text_line.c_str());

            /*--------   Read each line of element information  ------- */
            while ( iElem < (nElem)) {
                getline(mesh_file, text_line);
                istringstream ele_line(text_line);

                ele_line >> VTK_Type;

                switch (VTK_Type){
                case TRIANGLE:{
                    ele_line >> vnodes_triangle[0] >> vnodes_triangle[1] >> vnodes_triangle[2];
                    CElemTRI3 *ele_tri = new CElemTRI3(vnodes_triangle[0], vnodes_triangle[1], vnodes_triangle[2], iElem, nDim);
                    Elem.push_back(ele_tri);
                    iElem++;
                    nElem_Tri++;
                    break;
                }



                case TETRAHEDRON:{
                    ele_line >> vnodes_tetrahedron[0] >> vnodes_tetrahedron[1]  >> vnodes_tetrahedron[2]  >> vnodes_tetrahedron[3];
                    CElemTET4 *ele_tet = new CElemTET4(vnodes_tetrahedron[0], vnodes_tetrahedron[1], vnodes_tetrahedron[2], vnodes_tetrahedron[3], iElem,nDim);
                    Elem.push_back(ele_tet);
                    iElem++;
                    nElem_Tet++;
                    break;
                }


                }
            }
            cout << "SFEM : " << nElem << " elements" << endl;
            if(nElem_Tet > 0)
                cout << "SFEM : " << nElem_Tet << " tetrahedral elements. " << endl;
            if(nElem_Tri > 0)
                cout << "SFEM : " << nElem_Tri << " triangluar elements. " << endl;
            #ifdef DEBUG
            for(int i=0; i<Elem.size(); i++){
                Elem[i]->Display();
            }
            #endif // DEBUG
         }

        /*--------   Read information about the nodes  ------- */
        position = text_line.find("NPOIN=", 0);
        if (position != string::npos) {
            text_line.erase (0,6);

            stringstream stream_line(text_line);
            stream_line >> nPoint;
            Coord.resize(nDim);

            for ( iPoint = 0; iPoint < nPoint; iPoint++ ){
                getline(mesh_file, text_line);
                istringstream point_line(text_line);
                point_line >> Coord[0];
                point_line >> Coord[1];
                if ( nDim == 3) point_line >> Coord[2];
                CNode *Point = new CNode;
                Point->SetCoords(Coord);
                Point->SetGlobalID(iPoint);
                Node.push_back(Point);
            }
            cout << "SFEM : " << Node.size() << " nodes." << endl;
            //for debug Class CNode
            #ifdef DEBUG
            for(int i=0; i<Node.size(); i++){
                cout << "SFEM : Node [" << Node[i]->GetGlobalID() <<"]'s coords are : "
                << Node[i]->GetX() << ", "
                << Node[i]->GetY() << ", "
                << Node[i]->GetZ() << endl;
            }
            cout << endl;
            #endif

        }


        /*--------   Read information about markers  ------- */
        position = text_line.find("NMARK=", 0);
        if ( position != string::npos) {
            text_line.erase(0,6);  nMarker = atoi(text_line.c_str());
            cout << "SFEM : "<<nMarker << " surface markers." << endl;
            config->SetnMarker_All( nMarker );
            // Todo: By JC 2015-05-26-00.49, need to modify evey time add with new marker.
            if (nMarker != config->GetnMarker_Config()){
                cout << "SFEM: Warning: Number of markers in meshfile is not equal to number of markers in configure file!!!" << endl;
            }

            for ( iMarker = 0; iMarker < nMarker; iMarker ++){
                CBoundMarker *marker_temp = new CBoundMarker;
                // MARKER_TAG= xxxx
                getline (mesh_file,text_line);
                text_line.erase (0,11);
                string::size_type position;
                for (iChar = 0; iChar < 20; iChar++) {
                      position = text_line.find( " ", 0 );
                      if(position != string::npos) text_line.erase (position,1);
                      position = text_line.find( "\r", 0 );
                      if(position != string::npos) text_line.erase (position,1);
                      position = text_line.find( "\n", 0 );
                      if(position != string::npos) text_line.erase (position,1);
                }
                //
                marker_temp->SetTag(text_line);

                // MARKER_ELEMS= xxxx
                getline(mesh_file, text_line);
                text_line.erase(0,13);
                //vMarker_nElem[iMarker] = atoi(text_line.c_str());
                marker_temp->SetnBoundElem(atoi(text_line.c_str()));
                cout << "SFEM : "<< marker_temp->GetnBoundElem() <<
                " boundary elements in index "<< iMarker <<" (Marker = "
                << marker_temp->GetTag()<< ")." << endl;

                for ( iElem = 0; iElem < marker_temp->GetnBoundElem(); iElem++){
                    getline(mesh_file, text_line);
                    istringstream bound_line(text_line);
                    bound_line >> VTK_Type;

                    switch(VTK_Type){
                        case VERTEX :
                        {
                        bound_line >> vnodes_line[0];
                        CElemVERTEX1 *ele_bound_vertex = new CElemVERTEX1(vnodes_line[0], nElem_Vertex_Bound, nDim);
                        marker_temp->SetBoundElem(ele_bound_vertex);
                        nElem_Line_Bound++;
                        break;
                        }


                        case LINE :
                        {
                        bound_line >> vnodes_line[0] >> vnodes_line[1];
                        CElemLINE2 *ele_bound_line = new CElemLINE2(vnodes_line[0], vnodes_line[1], nElem_Line_Bound, nDim);
                        marker_temp->SetBoundElem(ele_bound_line);
                        nElem_Line_Bound++;
                        break;
                        }


                        case TRIANGLE :
                        {
                        bound_line >>vnodes_triangle[0] >> vnodes_triangle[1] >> vnodes_triangle[2];
                        CElemTRI3 *ele_bound_tri = new CElemTRI3(vnodes_triangle[0], vnodes_triangle[1], vnodes_triangle[2], nElem_Tri_Bound, nDim);
                        marker_temp->SetBoundElem(ele_bound_tri);
                        nElem_Tri_Bound++;
                        break;
                        }

                    }


                }
                marker_temp->SetNodes();
                vector<vector<double> > mat_nodecoord = GetNode_coord();
                marker_temp->SetBoundElemsNormalsAll(mat_nodecoord);
                marker_temp->SetBoundElemsCGsAll(mat_nodecoord);
                marker_temp->SetBoundElemsVolAll(mat_nodecoord);
                //For debug
                #ifdef DEBUG
                marker_temp->Display();
                #endif // DEBUG
                Markers.push_back(marker_temp);
            }

        }



   }

    cout << "SFEM : End of reading in meshfiles." << endl;
   mesh_file.close();

}

vector<vector<double> > CGeometry::GetNode_coord(){
    vector<vector<double> > node_coord;
    double x, y, z;
    vector<double> node_coord_row;

    for(unsigned long inode = 0; inode < GetnNode(); inode++){
        node_coord_row.clear();
        x = Node[inode]->GetX();
        y = Node[inode]->GetY();
        node_coord_row.push_back(x);
        node_coord_row.push_back(y);
        if(nDim == 3){
            z= Node[inode]->GetZ();
            node_coord_row.push_back(z);
        }
        node_coord.push_back(node_coord_row);
    }

    return node_coord;
}


void CGeometry::AssignElemstoNodes(){
    unsigned long iele, nele;
    nele = this->nElem;

    //loop all elements
    for(iele = 0; iele < nele; iele++){
        vector<unsigned long> iele_nodes = Elem[iele]->GetNodes();
        unsigned long globalID = Elem[iele]->GetGlobalID();
        //loop all nodes in element
        for(unsigned short inode = 0; inode < iele_nodes.size(); inode++){
            unsigned long nodeID = iele_nodes[inode];
            this->Node[nodeID]->AddElem(globalID);
        }
    }

    #ifdef DEBUG
    cout << "Debug for CGeometry::AssignElemstoNodes(): " << endl;
    for(int i=0; i<Node.size(); i++){
        cout << "Node [" << Node[i]->GetGlobalID() << "]'s shared elements are: ";
        vector<unsigned long> eles = Node[i]->GetElems();
        for(int j=0; j<eles.size(); j++){
            cout << eles[j] << ", ";
        }
        cout << ";"<< endl;
    }

    #endif // DEBUG
}

CBoundMarker* CGeometry::GetMarker(string val_string){

    for (unsigned short imarker=0; imarker<this->GetnMarker(); imarker++ ){
        if (Markers[imarker]->GetTag() == val_string){
            return Markers[imarker];
            break;
        }
    }
    cout << "SFEM : Error, No such a Marker = " << val_string << " in geometry file!!!" << endl;
    cout << "SFEM : Press any key to exit!!!" << endl;
    cin.get();
    exit(1);

}
