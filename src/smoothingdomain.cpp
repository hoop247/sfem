////////////////////////////////////////////////////////////////////////////
//            SSSSSS  FFFFFF  EEEEEE  MMM   MMM        ++       ++        //
//           SS       FF      EE      MM M M MM       ++       ++         //
//           SSSSSSS  FFFF    EEEE    MM  M  MM   ++++++++ ++++++++       //
//                SS  FF      EE      MM     MM     ++       ++           //
//                SS  FF      EE      MM     MM    ++       ++            //
//           SSSSSS   FF      EEEEEE  MM     MM   ++       ++             //
////////////////////////////////////////////////////////////////////////////
#include "smoothingdomain.hpp"

//+++++++++++++++++++++++++
// CSmoothingDomain
//+++++++++++++++++++++++++
CSmoothingDomain::CSmoothingDomain()
{
    //ctor
}

CSmoothingDomain::~CSmoothingDomain()
{
    //dtor
}

void CSmoothingDomain::Display(void) {

    cout << "Is the Boundary SD: " << this->isBoundSD << endl;

    cout << "Elem: ";
    for (int i=0; i<Elems.size(); i++)
      cout << Elems[i] << ", ";
    cout << endl;

    cout << "Nodes: ";
    for (int i=0; i<Nodes.size(); i++)
      cout << Nodes[i] << ", ";
    cout << endl;

    cout << "SupportNodes: ";
    for (int i=0; i<SupportNodes.size(); i++)
      cout << SupportNodes[i] << ", ";
    cout << endl;

    cout << "Volume: " << Vol << endl;
    cout << endl;

}
//+++++++++++++++++++++++++
// CEdgeBasedSD_T3
//+++++++++++++++++++++++++
unsigned short CEdgeBasedSD_T3::Type = 15;

CEdgeBasedSD_T3::CEdgeBasedSD_T3(){
    //ctor
}

CEdgeBasedSD_T3::~CEdgeBasedSD_T3(){
    //dtor
}

CEdgeBasedSD_T3::CEdgeBasedSD_T3(unsigned long val_node1, unsigned long val_node2,
                  CElem *val_Elem1, CElem *val_Elem2,
                  unsigned short val_nDim, bool val_isbound){
    //non-boundary SD
    nDim = val_nDim;
    isBoundSD = val_isbound;
    Nodes.push_back(val_node1);
    Nodes.push_back(val_node2);
    Elems.push_back(val_Elem1->GetGlobalID());
    Elems.push_back(val_Elem2->GetGlobalID());

    SupportNodes = val_Elem1->GetNodes();
    vector<unsigned long> SupportNodes_temp = val_Elem2->GetNodes();
    SupportNodes.insert(SupportNodes.begin(), SupportNodes_temp.begin(),
                         SupportNodes_temp.end());
    //unique support nodes
    sort(SupportNodes.begin(), SupportNodes.end());
    SupportNodes.erase(unique(SupportNodes.begin(), SupportNodes.end()), SupportNodes.end());
    nSupportNodes = SupportNodes.size();

    //
    SetVol(val_Elem1->GetVol(), val_Elem2->GetVol());
    if(Vol < 1e-010){
        cout << "SFEM : CEdgeBasedSD_T3::CEdgeBasedSD_T3(), Error, maybe volume of elements are not initialed!" << endl;
        cout << "SFEM : Press any key to exit!!!" << endl;
        cin.get();
        exit(1);
    }
}

CEdgeBasedSD_T3::CEdgeBasedSD_T3(unsigned long val_node1, unsigned long val_node2,
                  CElem *val_Elem1, unsigned short val_nDim,
                  bool val_isbound){
    //boundary SD
    nDim = val_nDim;
    isBoundSD = val_isbound;
    Nodes.push_back(val_node1);
    Nodes.push_back(val_node2);
    Elems.push_back(val_Elem1->GetGlobalID());

    SupportNodes = val_Elem1->GetNodes();
    nSupportNodes = SupportNodes.size();

    SetVol(val_Elem1->GetVol(),0.0);

    if(Vol < 1e-010){
        cout << "SFEM : CEdgeBasedSD_T3::CEdgeBasedSD_T3(), Error, maybe volume of elements are not initialed!" << endl;
        cout << "SFEM : Press any key to exit!!!" << endl;
        cin.get();
        exit(1);
    }
}

void CEdgeBasedSD_T3::Display(void){
    cout << "Type: " << Type << endl;
    CSmoothingDomain::Display();
}

//+++++++++++++++++++++++++
// CNodeBasedSD_T3
//+++++++++++++++++++++++++
unsigned short CNodeBasedSD_T3::Type = 25;

CNodeBasedSD_T3::CNodeBasedSD_T3(){
    //ctor
}

CNodeBasedSD_T3::CNodeBasedSD_T3(unsigned long val_node, vector<CElem*> val_Elems,
                                unsigned short val_ndim){
    //
    nDim = val_ndim;
    Nodes.push_back(val_node);
    unsigned short nspele = val_Elems.size();
    double vol_temp = 0.0;

    for (unsigned short ispele = 0; ispele < nspele; ispele++){
        CElem *Elem = val_Elems[ispele];
        Elems.push_back(Elem->GetGlobalID());

        vector<unsigned long> Nodes_ispele = Elem->GetNodes();
        SupportNodes.insert(SupportNodes.begin(), Nodes_ispele.begin(),
                            Nodes_ispele.end());

        vol_temp = vol_temp + 0.33333*Elem->GetVol();
    }

    //unique support nodes
    sort(SupportNodes.begin(), SupportNodes.end());
    SupportNodes.erase(unique(SupportNodes.begin(), SupportNodes.end()), SupportNodes.end());
    nSupportNodes = SupportNodes.size();

    //
    SetVol(vol_temp);
    if(Vol < 1e-010){
        cout << "SFEM : CNodeBasedSD_T3::CNodeBasedSD_T3(), Error, maybe volume of elements are not initialed!" << endl;
        cout << "SFEM : Press any key to exit!!!" << endl;
        cin.get();
        exit(1);
    }

}

void CNodeBasedSD_T3::Display(void){
    cout << "Type: " << Type << endl;
    CSmoothingDomain::Display();
}

CNodeBasedSD_T3::~CNodeBasedSD_T3(){
    //dctor
}

//+++++++++++++++++++++++++
// CFaceBasedSD_T4
//+++++++++++++++++++++++++

unsigned short CFaceBasedSD_T4::Type = 310;

CFaceBasedSD_T4::CFaceBasedSD_T4(){
    //ctor
}

CFaceBasedSD_T4::CFaceBasedSD_T4(unsigned long val_node0, unsigned long val_node1, unsigned long val_node2,
                    CElem *val_Elem0, CElem *val_Elem1, bool val_isbound){
    //non-boundary SD
    nDim = 3;
    isBoundSD = val_isbound;

    if(isBoundSD == true){
        cout << "SFEM : CFaceBasedSD_T4::CFaceBasedSD_T4(), Error, give non boundary SD with isBound tag!" << endl;
        cout << "SFEM : Press any key to exit!!!" << endl;
        cin.get();
        exit(1);
    }

    Nodes.push_back(val_node0);
    Nodes.push_back(val_node1);
    Nodes.push_back(val_node2);
    Elems.push_back(val_Elem0->GetGlobalID());
    Elems.push_back(val_Elem1->GetGlobalID());

    //get all support nodes
    SupportNodes = val_Elem0->GetNodes();
    vector<unsigned long> SupportNodes_temp = val_Elem1->GetNodes();
    SupportNodes.insert(SupportNodes.begin(), SupportNodes_temp.begin(),
                         SupportNodes_temp.end());

    //unique support nodes
    sort(SupportNodes.begin(), SupportNodes.end());
    SupportNodes.erase(unique(SupportNodes.begin(), SupportNodes.end()), SupportNodes.end());
    nSupportNodes = SupportNodes.size();

    //
    SetVol(val_Elem0->GetVol(), val_Elem1->GetVol());
    if(Vol < 1e-010){
        cout << "SFEM : CFaceBasedSD_T4::CFaceBasedSD_T4(), Error, maybe volume of elements are not initialed!" << endl;
        cout << "SFEM : Press any key to exit!!!" << endl;
        cin.get();
        exit(1);
    }


}

CFaceBasedSD_T4::CFaceBasedSD_T4(unsigned long val_node0, unsigned long val_node1, unsigned long val_node2,
                    CElem *val_Elem0, bool val_isbound){
    //non-boundary SD
    nDim = 3;
    isBoundSD = val_isbound;

    if(isBoundSD == false){
        cout << "SFEM : CFaceBasedSD_T4::CFaceBasedSD_T4(), Error, give boundary SD with isBound = false tag!" << endl;
        cout << "SFEM : Press any key to exit!!!" << endl;
        cin.get();
        exit(1);
    }

    Nodes.push_back(val_node0);
    Nodes.push_back(val_node1);
    Nodes.push_back(val_node2);
    Elems.push_back(val_Elem0->GetGlobalID());

    //get all support nodes
    SupportNodes = val_Elem0->GetNodes();
    nSupportNodes = SupportNodes.size();
    //
    SetVol(val_Elem0->GetVol(), 0.0);
    if(Vol < 1e-010){
        cout << "SFEM : CFaceBasedSD_T4::CFaceBasedSD_T4(), Error, maybe volume of elements are not initialed!" << endl;
        cout << "SFEM : Press any key to exit!!!" << endl;
        cin.get();
        exit(1);
    }
}

void CFaceBasedSD_T4::Display(void){
    cout << "Type: " << Type << endl;
    CSmoothingDomain::Display();
}

CFaceBasedSD_T4::~CFaceBasedSD_T4(){
    //dctor
}
