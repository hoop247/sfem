////////////////////////////////////////////////////////////////////////////
//            SSSSSS  FFFFFF  EEEEEE  MMM   MMM        ++       ++        //
//           SS       FF      EE      MM M M MM       ++       ++         //
//           SSSSSSS  FFFF    EEEE    MM  M  MM   ++++++++ ++++++++       //
//                SS  FF      EE      MM     MM     ++       ++           //
//                SS  FF      EE      MM     MM    ++       ++            //
//           SSSSSS   FF      EEEEEE  MM     MM   ++       ++             //
////////////////////////////////////////////////////////////////////////////
#include "node.hpp"

CNode::CNode()
{
    //ctor
    GlobalID = 0;
}

CNode::CNode(vector<double> val_coords, vector<unsigned long> val_elems, unsigned long val_globalID)
{
    Coords = val_coords;
    Elems = val_elems;
    GlobalID = val_globalID;
}

CNode::~CNode()
{
    //dtor
    Elems.clear();
    Coords.clear();
}
