////////////////////////////////////////////////////////////////////////////
//            SSSSSS  FFFFFF  EEEEEE  MMM   MMM        ++       ++        //
//           SS       FF      EE      MM M M MM       ++       ++         //
//           SSSSSSS  FFFF    EEEE    MM  M  MM   ++++++++ ++++++++       //
//                SS  FF      EE      MM     MM     ++       ++           //
//                SS  FF      EE      MM     MM    ++       ++            //
//           SSSSSS   FF      EEEEEE  MM     MM   ++       ++             //
////////////////////////////////////////////////////////////////////////////

#include "../include/config.hpp"

CConfig::CConfig(char case_filename[200]){
    string text_line, text2find, option_name, keyword;
    ifstream case_file;
    vector<string> option_value;

    /*--- Read the configuration file ---*/
    case_file.open(case_filename, ios::in);

	if (case_file.fail()) {
		cout << "There is no configuration file!!" << endl;
		cout << "Press any key to exit..." << endl;
		cin.get(); exit(1);
	}

  /*--- Reading config options  ---*/
  SetConfig_Options();

  /*--- Parsing the config file  ---*/
  SetParsing(case_filename);

}

CConfig::~CConfig(void)
{ }

void CConfig::AddMarkerOption(const string & name, unsigned short & num_marker, string* & marker) {
	//cout << "Adding Marker option " << name << endl;
	num_marker = 0;
	CAnyOptionRef* option_ref = new CMarkerOptionRef(marker, num_marker);
	param.insert( pair<string, CAnyOptionRef*>(name, option_ref) );
}

// Todo L2: how to add arbitary number of  keywords of displacement markers.
//       2015-05-27-15.40  JC
void CConfig::AddMarkerDisplacement(const string & name, unsigned short & nMarker_Displacement,string* & Marker_Displacement,
		unsigned short* & Displx_idx, unsigned short* & Disply_idx, unsigned short* & Displz_idx,
		double* & Displx, double* & Disply, double* & Displz) {
	nMarker_Displacement = 0;
	CAnyOptionRef* option_ref = new CMarkerDisplacementRef(nMarker_Displacement, Marker_Displacement,
                                       Displx_idx,Disply_idx,Displz_idx,
                                       Displx, Disply, Displz);
	param.insert( pair<string, CAnyOptionRef*>(name, option_ref) );
}

void CConfig::AddMarkerVelocity(const string & name, unsigned short & nMarker_Velocity,string* & Marker_Velocity,
								unsigned short* & Veloc_X_Idx, unsigned short* & Veloc_Y_Idx, unsigned short* & Veloc_Z_Idx,
								double* & Veloc_X, double* & Veloc_Y, double* & Veloc_Z ) {
	nMarker_Velocity = 0;
	// Since velocity marker and displacement marker have same pattern, reuse the CMarkerDisplacmentRef
	CAnyOptionRef* option_ref = new CMarkerVelocityRef(nMarker_Velocity, Marker_Velocity,
												Veloc_X_Idx, Veloc_Y_Idx, Veloc_Z_Idx,
												Veloc_X, Veloc_Y, Veloc_Z);
	param.insert( pair<string, CAnyOptionRef*>(name, option_ref) );
}

void CConfig::AddMarkerPressure(const string & name, unsigned short & nMarker_Pressure,
		string* & Marker_Pressure, double* & Pressure) {
	nMarker_Pressure = 0;
	CAnyOptionRef* option_ref = new CMarkerPressureRef(nMarker_Pressure, Marker_Pressure,
					Pressure);
	param.insert( pair<string, CAnyOptionRef*>(name, option_ref) );
}

void CConfig::AddMarkerInlet(const string & name, unsigned short & nMarker_Inlet,
		string* & Marker_Inlet, double* & Ttotal, double* & Ptotal) {
	nMarker_Inlet = 0;
	CAnyOptionRef* option_ref = new CMarkerInletRef_(nMarker_Inlet, Marker_Inlet,
			Ttotal, Ptotal);
	param.insert( pair<string, CAnyOptionRef*>(name, option_ref) );
}


void CConfig::SetConfig_Options(void) {

	double default_vec_2d[2], default_vec_3d[3], default_vec_4d[4],default_vec_6d[6];

	/* DESCRIPTION: Method for solid mechanics */
	AddEnumOption("METHOD", Kind_Method, Method_Map, "NONE");

	/* DESCRIPTION: Solver type */
	AddEnumOption("SOLVER_TYPE", Kind_Solver, Solver_Map, "EXPLICIT_DYNA");

	/* DESCRIPTION: Material type */
	AddEnumOption("MATERIAL_TYPE", Kind_Material, Material_Map, "LINEAR_ELASTIC");

	/* DESCRIPTION: Element type */
	AddEnumOption("ELEMENT_TYPE", Kind_Elem, Elem_Map, "NONE");

	/* DESCRIPTION: Input file of structural mesh.*/
	AddScalarOption("MESH_FILENAME", Mesh_FileName, string("mesh.su2"));

	/* DESCRIPTION: Output file of structural displacement.*/
	AddScalarOption("OUTPUT_FILENAME", Output_FileName, string("solid.dat"));

	/* DESCRIPTION: History file of structural displacement.*/
	AddScalarOption("History_FILENAME", History_FileName, string("history.dat"));

    /* DESCRIPTION: Input file of FSI loads.*/
	AddScalarOption("FSI_LOAD_FILENAME", FSI_Load_FileName, string("FSIload.dat"));

    /* DESCRIPTION: Output file of FSI displacement.*/
	AddScalarOption("FSI_DISPLACEMENT_FILENAME", FSI_Displacement_FileName, string("FSIDisplacement.dat"));

	    /* DESCRIPTION: Output file of FSI marker mesh new.*/
	AddScalarOption("FSI_MESH_FILENAME", FSI_Mesh_FileName, string("FSIMarker.su2"));

	/* DESCRIPTION: Number of external iteration.*/
	AddScalarOption("EXT_ITER", nExtIter, 999999);

	/* DESCRIPTION: Materials parameters.*/
	///TODO (JC): better implementation of material properties.
	///Because may encounter different materials problem in future.
	default_vec_4d[0] = 1.0; default_vec_4d[1] = 1.0;
	default_vec_4d[2] = 1.0; default_vec_4d[3] = 1.0;
	AddArrayOption("MAT_PARA",4,Mat_Para, default_vec_4d);

	/* DESCRIPTION: Materials parameters for fluids.*/
	///TODO (JC): better implementation of material properties.
	///Because may encounter different materials problem in future.
	default_vec_2d[0] = 1.0; default_vec_2d[1] = 1.0;
	AddArrayOption("MAT_PARA_FLUID",2,Mat_Para_Fluid, default_vec_2d);

	/* DESCRIPTION: Materials parameters.*/
	default_vec_3d[0] = 0.0; default_vec_3d[1] = 0.0;
	default_vec_3d[2] = 0.0;
	AddArrayOption("GRAVITY",3, Gravity, default_vec_3d);

	/* DESCRIPTION: Damping Coefficient.*/
	AddScalarOption("DAMP_COEF", Damp_Coef, 0.0);

	/* DESCRIPTION: Minimal residual to judge reach the steady state.*/
	AddScalarOption("RESIDUAL_MINVAL", Residual_min, 0.0);

	/* DESCRIPTION: Number of internal iteration.*/
	AddScalarOption("INT_ITER", nIntIter, 200);

	/* DESCRIPTION: Set current FSI iteration.*/
	AddScalarOption("CURRENT_FSI_ITER", CurrentFSIIter, 0);

	/* DESCRIPTION: Time step length */
	AddScalarOption("DELTA_TIME", DeltaTime, 0.0);

	/*DESCRIPTION: Total Time */
	AddScalarOption("TOTAL_TIME", TotalTime, 1.0);

	/* DESCRIPTION: Geometrical nonlinearity option */
	AddSpecialOption("GEO_NONLINEAR", Geo_NL, SetBoolOption, false);

	/* DESCRIPTION: Restart problem option */
	AddSpecialOption("RESTART", Restart, SetBoolOption, false);

	/* DESCRIPTION: Restart file of smoothing domain.*/
	AddScalarOption("RESTART_SMOOTHINGDOMAIN_FILENAME", Restart_SD_FileName, string("RestartSD.dat"));

    /* DESCRIPTION: Restart file of data, disp or veloc.*/
	AddScalarOption("RESTART_DATA_FILENAME", Restart_Data_FileName, string("RestartDATA.dat"));

	/* DESCRIPTION: Writing solution frequency */
	AddScalarOption("WRT_SOL_FREQ", Wrt_Sol_Freq,1);

	/* DESCRIPTION: printing on screen convergence history frequency */
	AddScalarOption("WRT_CON_FREQ", Wrt_Con_Freq, 1);

    /* DESCRIPTION: printing on screen convergence history frequency  of  dual-time strategy*/
	AddScalarOption("WRT_CON_FREQ_DUAL_TIME", Wrt_Con_Freq_DualTime, 1);

	/* DiMarker_VelocESCRIPTION: surface boundary elements to add displacement BCs*/
    AddMarkerDisplacement("MARKER_DISPLACEMENT", nMarker_Displacement, Marker_Displacement,
						Displ_X_Indicator, Displ_Y_Indicator, Displ_Z_Indicator,
						Displ_X, Displ_Y, Displ_Z);

    /* DESCRIPTION: surface boundary elements to add pressure BCs*/
    AddMarkerPressure("MARKER_PRESSURE", nMarker_Pressure, Marker_Pressure, Pressure);

    /* DESCRIPTION: surface boundary elements to add velocity BCs*/
    AddMarkerVelocity("MARKER_VELOCITY", nMarker_Velocity, Marker_Velocity,
					 Velocity_X_Indicator, Velocity_Y_Indicator, Velocity_Z_Indicator,
					 Velocity_X, Velocity_Y, Velocity_Z);

	/* DESCRIPTION: surface boundary elements to add fluid pressure inlet, only for test use.*/
    AddMarkerInlet("MARKER_INLET", nMarker_Inlet, Marker_Inlet, Ttotal, Ptotal);

    /* DESCRIPTION: surface boundary elements to indicate the FSI wet surface of solid.*/
    AddMarkerOption("MARKER_FSI_INTERFACE", nMarker_FSI, Marker_FSI);


}

void CConfig::SetParsing(char case_filename[200]) {
	string text_line, option_name;
	ifstream case_file;
	vector<string> option_value;

//	int rank = MASTER_NODE;
//#ifndef NO_MPI
//	rank = MPI::COMM_WORLD.Get_rank();
//#endif

  /*--- Read the configuration file ---*/
  case_file.open(case_filename, ios::in);

  if (case_file.fail()) {
    cout << "There is no configuration file!!" << endl;
    cout << "Press any key to exit..." << endl;
    cin.get(); exit(1);
	}

	/*--- Parse the configuration file and set the options ---*/
	while (getline (case_file,text_line)) {
		if (TokenizeString(text_line, option_name, option_value)) {
			map<string, CAnyOptionRef*>::iterator it;
			it = param.find(option_name);
			if (it != param.end()) {
				param[option_name]->SetValue(option_value);
			}
		}
	}

	case_file.close();

}

void CConfig::SetBoolOption(bool* ref, const vector<string> & value) {
	if ( (value[0] != "YES") && (value[0] != "NO") ) {
		cerr << "Error in CConfig::SetBoolOption(): "
				<< "option value provided must be \"YES\" or \"NO\";"
				<< "value given is " << value[0] << endl;
		throw(-1);
	}
	if (value[0] == "YES") {
		*ref = true;
	} else {
		*ref = false;
	}
}

bool CConfig::TokenizeString(string & str, string & option_name,
		vector<string> & option_value) {
	const string delimiters(" ()[]{}:,\t\n\v\f\r");
	// check for comments or empty string
	string::size_type pos, last_pos;
	pos = str.find_first_of("%");
	if ( (str.length() == 0) || (pos == 0) ) {
		// str is empty or a comment line, so no option here
		return false;
	}
	if (pos != string::npos) {
		// remove comment at end if necessary
		str.erase(pos);
	}

	// look for line composed on only delimiters (usually whitespace)
	pos = str.find_first_not_of(delimiters);
	if (pos == string::npos) {
		return false;
	}

	// find the equals sign and split string
	string name_part, value_part;
	pos = str.find("=");
	if (pos == string::npos) {
		cerr << "Error in TokenizeString(): "
				<< "line in the configuration file with no \"=\" sign."
				<< endl;
		cout << "Look for: " << str << endl;
		cout << "str.length() = " << str.length() << endl;
		throw(-1);
	}
	name_part = str.substr(0, pos);
	value_part = str.substr(pos+1,string::npos);
	//cout << "name_part  = |" << name_part  << "|" << endl;
	//cout << "value_part = |" << value_part << "|" << endl;

	// the first_part should consist of one string with no interior delimiters
	last_pos = name_part.find_first_not_of(delimiters, 0);
	pos = name_part.find_first_of(delimiters, last_pos);
	if ( (name_part.length() == 0) || (last_pos == string::npos) ) {
		cerr << "Error in CConfig::TokenizeString(): "
				<< "line in the configuration file with no name before the \"=\" sign."
				<< endl;
		throw(-1);
	}
	if (pos == string::npos) pos = name_part.length();
	option_name = name_part.substr(last_pos, pos - last_pos);
	last_pos = name_part.find_first_not_of(delimiters, pos);
	if (last_pos != string::npos) {
		cerr << "Error in TokenizeString(): "
				<< "two or more options before an \"=\" sign in the configuration file."
				<< endl;
		throw(-1);
	}
	StringToUpperCase(option_name);

	//cout << "option_name = |" << option_name << "|" << endl;
	//cout << "pos = " << pos << ": last_pos = " << last_pos << endl;

	// now fill the option value vector
	option_value.clear();
	last_pos = value_part.find_first_not_of(delimiters, 0);
	pos = value_part.find_first_of(delimiters, last_pos);
	while (string::npos != pos || string::npos != last_pos) {
		// add token to the vector<string>
		option_value.push_back(value_part.substr(last_pos, pos - last_pos));
		// skip delimiters
		last_pos = value_part.find_first_not_of(delimiters, pos);
		// find next "non-delimiter"
		pos = value_part.find_first_of(delimiters, last_pos);
	}
	if (option_value.size() == 0) {
		cerr << "Error inT okenizeString(): "
				<< "option " << option_name << " in configuration file with no value assigned."
				<< endl;
		throw(-1);
	}

#if 0
	cout << "option value(s) = ";
	for (unsigned int i = 0; i < option_value.size(); i++)
		cout << option_value[i] << " ";
	cout << endl;
#endif

	// look for ';' DV delimiters attached to values
	vector<string>::iterator it;
	it = option_value.begin();
	while (it != option_value.end()) {
		if (it->compare(";") == 0) {
			it++;
			continue;
		}

		pos = it->find(';');
		if (pos != string::npos) {
			string before_semi = it->substr(0, pos);
			string after_semi= it->substr(pos+1,string::npos);
			if (before_semi.empty()) {
				*it = ";";
				it++;
				option_value.insert(it, after_semi);
			} else {
				*it = before_semi;
				it++;
				vector<string> to_insert;
				to_insert.push_back(";");
				if (!after_semi.empty())
					to_insert.push_back(after_semi);
				option_value.insert(it, to_insert.begin(), to_insert.end());
			}
			it = option_value.begin(); // go back to beginning; not efficient
			continue;
		} else {
			it++;
		}
	}
#if 0
	cout << "option value(s) = ";
	for (unsigned int i = 0; i < option_value.size(); i++)
		cout << option_value[i] << " ";
	cout << endl;
#endif
	// remove any consecutive ";"
	it = option_value.begin();
	bool semi_at_prev = false;
	while (it != option_value.end()) {
		if (semi_at_prev) {
			if (it->compare(";") == 0) {
				option_value.erase(it);
				it = option_value.begin();
				semi_at_prev = false;
				continue;
			}
		}
		if (it->compare(";") == 0) {
			semi_at_prev = true;
		} else {
			semi_at_prev = false;
		}
		it++;
	}

#if 0
	cout << "option value(s) = ";
	for (unsigned int i = 0; i < option_value.size(); i++)
		cout << option_value[i] << " ";
	cout << endl;
#endif
	return true;
}

double CConfig::GetDisplX(string val_marker){
    unsigned short iMarker_Disp;
    for ( iMarker_Disp = 0; iMarker_Disp < nMarker_Displacement; iMarker_Disp++)
        if ( Marker_Displacement[iMarker_Disp] == val_marker) break;
    if(iMarker_Disp >= nMarker_Displacement){
		cout << "SFEM : CConfig::GetDisplX, no such a Marker named as" << val_marker << endl;
		cout << "Press any key to exit!!!" << endl;
		exit(1);
	}

    return Displ_X[iMarker_Disp];
}

double CConfig::GetDisplY(string val_marker){
    unsigned short iMarker_Disp;
    for ( iMarker_Disp = 0; iMarker_Disp < nMarker_Displacement; iMarker_Disp++)
        if ( Marker_Displacement[iMarker_Disp] == val_marker) break;

	if(iMarker_Disp >= nMarker_Displacement){
		cout << "SFEM : CConfig::GetDisplY, no such a Marker named as" << val_marker << endl;
		cout << "Press any key to exit!!!" << endl;
		exit(1);
	}
    return Displ_Y[iMarker_Disp];
}

double CConfig::GetDisplZ(string val_marker){
    unsigned short iMarker_Disp;
    for ( iMarker_Disp = 0; iMarker_Disp < nMarker_Displacement; iMarker_Disp++)
        if ( Marker_Displacement[iMarker_Disp] == val_marker) break;

	if(iMarker_Disp >= nMarker_Displacement){
		cout << "SFEM : CConfig::GetDisplZ, no such a Marker named as" << val_marker << endl;
		cout << "Press any key to exit!!!" << endl;
		exit(1);
	}
    return Displ_Z[iMarker_Disp];
}

unsigned short CConfig::GetDisplX_Indicator(string val_marker){
    unsigned short iMarker_Disp;
    for ( iMarker_Disp = 0; iMarker_Disp < nMarker_Displacement; iMarker_Disp++)
        if ( Marker_Displacement[iMarker_Disp] == val_marker) break;

	if(iMarker_Disp >= nMarker_Displacement){
		cout << "SFEM : CConfig::GetDisplX_Indicator, no such a Marker named as" << val_marker << endl;
		cout << "Press any key to exit!!!" << endl;
		exit(1);
	}
    return Displ_Z[iMarker_Disp];
    return Displ_X_Indicator[iMarker_Disp];
}

unsigned short CConfig::GetDisplY_Indicator(string val_marker){
    unsigned short iMarker_Disp;
    for ( iMarker_Disp = 0; iMarker_Disp < nMarker_Displacement; iMarker_Disp++)
        if ( Marker_Displacement[iMarker_Disp] == val_marker) break;

	if(iMarker_Disp >= nMarker_Displacement){
		cout << "SFEM : CConfig::GetDisplY_Indicator, no such a Marker named as" << val_marker << endl;
		cout << "Press any key to exit!!!" << endl;
		exit(1);
	}
    return Displ_Y_Indicator[iMarker_Disp];
}

unsigned short CConfig::GetDisplZ_Indicator(string val_marker){
    unsigned short iMarker_Disp;
    for ( iMarker_Disp = 0; iMarker_Disp < nMarker_Displacement; iMarker_Disp++)
        if ( Marker_Displacement[iMarker_Disp] == val_marker) break;

	if(iMarker_Disp >= nMarker_Displacement){
		cout << "SFEM : CConfig::GetDisplZ_Indicator, no such a Marker named as" << val_marker << endl;
		cout << "Press any key to exit!!!" << endl;
		exit(1);
	}
    return Displ_Z_Indicator[iMarker_Disp];
}

double CConfig::GetVelocX(string val_marker){
    unsigned short iMarker_Veloc;
    for ( iMarker_Veloc = 0; iMarker_Veloc < nMarker_Velocity; iMarker_Veloc++)
        if ( Marker_Velocity[iMarker_Veloc] == val_marker) break;

	if(iMarker_Veloc >= nMarker_Velocity){
		cout << "SFEM : CConfig::GetVelocX, no such a Marker named as" << val_marker << endl;
		cout << "Press any key to exit!!!" << endl;
		exit(1);
	}

    return Velocity_X[iMarker_Veloc];
}

double CConfig::GetVelocY(string val_marker){
    unsigned short iMarker_Veloc;
    for ( iMarker_Veloc = 0; iMarker_Veloc < nMarker_Velocity; iMarker_Veloc++)
        if ( Marker_Velocity[iMarker_Veloc] == val_marker) break;

	if(iMarker_Veloc >= nMarker_Velocity){
		cout << "SFEM : CConfig::GetVelocY, no such a Marker named as" << val_marker << endl;
		cout << "Press any key to exit!!!" << endl;
		exit(1);
	}

    return Velocity_Y[iMarker_Veloc];
}

double CConfig::GetVelocZ(string val_marker){
    unsigned short iMarker_Veloc;
    for ( iMarker_Veloc = 0; iMarker_Veloc < nMarker_Velocity; iMarker_Veloc++)
        if ( Marker_Velocity[iMarker_Veloc] == val_marker) break;

	if(iMarker_Veloc >= nMarker_Velocity){
		cout << "SFEM : CConfig::GetVelocZ, no such a Marker named as" << val_marker << endl;
		cout << "Press any key to exit!!!" << endl;
		exit(1);
	}

    return Velocity_Z[iMarker_Veloc];
}

unsigned short CConfig::GetVelocX_Indicator(string val_marker){
    unsigned short iMarker_Veloc;
    for ( iMarker_Veloc = 0; iMarker_Veloc < nMarker_Velocity; iMarker_Veloc++)
        if ( Marker_Velocity[iMarker_Veloc] == val_marker) break;

	if(iMarker_Veloc >= nMarker_Velocity){
		cout << "SFEM : CConfig::GetVelocX_Indicator, no such a Marker named as" << val_marker << endl;
		cout << "Press any key to exit!!!" << endl;
		exit(1);
	}

    return Velocity_X_Indicator[iMarker_Veloc];
}

unsigned short CConfig::GetVelocY_Indicator(string val_marker){
    unsigned short iMarker_Veloc;
    for ( iMarker_Veloc = 0; iMarker_Veloc < nMarker_Velocity; iMarker_Veloc++)
        if ( Marker_Velocity[iMarker_Veloc] == val_marker) break;

	if(iMarker_Veloc >= nMarker_Velocity){
		cout << "SFEM : CConfig::GetVelocY_Indicator, no such a Marker named as" << val_marker << endl;
		cout << "Press any key to exit!!!" << endl;
		exit(1);
	}
    return Velocity_Y_Indicator[iMarker_Veloc];
}

unsigned short CConfig::GetVelocZ_Indicator(string val_marker){
    unsigned short iMarker_Veloc;
    for ( iMarker_Veloc = 0; iMarker_Veloc < nMarker_Velocity; iMarker_Veloc++)
        if ( Marker_Velocity[iMarker_Veloc] == val_marker) break;

	if(iMarker_Veloc >= nMarker_Velocity){
		cout << "SFEM : CConfig::GetVelocZ_Indicator, no such a Marker named as" << val_marker << endl;
		cout << "Press any key to exit!!!" << endl;
		exit(1);
	}
    return Velocity_Z_Indicator[iMarker_Veloc];
}

double CConfig::GetPressure(string val_marker){
    unsigned short iMarker_Pressure;
    for ( iMarker_Pressure = 0; iMarker_Pressure < nMarker_Pressure; iMarker_Pressure++)
        if ( Marker_Pressure[iMarker_Pressure] == val_marker) break;

	if(iMarker_Pressure >= nMarker_Pressure){
		cout << "SFEM : CConfig::GetPressure, no such a Marker named as" << val_marker << endl;
		cout << "Press any key to exit!!!" << endl;
		exit(1);
	}
    return Pressure[iMarker_Pressure];
}

unsigned short CConfig::GetnMarker_Config(void){
	nMarker_Config = nMarker_Displacement + nMarker_Velocity + nMarker_Pressure + nMarker_FSI;
	return nMarker_Config;
}

string CConfig::GetMarkerTag_Displacement(unsigned short iMarker){
	if(iMarker < nMarker_Displacement){
		return Marker_Displacement[iMarker];
	}
	else{
		cout << "SFEM : Number of Markers exceeds the number of displacement markers!" << endl;
		cout << "SFEM : Press any key to exit!!!" << endl;
		cin.get();
		exit(1);
	}

}

string CConfig::GetMarkerTag_Velocity(unsigned short iMarker){
	if(iMarker < nMarker_Velocity){
		return Marker_Velocity[iMarker];
	}
	else{
		cout << "SFEM : Number of Markers exceeds the number of velocity markers!" << endl;
		cout << "SFEM : Press any key to exit!!!" << endl;
		cin.get();
		exit(1);
	}

}

string CConfig::GetMarkerTag_Pressure(unsigned short iMarker){
	if(iMarker < nMarker_Pressure){
		return Marker_Pressure[iMarker];
	}
	else{
		cout << "SFEM : Number of Markers exceeds the number of preesure markers!" << endl;
		cout << "SFEM : Press any key to exit!!!" << endl;
		cin.get();
		exit(1);
	}

}

string CConfig::GetMarkerTag_FSI(unsigned short iMarker){
	if(iMarker < nMarker_FSI){
		return Marker_FSI[iMarker];
	}
	else{
		cout << "SFEM : Number of Markers exceeds the number of preesure markers!" << endl;
		cout << "SFEM : Press any key to exit!!!" << endl;
		cin.get();
		exit(1);
	}

}
