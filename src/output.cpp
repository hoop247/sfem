////////////////////////////////////////////////////////////////////////////
//            SSSSSS  FFFFFF  EEEEEE  MMM   MMM        ++       ++        //
//           SS       FF      EE      MM M M MM       ++       ++         //
//           SSSSSSS  FFFF    EEEE    MM  M  MM   ++++++++ ++++++++       //
//                SS  FF      EE      MM     MM     ++       ++           //
//                SS  FF      EE      MM     MM    ++       ++            //
//           SSSSSS   FF      EEEEEE  MM     MM   ++       ++             //
////////////////////////////////////////////////////////////////////////////
#include "output.hpp"

COutput::COutput()
{
    //ctor
}


COutput::~COutput()
{
    //dtor
}


//+++++++++++++++++++++++++++++++++
// COutputFLuid
//+++++++++++++++++++++++++++++++++
COutputFluid::COutputFluid()
{
    //ctor

}

void COutputFluid::WriteSolutionFluid(CConfig *config, CGeometry *geometry, vector<double> &v,
                                    vector<double> &p, unsigned long n_t, double t)
{
    unsigned short kind_post = config->GetKind_PostProcessor();
    switch (kind_post)
    {
    case VTK:
        WriteSolutionVTK(config, geometry, v, p, n_t, t);
        break;

    default:
        WriteSolutionTecplot(config, geometry, v, p, n_t, t);
        break;
    }

}

void COutputFluid::WriteConvergeHistoryFluid(CConfig *config,
        double H_rv, double H_rp, unsigned long n){
    string fname = config->GetHistoryFileName();

    //Output on screen for converge infomation
    cout << "SFEM : Step " << n;
    cout << scientific << ", residual_v = " << H_rv;
    cout << scientific << ", residual_p = " << H_rp << endl;

    ConHisotry_n.push_back(n);
    ConHistory_rv.push_back(H_rv);
    ConHistory_rp.push_back(H_rp);

    ofstream fout;
    fout.open(fname, ios::out);

    if (!fout){
        cout << "SFEM: unable to open the solution file: " << fname << endl;
        cout << "Press any key to exit." << endl;
        cin.get();
        exit(1);
    }

    fout << n << " " << scientific << H_rv << " " << H_rp << "\n";


}

///TODO (JC): only for full  triangular and tetrahedral element right now!
///Need to add more feature for other elements.
void COutputFluid::WriteSolutionTecplot(CConfig *config, CGeometry *geometry, vector<double> &v,
                                         vector<double> &p, unsigned long n_t, double t_n)
{
    string fname = config->GetOutput_FileName();
    unsigned short ndim = geometry->GetnDim();
    vector<vector<double> > node_coord = geometry->GetNode_coord();
    unsigned long nnode = geometry->GetnNode();
    unsigned long nele = geometry->GetnElem();
    CElem* iele;
    ofstream fout;

    /* ------------   Open solution file  --------------*/
    stringstream ss;
    ss << "./" << fname << "_" << n_t << ".dat";
    fname = ss.str();
    fout.open(fname, ios::out);

    if (!fout){
        cout << "SFEM: unable to open the solution file: " << fname << endl;
        cout << "Press any key to exit." << endl;
        cin.get();
        exit(1);
    }

    /* ------------   write the output file  --------------*/
    fout << "TITLE = \"S-FEM CFD SOLUTION\"\n";
    if( ndim == 2){
        fout << "VARIABLES = \"X\", \"Y\", \"VX\", \"VY\", \"P\" \n";
        fout << "ZONE STRANDID=" << n_t <<", SOLUTIONTIME=" << t_n << ", NODES=" << nnode <<", ";
        fout << " ELEMENTS=" << nele << ", DATAPACKING=point, ZONETYPE=FETRIANGLE\n";

        for(unsigned long i=0; i<nnode; i++){
            fout << scientific <<  node_coord[i][0] << " " << node_coord[i][1] << " " ;
            fout << scientific << v[2*i] << " " << v[2*i+1] <<  " ";
            fout << scientific << p[i]<< "\n";
        }

        for(unsigned long i=0; i<nele; i++){
            iele = geometry->GetElem(i);
            fout << scientific << iele->GetNode(0)+1 << " " << iele->GetNode(1)+1 << " " << iele->GetNode(2)+1  << "\n";
        }

        fout.close();

    }
    else{
        fout << "VARIABLES = \"X\", \"Y\", \"Z\",\"VX\", \"VY\", \"VZ\", \"P\" \n";
        fout << "ZONE STRANDID=" << n_t <<", SOLUTIONTIME=" << t_n << ", NODES=" << nnode <<", ";
        fout << " ELEMENTS=" << nele << ", DATAPACKING=point, ZONETYPE=FETETRAHEDRON\n";

        for(unsigned long i=0; i<nnode; i++){
            fout << node_coord[i][0] << " " << node_coord[i][1] << " " << node_coord[i][2] << " ";
            fout << v[3*i] << " " << v[3*i+1] <<  " " << v[3*i+2] <<  " ";
            fout << p[i]<< "\n";
        }

        for(unsigned long i=0; i<nele; i++){
            iele = geometry->GetElem(i);
            fout << iele->GetNode(0)+1 << " " << iele->GetNode(1)+1 << " ";
            fout << iele->GetNode(2)+1 << " " << iele->GetNode(3)+1 << "\n";
        }

        fout.close();

    }

    cout << "SFEM : Fluid solution " << fname << " at " << t_n << " is saved." << endl;


}

COutputFluid::~COutputFluid()
{
    //dctor

}


//+++++++++++++++++++++++++++++++++
// COutputSolid
//+++++++++++++++++++++++++++++++++
COutputSolid::COutputSolid(){
    //ctor
}

void COutputSolid::WriteConvergeHistorySolid(CConfig *config, double r_u, double r_v, unsigned long n){
    string fname = config->GetHistoryFileName();

    //Output on screen for converge infomation
    cout << "SFEM : Step " << n;
    cout << scientific << ", residual_u = " << r_u;
    cout << scientific << ", residual_v = " << r_v << endl;

    ConHisotry_n.push_back(n);
    ConHistory_ru.push_back(r_u);
    ConHistory_rv.push_back(r_v);

    ofstream fout;
    fout.open(fname, ios::out);

    if (!fout){
        cout << "SFEM: unable to open the solution file: " << fname << endl;
        cout << "Press any key to exit." << endl;
        cin.get();
        exit(1);
    }

    fout << n << " " << scientific << r_u << " " << r_v << "\n";
}

void COutputSolid::WriteSolutionSolid(CConfig *config, CGeometry *geometry, vector<double> &u, vector<double> &v, vector<vector<double> > &S,
            unsigned long n_t, double t){
    unsigned short kind_post = config->GetKind_PostProcessor();
    switch (kind_post)
    {
    case VTK:
        WriteSolutionVTK(config, geometry, u, v, S, n_t, t);
        break;

    default:
        WriteSolutionTecplot(config, geometry, u, v, S, n_t, t);
        break;
    }

}

void COutputSolid::WriteSolutionTecplot(CConfig *config, CGeometry *geometry, vector<double> &u, vector<double> &v,
                                   vector<vector<double> > &S, unsigned long n_t, double t){
    string fname = config->GetOutput_FileName();
    unsigned short ndim = geometry->GetnDim();
    vector<vector<double> > node_coord = geometry->GetNode_coord();
    unsigned long nnode = geometry->GetnNode();
    unsigned long nele = geometry->GetnElem();
    CElem* iele;
    ofstream fout;

    /* ------------   Open solution file  --------------*/
    stringstream ss;
    ss << "./" << fname << "_" << n_t << ".dat";
    fname = ss.str();
    fout.open(fname, ios::out);

    if (!fout){
        cout << "SFEM: unable to open the solution file: " << fname << endl;
        cout << "Press any key to exit." << endl;
        cin.get();
        exit(1);
    }

    /* ------------   write the output file  --------------*/
    fout << "TITLE = \"S-FEM CSD SOLUTION\"\n";
    if( ndim == 2){
        fout << "VARIABLES = \"X\", \"Y\", \"UX\", \"UY\", \"VX\", \"VY\", \"S11\", \"S22\", \"S12\" \n";
        fout << "ZONE STRANDID=" << n_t <<", SOLUTIONTIME=" << t << ", NODES=" << nnode <<", ";
        fout << " ELEMENTS=" << nele << ", DATAPACKING=point, ZONETYPE=FETRIANGLE\n";

        for(unsigned long i=0; i<nnode; i++){
            fout << scientific <<  node_coord[i][0] << " " << node_coord[i][1] << " " ;
            fout << scientific << u[2*i] << " " << u[2*i+1] <<  " ";
            fout << scientific << v[2*i] << " " << v[2*i+1] <<  " ";
            fout << scientific << S[i][0] << " " << S[i][1] << " " << S[i][2]  << "\n";
        }

        for(unsigned long i=0; i<nele; i++){
            iele = geometry->GetElem(i);
            fout << scientific << iele->GetNode(0)+1 << " " << iele->GetNode(1)+1 << " " << iele->GetNode(2)+1  << "\n";
        }

        fout.close();

    }
    else{
        fout << "VARIABLES = \"X\", \"Y\", \"Z\",\"UX\", \"UY\", \"UZ\", \"VX\", \"VY\", \"VZ\", ";
        fout << "\"S11\", \"S22\", \"S33\", \"S12\", \"S23\", \"S31\"  \n";
        fout << "ZONE STRANDID=" << n_t <<", SOLUTIONTIME=" << t << ", NODES=" << nnode <<", ";
        fout << " ELEMENTS=" << nele << ", DATAPACKING=point, ZONETYPE=FETETRAHEDRON\n";

        for(unsigned long i=0; i<nnode; i++){
            fout << node_coord[i][0] << " " << node_coord[i][1] << " " << node_coord[i][2] << " ";
            fout << u[3*i] << " " << u[3*i+1] <<  " " << u[3*i+2] <<  " ";
            fout << v[3*i] << " " << v[3*i+1] <<  " " << v[3*i+2] <<  " ";
            fout << S[i][0] << " " << S[i][1] <<  " " << S[i][2] <<  " ";
            fout << S[i][3] << " " << S[i][4] <<  " " << S[i][5] <<  "\n";
        }

        for(unsigned long i=0; i<nele; i++){
            iele = geometry->GetElem(i);
            fout << iele->GetNode(0)+1 << " " << iele->GetNode(1)+1 << " ";
            fout << iele->GetNode(2)+1 << " " << iele->GetNode(3)+1 << "\n";
        }

        fout.close();

    }

    cout << "SFEM : Fluid solution " << fname << " at " << t << " is saved." << endl;
}


COutputSolid::~COutputSolid(){
    //dctor
}
