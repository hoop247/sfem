////////////////////////////////////////////////////////////////////////////
//            SSSSSS  FFFFFF  EEEEEE  MMM   MMM        ++       ++        //
//           SS       FF      EE      MM M M MM       ++       ++         //
//           SSSSSSS  FFFF    EEEE    MM  M  MM   ++++++++ ++++++++       //
//                SS  FF      EE      MM     MM     ++       ++           //
//                SS  FF      EE      MM     MM    ++       ++            //
//           SSSSSS   FF      EEEEEE  MM     MM   ++       ++             //
////////////////////////////////////////////////////////////////////////////
#include "elem.hpp"

CElem::CElem(){
    //ctor
    VTK_Type = 0; //non-initialed
    nDim = 3;
    nFaces = 0;
    nNodes = 0;
    nNodesFace = 0;
    Vol = 0.0;

}

// overloaded ctor
CElem::CElem(unsigned long val_GlobalID, unsigned short val_nDim){
    nDim = val_nDim;
    Vol = 0.0;
    GlobalID = val_GlobalID;

}

CElem& CElem::operator=(CElem &ele){
    this->VTK_Type = ele.GetType();
    this->CG_coord = ele.GetCG_coord();
    this->Faces = ele.GetFacesAll();
    this->ElemFaces_CG_coord = ele.GetFaceCG_coordAll();
    this->nDim = ele.GetnDim();
    this->nFaces = ele.GetnFaces();
    this->nNodes = ele.GetnNodes();
    this->nNodesFace = ele.GetnNodesFace();
    this->Vol = ele.GetVol();

    return ele;
}

void CElem::Display(){
    cout << "Element No. " << GlobalID << " 's vars:"<< endl;
    cout << "Type: " << VTK_Type << endl;

    cout << "nNodes: " << nNodes << endl;
    cout << "Nodes: ";
    for(int i=0; i<Nodes.size(); i++)
        cout << Nodes[i] << ", ";
    cout << endl;

    cout << "CG: ";
    for(int i=0; i<CG_coord.size(); i++)
        cout << CG_coord[i] << ", ";
    cout << endl;

    cout << "nDim: " << nDim << endl;
    cout << "nFaces: " << nFaces << endl;
    cout << "nNodesFace: " << nNodesFace << endl;
    cout << "Vol: " << Vol << endl;
    cout << "CElem.Display() finished!!" << endl;
    cout << endl;
}

CElem::~CElem(){
    //dtor
    Nodes.clear();
    CG_coord.clear();
    ElemFaces_CG_coord.clear();
    Faces.clear();
}

//+++++++++++++++++++++++++
// CElemVERTEX1
//+++++++++++++++++++++++++
CElemVERTEX1::CElemVERTEX1(){
    //ctor
    VTK_Type = 1;
    Vol = 0.0;
}

CElemVERTEX1::CElemVERTEX1(unsigned long p0, unsigned long val_globalID, unsigned short val_nDim){
    VTK_Type = 1;
    Vol = 0.0;
    nDim = val_nDim;
    GlobalID = val_globalID;
    SetElem(p0);
}

void CElemVERTEX1::SetElem(unsigned long p0){
    VTK_Type = 1;
    Nodes.push_back(p0);
    nFaces = 1;
    nNodes = 1;
    vector<unsigned long> face0 = {p0};//{1}
    Faces.push_back(face0);
    nNodesFace = 1;
}

void CElemVERTEX1::SetCG(vector<double> &p0_coord){
    double x;
    for(unsigned short iDim = 0; iDim < this->nDim; iDim++ )
    {
        x = p0_coord[iDim];
        this->CG_coord.push_back(x);
    }
}


CElemVERTEX1::~CElemVERTEX1(){
    //dctor
}


//+++++++++++++++++++++++++
// CElemLINE2
//+++++++++++++++++++++++++
CElemLINE2::CElemLINE2(){
    //ctor
    VTK_Type = 3;
    Vol = 0.0;
}

CElemLINE2::CElemLINE2(unsigned long p0, unsigned long p1, unsigned long val_globalID, unsigned short val_nDim){
    VTK_Type = 3;
    Vol = 0.0;
    nDim = val_nDim;
    GlobalID = val_globalID;
    SetElem(p0,p1);
}

void CElemLINE2::SetElem(unsigned long p0, unsigned long p1){
    VTK_Type = 3;
    Nodes.push_back(p0);
    Nodes.push_back(p1);
    nFaces = 2;
    nNodes = 2;
    vector<unsigned long> face0 = {p0};//{1}
    vector<unsigned long> face1 = {p1};//{2}
    Faces.push_back(face0);
    Faces.push_back(face1);
    nNodesFace = 1;
}

void CElemLINE2::SetCG(vector<double> &p0_coord, vector<double> &p1_coord){
    double x;
    for(unsigned short iDim = 0; iDim < this->nDim; iDim++ )
    {
        x = (p0_coord[iDim] + p1_coord[iDim] )/2.0;
        this->CG_coord.push_back(x);
    }
}

void CElemLINE2::SetVol(vector<double> &p0_coord, vector<double> &p1_coord){
    Vol = GetLengthL2(p0_coord, p1_coord);
}

double CElemLINE2::GetLengthL2(vector<double> &p0_coord, vector<double> &p1_coord){
    double L;
    double dx,dy,dz;

    dx = p1_coord[0] - p0_coord[0];
    dy = p1_coord[1] - p0_coord[1];
    if(this->nDim == 2){
        L = sqrt(dx*dx+dy*dy);
    }
    else if(this->nDim == 3){
        dz = p1_coord[2] - p0_coord[2];
        L = sqrt(dx*dx+dy*dy+dz*dz);
    }

    return L;
}

vector<double> CElemLINE2::GetShapeFuncAt(vector<double>& p_coord, vector<vector<double> >& node_coord){
    unsigned long I0, I1;
    double length;
    vector<double> p0_coord;
    vector<double> p1_coord;
    vector<double> gp_coord;

    vector<double> SF(2);

    if(Vol <= 0.0){
        cout << "SFEM : CElemLINE2::GetShapeFuncAt(), the volume of element " << GlobalID << " is not initialized !!!" << endl;
        cout << "SFEM : Press any key to exit!!!" << endl;
        cin.get();
        exit(1);
    }

    I0 = Nodes[0];
    I1 = Nodes[1];
    p0_coord = node_coord[I0];
    p1_coord = node_coord[I1];

    length = GetLengthL2(p_coord, p1_coord);//gp-1
    SF[0] = length/this->Vol;
    length = GetLengthL2(p_coord, p0_coord); //gp-0
    SF[1] = length/this->Vol;

    return SF;
}

vector<double> CElemLINE2::GetShapeFuncDXAt(vector<double>& p_coord, vector<vector<double> >& node_coord){
    vector<double> sfdx(2);
    sfdx[0] = -1.0/this->Vol;
    sfdx[1] = 1/this->Vol;
    return sfdx;
}

vector<double> CElemLINE2::GetShapeFuncDYAt(vector<double>& p_coord, vector<vector<double> >& node_coord){
    vector<double> sfdy(2);
    sfdy[0] = -1.0/this->Vol;
    sfdy[1] = 1/this->Vol;
    return sfdy;
}

vector<double> CElemLINE2::GetShapeFuncDZAt(vector<double>& p_coord, vector<vector<double> >& node_coord){
    vector<double> sfdz(2);
    sfdz[0] = -1.0/this->Vol;
    sfdz[1] = 1/this->Vol;
    return sfdz;
}


CElemLINE2::~CElemLINE2(){
    //dctor
}


//+++++++++++++++++++++++++
// CElemTRI3
//+++++++++++++++++++++++++
CElemTRI3::CElemTRI3()
{
    //ctor
    VTK_Type = 5;
    Vol = 0.0;

}

CElemTRI3::CElemTRI3(unsigned long p0, unsigned long p1, unsigned long p2,
                     unsigned long val_globalID, unsigned short val_nDim)
{
    VTK_Type = 5;
    Vol = 0.0;
    nDim = val_nDim;
    GlobalID = val_globalID;
    SetElem(p0, p1, p2);
}

void CElemTRI3::SetElem(unsigned long p0, unsigned long p1, unsigned long p2)
{
    VTK_Type = 5;
    Nodes.push_back(p0);
    Nodes.push_back(p1);
    Nodes.push_back(p2);
    nFaces = 3;
    nNodes = 3;
    vector<unsigned long> face0 = {p0,p1};//{1,2} - counter-clockwise element
    vector<unsigned long> face1 = {p1,p2};//{0,2}
    vector<unsigned long> face2 = {p2,p0};//{1,0}
    Faces.push_back(face0);
    Faces.push_back(face1);
    Faces.push_back(face2);
    nNodesFace = 2;
}

void CElemTRI3::SetCG(vector<double> &p0_coord, vector<double> &p1_coord, vector<double> &p2_coord)
{
    double x;
    for(unsigned short iDim = 0; iDim < this->nDim; iDim++ )
    {
        x = (p0_coord[iDim] + p1_coord[iDim] + p2_coord[iDim])/3.0;
        this->CG_coord.push_back(x);
    }
}

void CElemTRI3::SetVol(vector<double> &p0_coord, vector<double> &p1_coord, vector<double> &p2_coord)
{
    this->Vol = GetAreaT3(p0_coord, p1_coord, p2_coord);
}

double CElemTRI3::GetAreaT3(vector<double> &p0_coord, vector<double> &p1_coord, vector<double> &p2_coord){
    double area, I, I0, I1, I2;
    int ndim;
    ndim = p0_coord.size();

    double x0, y0, z0;
    double x1, y1, z1;
    double x2, y2, z2;
    x0 = p0_coord[0];
    y0 = p0_coord[1];
    x1 = p1_coord[0];
    y1 = p1_coord[1];
    x2 = p2_coord[0];
    y2 = p2_coord[1];

    if(ndim == 2)
    {
        area = 0.5*fabs((x0-x2)*(y1-y0) - (x0-x1)*(y2-y0));
    }
    else if(ndim == 3)
    {
        z0 = p0_coord[2];
        z1 = p1_coord[2];
        z2 = p2_coord[2];
        I0 = sqrt((x0-x1)*(x0-x1) + (y0-y1)*(y0-y1) + (z0-z1)*(z0-z1));
        I1 = sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) + (z2-z1)*(z2-z1));
        I2 = sqrt((x0-x2)*(x0-x2) + (y0-y2)*(y0-y2) + (z0-z2)*(z0-z2));
        I = (I0+I1+I2)*0.5;
        area = sqrt(I*(I-I0)*(I-I1)*(I-I2));
    }

    return area;
}

vector<double> CElemTRI3::GetShapeFuncAt(vector<double>& p_coord, vector<vector<double> >& node_coord){
    unsigned long I0, I1, I2;
    double area;
    vector<double> p0_coord, p1_coord, p2_coord;

    vector<double> SF(3);

    if(Vol <= 0.0){
        cout << "SFEM : CElemTRI3::GetShapeFuncAt(), the volume of element " << GlobalID << " is not initialized !!!" << endl;
        cout << "SFEM : Press any key to exit!!!" << endl;
        cin.get();
        exit(1);
    }

    //get the points coords of element.
    I0 = Nodes[0];
    I1 = Nodes[1];
    I2 = Nodes[2];
    p0_coord = node_coord[I0];
    p1_coord = node_coord[I1];
    p2_coord = node_coord[I2];

    area = GetAreaT3(p_coord, p1_coord, p2_coord); //gp-1-2
    SF[0] = area/this->Vol;
    area = GetAreaT3(p_coord, p0_coord, p2_coord); //gp-0-2
    SF[1] = area/this->Vol;
    area = GetAreaT3(p_coord, p1_coord, p0_coord); //gp-1-0
    SF[2] = area/this->Vol;

    return SF;
}

vector<double> CElemTRI3::GetShapeFuncDXAt(vector<double>& p_coord, vector<vector<double> >& node_coord){
    unsigned long I0, I1, I2;
    vector<double> p0_coord;
    vector<double> p1_coord;
    vector<double> p2_coord;

    vector<double> SFDX(3);

    if(Vol <= 0.0){
        cout << "SFEM : CElemTRI3::GetShapeFuncDXAt(), the volume of element " << GlobalID << " is not initialized !!!" << endl;
        cout << "SFEM : Press any key to exit!!!" << endl;
        cin.get();
        exit(1);
    }

    //TODO: T3 for 3D is not working here.
    if(nDim == 3){
        cout << "SFEM : CElemTRI3::GetShapeFuncDXAt(), element " << GlobalID << " is 3D, which is not support yet!!!" << endl;
        cout << "SFEM : Press any key to exit!!!" << endl;
        cin.get();
        exit(1);
    }

    //get the points coords of element.
    I0 = Nodes[0];
    I1 = Nodes[1];
    I2 = Nodes[2];
    p0_coord = node_coord[I0];
    p1_coord = node_coord[I1];
    p2_coord = node_coord[I2];

    SFDX[0] = (p1_coord[1] - p2_coord[1])/this->Vol*0.5;
    SFDX[1] = (p2_coord[1] - p0_coord[1])/this->Vol*0.5;
    SFDX[2] = (p0_coord[1] - p1_coord[1])/this->Vol*0.5;

    return SFDX;
}

vector<double> CElemTRI3::GetShapeFuncDYAt(vector<double>& p_coord, vector<vector<double> >& node_coord){
    unsigned long I0, I1, I2;
    vector<double> p0_coord;
    vector<double> p1_coord;
    vector<double> p2_coord;

    vector<double> SFDY(3);

    if(Vol <= 0.0){
        cout << "SFEM : CElemTRI3::GetShapeFuncDYAt(), the volume of element " << GlobalID << " is not initialized !!!" << endl;
        cout << "SFEM : Press any key to exit!!!" << endl;
        cin.get();
        exit(1);
    }

    //TODO: T3 for 3D is not working here.
    if(nDim == 3){
        cout << "SFEM : CElemTRI3::GetShapeFuncDYAt(), element " << GlobalID << " is 3D, which is not support yet!!!" << endl;
        cout << "SFEM : Press any key to exit!!!" << endl;
        cin.get();
        exit(1);
    }

    //get the points coords of element.
    I0 = Nodes[0];
    I1 = Nodes[1];
    I2 = Nodes[2];
    p0_coord = node_coord[I0];
    p1_coord = node_coord[I1];
    p2_coord = node_coord[I2];

    SFDY[0] = -(p1_coord[0] - p2_coord[0])/this->Vol*0.5;
    SFDY[1] = -(p2_coord[0] - p0_coord[0])/this->Vol*0.5;
    SFDY[2] = -(p0_coord[0] - p1_coord[0])/this->Vol*0.5;

    return SFDY;
}

vector<double> CElemTRI3::GetShapeFuncDZAt(vector<double>& p_coord, vector<vector<double> >& node_coord){
    //TODO: T3 for 3D is not working here.
    cout << "SFEM : CElemTRI3::GetShapeFuncDZAt(), element " << GlobalID << " is 3D, which is not support yet!!!" << endl;
        cout << "SFEM : Press any key to exit!!!" << endl;
        cin.get();
        exit(1);
    if(nDim == 3){
        cout << "SFEM : CElemTRI3::GetShapeFuncDZAt(), element " << GlobalID << " is 3D, which is not support yet!!!" << endl;
        cout << "SFEM : Press any key to exit!!!" << endl;
        cin.get();
        exit(1);
    }
}

CElemTRI3::~CElemTRI3()
{
    //dctor
}

//+++++++++++++++++++++++++
// CElemTET4
//+++++++++++++++++++++++++
CElemTET4::CElemTET4(){
    VTK_Type = 10;
    Vol = 0.0;
    nDim = 3;
}

CElemTET4::CElemTET4(unsigned long p0, unsigned long p1, unsigned long p2, unsigned long p3, unsigned long val_globalID, unsigned short val_nDim){
    VTK_Type = 10;
    Vol = 0.0;
    nDim = val_nDim;
    GlobalID = val_globalID;
    SetElem(p0, p1, p2, p3);
}

void CElemTET4::SetElem(unsigned long p0, unsigned long p1, unsigned long p2, unsigned long p3){
    VTK_Type = 10;
    Nodes.push_back(p0);
    Nodes.push_back(p1);
    Nodes.push_back(p2);
    Nodes.push_back(p3);
    nFaces = 4;
    nNodes = 4;
    vector<unsigned long> face0 = {p1,p2,p3};//{1,2,3}
    vector<unsigned long> face1 = {p0,p2,p3};//{0,2,3}
    vector<unsigned long> face2 = {p0,p1,p3};//{0,1,3}
    vector<unsigned long> face3 = {p0,p2,p1};//{0,1,2}
    Faces.push_back(face0);
    Faces.push_back(face1);
    Faces.push_back(face2);
    Faces.push_back(face3);
    nNodesFace = 3;
}

void CElemTET4::SetVol(vector<double>& p0_coord, vector<double>& p1_coord, vector<double>& p2_coord, vector<double>& p3_coord){
    Vol = GetVolT4(p0_coord, p1_coord, p2_coord, p3_coord);
}

double CElemTET4::GetVolT4(vector<double> &p0_coord, vector<double> &p1_coord, vector<double> &p2_coord,
                vector<double> &p3_coord){
    double m[3][3];
    double detm;
    m[0][0] = p0_coord[0] - p1_coord[0];
    m[0][1] = p0_coord[1] - p1_coord[1]; //y[0] - y[1];
    m[0][2] = p0_coord[2] - p1_coord[2]; //z[0] - z[1];

    m[1][0] = p0_coord[0] - p2_coord[0]; //x[0] - x[2];
    m[1][1] = p0_coord[1] - p2_coord[1]; //y[0] - y[2];
    m[1][2] = p0_coord[2] - p2_coord[2]; //z[0] - z[2];

    m[2][0] = p0_coord[0] - p3_coord[0]; //x[0] - x[3];
    m[2][1] = p0_coord[1] - p3_coord[1]; //y[0] - y[3];
    m[2][2] = p0_coord[2] - p3_coord[2]; //z[0] - z[3];

    //bug fixed
    detm = fabs(m[0][0]*m[1][1]*m[2][2]+m[0][1]*m[1][2]*m[2][0]+m[0][2]*m[1][0]*m[2][1]
            -m[2][0]*m[1][1]*m[0][2]-m[1][0]*m[0][1]*m[2][2]-m[0][0]*m[2][1]*m[1][2]);

    return detm*0.1666666667; //detm/6
}

void CElemTET4::SetCG(vector<double>& p0_coord, vector<double>& p1_coord, vector<double>& p2_coord, vector<double>& p3_coord){
    double x;
    for(unsigned short iDim = 0; iDim < this->nDim; iDim++ )
    {
        x = (p0_coord[iDim] + p1_coord[iDim] + p2_coord[iDim] + p3_coord[iDim])/4.0;
        this->CG_coord.push_back(x);
    }
}

vector<double> CElemTET4::GetShapeFuncAt(vector<double> &p_coord, vector<vector<double> > &node_coord){
    unsigned long I0, I1, I2, I3;
    double vol;
    vector<double> p0_coord;
    vector<double> p1_coord;
    vector<double> p2_coord;
    vector<double> p3_coord;

    vector<double> SF(4);

    if(Vol <= 0.0){
        cout << "SFEM : CElemTET4::GetShapeFuncAt(), the volume of element " << GlobalID << " is not initialized !!!" << endl;
        cout << "SFEM : Press any key to exit!!!" << endl;
        cin.get();
        exit(1);
    }

    I0 = Nodes[0];
    I1 = Nodes[1];
    I2 = Nodes[2];
    I3 = Nodes[3];

    p0_coord = node_coord[I0];
    p1_coord = node_coord[I1];
    p2_coord = node_coord[I2];
    p3_coord = node_coord[I3];

    vol = GetVolT4(p_coord, p1_coord, p2_coord, p3_coord); //gp-1-2-3
    SF[0] = vol/this->Vol;
    vol = GetVolT4(p_coord, p0_coord, p2_coord, p3_coord); //gp-0-2-3
    SF[1] = vol/this->Vol;
    vol = GetVolT4(p_coord, p0_coord, p1_coord, p3_coord); //gp-0-1-3
    SF[2] = vol/this->Vol;

    SF[3] = 1.0 - SF[0] - SF[1] - SF[2];

    return SF;

}

vector<double> CElemTET4::GetShapeFuncDXAt(vector<double>& p_coord, vector<vector<double> >& node_coord){
    //               [1 y1 z1]
    //dSDdx[0] = -det[1 y2 z2] / 6V.
    //               [1 y3 z3]
    unsigned long I0, I1, I2, I3;
    vector<double> p0_coord, p1_coord, p2_coord, p3_coord;
    vector<double> SFDX(4);
    double m[3][3];
    double detm;
    double c;

    I0 = Nodes[0];
    I1 = Nodes[1];
    I2 = Nodes[2];
    I3 = Nodes[3];

    p0_coord = node_coord[I0];
    p1_coord = node_coord[I1];
    p2_coord = node_coord[I2];
    p3_coord = node_coord[I3];

    for (int i=0; i<4; i++){
        if( i==0 ){
            c = -1.0;

            m[0][0] = 1.0;
            m[0][1] = p1_coord[1];
            m[0][2] = p1_coord[2];
            m[1][0] = 1.0;
            m[1][1] = p2_coord[1];
            m[1][2] = p2_coord[2];
            m[2][0] = 1.0;
            m[2][1] = p3_coord[1];
            m[2][2] = p3_coord[2];
        }
        else if( i==1 ){
            c = 1.0;

            m[0][0] = 1.0;
            m[0][1] = p0_coord[1];
            m[0][2] = p0_coord[2];
            m[1][0] = 1.0;
            m[1][1] = p2_coord[1];
            m[1][2] = p2_coord[2];
            m[2][0] = 1.0;
            m[2][1] = p3_coord[1];
            m[2][2] = p3_coord[2];
        }
        else if( i==2 ){
            c = -1.0;

            m[0][0] = 1.0;
            m[0][1] = p0_coord[1];
            m[0][2] = p0_coord[2];
            m[1][0] = 1.0;
            m[1][1] = p1_coord[1];
            m[1][2] = p1_coord[2];
            m[2][0] = 1.0;
            m[2][1] = p3_coord[1];
            m[2][2] = p3_coord[2];
        }
        else if( i==3 ){
            c =1.0;

            m[0][0] = 1.0;
            m[0][1] = p0_coord[1];
            m[0][2] = p0_coord[2];
            m[1][0] = 1.0;
            m[1][1] = p1_coord[1];
            m[1][2] = p1_coord[2];
            m[2][0] = 1.0;
            m[2][1] = p2_coord[1];
            m[2][2] = p2_coord[2];
        }
        detm = m[0][0]*m[1][1]*m[2][2]+m[0][1]*m[1][2]*m[2][0]+m[0][2]*m[1][0]*m[2][1]
            -m[2][0]*m[1][1]*m[0][2]-m[1][0]*m[0][1]*m[2][2]-m[0][0]*m[2][1]*m[1][2];
        SFDX[i] = c*detm/Vol/6.0;
    }

    return SFDX;
}

vector<double> CElemTET4::GetShapeFuncDYAt(vector<double>& p_coord, vector<vector<double> >& node_coord){
    //               [x1 1 z1]
    //dSDdy[0] = -det[x2 1 z2] / 6V.
    //               [x3 1 z3]
    unsigned long I0, I1, I2, I3;
    vector<double> p0_coord, p1_coord, p2_coord, p3_coord;
    vector<double> SFDY(4);
    double m[3][3];
    double detm;
    double c;

    I0 = Nodes[0];
    I1 = Nodes[1];
    I2 = Nodes[2];
    I3 = Nodes[3];

    p0_coord = node_coord[I0];
    p1_coord = node_coord[I1];
    p2_coord = node_coord[I2];
    p3_coord = node_coord[I3];

    for (int i=0; i<4; i++){
        if( i==0 ){
            c = -1.0;

            m[0][0] = p1_coord[0];
            m[0][1] = 1.0;
            m[0][2] = p1_coord[2];
            m[1][0] = p2_coord[0];
            m[1][1] = 1.0;
            m[1][2] = p2_coord[2];
            m[2][0] = p3_coord[0];
            m[2][1] = 1.0;
            m[2][2] = p3_coord[2];
        }
        else if( i==1 ){
            c = 1.0;

            m[0][0] = p0_coord[0];
            m[0][1] = 1.0;
            m[0][2] = p0_coord[2];
            m[1][0] = p2_coord[0];
            m[1][1] = 1.0;
            m[1][2] = p2_coord[2];
            m[2][0] = p3_coord[0];
            m[2][1] = 1.0;
            m[2][2] = p3_coord[2];
        }
        else if( i==2 ){
            c = -1.0;

            m[0][0] = p0_coord[0];
            m[0][1] = 1.0;
            m[0][2] = p0_coord[2];
            m[1][0] = p1_coord[0];
            m[1][1] = 1.0;
            m[1][2] = p1_coord[2];
            m[2][0] = p3_coord[0];
            m[2][1] = 1.0;
            m[2][2] = p3_coord[2];
        }
        else if( i==3 ){
            c = 1.0;

            m[0][0] = p0_coord[0];
            m[0][1] = 1.0;
            m[0][2] = p0_coord[2];
            m[1][0] = p1_coord[0];
            m[1][1] = 1.0;
            m[1][2] = p1_coord[2];
            m[2][0] = p2_coord[0];
            m[2][1] = 1.0;
            m[2][2] = p2_coord[2];
        }
        detm = m[0][0]*m[1][1]*m[2][2]+m[0][1]*m[1][2]*m[2][0]+m[0][2]*m[1][0]*m[2][1]
            -m[2][0]*m[1][1]*m[0][2]-m[1][0]*m[0][1]*m[2][2]-m[0][0]*m[2][1]*m[1][2];
        SFDY[i] = c*detm/Vol/6.0;
    }

    return SFDY;


}

vector<double> CElemTET4::GetShapeFuncDZAt(vector<double>& p_coord, vector<vector<double> >& node_coord){
    //               [x1 y1 1]
    //dSDdz[0] = -det[x2 y2 1] / 6V.
    //               [x3 y3 1]
    unsigned long I0, I1, I2, I3;
    vector<double> p0_coord, p1_coord, p2_coord, p3_coord;
    vector<double> SFDZ(4);
    double m[3][3];
    double detm;
    double c;

    I0 = Nodes[0];
    I1 = Nodes[1];
    I2 = Nodes[2];
    I3 = Nodes[3];

    p0_coord = node_coord[I0];
    p1_coord = node_coord[I1];
    p2_coord = node_coord[I2];
    p3_coord = node_coord[I3];

    for (int i=0; i<4; i++){
        if( i==0 ){
            c = -1.0;

            m[0][0] = p1_coord[0];
            m[0][1] = p1_coord[1];
            m[0][2] = 1.0;
            m[1][0] = p2_coord[0];
            m[1][1] = p2_coord[1];
            m[1][2] = 1.0;
            m[2][0] = p3_coord[0];
            m[2][1] = p3_coord[1];
            m[2][2] = 1.0;
        }
        else if( i==1 ){
            c = 1.0;

            m[0][0] = p0_coord[0];
            m[0][1] = p0_coord[1];
            m[0][2] = 1.0;
            m[1][0] = p2_coord[0];
            m[1][1] = p2_coord[1];
            m[1][2] = 1.0;
            m[2][0] = p3_coord[0];
            m[2][1] = p3_coord[1];
            m[2][2] = 1.0;
        }
        else if( i==2 ){
            c = -1.0;

            m[0][0] = p0_coord[0];
            m[0][1] = p0_coord[1];
            m[0][2] = 1.0;
            m[1][0] = p1_coord[0];
            m[1][1] = p1_coord[1];
            m[1][2] = 1.0;
            m[2][0] = p3_coord[0];
            m[2][1] = p3_coord[1];
            m[2][2] = 1.0;
        }
        else if( i==3 ){
            c = 1.0;

            m[0][0] = p0_coord[0];
            m[0][1] = p0_coord[1];
            m[0][2] = 1.0;
            m[1][0] = p1_coord[0];
            m[1][1] = p1_coord[1];
            m[1][2] = 1.0;
            m[2][0] = p2_coord[0];
            m[2][1] = p2_coord[1];
            m[2][2] = 1.0;
        }
        detm = m[0][0]*m[1][1]*m[2][2]+m[0][1]*m[1][2]*m[2][0]+m[0][2]*m[1][0]*m[2][1]
            -m[2][0]*m[1][1]*m[0][2]-m[1][0]*m[0][1]*m[2][2]-m[0][0]*m[2][1]*m[1][2];
        SFDZ[i] = c*detm/Vol/6.0;
    }

    return SFDZ;
}

CElemTET4::~CElemTET4()
{
    //dctor
}

