#include "SFEM.hpp"
#include "config.hpp"
#include "geometry.hpp"
#include "option.hpp"
#include "exdyna_auxiliary.h"
#include "exdyna_preproc_fem3d_T4.h"
#include "exdyna_preproc_sfem3d_T4.h"
#include "exdyna_solver_solid_expicit_3d.h"
#include "exdyna_read_write.h"
#include "Timer.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <algorithm>
#include <limits>


using namespace std;


SFEM::SFEM()
{   // Constructor

}


void SFEM::PreProcessing(CConfig *config, CGeometry *geometry)
{
    /////////////////////////////////////////////////////////////////////////
	//            COVERT Arma mat and vec types to C++ array
	/////////////////////////////////////////////////////////////////////////
	s_nnode = geometry->GetnNode();
	s_nele = geometry->GetnElem();
    // convert the element info
	umat ele_arma1 = geometry->GetElem();
	ele_arma = conv_to<imat>::from(ele_arma1);
	s_ele_node = ele_arma.memptr();
	ele_arma1.reset();

	node_coord_arma = geometry->GetNode_coord();
	s_node_coord = node_coord_arma.memptr();

    // convert the method types in config
	unsigned short method;
	method = config->GetKind_Method();
	switch (method)
	{
	case FEM:
	  s_method_type = 1.0;
	  break;
	case FSFEM:
	  s_method_type = 2.0;
	  break;
	case FSNSFEM:
	  s_method_type = 3.0;
	  break;
	default:
	  s_method_type = 0.0;
	  break;
	}

    // convert the material types in config
	unsigned short material;
	material = config->GetKind_Material();
	switch (material)
	{
	case LINEAR_ELASTIC:
	  s_mat_type = 0.0;
	  break;
	case ST_VENNAT:
	  s_mat_type = 1.0;
	  break;
	case MO_RIV:
	  s_mat_type = 2.0;
	  break;
	default:
	  s_mat_type = 0.0;
	  break;
	}

	s_mat_para = config->GetMat_Para();
	s_mat_damp_coef = config->GetDamp_Coef();
	gravity = config->GetGravity();
	dt = config->GetDeltaTime();
	MAX_TIME_STEP = config->GetnExtIter();
	flag_stepsave = 1.0;
	flag_stepinc = config->GetWrt_Sol_Freq();
	flag_stepscreen = config->GetWrt_Con_Freq();
	t_total = config->GetTotalTime();


	/////////////////////////////////////////////////////////////////////////
	//            START PREPROCESSING
	/////////////////////////////////////////////////////////////////////////

	//-----------------------------------------------------------------

	//-----------------------------------------------------------------
	// start FEM preprocessing procedure for solid,
	// generating Gaussian quadrature points, finding support
	// nodes of gp, calculating shape functions and derivatives,
	// calculating lumped mass matrix
	// and the inverse lumped mass matrix, and etc.
	// preprocessing for solid : Selective SFEM 3D T4
	printf("\n===============================================================\n");
	printf("SFEM : start preprocessing for solids.\n");

	s_ndof = 3*s_nnode;

	s_ele_vol = (double *)malloc( sizeof(double)*s_nele);
	prep_fem_ele_vol_3d_T4(s_node_coord, s_nnode, s_ele_node, s_nele, s_ele_vol);

	// finding the local element size associated with solid nodes
	s_h_node = (double *)malloc(sizeof(double)*s_nnode);
	prep_fem_local_ele_size_3d_T4(s_nnode, s_node_coord, s_nele, s_ele_node, s_ele_vol,
		s_h_node, s_h_node_bound);

	// calculate diagonal entries of the lumped mass matrix of solid
	s_mass_diag = (double *)malloc(sizeof(double)*s_ndof);
	s_mass_diag_inv = (double *)malloc(sizeof(double)*s_ndof);
	memset(s_mass_diag, 0.0, sizeof(double)*s_ndof);
	memset(s_mass_diag_inv, 0.0, sizeof(double)*s_ndof);

	prep_fem_lumped_mass_diag_entry_3d_T4(s_mat_para[0],
		s_nnode, s_nele, s_ele_node, s_ele_vol, s_mass_diag, s_mass_diag_inv);

	if ( s_method_type == 0.0 ||  s_method_type == 1.0 ) {
		// FEM
		s_ngp = s_nele;
		s_gp_max_nspnode = 4;
		s_ele_center = (double *)malloc( sizeof(double)*s_nele*3);
		s_gp_coord = (double *)malloc( sizeof(double)*s_ngp*3);
		s_gp_spnode = (int *)malloc( sizeof(int)*s_ngp*s_gp_max_nspnode);
		s_gp_spnode_num = (int *)malloc( sizeof(int)*s_ngp);
		s_gp_spele = (int *)malloc( sizeof(int)*s_ngp);
		s_gp_weight = (double *)malloc( sizeof(double)*s_ngp);
		s_gp_sf = (double *)malloc( sizeof(double)*s_ngp*s_gp_max_nspnode);
		s_gp_sfdx = (double *)malloc( sizeof(double)*s_ngp*s_gp_max_nspnode);
		s_gp_sfdy = (double *)malloc( sizeof(double)*s_ngp*s_gp_max_nspnode);
		s_gp_sfdz = (double *)malloc( sizeof(double)*s_ngp*s_gp_max_nspnode);

		prep_preprocessing_fem_3d_T4(s_nnode, s_node_coord, s_nele, s_ele_node, s_ngp,
			s_ele_vol, s_ele_center, s_gp_coord, s_gp_spnode, s_gp_spnode_num, s_gp_spele, s_gp_weight,
			s_gp_sf, s_gp_sfdx, s_gp_sfdy, s_gp_sfdz);

		// calculate BML matrices
		s_gp_BML = (double *)malloc(s_ngp*6*s_gp_max_nspnode*3*sizeof(double));
		memset(s_gp_BML, 0.0, s_ngp*6*s_gp_max_nspnode*3*sizeof(double));

		sol_exdyna_solid_BML_3d(s_ngp, s_gp_max_nspnode,
			s_gp_sfdx, s_gp_sfdy, s_gp_sfdz, s_gp_BML);
	}
	else if ( s_method_type == 2.0 ) {
		// ES-FEM (or FS-FEM)

		// calculate smoothed derivatives of shape function, ES-FEM
		double *rtn_dp_gp[4];
		int *rtn_ip_gp[2];
		int rtn_int_gp[2];

		prep_preprocessing_esfem_3d_T4(s_nnode, s_node_coord,
			s_nele, s_ele_node, s_ele_vol, rtn_dp_gp, rtn_ip_gp, rtn_int_gp);

		s_gp_sfdx = rtn_dp_gp[0];
		s_gp_sfdy = rtn_dp_gp[1];
		s_gp_sfdz = rtn_dp_gp[2];
		s_gp_weight = rtn_dp_gp[3];

		s_gp_spnode = rtn_ip_gp[0];
		s_gp_spnode_num = rtn_ip_gp[1];

		s_ngp = rtn_int_gp[0];
		s_gp_max_nspnode = rtn_int_gp[1];

		/////////////////////////////////////////////////////////
		// calculate BML matrices
		s_gp_BML = (double *)malloc(s_ngp*6*s_gp_max_nspnode*3*sizeof(double));
		memset(s_gp_BML, 0.0, s_ngp*6*s_gp_max_nspnode*3*sizeof(double));

		sol_exdyna_solid_BML_3d(s_ngp, s_gp_max_nspnode,
			s_gp_sfdx, s_gp_sfdy, s_gp_sfdz, s_gp_BML);

	}
	else if( s_method_type == 3.0)
	{
		// Selective SFEM

		// calculate smoothed derivatives of shape function, Selective SFEM
		double *rtn_dp_gp[4];
		int *rtn_ip_gp[2];
		int rtn_int_gp[2];

		double *rtn_dp_gpr[4];
		int *rtn_ip_gpr[2];
		int rtn_int_gpr[2];

		prep_preprocessing_selective_sfem_3d_T4(s_nnode, s_node_coord,
			s_nele, s_ele_node, s_ele_vol, rtn_dp_gp, rtn_ip_gp, rtn_int_gp,
			rtn_dp_gpr, rtn_ip_gpr, rtn_int_gpr);

		s_gp_sfdx = rtn_dp_gp[0];
		s_gp_sfdy = rtn_dp_gp[1];
		s_gp_sfdz = rtn_dp_gp[2];
		s_gp_weight = rtn_dp_gp[3];


		s_gp_spnode = rtn_ip_gp[0];
		s_gp_spnode_num = rtn_ip_gp[1];

		s_ngp = rtn_int_gp[0];
		s_gp_max_nspnode = rtn_int_gp[1];

		s_gpr_sfdx = rtn_dp_gpr[0];
		s_gpr_sfdy = rtn_dp_gpr[1];
		s_gpr_sfdz = rtn_dp_gpr[2];
		s_gpr_weight = rtn_dp_gpr[3];


		s_gpr_spnode = rtn_ip_gpr[0];
		s_gpr_spnode_num = rtn_ip_gpr[1];

		s_ngpr = rtn_int_gpr[0];
		s_gpr_max_nspnode = rtn_int_gpr[1];

		/////////////////////////////////////////////////////////
		// calculate BML matrices
		s_gp_BML = (double *)malloc(s_ngp*6*s_gp_max_nspnode*3*sizeof(double));
		memset(s_gp_BML, 0.0, s_ngp*6*s_gp_max_nspnode*3*sizeof(double));

		sol_exdyna_solid_BML_3d(s_ngp, s_gp_max_nspnode,
			s_gp_sfdx, s_gp_sfdy, s_gp_sfdz, s_gp_BML);

		s_gpr_BML = (double *)malloc(s_ngpr*6*s_gpr_max_nspnode*3*sizeof(double));
		memset(s_gpr_BML, 0.0, s_ngpr*6*s_gpr_max_nspnode*3*sizeof(double));

		sol_exdyna_solid_BML_3d(s_ngpr, s_gpr_max_nspnode,
			s_gpr_sfdx, s_gpr_sfdy, s_gpr_sfdz, s_gpr_BML);

	}
	printf("SFEM : Preprocessing for solids is done.\n");
	printf("===============================================================\n");


	/// # JC : initial s_node_coord
	s_node_coord_n_one = (double *) malloc (sizeof(double)*s_ndof);
	memcpy(s_node_coord_n_one, s_node_coord ,sizeof(double)*s_ndof);

    s_v_n = (double *) malloc (sizeof(double)*s_ndof);
	memset(s_v_n, 0.0 ,sizeof(double)*s_ndof);

	s_v_n_one = (double *) malloc (sizeof(double)*s_ndof);
	memset(s_v_n_one, 0.0 ,sizeof(double)*s_ndof);

	s_u_n = (double *) malloc (sizeof(double)*s_ndof);
	memset(s_u_n, 0.0 ,sizeof(double)*s_ndof);

	s_u_n_one = (double *) malloc (sizeof(double)*s_ndof);
	memset(s_u_n_one, 0.0 ,sizeof(double)*s_ndof);

	s_a_n = (double *) malloc (sizeof(double)*s_ndof);
	memset(s_a_n, 0.0 ,sizeof(double)*s_ndof);

	s_a_n_one = (double *) malloc (sizeof(double)*s_ndof);
	memset(s_a_n_one, 0.0 ,sizeof(double)*s_ndof);

	F_ext = (double *) malloc (sizeof(double)*s_ndof);
	memset(F_ext, 0.0 ,sizeof(double)*s_ndof);
}


void SFEM::LoadAndBC(CConfig* config, CGeometry* geometry){
	/*  */
	cout << "SFEM : Preprocessing the displacement and velocity BCs." << endl;
	//Get displacement BCs
	GetUBCDofAndVal(config, geometry);

	//Get velocity BCs
	GetVBCDofAndVal(config, geometry);

	//Get Loads
	cout << "SFEM : Preprocessing the Loads." << endl;


}

void SFEM::GetUBCDofAndVal(CConfig *config, CGeometry *geometry){
	unsigned short iMarker_disp;
	string iMarker_tag;
	int ux_idx, uy_idx,uz_idx;
	double ux,uy,uz;
	unsigned short nMarker_disp = config->GetnMarker_Displacement();
	ivec Node_iMarker;
	uvec Node_iMarker1;
	unsigned long nNode_iMarker, nNode_Markers;
	ivec uDOF, ux_DOF_iMarker, uy_DOF_iMarker, uz_DOF_iMarker;
	vec uVal,ux_Val_iMarker, uy_Val_iMarker, uz_Val_iMarker;

	nNode_Markers = 0;
	uDOF.reset();
	uVal.reset();


	for (iMarker_disp=0; iMarker_disp<nMarker_disp; iMarker_disp++){

	    // get Marker tags form config
	    iMarker_tag = config->GetMarkerTag_Displacement(iMarker_disp);
	    ux_idx = config->GetDisplX_Indicator(iMarker_tag);
	    uy_idx = config->GetDisplY_Indicator(iMarker_tag);
	    uz_idx = config->GetDisplZ_Indicator(iMarker_tag);
	    ux = config->GetDisplX(iMarker_tag);
	    uy = config->GetDisplY(iMarker_tag);
	    uz = config->GetDisplZ(iMarker_tag);
	    // get unique node ID and these nodes numbers from geometry
	    Node_iMarker1 = geometry->GetNode_iMarker(iMarker_tag);
	    Node_iMarker = conv_to<imat>::from(Node_iMarker1);
	    nNode_iMarker = Node_iMarker.n_elem;
	    nNode_Markers = nNode_Markers + nNode_iMarker;
	    //
	    if(ux_idx == 1) {
			ux_DOF_iMarker.set_size(nNode_iMarker);
			ux_Val_iMarker.set_size(nNode_iMarker);
			ux_DOF_iMarker = 3*Node_iMarker;
			ux_Val_iMarker.fill(ux);
			uDOF = join_vert(uDOF,ux_DOF_iMarker);
			uVal = join_vert(uVal,ux_Val_iMarker);
	    }
	    if(uy_idx == 1) {
			uy_DOF_iMarker.set_size(nNode_iMarker);
			uy_Val_iMarker.set_size(nNode_iMarker);
			uy_DOF_iMarker =  3*Node_iMarker+1;
			uy_Val_iMarker.fill(uy);
			uDOF = join_vert(uDOF,uy_DOF_iMarker);
			uVal = join_vert(uVal,uy_Val_iMarker);
	    }
	    if(uz_idx == 1) {
			uz_DOF_iMarker.set_size(nNode_iMarker);
			uz_Val_iMarker.set_size(nNode_iMarker);
			uz_DOF_iMarker =  3*Node_iMarker+2;
			uz_Val_iMarker.fill(uz);
			uDOF = join_vert(uDOF,uz_DOF_iMarker);
			uVal = join_vert(uVal,uz_Val_iMarker);
	    }
	    cout << "SFEM : Displacement marker ["<< iMarker_tag << "] is preprocessed." << endl;
	}

	//convert to s_ubc_ndof
	s_ubc_ndof = uDOF.n_elem;
	ubc_DOF = uDOF;
	ubc_Val = uVal;


}

ivec SFEM::FindRotateNode(CConfig *config, CGeometry *geometry){
    unsigned short imarker;
    string imarker_tag;
    ivec node_marker;
    uvec node_marker1;
    unsigned long nnode_imarker, nnode_markers;
    unsigned short nmarker;

    nmarker = config->GetnMarker_Displacement();
    for(imarker = 0; imarker < nmarker; imarker++ ){
        imarker_tag = config->GetMarkerTag_Displacement(imarker);
        if(imarker_tag == "LeadingEdge"){
            node_marker1 = geometry->GetNode_iMarker(imarker_tag);
            node_marker = conv_to<imat>::from(node_marker1);
            nnode_imarker = node_marker.n_elem;
        }
    }

    return node_marker;
}


void SFEM::GetUBCDofAndVal_rotate(CConfig *config, CGeometry *geometry, ivec &node_marker, double t){
    vec u(3), v(3);
    vec dist(3);
    unsigned long nnode_marker;
    int inode, nodeID;
    vec center(3);
    center(0) = 0.0; center(1) = 0.0; center(2) = 0.0;
    ivec uDOF;
    vec uVal;
    ivec vDOF;
    vec vVal;

    // get the current rotation angle
    double angle = 0.0;
    double vr = 0.0;
    double A = 0.785398;
    double w = 25.28;
    angle = A*sin(w*t);
    vr = A*w*cos(w*t);
    mat R(3,3);
    R(0,0) = cos(angle)-1.0; R(0,1) = 0.0; R(0,2) = sin(angle);
    R(1,0) = 0.0;            R(1,1) = 1.0-1.0; R(1,2) = 0.0;
    R(2,0) = -sin(angle);    R(2,1) = 0.0; R(2,2) = cos(angle)-1.0;

    nnode_marker = node_marker.n_elem;
    //cout << nnode_marker<< endl;
    uDOF.set_size(3*nnode_marker); // in this case only one ubc marker
    uVal.set_size(3*nnode_marker);
    vDOF.set_size(3*nnode_marker);
    vVal.set_size(3*nnode_marker);
    for(inode = 0; inode < nnode_marker; inode++){
        nodeID = node_marker(inode);
        // compute the distance to the rotate axis
        dist(0) = s_node_coord_n_one[nodeID] - center(0);
        dist(1) = s_node_coord_n_one[nodeID+s_nnode] - center(1);
        dist(2) = s_node_coord_n_one[nodeID+2*s_nnode] - center(2);
        // compute the displacement u = R*dist;
        u(0) = R(0,0)*dist(0)+R(0,2)*dist(2); u(1) = 0.0; u(2) = R(2,0)*dist(0) + R(2,2)*dist(2);
        uDOF(3*inode) = 3*nodeID; uDOF(3*inode+1) = 3*nodeID+1; uDOF(3*inode+2) = 3*nodeID+2;
        uVal(3*inode) = u(0); uVal(3*inode+1) = u(1); uVal(3*inode+2) = u(2);
        // compute the velocity
        v(0) = 1.0*vr*dist(2); v(1) = 0.0; v(2) = -1.0*vr*dist(0);
        vDOF(3*inode) = 3*nodeID; vDOF(3*inode+1) = 3*nodeID+1; vDOF(3*inode+2) = 3*nodeID+2;
        vVal(3*inode) = v(0); vVal(3*inode+1) = v(1); vVal(3*inode+2) = v(2);

    }

//    s_ubc_ndof = uDOF.n_elem;
//	ubc_DOF = uDOF;
//	ubc_Val = uVal;

	s_vbc_ndof = vDOF.n_elem;
	vbc_DOF = vDOF;
	vbc_Val = vVal;

//	vDOF.print();
//	vVal.print();

//	cout << "SFEM: update the displacement and velocity of rotation at t = "<< t <<"!" << endl;

}

void SFEM::GetVBCDofAndVal(CConfig *config, CGeometry *geometry){
	unsigned short iMarker_veloc;
	string iMarker_tag;
	int vx_idx, vy_idx,vz_idx;
	double vx,vy,vz;
	unsigned short nMarker_veloc = config->GetnMarker_Velocity();
	ivec Node_iMarker;
	uvec Node_iMarker1;
	unsigned long nNode_iMarker, nNode_Markers;
	ivec vDOF, vx_DOF_iMarker, vy_DOF_iMarker, vz_DOF_iMarker;
	vec vVal,vx_Val_iMarker, vy_Val_iMarker, vz_Val_iMarker;

	nNode_Markers = 0;
	vDOF.reset();
	vVal.reset();

	for (iMarker_veloc=0; iMarker_veloc<nMarker_veloc; iMarker_veloc++){
	    // get Marker tags form config
	    iMarker_tag = config->GetMarkerTag_Velocity(iMarker_veloc);
	    vx_idx = config->GetVelocX_Indicator(iMarker_tag);
	    vy_idx = config->GetVelocY_Indicator(iMarker_tag);
	    vz_idx = config->GetVelocZ_Indicator(iMarker_tag);
	    vx = config->GetVelocX(iMarker_tag);
	    vy = config->GetVelocY(iMarker_tag);
	    vz = config->GetVelocZ(iMarker_tag);
	    // get unique node ID and these nodes numbers from geometry
	    Node_iMarker1 = geometry->GetNode_iMarker(iMarker_tag);
	    Node_iMarker = conv_to<imat>::from(Node_iMarker1);
//	    Node_iMarker.print("Node_iMarker = \n");
	    nNode_iMarker = Node_iMarker.n_elem;
	    nNode_Markers = nNode_Markers + nNode_iMarker;
	    //
	    if(vx_idx == 1) {
			vx_DOF_iMarker.set_size(nNode_iMarker);
			vx_Val_iMarker.set_size(nNode_iMarker);
			vx_DOF_iMarker =  3*Node_iMarker;
			vx_Val_iMarker.fill(vx);
			vDOF = join_vert(vDOF,vx_DOF_iMarker);
			vVal = join_vert(vVal,vx_Val_iMarker);
	    }
	    if(vy_idx == 1) {
			vy_DOF_iMarker.set_size(nNode_iMarker);
			vy_Val_iMarker.set_size(nNode_iMarker);
			vy_DOF_iMarker =  3*Node_iMarker+1;
			vy_Val_iMarker.fill(vy);
			vDOF = join_vert(vDOF,vy_DOF_iMarker);
			vVal = join_vert(vVal,vy_Val_iMarker);
	    }
	    if(vz_idx == 1) {
			vz_DOF_iMarker.set_size(nNode_iMarker);
			vz_Val_iMarker.set_size(nNode_iMarker);
			vz_DOF_iMarker =  3*Node_iMarker+2;
			vz_Val_iMarker.fill(vz);
			vDOF = join_vert(vDOF,vz_DOF_iMarker);
			vVal = join_vert(vVal,vz_Val_iMarker);
	    }
	    cout << "SFEM : Velocity marker ["<< iMarker_tag << "] is preprocessed." << endl;

	}

	//convert to s_ubc_ndof
	s_vbc_ndof = vDOF.n_elem;
	vbc_DOF = vDOF;
	vbc_Val = vVal;

}

void SFEM::Solve(CConfig *config, CGeometry *geometry)
{
    Timer timer_total;
    Timer timer_loops;
    Timer timer_prof;

	//////////////////////////////////////////////////////////////
	/////////////// start time steps   ///////////////////////////
	//////////////////////////////////////////////////////////////
	timer_total.start();
	timer_loops.start();

    s_ubc_dof = ubc_DOF.memptr();
    s_ubc_val = ubc_Val.memptr();
	s_vbc_dof = vbc_DOF.memptr();
	s_vbc_val = vbc_Val.memptr();

    n_t = 0;
    if(!config->GetRestartOption()){
        restart_n_t = 0;
        t_n = 0.0;
    }


    double t_n_one_restart = 0.0;
    double t_n_restart = 0.0;
	double error = 1.0;
	double amp = 0.;
	double *F_ext1;
	F_ext1 = new double [s_ndof];

	//
	ivec node_rotatemarker;
	node_rotatemarker = FindRotateNode(config, geometry);


    cout << "[Debug] SFEM: restart_n_t = " << restart_n_t << endl;
	for ( n_t = restart_n_t+1 ; n_t < MAX_TIME_STEP ; n_t++) {

		t_n_one = t_n + dt;
		t_n_one_restart = t_n_restart + dt;

		if(t_n_one < (t_total/2.0)){
            amp = t_n_one/(t_total/2.0);
		}
		else{
            amp = 1.;
		}

		for(int idof=0; idof<s_ndof; idof++){
            F_ext1[idof]=F_ext[idof]*amp;
		}

        ubc_DOF.reset();  ubc_Val.reset();
        vbc_DOF.reset();  vbc_Val.reset();
        GetUBCDofAndVal_rotate(config, geometry, node_rotatemarker,t_n_one);
        //debug
        s_ubc_dof = ubc_DOF.memptr();
        s_ubc_val = ubc_Val.memptr();
        s_vbc_dof = vbc_DOF.memptr();
        s_vbc_val = vbc_Val.memptr();



		// -----------------------------------------------------------------------------
		//  solve solid transient solutions

		flag_ele_dist = sol_exdyna_solid_3d(s_method_type,
			s_mat_type, s_mat_damp_coef, s_mat_para,
			t_n, dt, gravity, s_mass_diag, s_mass_diag_inv,
			s_nnode, s_ndof, s_node_coord, s_nele, s_ele_node, s_ele_vol,
			s_ngp, s_gp_sfdx, s_gp_sfdy, s_gp_sfdz, s_gp_BML,
			s_gp_spnode, s_gp_spnode_num, s_gp_max_nspnode, s_gp_weight,
			s_ngpr, s_gpr_sfdx, s_gpr_sfdy, s_gpr_sfdz, s_gpr_BML,
			s_gpr_spnode, s_gpr_spnode_num, s_gpr_max_nspnode, s_gpr_weight,
			s_u_n, s_v_n, s_a_n, s_vbc_ndof, s_vbc_dof, s_vbc_val,
			s_ubc_ndof, s_ubc_dof, s_ubc_val,
			F_ext1,
			s_u_n_one, s_v_n_one, s_a_n_one, s_node_coord_n_one);


		if (flag_ele_dist !=0 ) {
            OutputSolutionToFile_TECPLOT();
//			post_write_solid_solutions_3d_tecplot(s_mat_para, s_nnode, s_nele, s_node_coord_n_one, s_ele_node,
//                                         s_u_n_one, n_t, t_n_one);
            printf("MSG : Press Any Key to exit.\n");
            cin.clear();
            cin.ignore( numeric_limits<streamsize>::max(), '\n' );
            cin.get();
            exit(1);
		}


	//// Put the error check here
		if ( fmod( double(n_t), double(flag_stepscreen) ) == 0.0 ){
            double norm1 = 0.0;
            double norm2 = 0.0;
            for ( int i = 0 ; i < s_nnode ; i++) {
                norm1 = (s_v_n_one[i] - s_v_n[i])*(s_v_n_one[i] - s_v_n[i]);
                norm2 = (s_v_n[i])*(s_v_n[i]);
            }
            error = sqrt(norm1)/sqrt(norm2);


            printf("MSG : step %d ; error = %8.7e \n", n_t, error);

		}

		// update solutions
		t_n = t_n_one;
		t_n_restart = t_n_one_restart;
		sol_exdyna_3d_update_solid_solutions(s_ndof,
			s_u_n, s_u_n_one, s_v_n, s_v_n_one, s_a_n, s_a_n_one);


        //////////////////////////////////////////////////////////////////
		if ( fmod( double(n_t), double(flag_stepinc) ) == 0.0 ) {

			if ( flag_stepsave == 1 ) {

				//post_write_solid_solutions_3d_tecplot(s_mat_para, s_nnode, s_nele, s_node_coord_n_one, s_ele_node,
                //                         s_u_n_one, n_t, t_n);
                OutputSolutionToFile_TECPLOT();
			}
            timer_loops.stop();
			printf("MSG : cpu time for %d steps: %f s \n",
				flag_stepinc, timer_loops.getElapsedTime());
			timer_loops.start();
		}

		if(config->GetnMarker_FSI() < 1){
            if ( (n_t >= MAX_TIME_STEP) ||(t_n >= t_total)) {

                //post_write_solid_solutions_3d_tecplot(s_mat_para, s_nnode, s_nele, s_node_coord_n_one, s_ele_node,
                //                            s_u_n_one, n_t, t_n);
                OutputSolutionToFile_TECPLOT();
                //Todo : a better strategy to do below
                OutputRestartDataFile_binary(config, geometry);
                //write FSI displacement
                if(config->GetnMarker_FSI() >= 1){
                    OutputFSIDisplacements(config, geometry);
                    OutputFSIMarkerMeshNew(config, geometry);
                }

                timer_total.stop();
                printf("MSG : total cpu time: %f s .\n",
                timer_total.getElapsedTime());

                printf("MSG : solution is done.\n");
                return;
            }
		}
		else if(config->GetnMarker_FSI() >= 1){
            /// if FSI_restart, t_total is equal to CFD one time step length.
            if ( (n_t >= MAX_TIME_STEP) ||(t_n_restart>t_total)) {

                n_t = config->GetCurrentFSIIter();

                //post_write_solid_solutions_3d_tecplot(s_mat_para, s_nnode, s_nele, s_node_coord_n_one, s_ele_node,
                //                             s_u_n_one, n_t, t_n-t_total);
                OutputSolutionToFile_TECPLOT();
                //Todo : a better strategy to do below
                OutputRestartDataFile_binary(config, geometry);
                //write FSI displacement
                if(config->GetnMarker_FSI() >= 1){
                    //Note: This displacement in fact is the displacement increment.
                    OutputFSIDisplacements(config, geometry);
                    OutputFSIMarkerMeshNew(config, geometry);
                }

                timer_total.stop();
                printf("MSG : total cpu time: %f s .\n",
                timer_total.getElapsedTime());

                printf("MSG : solution is done.\n");
                return;
            }
		}






    }
} // end loop n_t







/*! \brief Restart from previous state.
 *  Flowchart:
 1. if flag_restart == 1

     if restart_SD_file non-exist
        call function PreProcessing first.
        output SD data to restart_file.
     else
        Read in SD data into memory.

     if restart_data_file non-exist
        write a empty restart_data_file
     else
        read in displacement etc. into memory.

    if config->FSIflpag == 1
        read in the FSI forces.

    solve();

    save the new restart_data_file.
*
 */
void SFEM::Restart(CConfig *config, CGeometry *geometry){
    if(!config->GetRestartOption() ){
        return;
    }

    //{restart smoothing domain file
    char restart_SD_file[200];
    strcpy (restart_SD_file, config->GetRestartSDFileName().c_str());
    fstream fin_SD;
    fin_SD.open(restart_SD_file);

    if(!fin_SD){
        cout << "SFEM : The Restart file contain Smoothing domain data doesn't exist!" << endl;
        cout << "SFEM : Create the Restart_SD_File!" << endl;
        PreProcessing(config, geometry);
        OutputRestartSDFile_binary(config, geometry);
    }

    ReadinRestartSDFile_binary(config, geometry);
    //}


    //{restart data file
    char restart_Data_file[200];
    strcpy(restart_Data_file, config->GetRestartDATAFileName().c_str());
    fstream fin_Data;
    fin_Data.open(restart_Data_file);

    if(!fin_Data){
        cout << "SFEM : The Restart file contain displacement and velocity doesn't exist!" << endl;
        cout << "SFEM : Create the Restart_Data_File!" << endl;
        OutputRestartDataFile_binary(config, geometry);
    }

    ReadinRestartDataFile_binary(config, geometry);
    //}

    Solve(config, geometry);
}

/* s_ngp, s_gp_sfdx, s_gp_sfdy, s_gp_sfdz, s_gp_BML,
    s_gp_spnode, s_gp_spnode_num, s_gp_max_nspnode, s_gp_weight,
    s_ngpr, s_gpr_sfdx, s_gpr_sfdy, s_gpr_sfdz, s_gpr_BML,
    s_gpr_spnode, s_gpr_spnode_num, s_gpr_max_nspnode, s_gpr_weight,
*/
void SFEM::ReadinRestartSDFile_binary(CConfig *config, CGeometry *geometry){
    char restart_SD_file[200];
    strcpy (restart_SD_file, config->GetRestartSDFileName().c_str());
    fstream fin_SD;
    fin_SD.open(restart_SD_file, ios::in);

    // read s_ngp
    fin_SD.read((char *)(&s_ngp), sizeof(int));
    // read s_gp_max_nspnode
    fin_SD.read((char *)(&s_gp_max_nspnode), sizeof(int));
    // read s_gp_sfdx
    fin_SD.read((char *)(s_gp_sfdx), sizeof(double)*s_ngp*s_gp_max_nspnode);
    // read s_gp_sfdy
    fin_SD.read((char *)(s_gp_sfdy), sizeof(double)*s_ngp*s_gp_max_nspnode);
    // read s_gp_sfdz
    fin_SD.read((char *)(s_gp_sfdz), sizeof(double)*s_ngp*s_gp_max_nspnode);
    // read s_gp_spnode
    fin_SD.read((char *)(s_gp_spnode), sizeof(int)*s_ngp*s_gp_max_nspnode);
    // read s_gp_spnode_num
    fin_SD.read((char *)(s_gp_spnode_num), sizeof(int)*s_ngp);
    // read s_gp_weight
    fin_SD.read((char *)(s_gp_weight), sizeof(int)*s_ngp);
    // read s_gp_BML (no need any more)

    /* if selective S-FEM is used*/
    if (s_method_type == 3.0){
        // read s_ngpr
        fin_SD.read((char *)(&s_ngpr), sizeof(int));
        // read s_gpr_max_nspnode
        fin_SD.read((char *)(&s_gpr_max_nspnode), sizeof(int));
        // read s_gpr_sfdx
        fin_SD.read((char *)(s_gpr_sfdx), sizeof(double)*s_ngpr*s_gpr_max_nspnode);
        // read s_gpr_sfdy
        fin_SD.read((char *)(s_gpr_sfdy), sizeof(double)*s_ngpr*s_gpr_max_nspnode);
        // read s_gpr_sfdz
        fin_SD.read((char *)(s_gpr_sfdz), sizeof(double)*s_ngpr*s_gpr_max_nspnode);
        // read s_gpr_spnode
        fin_SD.read((char *)(s_gpr_spnode), sizeof(int)*s_ngpr*s_gpr_max_nspnode);
        // read s_gpr_spnode_num
        fin_SD.read((char *)(s_gpr_spnode_num), sizeof(int)*s_ngpr);
        // read s_gpr_weight
        fin_SD.read((char *)(s_gpr_weight), sizeof(int)*s_ngpr);
    }

    fin_SD.close();
}

void SFEM::ReadinRestartDataFile_binary(CConfig *config, CGeometry *geoemtry){
    char restart_Data_file[200];
    strcpy(restart_Data_file, config->GetRestartDATAFileName().c_str());
    ifstream fin_Data;
    fin_Data.open(restart_Data_file, ios::binary);

    //read n_t
    fin_Data.read((char *)(&restart_n_t), sizeof(int));
    //read t_n
    fin_Data.read((char *)(&t_n), sizeof(double));
    //read s_u_n
    fin_Data.read((char *)(s_u_n), sizeof(double)*s_ndof);
    //read s_v_n
    fin_Data.read((char *)(s_v_n), sizeof(double)*s_ndof);

    fin_Data.close();

    return;
}

void SFEM::OutputRestartSDFile_binary(CConfig *config, CGeometry *geometry){
    char restart_SD_file[200];
    strcpy (restart_SD_file, config->GetRestartSDFileName().c_str());
    ofstream fout_SD(restart_SD_file, ios::binary);

    if(!fout_SD){
        cout << "SFEM : SFEM::OutputRestart_Binary can't open the restart_SD_file!!!" << endl;
        cout << "SFEM : Press any key to exit!!!" << endl;
        cin.get();
        exit(1);
    }

    // write s_ngp
    fout_SD.write((char *)(&s_ngp), sizeof(int));
    // write s_gp_max_nspnode
    fout_SD.write((char *)(&s_gp_max_nspnode), sizeof(int));
    // write s_gp_sfdx
    fout_SD.write((char *)(s_gp_sfdx), sizeof(double)*s_ngp*s_gp_max_nspnode);
    // write s_gp_sfdy
    fout_SD.write((char *)(s_gp_sfdy), sizeof(double)*s_ngp*s_gp_max_nspnode);
    // write s_gp_sfdz
    fout_SD.write((char *)(s_gp_sfdz), sizeof(double)*s_ngp*s_gp_max_nspnode);
    // write s_gp_spnode
    fout_SD.write((char *)(s_gp_spnode), sizeof(int)*s_ngp*s_gp_max_nspnode);
    // write s_gp_spnode_num
    fout_SD.write((char *)(s_gp_spnode_num), sizeof(int)*s_ngp);
    // write s_gp_weight
    fout_SD.write((char *)(s_gp_weight), sizeof(int)*s_ngp);
    // write s_gp_BML (no need any more)

    /* if selective S-FEM is used*/
    if (s_method_type == 3.0){
        // write s_ngpr
        fout_SD.write((char *)(&s_ngpr), sizeof(int));
        // write s_gpr_max_nspnode
        fout_SD.write((char *)(&s_gpr_max_nspnode), sizeof(int));
        // write s_gpr_sfdx
        fout_SD.write((char *)(s_gpr_sfdx), sizeof(double)*s_ngpr*s_gpr_max_nspnode);
        // write s_gpr_sfdy
        fout_SD.write((char *)(s_gpr_sfdy), sizeof(double)*s_ngpr*s_gpr_max_nspnode);
        // write s_gpr_sfdz
        fout_SD.write((char *)(s_gpr_sfdz), sizeof(double)*s_ngpr*s_gpr_max_nspnode);
        // write s_gpr_spnode
        fout_SD.write((char *)(s_gpr_spnode), sizeof(int)*s_ngpr*s_gpr_max_nspnode);
        // write s_gpr_spnode_num
        fout_SD.write((char *)(s_gpr_spnode_num), sizeof(int)*s_ngpr);
        // write s_gpr_weight
        fout_SD.write((char *)(s_gpr_weight), sizeof(int)*s_ngpr);
    }

    fout_SD.close();

    return;
}

void SFEM::OutputRestartSDFile(CConfig *config, CGeometry *geometry){
    /* s_ngp, s_gp_sfdx, s_gp_sfdy, s_gp_sfdz, s_gp_BML,
    s_gp_spnode, s_gp_spnode_num, s_gp_max_nspnode, s_gp_weight,
    s_ngpr, s_gpr_sfdx, s_gpr_sfdy, s_gpr_sfdz, s_gpr_BML,
    s_gpr_spnode, s_gpr_spnode_num, s_gpr_max_nspnode, s_gpr_weight,
    */


    char restart_SD_file[200];
    strcpy (restart_SD_file, config->GetRestartSDFileName().c_str());
    ofstream fout_SD(restart_SD_file, ios::binary);

    if(!fout_SD){
        cout << "SFEM : SFEM::OutputRestart can't open the restart_SD_file!!!" << endl;
        cout << "SFEM : Press any key to exit!!!" << endl;
        cin.get();
        exit(1);
    }

    //{write SD information to file
    fout_SD << "s_ngp= " << s_ngp << "\n";
    fout_SD << "s_gp_max_nspnode= " << s_gp_max_nspnode << "\n";

    fout_SD << "s_gp_sfdx= " << "\n";
    for(int igp=0; igp<s_ngp; igp++){
        for( int j=0; j<s_gp_max_nspnode; j++){
             fout_SD << scientific << s_gp_sfdx[igp+j*s_ngp] << "\t";
        }
        fout_SD << "\n";
    }

    fout_SD << "s_gp_sfdy= " << "\n";
    for(int igp=0; igp<s_ngp; igp++){
        for( int j=0; j<s_gp_max_nspnode; j++){
             fout_SD << scientific << s_gp_sfdy[igp+j*s_ngp] << "\t";
        }
        fout_SD << "\n";
    }

    fout_SD << "s_gp_sfdz= " << "\n";
    for(int igp=0; igp<s_ngp; igp++){
        for( int j=0; j<s_gp_max_nspnode; j++){
             fout_SD << scientific << s_gp_sfdz[igp+j*s_ngp] << "\t";
        }
        fout_SD << "\n";
    }

    fout_SD << "s_gp_spnode= "<< "\n";
    for(int igp=0; igp<s_ngp; igp++){
        for( int j=0; j<s_gp_max_nspnode; j++){
             fout_SD << s_gp_spnode[igp+j*s_ngp] << "\t";
        }
        fout_SD << "\n";
    }

    fout_SD << "s_gp_spnode_num= "<< "\n";
    for(int igp=0; igp<s_ngp; igp++){
        fout_SD << s_gp_spnode_num[igp] <<"\n";
    }

    fout_SD << "s_gp_weight= "<< "\n";
    for(int igp=0; igp<s_ngp; igp++){
        fout_SD << scientific <<s_gp_weight[igp] <<"\n";
    }

    fout_SD << "s_gp_BML= "<< "\n";
    int nrow = 6*s_ngp;
    int ncol = 3*s_gp_max_nspnode;
    for(int irow=0; irow<nrow; irow++){
        for(int icol=0; icol<ncol; icol++){
            fout_SD << scientific << s_gp_BML[irow + icol*nrow] << "\t";
        }
        fout_SD << "\n";
    } //}

    // if method is selective SFEM, we also need data of gpr
    if (s_method_type == 3.0){
        fout_SD << "s_ngpr= " << s_ngpr << "\n";
        fout_SD << "s_gpr_max_nspnode= " << s_gpr_max_nspnode << "\n";

        fout_SD << "s_gpr_sfdx= " << "\n";
        for(int igpr=0; igpr<s_ngpr; igpr++){
            for( int j=0; j<s_gpr_max_nspnode; j++){
                 fout_SD << scientific << s_gpr_sfdx[igpr+j*s_ngpr] << "\t";
            }
            fout_SD << "\n";
        }

        fout_SD << "s_gpr_sfdy= " << "\n";
        for(int igpr=0; igpr<s_ngpr; igpr++){
            for( int j=0; j<s_gpr_max_nspnode; j++){
                 fout_SD << scientific << s_gpr_sfdy[igpr+j*s_ngpr] << "\t";
            }
            fout_SD << "\n";
        }

        fout_SD << "s_gpr_sfdz= " << "\n";
        for(int igpr=0; igpr<s_ngpr; igpr++){
            for( int j=0; j<s_gpr_max_nspnode; j++){
                 fout_SD << scientific << s_gpr_sfdz[igpr+j*s_ngpr] << "\t";
            }
            fout_SD << "\n";
        }

        fout_SD << "s_gpr_spnode= "<< "\n";
        for(int igpr=0; igpr<s_ngpr; igpr++){
            for( int j=0; j<s_gpr_max_nspnode; j++){
                 fout_SD << s_gpr_spnode[igpr+j*s_ngpr] << "\t";
            }
            fout_SD << "\n";
        }

        fout_SD << "s_gpr_spnode_num= "<< "\n";
        for(int igpr=0; igpr<s_ngpr; igpr++){
            fout_SD << s_gpr_spnode_num[igpr] <<"\n";
        }

        fout_SD << "s_gpr_weight= "<< "\n";
        for(int igpr=0; igpr<s_ngpr; igpr++){
            fout_SD << scientific <<s_gpr_weight[igpr] <<"\n";
        }

        fout_SD << "s_gpr_BML= "<< "\n";
        int nrow = 6*s_ngpr;
        int ncol = 3*s_gpr_max_nspnode;
        for(int irow=0; irow<nrow; irow++){
            for(int icol=0; icol<ncol; icol++){
                fout_SD << scientific << s_gpr_BML[irow + icol*nrow] << "\t";
            }
            fout_SD << "\n";
        }
    }


}

void SFEM::OutputRestartDataFile_binary(CConfig *config, CGeometry *geometry){
    char restart_Data_file[200];
    strcpy(restart_Data_file, config->GetRestartDATAFileName().c_str());
    ofstream fout_Data;
    fout_Data.open(restart_Data_file, ios::binary|ios::trunc);

    //write n_t
    fout_Data.write((char *)(&n_t), sizeof(int));
    //write t_n_one
    fout_Data.write((char *)(&t_n_one), sizeof(double));
    //write s_u_n_one
    fout_Data.write((char *)(s_u_n_one), sizeof(double)*s_ndof);
    //write s_v_n_one
    fout_Data.write((char *)(s_v_n_one), sizeof(double)*s_ndof);

    fout_Data.close();

    return;
}

void SFEM::RestartFSI(CConfig *config, CGeometry *geometry){
    if(!config->GetRestartOption() ){
        return;
    }

    //{restart smoothing domain file
    char restart_SD_file[200];
    strcpy (restart_SD_file, config->GetRestartSDFileName().c_str());
    fstream fin_SD;
    fin_SD.open(restart_SD_file);

    if(!fin_SD){
        cout << "SFEM : The Restart file contain Smoothing domain data doesn't exist!" << endl;
        cout << "SFEM : Create the Restart_SD_File!" << endl;
        PreProcessing(config, geometry);
        OutputRestartSDFile_binary(config, geometry);
    }

    ReadinRestartSDFile_binary(config, geometry);
    //}


    //{restart data file
    char restart_Data_file[200];
    strcpy(restart_Data_file, config->GetRestartDATAFileName().c_str());
    fstream fin_Data;
    fin_Data.open(restart_Data_file);

    if(!fin_Data){
        cout << "SFEM : The Restart file contain displacement and velocity doesn't exist!" << endl;
        cout << "SFEM : Create the Restart_Data_File!" << endl;
        OutputRestartDataFile_binary(config, geometry);
    }

    ReadinRestartDataFile_binary(config, geometry);

    s_u_n_old = new double [s_ndof];
    for(int idof=0; idof < s_ndof; idof++){
        s_u_n_old[idof] = s_u_n[idof];
    }
    //}

    //{if this is a FSI restart
    if(config->GetnMarker_FSI() >= 1){
        // read in the FSI forces in F_ext
        ReadinFSIForces(config, geometry);
    }

    //}


    Solve(config, geometry);
}

void SFEM::OutputFSIDisplacements(CConfig *config, CGeometry *geometry){
    int nMarker_FSI=config->GetnMarker_FSI();
    int iMarker_FSI;
    umat ele_iMarker_FSI;
    umat ele_FSI_marker;
    uvec node_iMarker_FSI;
    uvec node_FSI_marker;
    string iMarker_FSI_tag;
    for(iMarker_FSI=0; iMarker_FSI<nMarker_FSI; iMarker_FSI++){
        iMarker_FSI_tag = config->GetMarkerTag_FSI(iMarker_FSI);
        ele_iMarker_FSI = geometry->GetElem_iMarker(iMarker_FSI_tag);
        node_iMarker_FSI = geometry->GetNode_iMarker(iMarker_FSI_tag);
        ele_FSI_marker = join_vert(ele_FSI_marker, ele_iMarker_FSI);
        node_FSI_marker = join_vert(node_FSI_marker, node_iMarker_FSI);
    }


    char cstr[200];
    strcpy(cstr, config->GetFSIDisplacementFileName().c_str());
    ofstream FSI_disp_file(cstr, ios::out|ios::trunc);

    int nele = ele_FSI_marker.n_rows;
    int iele;
    int nnode = node_FSI_marker.n_elem;
    int inode;
    mat node_coord = geometry->GetNode_coord();
    int I;

    cout << "SFEM : Output the displacement of FSI markers to " << config->GetFSIDisplacementFileName() <<" !" << endl;
    cout << "SFEM : Only 3D problem with Triangular marker elements!" << endl;

    if(config->GetRestartOption()){
        for(inode=0; inode<nnode; inode++){

        I = node_FSI_marker(inode);
        FSI_disp_file << I << "\t";
 //       FSI_disp_file << scientific <<node_coord(I,0) << "\t";
 //       FSI_disp_file << scientific <<node_coord(I,1) << "\t";
 //      FSI_disp_file << scientific <<node_coord(I,2) << "\n";
        FSI_disp_file << scientific <<s_u_n_one[3*I]-s_u_n_old[3*I]<< "\t";
        FSI_disp_file << scientific <<s_u_n_one[3*I+1]-s_u_n_old[3*I+1] << "\t";
        FSI_disp_file << scientific <<s_u_n_one[3*I+2]-s_u_n_old[3*I+2] << "\n";

        }
    }
    else{
        for(inode=0; inode<nnode; inode++){

        I = node_FSI_marker(inode);
        FSI_disp_file << I << "\t";
 //       FSI_disp_file << scientific <<node_coord(I,0) << "\t";
 //       FSI_disp_file << scientific <<node_coord(I,1) << "\t";
 //      FSI_disp_file << scientific <<node_coord(I,2) << "\n";
        FSI_disp_file << scientific <<s_u_n_one[3*I]<< "\t";
        FSI_disp_file << scientific <<s_u_n_one[3*I+1] << "\t";
        FSI_disp_file << scientific <<s_u_n_one[3*I+2]<< "\n";

        }
    }


    FSI_disp_file.close();

}



void SFEM::OutputFSIMarkerMeshNew(CConfig *config, CGeometry *geometry){
    int nMarker_FSI=config->GetnMarker_FSI();
    int iMarker_FSI;
    umat ele_iMarker_FSI;
    umat ele_FSI_marker;
    uvec node_iMarker_FSI;
    uvec node_FSI_marker;
    string iMarker_FSI_tag;
    for(iMarker_FSI=0; iMarker_FSI<nMarker_FSI; iMarker_FSI++){
        iMarker_FSI_tag = config->GetMarkerTag_FSI(iMarker_FSI);
        ele_iMarker_FSI = geometry->GetElem_iMarker(iMarker_FSI_tag);
        node_iMarker_FSI = geometry->GetNode_iMarker(iMarker_FSI_tag);
        ele_FSI_marker = join_vert(ele_FSI_marker, ele_iMarker_FSI);
        node_FSI_marker = join_vert(node_FSI_marker, node_iMarker_FSI);
    }


    char cstr[200];
    strcpy(cstr, config->GetFSIMarkerMeshFileName().c_str());
    ofstream FSI_mesh_file(cstr, ios::out|ios::trunc);

    int nele = ele_FSI_marker.n_rows;
    int iele;
    int nnode = node_FSI_marker.n_elem;
    int inode;
    mat node_coord = geometry->GetNode_coord();
    int I;

    cout << "SFEM : Output the New mesh of FSI markers to " << config->GetFSIMarkerMeshFileName()<<" !" << endl;
    cout << "SFEM : Only 3D problem with Triangular marker elements!" << endl;

    // NDIME = 3
    FSI_mesh_file << "NDIME= 3\n";
    //
    FSI_mesh_file << "NELEM= " << nele <<"\n";
    //
    for (iele=0; iele<nele; iele++){
        FSI_mesh_file << 5 <<"\t";
        FSI_mesh_file << ele_FSI_marker(iele,0) << "\t";
        FSI_mesh_file << ele_FSI_marker(iele,1) << "\t";
        FSI_mesh_file << ele_FSI_marker(iele,2) << "\n";
    }

    FSI_mesh_file << "NPOIN= " << nnode << "\n";

    for(inode=0; inode<nnode; inode++){
        I = node_FSI_marker(inode);
        FSI_mesh_file << scientific <<node_coord(I,0)+s_u_n_one[3*I] << "\t";
        FSI_mesh_file << scientific <<node_coord(I,1)+s_u_n_one[3*I+1]  << "\t";
        FSI_mesh_file << scientific <<node_coord(I,2)+s_u_n_one[3*I+2]  << "\t";
        FSI_mesh_file << I << "\n";
    }

    FSI_mesh_file.close();


}

void SFEM::ReadinFSIForces(CConfig *config, CGeometry *geometry){
    string text_line;
    string::size_type position;
    char cstr[200];
    ifstream FSI_file;

    int iNode;
    double fx_FSI, fy_FSI, fz_FSI;


    strcpy (cstr, config->GetFSILoadFileName().c_str());
    FSI_file.open(cstr, ios::in);
    if(!FSI_file.is_open()){
        cout << "SFEM : Error! File contains FSI forces doesn't exist!!!" << endl;
        cout << "SFEM : Press any key to exit!!!" << endl;
        cin.get();
        exit(1);
    }


    while ( getline(FSI_file, text_line)){
        istringstream node_line(text_line);
        node_line >> iNode;

        if(iNode>s_nnode){
            cout << "SFEM :" << iNode << " node in FSI forces out of bound of whole model!" << endl;
            cout << "SFEM : Press any key to exit!!!" << endl;
            cin.get();
            exit(1);
        }

        node_line >> fx_FSI >> fy_FSI;
        F_ext[3*iNode] = fx_FSI;
        F_ext[3*iNode+1] = fy_FSI;

        if(geometry->GetnDim() == 3){
            node_line >> fz_FSI;
            F_ext[3*iNode+2] = fz_FSI;
        }
    }

    FSI_file.close();

}

void SFEM::OutputSolutionToFile_TECPLOT(void){
    char fname[100];
    sprintf(fname, "./solid_result_%d.dat", n_t);

    ofstream fout(fname, ios::out|ios::trunc);
    fout << "TITLE = \"FE DATA\"\n";
    fout << "VARIABLES = \"X\", \"Y\", \"Z\", \"UX\", \"UY\", \"UZ\", \"VX\", \"VY\", \"VZ\" \n";
    fout << "ZONE STRANDID=" << n_t <<", SOLUTIONTIME=" << t_n << ", NODES=" << s_nnode <<", ";
    fout << " ELEMENTS=" << s_nele << ", DATAPACKING=point, ZONETYPE=FETETRAHEDRON\n";

    for(int i=0; i<s_nnode; i++){
        fout << s_node_coord_n_one[i+0*s_nnode] << " " << s_node_coord_n_one[i+1*s_nnode] << " " << s_node_coord_n_one[i+2*s_nnode] << " ";
        fout << s_u_n_one[3*i] << " " << s_u_n_one[3*i+1] << " " << s_u_n_one[3*i+2] << " ";
        fout << s_v_n_one[3*i] << " " << s_v_n_one[3*i+1] << " " << s_v_n_one[3*i+2] << "\n";
    }

    for(int i=0; i<s_nele; i++){
        fout << s_ele_node[i+0*s_nele]+1 << " " << s_ele_node[i+1*s_nele]+1 << " ";
        fout << s_ele_node[i+2*s_nele]+1 << " "<< s_ele_node[i+3*s_nele]+1 << "\n";
    }

    fout.close();

    printf("MSG : %s at %lf is saved.\n", fname, t_n);

}

SFEM::~SFEM()
{
    //dtor
}
