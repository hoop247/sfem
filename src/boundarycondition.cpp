////////////////////////////////////////////////////////////////////////////
//            SSSSSS  FFFFFF  EEEEEE  MMM   MMM        ++       ++        //
//           SS       FF      EE      MM M M MM       ++       ++         //
//           SSSSSSS  FFFF    EEEE    MM  M  MM   ++++++++ ++++++++       //
//                SS  FF      EE      MM     MM     ++       ++           //
//                SS  FF      EE      MM     MM    ++       ++            //
//           SSSSSS   FF      EEEEEE  MM     MM   ++       ++             //
////////////////////////////////////////////////////////////////////////////
#include "boundarycondition.hpp"

CBoundaryCondition::CBoundaryCondition()
{
    //ctor

}


CBoundaryCondition::~CBoundaryCondition()
{
    //dtor
}


//+++++++++++++++++++++++++
// CBoundaryConditionCFD2D
//+++++++++++++++++++++++++
CBoundaryConditionCFD2D::CBoundaryConditionCFD2D()
{
    //ctor
    // For CFD, no displacement BCs.
    UBC_DOF.resize(0);
    UBC_VAL.resize(0);
}

CBoundaryConditionCFD2D::CBoundaryConditionCFD2D(CConfig *config, CGeometry *geometry)
{
    //ctor
    // For CFD, no displacement BCs.
    UBC_DOF.resize(0);
    UBC_VAL.resize(0);

    //Velocity Boundary Condition
    SetVelocityBC(config, geometry);

    //Pressure Boundary Condition
    SetPressureBC(config, geometry);

}


void CBoundaryConditionCFD2D::SetVelocityBC(CConfig* config, CGeometry* geometry)
{
    vector<string> marker_tags;
    unsigned short nmarker;
    string imarker_tag;
    unsigned short vx_idc, vy_idc, vz_idc; // 0 or 1
    double vx, vy, vz;

    //loop all markers
    cout << "SFEM : ----------------------  Prepare Velocity BC (CFD) --------------------" << endl;
    nmarker = config->GetnMarker_Velocity();
    for(unsigned short imarker=0; imarker < nmarker; imarker++){
        // find velocity makers
        imarker_tag = config->GetMarkerTag_Velocity(imarker);
        CBoundMarker *marker = geometry->GetMarker(imarker_tag);


        cout << "SFEM : VBC  for marker = " << imarker_tag << endl;

        vx_idc = config->GetVelocX_Indicator(imarker_tag);
        vy_idc = config->GetVelocY_Indicator(imarker_tag);
        vz_idc = config->GetVelocZ_Indicator(imarker_tag);
        if (vz_idc != 0) {
            cout << "SFEM : Error, for 2D case, VelocityZ_Indicator should be zero" << endl;
            cout << "SFEM : Press any key to exit!!!" << endl;
            cin.get();
            exit(1);
        }

        vx = config->GetVelocX(imarker_tag);
        vy = config->GetVelocY(imarker_tag);
        //vz = config->GetVelocZ(imarker_tag);
        unsigned long nbn = marker->GetnNode();
        for(unsigned long ibn=0; ibn < nbn; ibn++){
            unsigned long node_id = marker->GetNode(ibn);
            // add vx of VBC
            if(vx_idc == 1){
                VBC_DOF.push_back(2*node_id);
                VBC_VAL.push_back(vx);
            }
            // add vy of VBC
            if(vy_idc == 1){ //bug fixed
                VBC_DOF.push_back(2*node_id+1);
                VBC_VAL.push_back(vy);
            }

        }

        cout << "SFEM : " << nbn << " VBC nodes, with V = [";
        if(vx_idc == 1){
            cout << vx;
        }
        if(vy_idc == 1){
            cout << ", " << vy ;
        }
        cout << "]" << endl;
        cout << endl;
    }
}

void CBoundaryConditionCFD2D::SetPressureBC(CConfig* config, CGeometry* geometry)
{
    vector<string> marker_tags;
    unsigned short nmarker;
    string imarker_tag;
    double pressure;

    //loop all markers
    cout << "SFEM : ----------------------  Prepare Pressure BC (CFD) --------------------" << endl;
    nmarker = config->GetnMarker_Pressure();
    for(unsigned short imarker=0; imarker < nmarker; imarker++){
        // find pressure makers
        imarker_tag = config->GetMarkerTag_Pressure(imarker);
        CBoundMarker *marker = geometry->GetMarker(imarker_tag);

        cout << "SFEM :PBC for marker = " << imarker_tag << endl;
        pressure = config->GetPressure(imarker_tag);
        unsigned long nbn = marker->GetnNode();
        for(unsigned long ibn=0; ibn < nbn; ibn++){
            unsigned long node_id = marker->GetNode(ibn);
            PBC_DOF.push_back(node_id);
            PBC_VAL.push_back(pressure);
        }
        cout << "SFEM : " << nbn << " PBC nodes, with P = " << pressure  << endl;
        cout << endl;
    }
}

CBoundaryConditionCFD2D::~CBoundaryConditionCFD2D()
{
    //ctor
    // For CFD, no displacement BCs.
    UBC_DOF.clear();
    UBC_VAL.clear();

    VBC_DOF.clear();
    VBC_VAL.clear();

    PBC_DOF.clear();
    PBC_VAL.clear();

}

//+++++++++++++++++++++++++
// CBoundaryConditionCFD3D
//+++++++++++++++++++++++++
CBoundaryConditionCFD3D::CBoundaryConditionCFD3D()
{
    //ctor
    // For CFD, no displacement BCs.
    UBC_DOF.resize(0);
    UBC_VAL.resize(0);
}

CBoundaryConditionCFD3D::CBoundaryConditionCFD3D(CConfig *config, CGeometry *geometry)
{
    //ctor
    // For CFD, no displacement BCs.
    UBC_DOF.resize(0);
    UBC_VAL.resize(0);

    //Velocity Boundary Condition
    SetVelocityBC(config, geometry);

    //Pressure Boundary Condition
    SetPressureBC(config, geometry);

}


void CBoundaryConditionCFD3D::SetVelocityBC(CConfig* config, CGeometry* geometry)
{
    vector<string> marker_tags;
    unsigned short nmarker;
    string imarker_tag;
    unsigned short vx_idc, vy_idc, vz_idc; // 0 or 1
    double vx, vy, vz;

    //loop all markers
    cout << "SFEM : ----------------------  Prepare Velocity BC (CFD) --------------------" << endl;
    nmarker = config->GetnMarker_Velocity();
    for(unsigned short imarker=0; imarker < nmarker; imarker++){
        // find velocity makers
        imarker_tag = config->GetMarkerTag_Velocity(imarker);
        CBoundMarker *marker = geometry->GetMarker(imarker_tag);


        cout << "SFEM : VBC  for marker = " << imarker_tag << endl;

        vx_idc = config->GetVelocX_Indicator(imarker_tag);
        vy_idc = config->GetVelocY_Indicator(imarker_tag);
        vz_idc = config->GetVelocZ_Indicator(imarker_tag);

        vx = config->GetVelocX(imarker_tag);
        vy = config->GetVelocY(imarker_tag);
        vz = config->GetVelocZ(imarker_tag);
        unsigned long nbn = marker->GetnNode();
        for(unsigned long ibn=0; ibn < nbn; ibn++){
            unsigned long node_id = marker->GetNode(ibn);
            // add vx of VBC
            if(vx_idc == 1){
                VBC_DOF.push_back(3*node_id);
                VBC_VAL.push_back(vx);
            }
            // add vy of VBC
            if(vy_idc == 1){
                VBC_DOF.push_back(3*node_id+1);
                VBC_VAL.push_back(vy);
            }
            // add vy of VBC
            if(vz_idc == 1){
                VBC_DOF.push_back(3*node_id+2);
                VBC_VAL.push_back(vz);
            }

        }

        cout << "SFEM : " << nbn << " VBC nodes, with V = [";
        if(vx_idc == 1){
            cout << vx;
        }
        if(vy_idc == 1){
            cout << ", " << vy ;
        }
        if(vz_idc == 1){
            cout << ", " << vz ;
        }
        cout << "]" << endl;
        cout << endl;
    }
}

void CBoundaryConditionCFD3D::SetPressureBC(CConfig* config, CGeometry* geometry)
{
    vector<string> marker_tags;
    unsigned short nmarker;
    string imarker_tag;
    double pressure;

    //loop all markers
    cout << "SFEM : ----------------------  Prepare Pressure BC (CFD) --------------------" << endl;
    nmarker = config->GetnMarker_Pressure();
    for(unsigned short imarker=0; imarker < nmarker; imarker++){
        // find pressure makers
        imarker_tag = config->GetMarkerTag_Pressure(imarker);
        CBoundMarker *marker = geometry->GetMarker(imarker_tag);

        cout << "SFEM :PBC for marker = " << imarker_tag << endl;
        pressure = config->GetPressure(imarker_tag);
        unsigned long nbn = marker->GetnNode();
        for(unsigned long ibn=0; ibn < nbn; ibn++){
            unsigned long node_id = marker->GetNode(ibn);
            PBC_DOF.push_back(node_id);
            PBC_VAL.push_back(pressure);
        }
        cout << "SFEM : " << nbn << " PBC nodes, with P = " << pressure  << endl;
        cout << endl;
    }
}

CBoundaryConditionCFD3D::~CBoundaryConditionCFD3D()
{
    //ctor
    // For CFD, no displacement BCs.
    UBC_DOF.clear();
    UBC_VAL.clear();

    VBC_DOF.clear();
    VBC_VAL.clear();

    PBC_DOF.clear();
    PBC_VAL.clear();

}

//+++++++++++++++++++++++++
// CBoundaryConditionCSD2D
//+++++++++++++++++++++++++
CBoundaryConditionCSD2D::CBoundaryConditionCSD2D(){
    //ctor
    //For CSD, pressure is not a independent field variable
    PBC_DOF.resize(0);
    PBC_VAL.resize(0);
}

CBoundaryConditionCSD2D::CBoundaryConditionCSD2D(CConfig* config, CGeometry* geometry){
    //ctor
    PBC_DOF.resize(0);
    PBC_VAL.resize(0);

    //VBC
    SetVelocityBC(config, geometry);

    //UBC
    SetDisplacementBC(config, geometry);

}

void CBoundaryConditionCSD2D::SetVelocityBC(CConfig* config, CGeometry* geometry){
    vector<string> marker_tags;
    unsigned short nmarker;
    string imarker_tag;
    unsigned short vx_idc, vy_idc, vz_idc; // 0 or 1
    double vx, vy, vz;

    //loop all markers
    cout << "SFEM : ----------------------  Prepare Velocity BC (CSD) --------------------" << endl;
    nmarker = config->GetnMarker_Velocity();
    for(unsigned short imarker=0; imarker < nmarker; imarker++){
        // find velocity makers
        imarker_tag = config->GetMarkerTag_Velocity(imarker);
        CBoundMarker *marker = geometry->GetMarker(imarker_tag);


        cout << "SFEM : VBC  for marker = " << imarker_tag << endl;

        vx_idc = config->GetVelocX_Indicator(imarker_tag);
        vy_idc = config->GetVelocY_Indicator(imarker_tag);
        vz_idc = config->GetVelocZ_Indicator(imarker_tag);
        if (vz_idc != 0) {
            cout << "SFEM : Error, for 2D case, VelocityZ_Indicator should be zero" << endl;
            cout << "SFEM : Press any key to exit!!!" << endl;
            cin.get();
            exit(1);
        }

        vx = config->GetVelocX(imarker_tag);
        vy = config->GetVelocY(imarker_tag);
        //vz = config->GetVelocZ(imarker_tag);
        unsigned long nbn = marker->GetnNode();
        for(unsigned long ibn=0; ibn < nbn; ibn++){
            unsigned long node_id = marker->GetNode(ibn);
            // add vx of VBC
            if(vx_idc == 1){
                VBC_DOF.push_back(3*node_id);
                VBC_VAL.push_back(vx);
            }
            // add vy of VBC
            if(vy_idc == 1){
                VBC_DOF.push_back(3*node_id+1);
                VBC_VAL.push_back(vy);
            }
            // add vy of VBC
//            if(vz_idc == 1){
//                VBC_DOF.push_back(3*node_id+2);
//                VBC_VAL.push_back(vz);
//            }

        }

        cout << "SFEM : " << nbn << " VBC nodes, with V = [";
        if(vx_idc == 1){
            cout << vx;
        }
        cout << ", ";
        if(vy_idc == 1){
            cout  << vy ;
        }
        cout << "]" << endl;
        cout << endl;
    }
}

void CBoundaryConditionCSD2D::SetDisplacementBC(CConfig *config, CGeometry *geometry){
    vector<string> marker_tags;
    unsigned short nmarker;
    string imarker_tag;
    unsigned short ux_idc, uy_idc, uz_idc; // 0 or 1
    double ux, uy, uz;

    //loop all markers
    cout << "SFEM : ----------------------  Prepare Displacement BC (CSD) --------------------" << endl;
    nmarker = config->GetnMarker_Displacement();
    for(unsigned short imarker=0; imarker < nmarker; imarker++){
        // find velocity makers
        imarker_tag = config->GetMarkerTag_Displacement(imarker);
        CBoundMarker *marker = geometry->GetMarker(imarker_tag);


        cout << "SFEM : UBC  for marker = " << imarker_tag << endl;

        ux_idc = config->GetDisplX_Indicator(imarker_tag);
        uy_idc = config->GetDisplY_Indicator(imarker_tag);
        uz_idc = config->GetDisplZ_Indicator(imarker_tag);
        if (uz_idc != 0) {
            cout << "SFEM : Error, for 2D case, DisplacementZ_Indicator should be zero" << endl;
            cout << "SFEM : Press any key to exit!!!" << endl;
            cin.get();
            exit(1);
        }

        ux = config->GetDisplX(imarker_tag);
        uy = config->GetDisplY(imarker_tag);
//        uz = config->GetDisplZ(imarker_tag);
        unsigned long nbn = marker->GetnNode();
        for(unsigned long ibn=0; ibn < nbn; ibn++){
            unsigned long node_id = marker->GetNode(ibn);
            // add ux of UBC
            if(ux_idc == 1){
                UBC_DOF.push_back(3*node_id);
                UBC_VAL.push_back(ux);
            }
            // add uy of UBC
            if(uy_idc == 1){
                UBC_DOF.push_back(3*node_id+1);
                UBC_VAL.push_back(uy);
            }
            // add uy of UBC
//            if(uz_idc == 1){
//                UBC_DOF.push_back(3*node_id+2);
//                UBC_VAL.push_back(uz);
//            }

        }

        cout << "SFEM : " << nbn << " UBC nodes, with V = [";
        if(ux_idc == 1){
            cout << ux;
        }
        if(uy_idc == 1){
            cout << ", " << uy ;
        }
        cout << "]" << endl;
        cout << endl;
    }
}


//+++++++++++++++++++++++++
// CBoundaryConditionCSD3D
//+++++++++++++++++++++++++
CBoundaryConditionCSD3D::CBoundaryConditionCSD3D(){
    //ctor
    //For CSD, pressure is not a independent field variable
    PBC_DOF.resize(0);
    PBC_VAL.resize(0);
}

CBoundaryConditionCSD3D::CBoundaryConditionCSD3D(CConfig *config, CGeometry *geometry){
    //ctor
    PBC_DOF.resize(0);
    PBC_VAL.resize(0);

    //VBC
    SetVelocityBC(config, geometry);

    //UBC
    SetDisplacementBC(config, geometry);

}

CBoundaryConditionCSD2D::~CBoundaryConditionCSD2D(){
    //dctor
}

void CBoundaryConditionCSD3D::SetVelocityBC(CConfig *config, CGeometry *geometry){
    vector<string> marker_tags;
    unsigned short nmarker;
    string imarker_tag;
    unsigned short vx_idc, vy_idc, vz_idc; // 0 or 1
    double vx, vy, vz;

    //loop all markers
    cout << "SFEM : ----------------------  Prepare Velocity BC (CSD) --------------------" << endl;
    nmarker = config->GetnMarker_Velocity();
    for(unsigned short imarker=0; imarker < nmarker; imarker++){
        // find velocity makers
        imarker_tag = config->GetMarkerTag_Velocity(imarker);
        CBoundMarker *marker = geometry->GetMarker(imarker_tag);


        cout << "SFEM : VBC  for marker = " << imarker_tag << endl;

        vx_idc = config->GetVelocX_Indicator(imarker_tag);
        vy_idc = config->GetVelocY_Indicator(imarker_tag);
        vz_idc = config->GetVelocZ_Indicator(imarker_tag);

        vx = config->GetVelocX(imarker_tag);
        vy = config->GetVelocY(imarker_tag);
        vz = config->GetVelocZ(imarker_tag);
        unsigned long nbn = marker->GetnNode();
        for(unsigned long ibn=0; ibn < nbn; ibn++){
            unsigned long node_id = marker->GetNode(ibn);
            // add vx of VBC
            if(vx_idc == 1){
                VBC_DOF.push_back(3*node_id);
                VBC_VAL.push_back(vx);
            }
            // add vy of VBC
            if(vy_idc == 1){
                VBC_DOF.push_back(3*node_id+1);
                VBC_VAL.push_back(vy);
            }
            // add vy of VBC
            if(vz_idc == 1){
                VBC_DOF.push_back(3*node_id+2);
                VBC_VAL.push_back(vz);
            }

        }

        cout << "SFEM : " << nbn << " VBC nodes, with V = [";
        if(vx_idc == 1){
            cout << vx;
        }
        cout << ", ";
        if(vy_idc == 1){
            cout  << vy ;
        }
        cout << ", ";
        if(vz_idc == 1){
            cout  << vz ;
        }
        cout << "]" << endl;
        cout << endl;
    }
}

void CBoundaryConditionCSD3D::SetDisplacementBC(CConfig *config, CGeometry *geometry){
    vector<string> marker_tags;
    unsigned short nmarker;
    string imarker_tag;
    unsigned short ux_idc, uy_idc, uz_idc; // 0 or 1
    double ux, uy, uz;

    //loop all markers
    cout << "SFEM : ----------------------  Prepare Displacement BC (CSD) --------------------" << endl;
    nmarker = config->GetnMarker_Displacement();
    for(unsigned short imarker=0; imarker < nmarker; imarker++){
        // find velocity makers
        imarker_tag = config->GetMarkerTag_Displacement(imarker);
        CBoundMarker *marker = geometry->GetMarker(imarker_tag);


        cout << "SFEM : UBC  for marker = " << imarker_tag << endl;

        ux_idc = config->GetDisplX_Indicator(imarker_tag);
        uy_idc = config->GetDisplY_Indicator(imarker_tag);
        uz_idc = config->GetDisplZ_Indicator(imarker_tag);

        ux = config->GetDisplX(imarker_tag);
        uy = config->GetDisplY(imarker_tag);
        uz = config->GetDisplZ(imarker_tag);
        unsigned long nbn = marker->GetnNode();
        for(unsigned long ibn=0; ibn < nbn; ibn++){
            unsigned long node_id = marker->GetNode(ibn);
            // add ux of UBC
            if(ux_idc == 1){
                UBC_DOF.push_back(3*node_id);
                UBC_VAL.push_back(ux);
            }
            // add uy of UBC
            if(uy_idc == 1){
                UBC_DOF.push_back(3*node_id+1);
                UBC_VAL.push_back(uy);
            }
            // add uy of UBC
            if(uz_idc == 1){
                UBC_DOF.push_back(3*node_id+2);
                UBC_VAL.push_back(uz);
            }

        }

        cout << "SFEM : " << nbn << " UBC nodes, with V = [";
        if(ux_idc == 1){
            cout << ux;
        }
        if(uy_idc == 1){
            cout << ", " << uy ;
        }
        if(uz_idc == 1){
            cout << ", " << uz ;
        }
        cout << "]" << endl;
        cout << endl;
    }
}

CBoundaryConditionCSD3D::~CBoundaryConditionCSD3D(){
    //dctor
}
