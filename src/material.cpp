////////////////////////////////////////////////////////////////////////////
//            SSSSSS  FFFFFF  EEEEEE  MMM   MMM        ++       ++        //
//           SS       FF      EE      MM M M MM       ++       ++         //
//           SSSSSSS  FFFF    EEEE    MM  M  MM   ++++++++ ++++++++       //
//                SS  FF      EE      MM     MM     ++       ++           //
//                SS  FF      EE      MM     MM    ++       ++            //
//           SSSSSS   FF      EEEEEE  MM     MM   ++       ++             //
////////////////////////////////////////////////////////////////////////////
#include "material.hpp"

CMaterial::CMaterial()
{
    //ctor
}

CMaterial::~CMaterial()
{
    //dtor
}

//+++++++++++++++++++++++++++++++++
// CMaterialSolid
//+++++++++++++++++++++++++++++++++

CMaterialSolid::CMaterialSolid(){
    //ctor
}

void CMaterialSolid::GetGPsPK2Stress(vector<vector<double> > &gp_S, vector<vector<double> > &gp_F,
                        unsigned short mat_type_val, double* mat_para_val, unsigned short ndim){
    nDim = ndim;
    mat_type = mat_type_val;
    mat_para = mat_para_val;


    //-----------------------------------------------------------------
    if(nDim == 2){
        a1[0] = 0; a1[1] = 1; a1[2] = 0;
        a2[0] = 0; a2[1] = 1; a2[2] = 2;
    }
    else{
        a1[0] = 0; a1[1] = 1; a1[2] = 2; a1[3] = 0; a1[4] = 1; a1[5] = 2;
        a2[0] = 0; a2[1] = 1; a2[2] = 2; a2[3] = 1; a2[4] = 2; a2[5] = 0;
    }


    if(nDim == 2){
        Delta[0][0] = 1.0;  Delta[0][1] = 0.0;
        Delta[1][0] = 0.0;  Delta[1][1] = 1.0;
    }
    else{
        Delta[0][0] = 1.0;  Delta[0][1] = 0.0; Delta[0][2] = 0.0;
        Delta[1][0] = 0.0;  Delta[1][1] = 1.0; Delta[1][2] = 0.0;
        Delta[2][0] = 0.0;  Delta[2][1] = 0.0; Delta[2][2] = 1.0;
    }

    //--------------------------------------------------------------
    vector<vector<double> >  D(6, vector<double>(6) );
    if(mat_type == ST_VENANT){
        double E = mat_para_val[1];
        double v = mat_para_val[2];
        double E0, v0;

        for(unsigned short i = 0; i < 6; i++)
            for(unsigned short j = 0; j < 6; j++)
                D[i][j] = 0.0;

        double D0;
        if(ndim == 2){
//            cout << "SFEM : Error, Only 3D material is ready to use!" << endl;
//            cout << "SFEM : Press any key to exit!!!" << endl;
//            cin.get();
//            exit(1);
            E0 = E/(1.0 - v*v);
            v0 = v/(1.0 - v);
            D0 = E0/(1.0-v0*v0);

            D[0][0] = D0;  D[0][1] = D0*v0;  D[0][2] = 0.0;
            D[1][0] = D[0][1];  D[1][1] = D0;  D[1][2] = 0.0;
            D[2][0] = D[0][2];  D[2][1] = D[1][2];  D[2][2] = D0*(1.0-v0)/2.0;
        }
        else{
            D0 = E*(1.0-v)/((1.0+v)*(1.0-2.0*v));
            D[0][0] = D0;      D[0][1] = D0*v/(1.0-v); D[0][2] = D[0][1];
            D[1][0] = D[0][1]; D[1][1] = D0;           D[1][2] = D[0][1];
            D[2][0] = D[0][2]; D[2][1] = D[1][2];      D[2][2] = D0;
            D[3][3] = D0*(1.0 - 2.0*v)/(2.0*(1.0-v));
            D[4][4] = D[3][3];
            D[5][5] = D[3][3];
        }
    }

    //--------------------------------------------------------------
    unsigned long ngp = gp_F.size();
    // loop all gp
    for (unsigned long igp = 0; igp < ngp; igp++){
        vector<vector<double> > igp_F;
        igp_F.resize(nDim, vector<double>(nDim));
        // get igp_F
        for (unsigned short i = 0; i < nDim; i++){
            for (unsigned short j = 0; j < nDim; j++){
                igp_F[i][j] = gp_F[igp][i*nDim+j]; //be careful here.
            }
        }
        // define igp_S
        vector<double> igp_S((nDim-1)*3, 0.0);

        if(mat_type == ST_VENANT){
            GetiGPPK2Stress_StVenant(igp_S, igp_F, D);
        }
        else{
            cout << "SFEM : Error, Only St_Venat material is ready to use!" << endl;
            cout << "SFEM : Press any key to exit!!!" << endl;
            cin.get();
            exit(1);
        }

        gp_S[igp] = igp_S;

    }



}

void CMaterialSolid::GetiGPPK2Stress_StVenant(vector<double> &S, vector<vector<double> > &F, vector<vector<double> > &D){

    //get Green strain
    double C[3][3], E[3][3];
    for(unsigned short i = 0; i < nDim; i++){
        for(unsigned short j = 0; j < nDim; j++){
            C[i][j] = 0.0;
            for(unsigned short k=0; k < nDim; k++){
                C[i][j] = C[i][j] + F[k][i]*F[k][j]; //C = F^t*F;
            }
            E[i][j] = (C[i][j] - Delta[i][j])/2.0;
        }
    }

    double GE[6];
    unsigned short nE = (nDim-1)*3;

    for(unsigned short k = 0; k < nE; k++){
        unsigned short i = a1[k];
        unsigned short j = a2[k];
        GE[k] = E[i][j];
    }

    //get PK2 stress
    for(unsigned short i = 0; i < nE; i++){
        for(unsigned short j = 0; j < nE; j++){
            S[i] = S[i] + D[i][j]*GE[j];
        }
    }
}



CMaterialSolid::~CMaterialSolid(){
    //ctor
}

