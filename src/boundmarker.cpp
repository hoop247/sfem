////////////////////////////////////////////////////////////////////////////
//            SSSSSS  FFFFFF  EEEEEE  MMM   MMM        ++       ++        //
//           SS       FF      EE      MM M M MM       ++       ++         //
//           SSSSSSS  FFFF    EEEE    MM  M  MM   ++++++++ ++++++++       //
//                SS  FF      EE      MM     MM     ++       ++           //
//                SS  FF      EE      MM     MM    ++       ++            //
//           SSSSSS   FF      EEEEEE  MM     MM   ++       ++             //
////////////////////////////////////////////////////////////////////////////
#include "boundmarker.hpp"

CBoundMarker::CBoundMarker(){
    //ctor
    nBoundElems = 0;
    nNodes = 0;

}

void CBoundMarker::SetBoundElemsCGsAll(vector<vector<double> > &node_coord){
    unsigned long iBE, nBE;

    nBE = GetnBoundElem();

    for(iBE = 0; iBE < nBE; iBE++){
        // judge the type of boundary element
        CElem* BE = BoundElems[iBE];
        vector<unsigned long> BENodes = BE->GetNodes();
        vector<double> BECG_coord;
        vector<double> p0_xyz, p1_xyz, p2_xyz;
        // call corresponding functions to calculate the coordinates of this boundary element's CG
        switch(BE->GetType()){
            case TRIANGLE:{
//                p0_xyz = conv_to<vector<double> >::from(node_coord.row(BENodes[0]));
//                p1_xyz = conv_to<vector<double> >::from(node_coord.row(BENodes[1]));
//                p2_xyz = conv_to<vector<double> >::from(node_coord.row(BENodes[2]));
                p0_xyz = node_coord[BENodes[0]];
                p1_xyz = node_coord[BENodes[1]];
                p2_xyz = node_coord[BENodes[2]];
                BECG_coord = GetCG_TRI(p0_xyz, p1_xyz, p2_xyz);
                BoundElemsCGs.push_back(BECG_coord);
                break;
            }
            case LINE:{
//                p0_xyz = conv_to<vector<double> >::from(node_coord.row(BENodes[0]));
//                p1_xyz = conv_to<vector<double> >::from(node_coord.row(BENodes[1]));
                p0_xyz = node_coord[BENodes[0]];
                p1_xyz = node_coord[BENodes[1]];
                BECG_coord = GetCG_LINE(p0_xyz, p1_xyz);
                BoundElemsCGs.push_back(BECG_coord);
                break;
            }
        }
    }
}

void CBoundMarker::SetBoundElemsNormalsAll(vector<vector<double> > &node_coord){
    // TODO 2015-08-26-11.49
    unsigned long iBE, nBE;
    nBE = GetnBoundElem();

    //loop all boundary elements in this marker
    for(iBE=0; iBE<nBE; iBE++){
        // judge the type of boundary element
        CElem* BE = BoundElems[iBE]; // be careful about the deep copy and shallow copy.
        vector<unsigned long> BENodes = BE->GetNodes();
        vector<double> BENormal;
        vector<double> p0_xyz, p1_xyz, p2_xyz;
        unsigned short type = BE->GetType();
        // call corresponding functions to calculate the normal of this boundary element
        switch(type){
            case TRIANGLE : {
//                p0_xyz = conv_to<vector<double> >::from(node_coord.row(BENodes[0]));
//                p1_xyz = conv_to<vector<double> >::from(node_coord.row(BENodes[1]));
//                p2_xyz = conv_to<vector<double> >::from(node_coord.row(BENodes[2]));
                p0_xyz = node_coord[BENodes[0]];
                p1_xyz = node_coord[BENodes[1]];
                p2_xyz = node_coord[BENodes[2]];
                BENormal = GetNormal_TRI(p0_xyz, p1_xyz, p2_xyz);
                BoundElemsNormals.push_back(BENormal);
                break;
            }
            case LINE : {
//                p0_xyz = conv_to<vector<double> >::from(node_coord.row(BENodes[0]));
//                p1_xyz = conv_to<vector<double> >::from(node_coord.row(BENodes[1]));
                p0_xyz = node_coord[BENodes[0]];
                p1_xyz = node_coord[BENodes[1]];
                BENormal = GetNormal_LINE(p0_xyz, p1_xyz);
                BoundElemsNormals.push_back(BENormal);
                break;
            }
        }

    }//end loop
}

void CBoundMarker::SetBoundElemsVolAll(vector<vector<double> > &node_coord){
    unsigned long nBE = GetnBoundElem();

    for(unsigned long iBE=0; iBE<nBE; iBE++){
        // judge the type of boundary element
        CElem* BE = BoundElems[iBE]; // be careful about the deep copy and shallow copy.
        vector<unsigned long> BENodes = BE->GetNodes();
        vector<double> BENormal;
        vector<double> p0_xyz, p1_xyz, p2_xyz;
        unsigned short type = BE->GetType();
        double vol;

        switch (type){
            case TRIANGLE:{
//                p0_xyz = conv_to<vector<double> >::from(node_coord.row(BENodes[0]));
//                p1_xyz = conv_to<vector<double> >::from(node_coord.row(BENodes[1]));
//                p2_xyz = conv_to<vector<double> >::from(node_coord.row(BENodes[2]));
                p0_xyz = node_coord[BENodes[0]];
                p1_xyz = node_coord[BENodes[1]];
                p2_xyz = node_coord[BENodes[2]];
                vol = GetArea_TRI(p0_xyz, p1_xyz, p2_xyz);
                BoundElemsVols.push_back(vol);
                break;
            }

            case LINE : {
//                p0_xyz = conv_to<vector<double> >::from(node_coord.row(BENodes[0]));
//                p1_xyz = conv_to<vector<double> >::from(node_coord.row(BENodes[1]));
                p0_xyz = node_coord[BENodes[0]];
                p1_xyz = node_coord[BENodes[1]];
                vol = GetLength_LINE(p0_xyz, p1_xyz);
                BoundElemsVols.push_back(vol);
                break;
            }
        }

    }

}
vector<double> CBoundMarker::GetCG_TRI(vector<double> &p0_xyz, vector<double> &p1_xyz, vector<double> &p2_xyz){
    vector<double> cg_coord;

    cg_coord.push_back((p0_xyz[0] + p1_xyz[0] + p2_xyz[0])/3.0);
    cg_coord.push_back((p0_xyz[1] + p1_xyz[1] + p2_xyz[1])/3.0);
    if(p0_xyz.size() == 3) // nDim=3
        cg_coord.push_back((p0_xyz[2] + p1_xyz[2] + p2_xyz[2])/3.0);

    return cg_coord;
}

vector<double> CBoundMarker::GetCG_LINE(vector<double> &p0_xyz, vector<double> &p1_xyz){
    vector<double> cg_coord;

    cg_coord.push_back((p0_xyz[0] + p1_xyz[0])/2.0);
    cg_coord.push_back((p0_xyz[1] + p1_xyz[1])/2.0);
    if(p0_xyz.size() == 3)
        cg_coord.push_back((p0_xyz[2] + p1_xyz[2])/2.0);

    return cg_coord;
}

vector<double> CBoundMarker::GetNormal_TRI(vector<double> &p0_xyz, vector<double> &p1_xyz, vector<double> &p2_xyz){
    vector<double> normal; // outward direction
    unsigned short ndim = p0_xyz.size();
    double normal_length;
    // calculate edge0, edge1;
    vector<double> edge0, edge1; //[0,1],[0,2]
    for(int i=0; i<ndim; i++){
        edge0.push_back(p2_xyz[i] - p0_xyz[i]);
        edge1.push_back(p1_xyz[i] - p2_xyz[i]);
    }

    //axb = (a0*b2-a2*b1, a2*b0-a0*b2, a0*b1-a1*b0)
    normal.push_back(edge0[1]*edge1[2] - edge0[2]*edge1[1]);
    normal.push_back(edge0[3]*edge1[0] - edge0[0]*edge1[2]);
    normal_length = sqrt(normal[0]*normal[0] + normal[1]*normal[1]);
    if(ndim == 3){
        normal.push_back(edge0[0]*edge1[1] - edge0[1]*edge1[0]);
        normal_length = sqrt(normal[0]*normal[0] + normal[1]*normal[1] + normal[2]*normal[2]);
    }

    for (int i=0; i<ndim; i++)
      normal[i] = normal[i]/normal_length;

    return normal;
}

vector<double> CBoundMarker::GetNormal_LINE(vector<double> &p0_xyz, vector<double> &p1_xyz){
    vector<double> normal;
    double normal_length;
    unsigned short ndim = p0_xyz.size();
    if(p0_xyz.size()>2){
        cout << "SFEM: Error - CBoundMarker::GetNormal_LINE,";
        cout << "Line type boundary element for marker only avialiable for 2D case now!" << endl;
        cout << "SFEM : Press any key to exit!!!" << endl;
        cin.get();
        exit(1);
    }
    // calculate edge0, edge1;
    vector<double> edge0, edge1; //[0,1],[z-axis]
    for(unsigned long i=0; i<p0_xyz.size(); i++){
        edge0.push_back(p1_xyz[i] - p0_xyz[i]);
    }

    normal.push_back(-edge0[1]/edge0[0]);
    normal.push_back(1.0);
    normal_length = sqrt(normal[0]*normal[0] + normal[1]*normal[1]);
    for (int i=0; i<ndim; i++)
      normal[i] = normal[i]/normal_length;

    return normal;
}

void CBoundMarker::SetNodes(void){
    unsigned long nBE = GetnBoundElem();
    vector<unsigned long> BENodes;

    if(BoundElems.size() < 1){
        cout << "SFEM : Error - CBoundMarker::SetNodes, No initialization of boundary elements of MARKER " << GetTag() << endl;
        cout << "SFEM : Press any key to exit!!!" << endl;
        cin.get();
        exit(1);
    }

    for(unsigned long iBE = 0; iBE < nBE; iBE++){
        CElem* BE = BoundElems[iBE];
        unsigned short nnode = BE->GetnNodes();
        vector<unsigned long> nodes = BE->GetNodes();
        for(unsigned short inode=0; inode < nnode; inode++)
            BENodes.push_back(nodes[inode]);
    }

    sort(BENodes.begin(), BENodes.end());
    BENodes.erase(unique(BENodes.begin(), BENodes.end()), BENodes.end());

    Nodes = BENodes;
    nNodes = Nodes.size();
}

//For debug usage
void CBoundMarker::Display(){
    cout << "CBoundMarker.Display() is using for debugging!" << endl;
    cout << "CBoundMarker.Tag: " << GetTag() << endl;
    cout << "CBoundMarker.nBoundElems: " << GetnBoundElem() << endl;
    cout << "CBoundMarker.nNodes: " << GetnNode() << endl;
    for(int i=0; i<GetnBoundElem(); i++){
        cout << "CBoundMarker.BoundElemsNormals[" << i << "] :" << BoundElemsNormals[i][0];
        cout << ", "<< BoundElemsNormals[i][1] << ", "<< BoundElemsNormals[i][2] << endl;
    }


    for(int i=0; i<GetnBoundElem(); i++){
        cout << "CBoundMarker.BoundElemsCGs[" << i << "] :" << BoundElemsCGs[i][0];
        cout << ", "<< BoundElemsCGs[i][1] << ", "<< BoundElemsCGs[i][2] << endl;
    }

    for(int i=0; i<GetnBoundElem(); i++){
        cout << "CBoundMarker.BoundElemsVOLs[" << i << "] :" << BoundElemsVols[i]<< endl;
    }

    cout << endl;
}

CBoundMarker::~CBoundMarker(){
    //dtor
}

double CBoundMarker::GetArea_TRI(vector<double>& p0_xyz, vector<double>& p1_xyz, vector<double>& p2_xyz){
    double area, I, I0, I1, I2;
    int ndim;
    ndim = p0_xyz.size();

    double x0, y0, z0;
    double x1, y1, z1;
    double x2, y2, z2;
    x0 = p0_xyz[0];
    y0 = p0_xyz[1];
    x1 = p1_xyz[0];
    y1 = p1_xyz[1];
    x2 = p2_xyz[0];
    y2 = p2_xyz[1];

    if(ndim == 2)
    {
        area = 0.5*fabs((x0-x2)*(y1-y0) - (x0-x1)*(y2-y0));
    }
    else if(ndim == 3)
    {
        z0 = p0_xyz[2];
        z1 = p1_xyz[2];
        z2 = p2_xyz[2];
        I0 = sqrt((x0-x1)*(x0-x1) + (y0-y1)*(y0-y1) + (z0-z1)*(z0-z1));
        I1 = sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) + (z2-z1)*(z2-z1));
        I2 = sqrt((x0-x2)*(x0-x2) + (y0-y2)*(y0-y2) + (z0-z2)*(z0-z2));
        I = (I0+I1+I2)*0.5;
        area = sqrt(I*(I-I0)*(I-I1)*(I-I2));
    }

    return area;
}

double CBoundMarker::GetLength_LINE(vector<double>& p0_xyz, vector<double>& p1_xyz){
    double length;
    double x0, x1;
    double y0, y1;

    x0 = p0_xyz[0]; y0 = p0_xyz[1];
    x1 = p1_xyz[0]; y1 = p1_xyz[1];

    length = sqrt((x0-x1)*(x0-x1) + (y0-y1)*(y0-y1));

    return length;
}
