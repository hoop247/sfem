# -*- coding: utf-8 -*-
"""
Code for convert the hypermesh-style .inp to SU2.
Now only for TET4 and Tri3.

Brief: when create mesh in Hypermesh (HM), only need to add "Elem" before your 
       HM component contains elements. Also add "Marker" before your HM 
       component contains boundary elements. Then export in HM as ABAQUS .inp file.
       Enjoy!

Created on Thu Jul 09 14:49:05 2015

@author: Chen Jiang
"""

import gc

nnode = 0
nele = 0
nele_bound = 0
nele_TET = 0
nele_TRI = 0
nele_bound_TET = 0
nele_bound_TRI = 0
nMarker = 0
Marker_tag = []
Marker_ele = []
node_ID = []
node_coord_x = []
node_coord_y = []
node_coord_z = []
ele_TET = []
ele_TRI = []
ele_WEDGE = []
ele_bound_TRI = []

print "NOTE: The .inp file in HM should always renumbered first, otherwise SU2_CFD will give segement fault!"
inpfilename = raw_input("Please type in the file name of .inp :")
su2filename = raw_input("Please type in the file name of .su2 :")

#------------------------------------------------------------------------------
# 1. Open the .inp file                                                       |
#------------------------------------------------------------------------------


fin = open(inpfilename)
print "--------- Start  read in .inp file -------------------"

#------------------------------------------------------------------------------
# 2. Read in .inp file row-by-row                                             |
#------------------------------------------------------------------------------
line = [];
line = fin.readline()
while line:
    line = line.strip(' \n\t\r')
    line_split = line.split(',')
    #--------------------------------------------------------------------------
    # 2.1 If this line contain target .inp keyword                            |
    #--------------------------------------------------------------------------
    # 2.1.1 Read in lines contain node info
    
    if line_split[0] == "*NODE" :
        print "Start read in *NODE block"        
        line_node = fin.readline()        
        while line_node:
            line_node = line_node.strip(' \n\t\r')
            line_node_sp = line_node.split(',') # judge this line is/not a keyword line
            if line_node_sp[0].isdigit():
                node_ID.append(int(line_node_sp[0])-1) # because .su2 is 0-index format
                node_coord_x.append(float(line_node_sp[1]))
                node_coord_y.append(float(line_node_sp[2]))
                if len(line_node_sp)>2: node_coord_z.append(float(line_node_sp[3]))
            else:
                break            
            line_node = fin.readline()
        print "End read in *NODE block"
            
    # 2.1.2 Read in lines contain ele info
    if (line_split[0] == "*ELEMENT") and ('Elem' in line_split[2]):
       print "Start read in *ELEMENT block for " + line_split[2]  
       line_ele = fin.readline()
       while line_ele:
           line_ele = line_ele.strip(' \n\t\r')
           line_ele_sp = line_ele.split(',')
           if line_ele_sp[0].isdigit() and 'C3D4' in line_split[1]:
               # ID, node1, node2, node3, node4
               ele_TET.append([int(line_ele_sp[0])-1, int(line_ele_sp[1])-1, int(line_ele_sp[2])-1, int(line_ele_sp[3])-1, int(line_ele_sp[4])-1 ])
           elif line_ele_sp[0].isdigit() and 'S3' in line_split[1]:
               ele_TRI.append( [ int(line_ele_sp[0])-1, int(line_ele_sp[1])-1, int(line_ele_sp[2])-1, int(line_ele_sp[3])-1] )
           elif line_ele_sp[0].isdigit() and 'C3D6' in line_split[1]:
               ele_WEDGE.append( [int(line_ele_sp[0])-1, int(line_ele_sp[1])-1, int(line_ele_sp[2])-1, int(line_ele_sp[3])-1, 
                                  int(line_ele_sp[4])-1, int(line_ele_sp[5])-1, int(line_ele_sp[6])-1])
           else:
               break
           line_ele = fin.readline()
       print "End read in *ELEMENT block for "  + line_split[2]
    
     
    # 2.1.3 Read in lines contain mark info 
    if (line_split[0]) == "*ELEMENT" and ('Marker' in line_split[2]):
        print "Start read in *ELEMENT block for " + line_split[2]
        #iMarker_tag = line_split[2].lstrip('ELSET=Marker')
        iMarker_tag = line_split[2][12:]
        Marker_tag.append(iMarker_tag)
        ele_bound_TRI = []
        line_marker = fin.readline()
        while line_marker:
            line_marker = line_marker.strip(' \n\t\r')
            line_marker_sp = line_marker.split(',')
            if line_marker_sp[0].isdigit() and 'S3' in line_split[1]:
                # ID, node1, node2, node3
                ele_bound_TRI.append( [ int(line_marker_sp[1])-1, int(line_marker_sp[2])-1, int(line_marker_sp[3])-1 ] )
            #Todo add elif line boundary elements
            else:
                print "Marker element type in .inp file is not S3!"
                break                 
            line_marker = fin.readline()
        print "End read in *ELEMENT block for " + line_split[2] 
        Marker_ele.append(ele_bound_TRI)
    

    line = fin.readline()
    
print "--------- Success in read in .inp file -------------------"
del ele_bound_TRI
gc.collect()
fin.close()

#------------------------------------------------------------------------------
# 3. Write .su2 file                                                          |
#------------------------------------------------------------------------------
nnode = len(node_ID)
nele = len(ele_TET) + len(ele_TRI) + len(ele_WEDGE) #Todo: add more option for future 3D Tri elements\
nMarker = len(Marker_tag)
nDim =2
if len(node_coord_z) > 0:
    nDim = 3

fout = open(su2filename, 'w')
print "--------- Start write .su2 file -------------------"
#write NDIME
fout.write("NDIME= %s\n" % nDim)
#write NELEM
fout.write("NELEM= %s\n" % nele)
#if elements are TET
if len(ele_TET) > 0:
    for iele in range(len(ele_TET)):        
        fout.write("10\t{}\t{}\t{}\t{}\t{}\n".format(ele_TET[iele][1], 
                    ele_TET[iele][2], ele_TET[iele][3], ele_TET[iele][4],
                    ele_TET[iele][0]))
elif len(ele_TRI) > 0:
    for iele in range(len(ele_TRI)):        
        fout.write("5\t{}\t{}\t{}\t{}\n".format(ele_TRI[iele][1], 
                    ele_TRI[iele][2], ele_TRI[iele][3],
                    ele_TRI[iele][0]))
if len(ele_WEDGE) > 0:
    for iele in range(len(ele_WEDGE)):
        fout.write("13\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(ele_WEDGE[iele][1],
                    ele_WEDGE[iele][2], ele_WEDGE[iele][3], ele_WEDGE[iele][4],
                    ele_WEDGE[iele][5], ele_WEDGE[iele][6], ele_WEDGE[iele][0]))
    
    
#write NPOIN
fout.write("NPOIN= %s\n" % nnode)
for inode in range(nnode):
    if nDim == 2:
        fout.write("{}\t{}\t{}\n".format(node_coord_x[inode],
                   node_coord_y[inode], node_ID[inode]))
    elif nDim == 3:
        fout.write("{}\t{}\t{}\t{}\n".format(node_coord_x[inode],
                   node_coord_y[inode], node_coord_z[inode], node_ID[inode]))
    else:
        pass

#write NMARK
if nMarker >=1:
    fout.write("NMARK= %s\n" % nMarker)
    for iMarker in range(nMarker):
        fout.write("MARKER_TAG= %s\n" % Marker_tag[iMarker])    
        n = len(Marker_ele[iMarker])
        fout.write("MARKER_ELEMS= %s\n" % n)
        if len(Marker_ele[iMarker][0]) > 3:
            print "Error : Marker element is not S3"
        if len(Marker_ele[iMarker][0]) == 3:    
            for i in range(n):
                fout.write("5\t{}\t{}\t{}\n".format(Marker_ele[iMarker][i][0],
                    Marker_ele[iMarker][i][1], Marker_ele[iMarker][i][2]))
        elif len(Marker_ele[iMarker][0]) == 2:
            for i in range(n):
                fout.write("3\t{}\t{}\n".format(Marker_ele[iMarker][i][0],
                    Marker_ele[iMarker][i][1],Marker_ele[iMarker][i][2]))


fout.close()
print "--------- Success in write .su2 file -------------------"