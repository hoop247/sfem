////////////////////////////////////////////////////////////////////////////
//            SSSSSS  FFFFFF  EEEEEE  MMM   MMM        ++       ++        //
//           SS       FF      EE      MM M M MM       ++       ++         //
//           SSSSSSS  FFFF    EEEE    MM  M  MM   ++++++++ ++++++++       //
//                SS  FF      EE      MM     MM     ++       ++           //
//                SS  FF      EE      MM     MM    ++       ++            //
//           SSSSSS   FF      EEEEEE  MM     MM   ++       ++             //
////////////////////////////////////////////////////////////////////////////

/*!
 * \file main.cpp
 * \brief Main file of S-FEM Code (S-FEM).
 * \author Chen Jiang, GR Lab.
 * \version .1.0.0
 *
 *  This code want to achieve such goals:
 *   1.   SU2 mesh format and configure file.
 *   2.   Output Tecplot readable file or Paraview readable file.
 *   3.   Use Eigen as math solver.
 *   4.   Use C++ feature and STL to most works.
 *
 *  \Todo   By JC 2015-06-02-15.23
 *          1. add multiple displacement or velocity markers options in config file.
 *          2. add the output codes of config.
 *          3. add the pressure and concentrated forces to SFEM.
 *          By JC 2015-08-25-09.01
 *          1. rewrite code using object-oriented way.
 *
 */

#include <iostream>
#include <ctime>
#include <cmath>
#include <vector>

#include "./include/config.hpp"
#include "./include/geometry.hpp"
#include "./include/preprocessor.hpp"
#include "./include/load.hpp"
#include "./include/option.hpp"
#include "./include/boundarycondition.hpp"
#include "./include/output.hpp"
#include "./include/solver.hpp"
#include <Eigen/Core>
#include "./include/Timer.h"


using namespace std;

int main(int argc, char *argv[])
{
    /* -------------  output project information -------------*/
    cout << "SFEM : ----------------------------------------------------------" << endl;
    cout << "SFEM : |                        S-FEM ++                        |" << endl;
    cout << "SFEM : ----------------------------------------------------------" << endl;
    cout << "SFEM : Author : Chen Jiang, GR Lab." << endl;
    cout << "SFEM : Version : 0.0.1." << endl;
    cout << "SFEM : Modified Time : 2016-11-15-15.38" << endl << endl;



    /* -------------  Allocate memory for classes -------------*/
    cout << "SFEM : ---------------------- Read configure file --------------------" << endl;
    char config_file_name[200];
    if (argc >= 2){ strcpy(config_file_name,argv[1]); }
    else{ strcpy(config_file_name, "default.cfg"); }
    cout << "SFEM : configure file name : " << config_file_name << "."<< endl;

    CConfig *config = NULL;
    config = new CConfig(config_file_name);

    cout << "SFEM : --------------  Read MeshFile (SU2 format) --------------------" << endl;
    CGeometry *geo = NULL;
    geo = new CGeometry(config);



    /// test for Class CPreprocessorFEMTRI3
//    CPreprocessor *prep = NULL;
//    prep = new CPreprocessorFEMTRI3(config, geo);
//    prep->Display();
//
//    /// test for Class CLoad (good for 2D)
//    CLoad *load = NULL;
//    load = new CLoad2D(config, geo);
//    load->Display();
//
//    /// test for Class CBoundarConditionCFD2D (good for 2D)
//    CBoundaryCondition *BC = NULL;
//    BC = new CBoundaryConditionCFD2D(config, geo);

    /// test for Class COutput (for 2D)
//    mat v;
//    v << 1.0 << 1.1 << endr
//      << 2.0 << 0.2 << endr
//      << 3.0 << 0.31 << endr
//      << 4.5 << 0.23123 << endr;
//    vec p;
//    p << 0.1 << 2.9 << 4.1 << 8.2;
//    COutput *Output = NULL;
//    Output = new COutputFluid();
//    Output->WriteSolutionFluid(config, geo, v, p, 0, 0.0);

    /// test for Class COutput (for 3D)
//    mat v;
//    v << 1.0 << 1.1 << 0.2 << endr
//      << 2.0 << 0.2 << 0.3 << endr
//      << 3.0 << 0.31 << 0.32 << endr
//      << 4.5 << 0.23123 <<  0.2132 << endr
//      << 5.6 << 0.2234 <<  0.333 << endr
//      << 4.2312 << 0.9123 <<  0.33132 << endr
//      << 7.4 << 0.23123 <<  0.1111 << endr
//      << 41.5 << 1.8 <<  9.45<< endr
//      << 4.2111 << 0.5645 <<  0.23423 << endr;
//    vec p;
//    p << 0.1 << 2.9 << 4.1 << 8.2 << 1.1 << 221.1 << 2312.2 << 2111.0 << 85.4;
//    COutput *Output = NULL;
//    Output = new COutputFluid();
//    Output->WriteSolutionFluid(config, geo, v, p, 0, 0.0);

//    SFEM *sfem = NULL;
//    sfem = new SFEM();
//    /* -------------  Preprocessing data -------------*/
//    cout << "SFEM : ------------------  Preprocessing --------------------------" << endl;
//    sfem->PreProcessing(config, geo);
//    cout << "SFEM : ---------------------  Load&BC -----------------------------" << endl;
//    sfem->LoadAndBC(config,geo);
//
//
//    /* -------------  Solving -------------*/
//
//    if(config->GetRestartOption()){
//        cout << "SFEM : ---------------------  RestartFSI ----------------------" << endl;
//        sfem->RestartFSI(config,geo);
//    }
//    else{
//        cout << "SFEM : ----------------------  Solving ------------------------" << endl;
//        sfem->Solve(config, geo);
//    }

//    CSolver *SemiImpCBS2DLam = NULL;
//    SemiImpCBS2DLam = new CSolverSemiImpCBSLaminar(config, geo);
//    CPreprocessor *prep_NSFEMT3 = NULL;
//    prep_NSFEMT3 = new CPreprocessorNSFEMTRI3(config, geo);

//    CPreprocessor *prep_ESFEMT3 = NULL;
//    prep_ESFEMT3 = new CPreprocessorESFEMTRI3(config, geo);

//    CSolver *SemiImpCBS3DLam = NULL;
//    SemiImpCBS3DLam = new CSolverSemiImpCBSLaminar(config, geo);
    CSolver *TLXpl3D = NULL;
    TLXpl3D = new CSolverSolidTL(config, geo);

    cout << "SFEM : -------------------  Exit success (SFEM_OO) ---------------" << endl;
    cout << "SFEM : Press any key to exit!!!" << endl;
    cin.get();
    exit(0);

    delete config;
    delete geo;
//    delete sfem;
    return 0;
}
