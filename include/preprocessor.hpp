#ifndef CPREPROCESSOR_H
#define CPREPROCESSOR_H


#include <vector>
#include <algorithm>
#include <armadillo>
#include <iostream>
#include "config.hpp"
#include "geometry.hpp"
#include "gausspoint.hpp"
#include "smoothingdomain.hpp"
#include "Timer.h"

using namespace std;
using namespace arma;

/** \class  CPreprocessor
 *  \brief  Base class which constructs the data of GaussPoints for FEM or Smoothing Domain S-FEM.
 *          Espeacially, for S-FEM, the SD will be regarded as GP!!! Very important!.
 *  \author Chen Jiang, GR Lab
 *  \date   2015-09-08-16.17
 */


class CPreprocessor
{
    protected:
        unsigned long nGP;
        vector<CGaussPoint*> GPs;
        vector<vector<double> > GPsShapeFunc; //nGP x nNode_oneElem
        vector<vector<double> > GPsShapeFuncDX;
        vector<vector<double> > GPsShapeFuncDY;
        vector<vector<double> > GPsShapeFuncDZ;

        vector<double> LumpedMass;

    private:

    public:
        /** Default constructor */
        CPreprocessor();
        /** Default destructor */
        virtual ~CPreprocessor();

        /** Public member functions*/
        virtual void Display();

        virtual void SetGPs() {};
        inline vector<CGaussPoint*> GetGPs() { return GPs;};
        inline CGaussPoint* GetGP(unsigned long val_igp) { return GPs[val_igp];}

        virtual void SetGPsShapeFunc() {};
        inline vector<vector<double> > GetGPsShapeFuncAll() {return GPsShapeFunc;}
        inline vector<double> GetGPsShapeFunc(unsigned long val_igp) {return GPsShapeFunc[val_igp];}
        inline double GetGPsShapeFunc(unsigned long val_igp, unsigned short i) { return GPsShapeFunc[val_igp][i];}


        virtual void SetGPsShapeFuncDX() {};
        inline vector<vector<double> > GetGPsShapeFuncDXAll() { return GPsShapeFuncDX;}
        inline vector<double> GetGPsShapeFuncDX(unsigned long val_igp) { return GPsShapeFuncDX[val_igp];}
        inline double GetGPsShapeFuncDX(unsigned long val_igp, unsigned short i) { return GPsShapeFuncDX[val_igp][i];}

        virtual void SetGPsShapeFuncDY() {};
        inline vector<vector<double> > GetGPsShapeFuncDYAll() { return GPsShapeFuncDY;}
        inline vector<double> GetGPsShapeFuncDY(unsigned long val_igp) { return GPsShapeFuncDY[val_igp];}
        inline double GetGPsShapeFuncDY(unsigned long val_igp, unsigned short i) { return GPsShapeFuncDY[val_igp][i];}

        virtual void SetGPsShapeFuncDZ() {};
        inline vector<vector<double> > GetGPsShapeFuncDZAll() { return GPsShapeFuncDZ;}
        inline vector<double> GetGPsShapeFuncDZ(unsigned long val_igp) { return GPsShapeFuncDZ[val_igp];}
        inline double GetGPsShapeFuncDZ(unsigned long val_igp, unsigned short i) { return GPsShapeFuncDZ[val_igp][i];}

        /** inline public member functions*/
        inline void SetnGP(unsigned long val_nGP) { nGP = val_nGP;}
        inline unsigned long GetnGP() { return nGP; }

        virtual void SetLumpedMassAll(CConfig *config, CGeometry *geometry) {};
        inline vector<double> GetLumpedMassAll() { return LumpedMass; }
        inline double GetLumpedMass(unsigned long val_i) { return LumpedMass[val_i];}

};

/** \class CPreprocessorFEMTRI3, FEM preprocessing of 3nodes triangular  element,
 *   derived from Class CPreprocessor
 *  \author Chen Jiang, GR Lab
 *  \date 2015-09-15-15.07
 */
class CPreprocessorFEMTRI3 : public CPreprocessor
{
protected:

private:

public:
    /** Default constructor*/
    CPreprocessorFEMTRI3();
    CPreprocessorFEMTRI3(CConfig *config, CGeometry *geometry);
    /** Default deconstructor*/
    ~CPreprocessorFEMTRI3();

    /** Public member funcs*/
    virtual void SetElemsCGAndVol(CConfig *config, CGeometry *geometry);
    virtual void SetElemsSize(CConfig *config, CGeometry *geometry);

    virtual void SetGPs(CConfig *config, CGeometry *geometry);
    virtual void SetGPsShapeFunc(CConfig *config, CGeometry *geometry);
    virtual void SetGPsShapeFuncDX(CConfig *config, CGeometry *geometry);
    virtual void SetGPsShapeFuncDY(CConfig *config, CGeometry *geometry);
    virtual void SetGPsShapeFuncDZ(CConfig *config, CGeometry *geometry);
    virtual void Display();
    virtual void SetLumpedMassAll(CConfig *config, CGeometry *geometry);

};

/** \class CPreprocessorFEMTET4, FEM preprocessing of 4nodes tetrahedral  element,
 *   derived from Class CPreprocessor
 *  \author Chen Jiang, GR Lab
 *  \date 2016-02-29-09.49
 */
class CPreprocessorFEMTET4 : public CPreprocessor
{
protected:

private:

public:
    /** Default constructor*/
    CPreprocessorFEMTET4();
    CPreprocessorFEMTET4(CConfig *config, CGeometry *geometry);
    /** Default deconstructor*/
    ~CPreprocessorFEMTET4();

    /** Public member funcs*/
    virtual void SetElemsCGAndVol(CConfig *config, CGeometry *geometry);
    virtual void SetElemsSize(CConfig *config, CGeometry *geometry);

    virtual void SetGPs(CConfig *config, CGeometry *geometry);
    virtual void SetGPsShapeFunc(CConfig *config, CGeometry *geometry);
    virtual void SetGPsShapeFuncDX(CConfig *config, CGeometry *geometry);
    virtual void SetGPsShapeFuncDY(CConfig *config, CGeometry *geometry);
    virtual void SetGPsShapeFuncDZ(CConfig *config, CGeometry *geometry);
    virtual void Display();
    virtual void SetLumpedMassAll(CConfig *config, CGeometry *geometry);

};

/** \class CPreprocessorESFEMTRI3, ES-FEM preprocessing of 3nodes triangular  element,
 *   derived from Class CPreprocessor
 *  \author Chen Jiang, GR Lab
 *  \date 2016-03-02-09.34
 *  \brief To ease the implementation, just average the strain of support elems to get
 *        Edge-SD's strain.
 *  \note Chen Jiang, average strain seems not equivalent to ZhiQian's implementation
 *          which is also suggested by S-FEM book.
 */
class CPreprocessorESFEMTRI3 : public CPreprocessorFEMTRI3
{
protected:
    vector<CSmoothingDomain*> ESDs;
    unsigned long nGP_FEM;
    vector<CGaussPoint*> GPs_FEM;
    vector<vector<double> > GPsShapeFunc_FEM; //nGP x nNode_oneElem
    vector<vector<double> > GPsShapeFuncDX_FEM;
    vector<vector<double> > GPsShapeFuncDY_FEM;
    vector<vector<double> > GPsShapeFuncDZ_FEM;
private:

public:
    /** Default constructor*/
    CPreprocessorESFEMTRI3();
    CPreprocessorESFEMTRI3(CConfig *config, CGeometry *geometry);
    /** Default deconstructor*/
    ~CPreprocessorESFEMTRI3();
    /** Public member funcs*/
    virtual void SetGPs(CConfig *config, CGeometry *geometry);
    virtual void SetGPsShapeFunc(CConfig *config, CGeometry *geometry);
    virtual void SetGPsShapeFuncDX(CConfig *config, CGeometry *geometry);
    virtual void SetGPsShapeFuncDY(CConfig *config, CGeometry *geometry);
    virtual void SetGPsShapeFuncDZ(CConfig *config, CGeometry *geometry);
    virtual void Display() {};

    void GetESD(CConfig *config, CGeometry *geometry);
    void CopyGPsToGPs_FEM();

};

/** \class CPreprocessorNSFEMTRI3, NS-FEM preprocessing of 3nodes triangular  element,
 *   derived from Class CPreprocessor
 *  \author Chen Jiang, GR Lab
 *  \date 2016-03-04-07.59
 *  \brief To ease the implementation, just average the strain of support elems to get
 *        Nodebased-SD's strain.
 *  \note Chen Jiang, average strain seems not equivalent to ZhiQian's implementation
 *          which is also suggested by S-FEM book.
 */
class CPreprocessorNSFEMTRI3 : public CPreprocessorFEMTRI3
{
protected:
    vector<CSmoothingDomain*> NSDs;
    unsigned long nGP_FEM;
    vector<CGaussPoint*> GPs_FEM;
    vector<vector<double> > GPsShapeFunc_FEM; //nGP x nNode_oneElem
    vector<vector<double> > GPsShapeFuncDX_FEM;
    vector<vector<double> > GPsShapeFuncDY_FEM;
    vector<vector<double> > GPsShapeFuncDZ_FEM;
private:

public:
/** Default constructor*/
    CPreprocessorNSFEMTRI3();
    CPreprocessorNSFEMTRI3(CConfig *config, CGeometry *geometry);
/** Default deconstructor*/
    ~CPreprocessorNSFEMTRI3();
/** Public member functions*/
    virtual void SetGPs(CConfig *config, CGeometry *geometry);
    virtual void SetGPsShapeFunc(CConfig *config, CGeometry *geometry);
    virtual void SetGPsShapeFuncD(CConfig *config, CGeometry *geometry);
    virtual void Display() {};

    void GetNSD(CConfig *config, CGeometry *geometry);
    void CopyGPsToGPs_FEM();
};



/** \class CPreprocessorFSFEMTET4, FS-FEM preprocessing of 4nodes tetrahedral element,
 *   derived from Class CPreprocessor
 *  \author Chen Jiang, GR Lab
 *  \date 2016-03-03-10.49
 *  \brief To ease the implementation, just average the strain of support elems to get
 *        Face-SD's strain.
 */
class CPreprocessorFSFEMTET4 : public CPreprocessorFEMTET4
{
protected:
    vector<CSmoothingDomain*> FSDs;
    unsigned long nGP_FEM;
    vector<CGaussPoint*> GPs_FEM;
    vector<vector<double> > GPsShapeFunc_FEM; //nGP x nNode_oneElem
    vector<vector<double> > GPsShapeFuncDX_FEM;
    vector<vector<double> > GPsShapeFuncDY_FEM;
    vector<vector<double> > GPsShapeFuncDZ_FEM;

private:

public:
    /** Default constructor*/
    CPreprocessorFSFEMTET4();
    CPreprocessorFSFEMTET4(CConfig *config, CGeometry *geoemtry);
    /** Default deconstructor*/
    ~CPreprocessorFSFEMTET4();
    /** Public member funcs*/
    virtual void SetGPs(CConfig *config, CGeometry *geometry);
    virtual void SetGPsShapeFunc(CConfig *config, CGeometry *geometry);
    virtual void SetGPsShapeFuncD(CConfig *config, CGeometry *geometry);
    virtual void Display() {};

    void GetFSD(CConfig *config, CGeometry *geometry);
    void CopyGPsToGPs_FEM();
};


#endif // CPREPROCESSOR_H
