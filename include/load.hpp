#ifndef CLOAD_H
#define CLOAD_H

#include <vector>
#include <iostream>
#include <algorithm>
#include <string>
#include <armadillo>
#include "config.hpp"
#include "geometry.hpp"
#include "boundmarker.hpp"

using namespace std;
using namespace arma;

/** \brief  CLoad is base class for all type of loading conditions.
 *  \date   2015-09-19-09.42
 *  \author Chen Jiang, GR Lab
 *
 */
class CLoad
{
protected:
    vector<double> F_ext_CF; //concentrated nodal force
    vector<double> F_ext_pressure; //nodal force from pressure
    vector<double> F_ext_traction;
    vector<double> F_ext; // totoal nodal external force
    unsigned long nNode;
    unsigned short nDim;
    double amplitude; //TODO: add the load curve
private:

public:
    /** Default constructor */
    CLoad();
    CLoad(CConfig *config, CGeometry *geometry);
    /** Default destructor */
    virtual ~CLoad();

    /** public member funcs*/
    //need to add correspongding codes in class CConfig, CGeometry.
    virtual void SetExtF_CF(CConfig *config, CGeometry *geometry) {};
    virtual void SetExtF_Pressure(CConfig *config, CGeometry *geometry) {};
    virtual void SetExtF_Traction(CConfig *config, CGeometry *geometry) {};
    virtual void Display();


    inline void SetnNode(unsigned long val_nnode) { this->nNode = val_nnode;}
    inline void SetnDim(unsigned short val_nDim) { this->nDim = val_nDim;}

    inline vector<double> GetExtFAll() { return F_ext;}
    inline vector<double> GetExtF_Pressure() { return F_ext_pressure;}
    inline vector<double> GetExtF_CF() { return F_ext_CF;}
    inline vector<double> GetExtF_Traction() { return F_ext_traction;}

    inline double GetExtF(unsigned long val_inode, unsigned short val_iDim) {
         return F_ext[val_inode*nDim+val_iDim];}
};


/** \brief  CLoad2D is a derived class from CLoad to handle all loading condition in 2D case.
 *  \date   2015-09-19-09.42
 *  \author Chen Jiang, GR Lab
 *  \todo add codes in Class CConfig, CGeometry about the F_ext_CF, F_ext_Pressure, F_ext_traction.
 */
class CLoad2D : public CLoad
{
protected:

private:

public:
  /** Default constructor */
  CLoad2D();
  CLoad2D(CConfig *config, CGeometry *geometry);
  /** Default destructor */
  virtual ~CLoad2D();

  /** public member funcs*/
  virtual void SetExtF_CF(CConfig *config, CGeometry *geometry);
  virtual void SetExtF_Pressure(CConfig *config, CGeometry *geometry);
  virtual void SetExtF_Traction(CConfig *config, CGeometry *geometry);
};


/** \brief  CLoad3D is a derived class from CLoad to handle all loading condition in 3D case.
 *  \date   2016-03-10-09.42
 *  \author Chen Jiang, GR Lab
 *  \todo add codes in Class CConfig, CGeometry about the F_ext_CF, F_ext_Pressure, F_ext_traction.
 *  \note only 1st order elements are considered.
 */
class CLoad3D : public CLoad
{
protected:

private:

public:
  /** Default constructor */
  CLoad3D();
  CLoad3D(CConfig *config, CGeometry *geometry);
  /** Default destructor */
  virtual ~CLoad3D();

  /** public member funcs*/
  virtual void SetExtF_CF(CConfig *config, CGeometry *geometry);
  virtual void SetExtF_Pressure(CConfig *config, CGeometry *geometry);
  virtual void SetExtF_Traction(CConfig *config, CGeometry *geometry);
};


#endif // CLOAD_H
