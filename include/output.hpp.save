#ifndef COUTPUT_H
#define COUTPUT_H

#include "config.hpp"
#include "geometry.hpp"
#include <ar

/** \class  COutput
 *  \brief  Base class which output any data needed for post-processing.
 *  \author Chen Jiang, GR Lab
 *  \date   2015-12-16-21.32
 */

class COutput
{
    public:
        /** Default constructor */
        COutput();
        /** Default destructor */
        virtual ~COutput();

        /** Member functions */
        virtual void WriteSolutionSolid
            (CConfig *config, CGeometry *geoemtry, mat &u, mat &stress, unsigned long n_t, double t) {};

        virtual void WriteSolutionFluid
            (CConfig *config, CGeometry *geometry, mat &v, vec &p, unsigned long n_t, double t) {};

        virtual void WriteConvergeHistorySolid
            (CConfig *config, CGeometry *geometry, vec &error_u, vec &error_v) {};

        virtual void WriteConvergeHistoryFluid
            (CConfig *config, CGeometry *geometry, vec &error_v, vec &error_p) {};
    protected:

    private:
};

/** \class  COutput
 *  \brief  Base class which output any data needed for post-processing.
 *  \author Chen Jiang, GR Lab
 *  \date   2015-12-16-21.32
 */
class COutputFluid : public COutput
{
    public:
        /** Default constructor */
        COutputFluid();
        /** Default destructor */
        virtual ~COutput();

        /** Member functions */
        virtual void WriteSolutionFluid
            (CConfig *config, CGeometry *geometry, mat &v, vec &p, unsigned long n_t, double t);
    protected:
        void WriteSolutionTecplot(CConfig *config, CGeometry *geometry, mat &v, vec &p, unsigned long n_t, double t);

        void WriteSolutionVTK(CConfig *config, CGeometry *geometry, mat &v, vec &p, unsigned long n_t, double t);
    private:
};

#endif // COUTPUT_H
