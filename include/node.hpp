#ifndef CNODE_H
#define CNODE_H

#include <vector>
#include <algorithm>
#include <iostream>

using namespace std;

class CNode
{

public:
    /** Default constructor */
    CNode();
    CNode(vector<double> val_coords, vector<unsigned long> val_elems, unsigned long val_globalID);
    /** Default destructor */
    virtual ~CNode();


    /** Access GlobalID
     * \return The current value of GlobalID
     */
    unsigned long GetGlobalID()
    {
        return GlobalID;
    }
    /** Set GlobalID
     * \param val New value to set
     */
    void SetGlobalID(unsigned long val)
    {
        GlobalID = val;
    }
    /** Access Coords
     * \return The current value of Coords
     */
    vector<double> GetCoords()
    {
        return Coords;
    }
    double GetX()
    {
        return Coords[0];
    }
    double GetY()
    {
        return Coords[1];
    }
    double GetZ()
    {
        return Coords[2];
    }
    /** Set Coords
     * \param val New value to set
     */
    void SetCoords(vector<double> &val)
    {
        Coords = val;
    }
    void SetCoords(double val_x, double val_y)
    {
        Coords.push_back(val_x);
        Coords.push_back(val_y);
    }
    void SetCoords(double val_x, double val_y, double val_z)
    {
        Coords.push_back(val_x);
        Coords.push_back(val_y);
        Coords.push_back(val_z);
    }


    void SetElems(vector <unsigned long> val_Elems)
    {
        Elems = val_Elems;
    }
    void AddElem(unsigned long val_Elem)
    {
        Elems.push_back(val_Elem);
    }
    vector<unsigned long> GetElems()
    {
        return Elems;
    }
    unsigned long GetElem(unsigned short i){
        return Elems[i];
    }

    unsigned short GetnElems()
    {
        return Elems.size();
    }
protected:
private:
    unsigned long GlobalID; //!< Member variable "GlobalID"
    vector<double> Coords; //!< Member variable "Coords", Coords=[x, y, z]
    vector<unsigned long> Elems; //!< Member variable "Elems", Elems contain this node.

};

#endif // CNODE_H
