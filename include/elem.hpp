#pragma once
#ifndef CELEM_H
#define CELEM_H

/** \brief  element class is the base class for other sub element-type objects (Tetrahedron, Triangle, etc).
 *  \date   2015-09-09-10.39
 *  \author Chen Jiang, GR Lab
 *
 */
#include <string>
#include <armadillo>
#include <vector>
#include <iostream>
#include <algorithm>
#include <cmath>
#include "option.hpp"

using namespace std;
using namespace arma;

class CElem
{
protected:
    /** vars*/
    unsigned long GlobalID;
    unsigned short VTK_Type;
    vector<unsigned long> Nodes;
    vector<double> CG_coord;
    vector<vector<double> > ElemFaces_CG_coord; // 1st row contain 1st element face CG coord.
    unsigned short nDim;
    unsigned short nFaces;
    unsigned short nNodes;
    unsigned short nNodesFace;
    vector<vector<unsigned long> > Faces;
    double Vol;
    double h; //characteristic length
private:

public:
    /** Default constructor */
    CElem();
    CElem(unsigned long val_GlobalID,unsigned short val_nDim);

    /** Default destructor */
    virtual ~CElem();

    /** Public member funcs*/
    virtual void SetElem() {};
    virtual void SetCG() {}; //set elem CG and faces' CG
    virtual void SetVol() {};
    //LINE2 element
    virtual void SetElem(unsigned long p0, unsigned long p1) {};
    virtual void SetVol(vector<double> &p0_coord, vector<double> &p1_coord) {};
    virtual void SetCG(vector<double> &p0_coord, vector<double> &p1_coord) {};
    //TRI3 element
    virtual void SetElem(unsigned long p0, unsigned long p1, unsigned long p2) {};
    virtual void SetVol(vector<double> &p0_coord, vector<double> &p1_coord, vector<double> &p2_coord) {};
    virtual void SetCG(vector<double> &p0_coord, vector<double> &p1_coord, vector<double> &p2_coord) {};
    //TET4 element
    virtual void SetElem(unsigned long p0, unsigned long p1, unsigned long p2, unsigned long p3) {};
    virtual void SetVol(vector<double> &p0_coord, vector<double> &p1_coord, vector<double> &p2_coord,
                vector<double> &p3_coord) {};
    virtual void SetCG(vector<double> &p0_coord, vector<double> &p1_coord, vector<double> &p2_coord,
                vector<double> &p3_coord) {};


    virtual vector<double> GetShapeFuncAt(vector<double> &p_coord, vector<vector<double> > &node_coord) {};
    virtual vector<double> GetShapeFuncDXAt(vector<double> &p_coord, vector<vector<double> > &node_coord) {};
    virtual vector<double> GetShapeFuncDYAt(vector<double> &p_coord, vector<vector<double> > &node_coord) {};
    virtual vector<double> GetShapeFuncDZAt(vector<double> &p_coord, vector<vector<double> > &node_coord) {};
    virtual void Display();
    /** Overload operator*/
    CElem& operator =(CElem &ele);

    /** inline Public member funcs*/
    inline unsigned short GetType(void) { return VTK_Type; }
    inline vector<unsigned long> GetNodes(void) { return Nodes; }
    inline unsigned long GetNode(unsigned short val_i) { return Nodes[val_i];}
    inline vector<double> GetCG_coord(void) { return CG_coord; }
    inline vector<double> GetFaceCG_coord(unsigned short iFace) { return ElemFaces_CG_coord[iFace]; }
    inline vector<vector<double> > GetFaceCG_coordAll(void) { return ElemFaces_CG_coord; }
    inline unsigned short GetnDim(void){ return nDim; }
    inline unsigned short GetnFaces(void) { return nFaces; }
    inline unsigned short GetnNodes(void) { return nNodes; }
    inline unsigned short GetnNodesFace(void) { return nNodesFace; }
    inline vector<unsigned long> GetFace(unsigned short iFace) { return Faces[iFace]; }
    inline vector<vector<unsigned long> > GetFacesAll(void) { return Faces; }
    inline double GetVol(void){ return Vol; }
    inline void SetGlobalID(unsigned long val_GlobalID) { GlobalID = val_GlobalID; }
    inline unsigned long GetGlobalID(void){ return GlobalID; }
    inline void Seth(double CL) {h = CL;}
    inline double Geth(void) {return h;}


};

/** \class CElemVertex1, the 1node vertex element, derived from Class CElem
 *  \author Chen Jiang, GR Lab
 *  \date 2016-02-22-16.37
 */
class CElemVERTEX1 : public CElem
{
protected:

private:

public:
    /** Default constructor*/
    CElemVERTEX1();
    CElemVERTEX1(unsigned long p0, unsigned long val_globalID, unsigned short val_ndim);
    /** Default deconstructor*/
    ~CElemVERTEX1();
    /** public member funcs*/
    void SetElem(unsigned long p0);
    inline void SetVol(vector<double> &p0_coord) { Vol = 0.0;};
    void SetCG(vector<double> &p0_coord);
};




/** \class CElemLINE2, the 2node line element, derived from Class CElem
 *  \author Chen Jiang, GR Lab
 *  \date 2015-09-14-21.07
 */
class CElemLINE2 : public CElem
{
protected:

private:

public:
    /** Default constructor*/
    CElemLINE2();
    CElemLINE2(unsigned long p0, unsigned long p1, unsigned long val_globalID, unsigned short val_nDim);
    /** Default deconstructor*/
    ~CElemLINE2();
    /** public member funcs*/
    void SetElem(unsigned long p0, unsigned long p1);
    void SetVol(vector<double> &p0_coord, vector<double> &p1_coord);
    void SetCG(vector<double> &p0_coord, vector<double> &p1_coord);
    virtual vector<double> GetShapeFuncAt(vector<double> &p_coord, vector<vector<double> > &node_coord);
    virtual vector<double> GetShapeFuncDXAt(vector<double> &p_coord, vector<vector<double> > &node_coord);
    virtual vector<double> GetShapeFuncDYAt(vector<double> &p_coord, vector<vector<double> > &node_coord);
    virtual vector<double> GetShapeFuncDZAt(vector<double> &p_coord, vector<vector<double> > &node_coord);
    double GetLengthL2(vector<double> &p0_coord, vector<double> &p1_coord);
};

/** \class CElemTRI3, the 3node triangular element, derived from Class CElem
 *  \author Chen Jiang, GR Lab
 *  \date 2015-09-09-10.39
 */
class CElemTRI3 : public CElem
{
protected:

private:

public:
    /** Default constructor*/
    CElemTRI3();
    /** Overload constructor*/
    CElemTRI3(unsigned long p0, unsigned long p1, unsigned long p2,
              unsigned long val_globalID, unsigned short val_nDim);
    /** Default deconstructor*/
    virtual ~CElemTRI3();

    /** public member funcs*/
    void SetElem(unsigned long p0, unsigned long p1, unsigned long p2);
    void SetVol(vector<double> &p0_coord, vector<double> &p1_coord, vector<double> &p2_coord); //infact this vol is area
    void SetCG(vector<double> &p0_coord, vector<double> &p1_coord, vector<double> &p2_coord);
    virtual vector<double> GetShapeFuncAt(vector<double> &p_coord, vector<vector<double> > &node_coord);
    virtual vector<double> GetShapeFuncDXAt(vector<double> &p_coord, vector<vector<double> > &node_coord);
    virtual vector<double> GetShapeFuncDYAt(vector<double> &p_coord, vector<vector<double> > &node_coord);
    virtual vector<double> GetShapeFuncDZAt(vector<double> &p_coord, vector<vector<double> > &node_coord);
    double GetAreaT3(vector<double> &p0_coord, vector<double> &p1_coord,
                     vector<double> &p2_coord);
};

/** \class CElemTET4, the 4node triangular element, derived from Class CElem
 *  \author Chen Jiang, GR Lab
 *  \date 2015-09-14-11.20
 */
class CElemTET4 : public CElem
{
protected:

private:

public:
    /** Default constructor*/
    CElemTET4();
    /** Overload constructor*/
    CElemTET4(unsigned long p0, unsigned long p1, unsigned long p2,
              unsigned long p3, unsigned long val_globalID,
              unsigned short val_nDim);
    /** Default deconstructor*/
    virtual ~CElemTET4();

    /** public member funcs*/
    void SetElem(unsigned long p0, unsigned long p1, unsigned long p2, unsigned long p3);
    void SetVol(vector<double> &p0_coord, vector<double> &p1_coord, vector<double> &p2_coord,
                vector<double> &p3_coord);
    void SetCG(vector<double> &p0_coord, vector<double> &p1_coord, vector<double> &p2_coord,
                vector<double> &p3_coord);
    virtual vector<double> GetShapeFuncAt(vector<double> &p_coord, vector<vector<double> > &node_coord);
    virtual vector<double> GetShapeFuncDXAt(vector<double> &p_coord, vector<vector<double> > &node_coord);
    virtual vector<double> GetShapeFuncDYAt(vector<double> &p_coord, vector<vector<double> > &node_coord);
    virtual vector<double> GetShapeFuncDZAt(vector<double> &p_coord, vector<vector<double> > &node_coord);
    double GetVolT4(vector<double> &p0_coord, vector<double> &p1_coord,
                    vector<double> &p2_coord, vector<double> &p3_coord);
};


#endif
// CELEM_H
