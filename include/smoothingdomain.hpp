#ifndef CSMOOTHINGDOMAIN_H
#define CSMOOTHINGDOMAIN_H

#include <vector>
#include <armadillo>
#include <string>
#include "elem.hpp"
#include "option.hpp"

using namespace std;
using namespace arma;

/* -------------  Declaration of Class CSmoothingDomain -------------*/
/** \brief  CSmoothingDomain class is the base-class store single smoothingdomain's connectivities and other info.
 *          Other classes (like EdgebasdSD, CellBasedSD, NodeBasedSD) will derived from this base class.
 *          2015-09-02-16.04
 *
 *  \author Chen Jiang
 * \param
 * \param
 * \return
 *
 */
class CSmoothingDomain
{
    protected:
        /** vars*/
        bool isBoundSD;
        unsigned short nDim;
        vector<unsigned long> Elems; // who's CG is one vertex of SD
        unsigned short nNodes; // number of nodes
        vector<unsigned long> Nodes; // nodes
        unsigned short nSupportNodes;
        vector<unsigned long> SupportNodes;
        double Vol;

    private:

    public:
        /** Default constructor */
        CSmoothingDomain();
        /** Default destructor */
        virtual ~CSmoothingDomain();

        /** Public member functions*/
        inline void SetisBoundSD(bool _isBound) { isBoundSD = _isBound;}
        inline void SetnDim(unsigned short _nDim) {nDim = _nDim;}
        inline void SetnNodes(unsigned short _nNodes) { nNodes = _nNodes;}
        inline void SetnSupportNodes(unsigned short _nSupportNodes) { nSupportNodes = _nSupportNodes;}
        inline unsigned short GetnSupportNodes(void) { return nSupportNodes; }
        inline bool GetisBoundSD(void) { return isBoundSD;}
        inline unsigned short GetnDim(void) { return nDim;}
        inline unsigned short GetnNodes(void) { return nNodes;}
        inline vector<unsigned long> GetElems(void) { return Elems;}
        inline unsigned long GetElem(unsigned short _i) { return Elems[_i];}
        inline unsigned short GetnElem(void) { return Elems.size();}
        inline unsigned long GetNode(unsigned short _i) { return Nodes[_i];}
        inline unsigned long GetSupportNode(unsigned short _i) { return SupportNodes[_i];}
        inline vector<unsigned long> GetSupportNodes(void) { return SupportNodes;}
        //inline void SetVol(double _vol) { Vol = _vol;}
        inline double GetVol(void) { return Vol;}

        virtual void SetElems(vector<unsigned long> _Elems) { Elems = _Elems;}
        virtual void SetNodes(vector<unsigned long> _node) { Nodes = _node; }
        virtual void AddElem(unsigned long val_Elem) { Elems.push_back(val_Elem);}
        virtual void SetVol(void) {};

        virtual void Display(void); //for testing and debugging
};

/* -------------  Declaration of Class CEdgeBasedSD_T3 -------------*/
class CEdgeBasedSD_T3 : public CSmoothingDomain
{
public:
  CEdgeBasedSD_T3();
  CEdgeBasedSD_T3(unsigned long val_node1, unsigned long val_node2,
                  CElem *val_Elem1, CElem *val_Elem2,
                  unsigned short val_nDim, bool val_isbound);
  CEdgeBasedSD_T3(unsigned long val_node1, unsigned long val_node2,
                  CElem *val_Elem1, unsigned short val_nDim,
                  bool val_isbound);
  ~CEdgeBasedSD_T3();

  /** public funcs*/
  void Display();


  /** inline public funcs*/
  inline unsigned short GetType(void) { return Type;}

  inline void SetElems(unsigned long val_ele1, unsigned long val_ele2)
  { Elems.push_back(val_ele1); Elems.push_back(val_ele2);}
  inline void SetElems(unsigned long val_ele1) { Elems.push_back(val_ele1);} // for bound SD
  inline void SetElems(vector<unsigned long> val_Elems) { Elems = val_Elems;}

  inline vector<unsigned long> GetElems(void) { return Elems;}
  inline unsigned long GetElem(unsigned short val_i) { return Elems[val_i];}

  inline void SetNodes(unsigned long val_node1, unsigned long val_node2)
  { Nodes.push_back(val_node1); Nodes.push_back(val_node2);}
  inline void SetVol(double val_ele1vol, double val_ele2vol) { Vol = (val_ele1vol + val_ele2vol)/3.0; }
  inline void SetVol(double val_SDvol) { Vol = val_SDvol;}
  inline double GetVol(void) {return Vol;}


protected:
private:
    // Type = [VTK_type][SD_Type], SD_Type: 1, Cell-based; 2, Node-based, 3, Edge-based.
    static unsigned short Type;

};

/** \class CNodeBasedSD_T3, Store data for NodeBasedSD,derived from Class CSmoothingDomain
 *  \author Chen Jiang, GR Lab
 *  \date 2016-03-04-07.45
 */
class CNodeBasedSD_T3 : public CSmoothingDomain
{
protected:

private:
    static unsigned short Type;
public:
    /** Default constructor*/
    CNodeBasedSD_T3();
    CNodeBasedSD_T3(unsigned long val_node, vector<CElem*> val_eles, unsigned short ndim);
    /** Default deconstructor*/
    ~CNodeBasedSD_T3();
    /** public member function*/
    void Display();
    inline unsigned short GetType(void) { return Type;}

    inline void SetElems(vector<unsigned long> val_eles) {Elems = val_eles;}
    inline void AddElems(unsigned long val_ele) {Elems.push_back(val_ele);}
    inline vector<unsigned long> GetElems(void) { return Elems;}
    inline unsigned long GetElem(unsigned short i) { return Elems[i];}
    inline void SetNode(unsigned long val_node) { Nodes.push_back(val_node);}
    inline void SetVol(double val_SDvol) {Vol = val_SDvol;}
    inline double GetVol(void) { return Vol;}

};



/** \class CFaceBasedSD_T4, Store data for FaceBasedSD,derived from Class CSmoothingDomain
 *  \author Chen Jiang, GR Lab
 *  \date 2016-03-03-11.07
 */
class CFaceBasedSD_T4 : public CSmoothingDomain
{
protected:

private:
    static unsigned short Type;

public:
    /** Default constructor*/
    CFaceBasedSD_T4();
    CFaceBasedSD_T4(unsigned long val_node0, unsigned long val_node1, unsigned long val_node2,
                    CElem *val_Elem0, CElem *val_Elem1, bool val_isbound);
    CFaceBasedSD_T4(unsigned long val_node0, unsigned long val_node1, unsigned long val_node2,
                    CElem *val_Elem0, bool val_isbound);
    /** Default deconstructor*/
    ~CFaceBasedSD_T4();
    /** Public member funcs*/
    void Display();
    inline unsigned short GetType(void) { return Type;}

    inline void SetElems(unsigned long val_ele0, unsigned long val_ele1)
    { Elems.push_back(val_ele0); Elems.push_back(val_ele1);}
    inline void SetElems(unsigned long val_ele0) { Elems.push_back(val_ele0);} // for bound SD
    inline void SetElems(vector<unsigned long> val_Elems) { Elems = val_Elems;}

    inline vector<unsigned long> GetElems(void) { return Elems;}
    inline unsigned long GetElem(unsigned short val_i) { return Elems[val_i];}

    inline void SetNodes(unsigned long val_node0, unsigned long val_node1, unsigned long val_node2)
    { Nodes.push_back(val_node0); Nodes.push_back(val_node1); Nodes.push_back(val_node2);}
    inline void SetVol(double val_ele1vol, double val_ele2vol) { Vol = (val_ele1vol + val_ele2vol)/4.0; }
    inline void SetVol(double val_SDvol) { Vol = val_SDvol;}
    inline double GetVol(void) {return Vol;}

};


#endif // CSMOOTHINGDOMAIN_H
