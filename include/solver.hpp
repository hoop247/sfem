#ifndef CSOLVER_H
#define CSOLVER_H


/** \brief Base class for all kind of solvers.
        Hierarcy:
        CSolver
        -----------
            CSolverSolid
            ---------------
                CSolverSolidStatic
                CSolverSolidExplicit
                CSolverSolidImplicit
                CSolverSolidDRM
                ....
            CSolverFluid
            ---------------
                CSolverFluidCBS
            ....


 */

#include <vector>
#include <algorithm>
#include <cmath>
#include "config.hpp"
#include "geometry.hpp"
#include "option.hpp"
#include "elem.hpp"
#include "smoothingdomain.hpp"
#include "gausspoint.hpp"
#include "timeIntegration.hpp"
#include "spaceIntegration.hpp"
#include "preprocessor.hpp"
#include "load.hpp"
#include "boundarycondition.hpp"
#include "material.hpp"
#include "output.hpp"
#include "Toolbox.hpp"
#include <Eigen/Core>
#include <Eigen/Sparse>

using namespace std;
using namespace Eigen;

/** \brief  CSolver class is the base class for other solver objects (FEM sovler, S-FEM solver, T, etc).
 *  \date   2015-09-09-10.39
 *  \author Chen Jiang, GR Lab
 *
 */
class CSolver
{
protected:

private:

public:
    /** Default constructor */
    CSolver();
    CSolver(CConfig *config, CGeometry *geometry);
    /** Default destructor */
    virtual ~CSolver();

    /** public member vars*/
    virtual void Preprocessing(CConfig *config, CGeometry *geometry) {};
    virtual void GetLoads(CConfig *config, CGeometry *geometry) {};
    virtual void GetMaterial(CConfig *config, CGeometry *geometry) {};
    virtual void GetBCs(CConfig *config, CGeometry *geometry) {};
    virtual void Solve(CConfig *config, CGeometry *geometry) {};
    virtual void SingleIteration(CConfig *config, CGeometry *geometry) {};

    vector<double> GetNodalElemSize(CConfig *config, CGeometry *geometry);
    void GetGPVelocity(vector<vector<double> > &gp_v_n_val, CPreprocessor *Prep_val,
                       vector<double> &v_n_val, unsigned short nDim);
    void GetGPVelocityGrad(vector<vector<double> > &gp_gradv_n_val, CPreprocessor *Prep_val,
                           vector<double> &v_n_val, unsigned short nDim);
    void GetGPBML(vector<vector<double> > &gp_BML, CPreprocessor *Prep_val, unsigned short nDim);

    //deformation gradient
    void GetGPDeformGrad(vector<vector<double> > &gp_F, vector<double> &u, CPreprocessor *Prep_val,
                unsigned short nDim, bool &flag_distorted);

    void GetGPB(vector<vector<double> > &gp_B, vector<vector<double> > &gp_F, CPreprocessor *Prep_val,
                unsigned short nDim);


};

/** \brief  CSolverSemiImpCBSLaminar class is the class for using S-FEM for CFD
 *          with SemiImplicit CBS scheme. No Boundary terms.
 *  \date   2016-01-25-11.06
 *  \author Chen Jiang, GR Lab
 *
 */
class CSolverSemiImpCBSLaminar : public CSolver {

protected:
    CTimeIntegration *TimeIntegrationScheme;
    CSpaceIntegration *SpaceIntegration;
    CPreprocessor *Prep;
    COutput *Output;
    CLoad *Loads;
    CBoundaryCondition *BCs;
    CMaterial *Materials;

    /** New add vars*/
    unsigned long nnode, nele;
    double density, mu;
    double *gravity;
    unsigned short nDim;

    vector<unsigned long> H_ir, H_jc;
    vector<double> H_val; //pressure stiffness
    SparseMatrix<double> H_eigen;

    vector<double> mass, mass_inv; //Lumped Mass only!

    vector<double> h_node;

    vector<double> v_n, v_n_one;
    vector<double> p_n, p_n_one;
    vector<double> v_m, rhs_m, rhs_n_one;
    vector<double> F; //nodal fluid foreces (to get lift and drag force)
    double t_n, t_n_one, dt;
    bool flag_fluid_fail;

    vector<vector<double> > gp_v_n; //gp_v_n[i] = [vx_igp, vy_igp, vz_igp]
    vector<vector<double> > gp_gradv_n; //gp_gradv_n = [dvxdx, dvxdy, (dvxdz);
                                        //              dvydx, dvydy, (dvydz);
                                        //              dvzdx, dvzdy, (dvxdz)]

    vector<vector<double> > History_r_v, History_r_p;

    /** New add mem functions*/
    void SingleIterSemiImplicitCBS();
    void SetPressureStiffness(vector<unsigned long> &ic, vector<unsigned long> &jc, vector<double> &val);
    void SetPressureStiffness(Eigen::SparseMatrix<double> &H);

    void Step1(vector<vector<double> > &gp_v_n, vector<vector<double> > &gp_grad_v_n);
    void Step2(vector<double> &v_m);
    void Step3(double theta); ///TODO

    void GetForce(vector<double> &F_val);
    bool CheckSolution();
    void ApplyPBC_H(vector<unsigned long> &ir, vector<unsigned long> &jc,
                    vector<double> &val); ///TODO
    void ApplyPBC_H(Eigen::SparseMatrix<double> &H); ///TODO
    void ApplyPBC_rhs(vector<double> &p_rhs, Eigen::SparseMatrix<double> &H); ///TODO
    void ApplyVBC(vector<double> &v_val); ///TODO


private:



public:
    /** Default constructor */
    CSolverSemiImpCBSLaminar();
    CSolverSemiImpCBSLaminar(CConfig *config, CGeometry *geometry);
    /** Default destructor */
    virtual ~CSolverSemiImpCBSLaminar();

    /** public member functions*/
    void Preprocessing(CConfig *config, CGeometry *geometry);
    //void GetLoads(CConfig *config, CGeometry *geometry) {}; //For CFD, no load (or source terms right now)
    void GetMaterial(CConfig *config, CGeometry *geometry);
    void GetBCs(CConfig *config, CGeometry *geometry);
    void Solve(CConfig *config, CGeometry *geometry);
    void SingleIteration(CConfig *config, CGeometry *geometry);
};


/** \brief  CSolverSolidTL class is the class for using S-FEM or FEM for CSD
 *          with Total Lagrangian formulation with explicit or implicit time integration.
 *  \date   2016-03-08-13.31
 *  \author Chen Jiang, GR Lab
 *  \todo   only one solid and explicit algorithm can be achieved.
 *
 */
class CSolverSolidTL : public CSolver {
protected:
    CTimeIntegration *TimeIntegrationScheme;
    CSpaceIntegration *SpaceIntegration;
    CPreprocessor *Prep;
    COutput *Output;
    CLoad *Loads;
    CBoundaryCondition *BCs;
    CMaterialSolid *Materials; //NOTE (JC)

    /** New add vars*/
    unsigned long nele, nnode, ndof;
    unsigned short nDim;
    double density;
    double *gravity;
    double *mat_para;
    unsigned short mat_type;
    vector<double> mass, mass_inv;
    vector<double> h_node;
    double amp;

    vector<vector<double> > node_coord_n;
    vector<vector<double> > node_coord_n_one;
    vector<double> u_n, u_n_one;
    vector<double> v_n, v_n_one;
    vector<double> a_n, a_n_one;
    double t_n, t_n_one, dt;
    double t_tot;
    bool flag_solid_fail;

    //below vectors are all about step n
    vector<vector<double> > gp_BML;
    vector<vector<double> > gp_B;
    vector<vector<double> > gp_F;
    vector<vector<double> > gp_S;

    vector<double> F_int;
    vector<double> F_ext;
    vector<double> F_damp;
    vector<double> F_gravity;
    vector<double> F;
    vector<vector<double> > Nodal_S_n;



    /** New add mem functions*/
    void SingleIterXplTL(CConfig *config, CGeometry *geometry);
    void ApplyVBC(vector<double> &v_val);
    void ApplyUBC(vector<double> &u_val);
    bool CheckSolution();



private:
    void GetGravityForce(vector<double> &F_val);
    void GetInternalForce(vector<double> &F_val);
    void GetExternalForce(vector<double> &F_val);
    void GetDampingForce(vector<double> &F_val);



public:
/** Default constructor*/
    CSolverSolidTL();
    CSolverSolidTL(CConfig *config, CGeometry *geometry);
/** Default deconstructor*/
    ~CSolverSolidTL();
/** Public member functions*/
    void Preprocessing(CConfig *config, CGeometry *geometry);
    void GetLoads(CConfig *config, CGeometry *geometry);
    void GetBCs(CConfig *config, CGeometry *geometry);
    void GetMaterial(CConfig *config, CGeometry *geometry);
    void Solve(CConfig *config, CGeometry *geometry);
    void SingleIteration(CConfig *config, CGeometry *geometry);

/** Inline Public member functions*/
    inline unsigned short GetnDim() { return nDim;}
    inline vector<vector<double> >& GetGPsS() { return gp_S;}
    inline vector<double>& GetFint() { return F_int;}
};


#endif // CSOLVER_H
