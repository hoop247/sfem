#ifndef CGAUSSPOINT_H
#define CGAUSSPOINT_H

#include <vector>
#include <iostream>
#include "config.hpp"
#include "geometry.hpp"

using namespace std;

class CGaussPoint
{
    private:
        double x; //!< Member variable "x"
        double y; //!< Member variable "y"
        double z; //!< Member variable "z"
        vector<unsigned long> SupportNodes; //!< Member variable "SupportNodes"
        vector<unsigned long> SupportElems; //!< Member variable "SupportElems"
        double Weight; //!< Member variable "Weight"
        vector<double> Normal; //!< Member variable "Normal"
        unsigned long SDidx; //!< Member variable "SDidx"
        unsigned short nDim;

    public:
        /** Default constructor */
        CGaussPoint();
        CGaussPoint(vector<double> &val_coord, double val_weight,
                    vector<unsigned long> &val_spnodes, vector<unsigned long> &val_speles,
                    vector<double> &val_normal, unsigned long val_SDidx);
        /** Default destructor */
        virtual ~CGaussPoint();
        void SetCoords(vector<double> val_coord) {
            x=val_coord[0]; y=val_coord[1]; if(val_coord.size()==3) z=val_coord[2];}
        vector<double> GetCoords() {
            vector<double> val_coord;
            val_coord.push_back(x);
            val_coord.push_back(y);
            if(nDim == 3) val_coord.push_back(z);
            return val_coord;}

        inline double Getx() { return x; }

        inline void Setx(double val) { x = val; }

        inline double Gety() { return y; }

        inline void Sety(double val) { y = val; }

        inline double Getz() { return z; }

        inline void Setz(double val) { z = val; }

        inline vector<unsigned long> GetSupportNodes() { return SupportNodes; }
        inline unsigned long GetSupportNode(unsigned short i) { return SupportNodes[i]; }

        inline void SetSupportNodes(vector<unsigned long> val) { SupportNodes = val; }

        inline void AddSupportNodes(unsigned long val) { SupportNodes.push_back(val);}

        inline vector<unsigned long> GetSupportElems() { return SupportElems; }

        inline void SetSupportElems(vector<unsigned long> val) { SupportElems = val; }

        inline void AddSupportElems(unsigned long val) { SupportElems.push_back(val);}

        inline double GetWeight() { return Weight; }

        inline void SetWeight(double val) { Weight = val; }

        inline vector<double> GetNormal() { return Normal; }

        inline void SetNormal(vector<double> val) { Normal = val; }

        inline unsigned long GetSDidx() { return SDidx; }

        inline void SetSDidx(unsigned long val) { SDidx = val; }

        inline unsigned short GetnSupportNodes() { return SupportNodes.size();}

        inline unsigned short GetnSupportElems() { return SupportElems.size();}

        inline void SetnDim(unsigned short val_nDim) { nDim = val_nDim;}

        inline unsigned short GetnDim() { return nDim;}

    protected:

};

#endif // CGAUSSPOINT_H
