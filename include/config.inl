

inline string CConfig::GetMesh_FileName(void) { return Mesh_FileName; }

inline string CConfig::GetOutput_FileName(void) { return Output_FileName; }

inline unsigned short CConfig::GetKind_Method(void) { return Kind_Method; }

inline unsigned short CConfig::GetKind_Material(void) {return Kind_Material;}

inline unsigned short CConfig::GetKind_Elem(void) {return Kind_Elem;}

inline unsigned short CConfig::GetKind_Solver(void) { return Kind_Solver; }

inline unsigned short CConfig::GetnMarker_All(void) {return nMarker_All;}

inline unsigned short CConfig::GetnMarker_Displacement(void) { return nMarker_Displacement; }

inline unsigned short CConfig::GetnMarker_Velocity(void) { return nMarker_Velocity; }

inline unsigned short CConfig::GetnMarker_Pressure(void) { return nMarker_Pressure; }

inline unsigned short CConfig::GetnMarker_Inlet(void) { return nMarker_Inlet; }

inline unsigned short CConfig::GetnMarker_FSI(void) { return nMarker_FSI; }

inline unsigned long CConfig::GetExtIter(void) { return ExtIter; }

inline unsigned long CConfig::GetnExtIter(void) { return nExtIter;}

inline unsigned long CConfig::GetIntIter(void) { return IntIter; }

inline unsigned long CConfig::GetnIntIter(void) { return nIntIter;}

inline bool CConfig::GetGeo_NL(void) {return Geo_NL;}

inline unsigned long CConfig::GetWrt_Sol_Freq(void) {return Wrt_Sol_Freq;}

inline unsigned long CConfig::GetWrt_Con_Freq(void) {return Wrt_Con_Freq;}

inline unsigned long CConfig::GetWrt_Con_Freq_DualTime(void) { return Wrt_Con_Freq_DualTime; }

inline double CConfig::GetTotalTime(void) { return TotalTime; }

inline double CConfig::GetDeltaTime(void) { return DeltaTime; }

inline void CConfig::SetnMarker_All( unsigned short nMarker) { nMarker_All = nMarker;}

inline double* CConfig::GetMat_Para(void){	return Mat_Para;}

inline double* CConfig::GetMat_Para_Fluid(void) { return Mat_Para_Fluid;}

inline double CConfig::GetDamp_Coef(void){	return Damp_Coef;}

inline double CConfig::GetDensity(void) { return Mat_Para[0];}

inline double* CConfig::GetGravity(void) { return Gravity;}
//
//inline unsigned short CConfig::GetnMarker_Config( void ) {  nMarker_Config = nMarker_Displacement + nMarker_Velocity + nMarker_Pressure;  return nMarker_Config;}

inline bool CConfig::GetRestartOption(void) { return Restart;}

inline string CConfig::GetRestartSDFileName(void) { return Restart_SD_FileName;}

inline string CConfig::GetRestartDATAFileName(void) { return Restart_Data_FileName;}

inline string CConfig::GetFSILoadFileName(void) { return FSI_Load_FileName;}

inline string CConfig::GetFSIDisplacementFileName(void) { return FSI_Displacement_FileName;}

inline string CConfig::GetFSIMarkerMeshFileName(void) { return FSI_Mesh_FileName;}

inline unsigned long CConfig::GetCurrentFSIIter(void) { return CurrentFSIIter;}

inline unsigned short CConfig::GetKind_PostProcessor(void) { return Kind_PostProcessor;}

inline string CConfig::GetHistoryFileName(void) { return History_FileName;}

inline double CConfig::GetResidualMin(void) { return Residual_min;}
