#ifndef GEOMETRY_H
#define GEOMETRY_H

#include <iostream>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <stdlib.h>
#include <cmath>
#include <map>

#include "config.hpp"
#include "node.hpp"
#include "elem.hpp"
#include "boundmarker.hpp"


/** \brief  geometry class is the class read in the mesh or grid.
 *   2015-05-25-17.34, only can read in tetrahedron and triangles for S-FEM.
 *   2015-08-25-14.37, add new class CElem to store all element info.
 *  \author Chen Jiang
 * \param
 * \param
 * \return
 *
 */

using namespace std;


class CGeometry
{
protected:

private:
    string mesh_filename;

    unsigned short nDim;
    unsigned long nElem;
    unsigned long nPoint;
    unsigned long nElem_Tri;
    unsigned long nElem_Tet;
    vector<CElem*> Elem; //modified
    vector<CNode*> Node;

    unsigned short nMarker;
    vector<CBoundMarker*> Markers;
    unsigned long nElem_Tri_Bound;
    unsigned long nElem_Line_Bound;
    unsigned long nElem_Vertex_Bound;

    /** private member function*/
    void AssignElemstoNodes();

public:
    /** Default constructor */
    CGeometry();
    CGeometry(CConfig *config);

    /** Default destructor */
    virtual ~CGeometry();

    /** Public member functions */
    void Read_SU2_format(CConfig *config);
    vector<vector<double> > GetNode_coord();
    CBoundMarker* GetMarker(string val_string);
    /** inline Public member functions */
    inline unsigned short GetnDim(){ return nDim ;}
    inline unsigned long GetnElem() { return nElem; }
    inline unsigned long GetnNode() { return nPoint; }
    inline unsigned long GetnElem_Tri() { return nElem_Tri;}
    inline unsigned long GetnElem_Tet() { return nElem_Tet; }
    inline unsigned short GetnMarker() {return nMarker;}
    inline vector<CElem*> GetElemAll() {return Elem;}
    inline CElem* GetElem(unsigned long val_i) { return Elem[val_i];}
    inline vector<CNode*> GetNodeAll() { return Node; }
    inline CNode* GetNode(unsigned long val_i) { return Node[val_i];}
    inline vector<CBoundMarker*> GetMarkerAll() { return Markers;}
    inline CBoundMarker* GetMarker(unsigned short val_i) { return Markers[val_i];}

};

#endif // GEOMETRY_H
