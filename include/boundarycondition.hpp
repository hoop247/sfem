#ifndef CBOUNDARYCONDITION_H
#define CBOUNDARYCONDITION_H

#include <vector>
#include <algorithm>
#include <iostream>
#include <string>
#include "config.hpp"
#include "geometry.hpp"
#include "boundmarker.hpp"

using namespace std;

/** \brief  CBoundaryCondition class is the base class for BCs.
 *  \date   2015-12-12-09.26
 *  \author Chen Jiang, GR Lab
 *
 */
class CBoundaryCondition
{
    public:
        /** Default constructor */
        CBoundaryCondition();
        CBoundaryCondition(CConfig *config, CGeometry *geometry);
        /** Default destructor */
        virtual ~CBoundaryCondition();

        /** Public member function */
        virtual void SetVelocityBC(CConfig *config, CGeometry *geometry) {};
        virtual void SetPressureBC(CConfig *config, CGeometry *geometry) {};
        virtual void SetDisplacementBC(CConfig *config, CGeometry *geometry) {};

        /** inline functions*/
        inline unsigned long GetVBC_nDOF() { return VBC_DOF.size();}
        inline unsigned long GetVBC_DOF(unsigned long ival) {return VBC_DOF[ival];}
        inline double GetVBC_VAL(unsigned long ival) {return VBC_VAL[ival];}

        inline unsigned long GetPBC_nDOF() { return PBC_DOF.size();}
        inline unsigned long GetPBC_DOF(unsigned long ival) {return PBC_DOF[ival];}
        inline unsigned long GetPBC_VAL(unsigned long ival) {return PBC_VAL[ival];}

        inline void SetPBC_DOF(vector<unsigned long> PBC_DOF_val) { PBC_DOF = PBC_DOF_val; }
        inline void SetPBC_VAL(vector<double> PBC_VAL_val) { PBC_VAL = PBC_VAL_val; }

        inline unsigned long GetUBC_nDOF() { return UBC_DOF.size();}
        inline unsigned long GetUBC_DOF(unsigned long ival) {return UBC_DOF[ival];}
        inline unsigned long GetUBC_VAL(unsigned long ival) {return UBC_VAL[ival];}

    protected:
        vector<unsigned long> VBC_DOF;
        vector<double> VBC_VAL;
        vector<unsigned long> PBC_DOF;
        vector<double> PBC_VAL;
        vector<unsigned long> UBC_DOF;
        vector<double> UBC_VAL;

    private:
};


/** \brief  CBoundaryConditionCFD2D class is the class for BCs in CFD of SFEM++.
 *  \date   2015-12-12-09.26
 *  \author Chen Jiang, GR Lab
 *
 */
class CBoundaryConditionCFD2D : public CBoundaryCondition
{
protected:

private:

public:
    /** Default constructor */
    CBoundaryConditionCFD2D();
    CBoundaryConditionCFD2D(CConfig *config, CGeometry *geometry);
    /** Default deconstructor */
    virtual ~CBoundaryConditionCFD2D();
    /** Public member function */
    virtual void SetVelocityBC(CConfig *config, CGeometry *geometry);
    virtual void SetPressureBC(CConfig *config, CGeometry *geometry);

};

/** \brief  CBoundaryConditionCFD3D class is the class for BCs in CFD of SFEM++.
 *  \date   2016-02-29-10.43
 *  \author Chen Jiang, GR Lab
 *
 */
class CBoundaryConditionCFD3D : public CBoundaryCondition
{
protected:

private:

public:
    /** Default constructor */
    CBoundaryConditionCFD3D();
    CBoundaryConditionCFD3D(CConfig *config, CGeometry *geometry);
    /** Default deconstructor */
    virtual ~CBoundaryConditionCFD3D();
    /** Public member function */
    virtual void SetVelocityBC(CConfig *config, CGeometry *geometry);
    virtual void SetPressureBC(CConfig *config, CGeometry *geometry);

};

/** \brief  CBoundaryConditionCSD2D class is the class for BCs in CSD of SFEM++.
 *  \date   2016-09-13-09.25
 *  \author Chen Jiang, GR Lab
 *
 */
class CBoundaryConditionCSD2D : public CBoundaryCondition
{
protected:

private:

public:
    /** Default constructor */
    CBoundaryConditionCSD2D();
    CBoundaryConditionCSD2D(CConfig *config, CGeometry *geometry);
    /** Default deconstructor */
    virtual ~CBoundaryConditionCSD2D();
    /** Public member function */
    virtual void SetVelocityBC(CConfig *config, CGeometry *geometry);
    virtual void SetDisplacementBC(CConfig *config, CGeometry *geometry);

};


/** \brief  CBoundaryConditionCSD3D class is the class for BCs in CSD of SFEM++.
 *  \date   2016-03-10-09.25
 *  \author Chen Jiang, GR Lab
 *
 */
class CBoundaryConditionCSD3D : public CBoundaryCondition
{
protected:

private:

public:
    /** Default constructor */
    CBoundaryConditionCSD3D();
    CBoundaryConditionCSD3D(CConfig *config, CGeometry *geometry);
    /** Default deconstructor */
    virtual ~CBoundaryConditionCSD3D();
    /** Public member function */
    virtual void SetVelocityBC(CConfig *config, CGeometry *geometry);
    virtual void SetDisplacementBC(CConfig *config, CGeometry *geometry);

};

#endif // CBOUNDARYCONDITION_H
