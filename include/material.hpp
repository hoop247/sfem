#ifndef CMATERIAL_H
#define CMATERIAL_H

#include <vector>
#include <cmath>
#include <algorithm>
#include "config.hpp"
#include "geometry.hpp"
#include "preprocessor.hpp"
#include <Eigen/Core>

using namespace std;
using namespace Eigen;


/** \brief  CMaterial class is the base class for other material objects
 *  \note   This class is only for one Gauss Points.
 *  \date   2016-03-09-10.02
 *  \author Chen Jiang, GR Lab
 *
 */
class CMaterial
{
    protected:

    private:

    public:
        /** Default constructor */
        CMaterial();
        /** Default destructor */
        virtual ~CMaterial();


};

/** \brief  CMaterialSolid class is the base class for other material objects
 *  \date   2016-03-09-10.02
 *  \author Chen Jiang, GR Lab
 *
 */
class CMaterialSolid : public CMaterial
{
    protected:
        unsigned short nDim;
        unsigned short mat_type;
        double *mat_para;
        unsigned short a1[6];
        unsigned short a2[6];
        double Delta[3][3];

        //
        void GetiGPPK2Stress_StVenant(vector<double> &igp_S, vector<vector<double> > &igp_F, vector<vector<double> > &D_val);
        //Todo (JC)
        void GetiGPRightCauchyGreenTensor(vector<vector<double> > &igp_C, vector<vector<double> > &igp_F);
        void GetiGPRightCauchyGreenTensor_Inv(vector<vector<double> > &igp_Cinv, vector<vector<double> > &igp_C);
        void GetiGPRightCauchyGreenTensor_Invar(vector<vector<double> > &igp_CInvar, vector<vector<double> > &igp_C);
        void GetiGPGreenStrain(vector<vector<double> > &igp_E, vector<vector<double> > &igp_F);

    private:

    public:
        /** Default constructor */
        CMaterialSolid();
        /** Default destructor */
        virtual ~CMaterialSolid();

        /** Public func */
        virtual void GetGPsPK2Stress(vector<vector<double> > &gp_S, vector<vector<double> > &gp_F,
                                     unsigned short mat_type_val, double *mat_para_val, unsigned short ndim);

};

#endif // CMATERIAL_H
