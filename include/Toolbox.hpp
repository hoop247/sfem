#ifndef TOOLBOX_INCLUDED
#define TOOLBOX_INCLUDED

#include <vector>
#include <iostream>
#include <algorithm>
#include <cmath>

using namespace std;

double GetLength(vector<double> &p0_coord, vector<double> &p1_coord);

double GetAreaT3(vector<double> &p0_coord, vector<double> &p1_coord, vector<double> &p2_coord);

double GetVolT4(vector<double> &p0_coord, vector<double> &p1_coord, vector<double> &p2_coord,
                vector<double> &p3_coord);

double L2norm(vector<double> &a);

double GetResidual(vector<double> &a_n_one, vector<double> &a_n);

template <typename t>
std::vector<std::vector<t> > GetUniqueRows(std::vector<std::vector<t> > &input);

// several faster functions than built-in functions.
inline double fastInvSqrt(double x);

inline double fastSqrt(double x);

inline double fastPow(const double a, const double b);

void Print2DVecDouble(vector<vector<double> >& A);



#endif // TOOLBOX_INCLUDED
