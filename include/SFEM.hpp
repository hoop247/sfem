#ifndef SFEM_H
#define SFEM_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <fstream>
#include <iostream>
#include <string>
#include "Timer.h"
#include "config.hpp"
#include "geometry.hpp"
#include "option.hpp"
#include <armadillo>

#define MIN(a,b)  (((a) > (b)) ? (b) : (a))
#define MAX(a,b)  (((a) > (b)) ? (a) : (b))
#define pi 3.141592653589793
//#define isnan(x) _isnan(x)
//#define isinf(x) (!_finite(x))
#define fpu_error(x) (isinf(x) || isnan(x))


using namespace std;
using namespace arma;


class SFEM
{
    public:
        SFEM();
        virtual ~SFEM();

        //functions
        void PreProcessing(CConfig *config, CGeometry *geometry);
        void LoadAndBC(CConfig *config, CGeometry *geometry);
        void Solve(CConfig *config, CGeometry *geometry);
        void Restart(CConfig *config, CGeometry *geometry);
        void RestartFSI(CConfig *config, CGeometry *geometry);


        //Variables
        double t_total;
        double PARA_SOLID[7];
        double s_method_type;
        double s_mat_type;
        double s_mat_damp_coef;
        double *s_mat_para;
        double dt;
        double *gravity;
        int MAX_TIME_STEP;

        int FLAG[2];
        int flag_stepinc; // every STEP_INC_SAVE steps to write results
        int flag_stepsave;  // 0 : not write results; 1 : write results
        int flag_stepscreen;

        int n_t=0;
        double t_n=0.0;
        double t_n_one=0.0;

        int s_nnode;

        int s_nele; // number of solid elements

        int s_vbc_ndof;
        int *s_vbc_dof=NULL;
        double *s_vbc_val=NULL;

        int s_ubc_ndof;
        int *s_ubc_dof=NULL;
        double *s_ubc_val=NULL;

        double *s_node_coord;
        int s_ndof;

        // dynamic array for element nodes information, array size: nele*4 for T4 elements
        int *s_ele_node;
        double *s_gp_coord;
        double *s_gpr_coord;
        double *s_ele_center;


        // shape function and derivatives at gp

        // dynamic array for diagonal entries of lumped mass matrix

        double *s_mass_diag;
        double *s_mass_diag_inv;

        double *s_v_n;
        double *s_v_n_one;
        double *s_u_n;
        double *s_u_n_one;
        double *s_a_n;
        double *s_a_n_one;
        double *s_node_coord_n_one;

        int  *s_FSI_interface;
        int  s_FSI_interface_num;
        int  *s_FSI_interface_node;
        int s_FSI_interface_node_num;
        double  *F_ext; /// #JC : F_external



    protected:

    private:
		int s_ngp; // number of Gaussian quadrature points for solid
        int s_ngpr; // number of Gaussian quadrature points for solid

        double *s_gp_weight; // weight of the GP
        double *s_gpr_weight; // weight of the GP

        // maximum spnode number of one GP. for FEM, max_nspnode = number of nodes of one element
        int f_max_nspnode;

		// dynamic array for support nodes of Gaussian quadrature points
        int *s_gp_spnode; // array size :
        int *s_gp_spnode_num;
        int s_gp_max_nspnode;

        int *s_gpr_spnode;
        int *s_gpr_spnode_num;
        int s_gpr_max_nspnode;

        int *s_gp_spele; //
        int *s_gpr_spele; //

		double *s_h_node;
        double s_h_node_bound[2]; // min and max value of s_h_node
        double max_s_h;

        double *s_ele_vol;

        double *s_gp_BML;
        double *s_gpr_BML;

        double *s_gp_sf;
        double *s_gp_sfdx;
        double *s_gp_sfdy;
        double *s_gp_sfdz;

        double *s_gpr_sf;
        double *s_gpr_sfdx;
        double *s_gpr_sfdy;
        double *s_gpr_sfdz;

        int flag_ele_dist;

        void GetUBCDofAndVal(CConfig *config, CGeometry *geometry);
        void GetVBCDofAndVal(CConfig *config, CGeometry *geometry);
        void GetUBCDofAndVal_rotate(CConfig *config, CGeometry *geometry, ivec &node_marker,double t);

        ivec vbc_DOF, ubc_DOF;
        vec vbc_Val, ubc_Val;
        mat node_coord_arma;
        imat ele_arma;
        double *s_u_n_old;


        //restart

        int restart_n_t;
        void ReadinRestartSDFile_binary(CConfig *config, CGeometry *geometry);

        void ReadinRestartDataFile_binary(CConfig *config, CGeometry *geometry);

        void OutputRestartSDFile_binary(CConfig *config, CGeometry *geometry);

        void OutputRestartDataFile_binary(CConfig *config, CGeometry *geometry);

        void OutputRestartSDFile(CConfig *config, CGeometry *geometry);

        void OutputRestartDataFile(CConfig *config, CGeometry *geometry);

        void ReadinFSIForces(CConfig *config, CGeometry *geoemtry);

        void OutputFSIDisplacements(CConfig *config, CGeometry *geometry);

        void OutputFSIMarkerMeshNew(CConfig *config, CGeometry *geometry);

        void OutputSolutionToFile_TECPLOT(void);

        ivec FindRotateNode(CConfig *config, CGeometry *geometry);
};

#endif // SFEM_SU2_H
