#pragma once

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <cstdlib>
#include <algorithm>

using namespace std;

/*!
 * \class CCreateMap
 * \brief creates a map from a list by overloading operator()
 * \tparam T - the key type in the map
 * \tparam U - the mapped value type in the map
 * \author Boost.Assign and anonymous person on stackoverflow
 *
 * We need this to create static const maps that map strings to enum
 * types.  The implementation is based on the Boost.Assign library.  This
 * particular version is taken from
 * http://stackoverflow.com/questions/138600/initializing-a-static-stdmapint-int-in-c
 */
template <typename T, typename U>
class CCreateMap
{
private:
    std::map<T, U> m_map;
public:
    CCreateMap(const T& key, const U& val)
    {
        m_map[key] = val;
    }
    CCreateMap<T, U>& operator()(const T& key, const U& val)
    {
        m_map[key] = val;
        return *this;
    }
    operator std::map<T, U>()
    {
        return m_map;
    }
};

/*!
 * \brief utility function for converting strings to uppercase
 * \param[in,out] str - string we want to convert
 */
inline void StringToUpperCase(string & str)
{
    std::transform(str.begin(), str.end(), str.begin(), ::toupper);
}

/*!
 * \brief utility function for converting strings to uppercase
 * \param[in] str - string we want a copy of converted to uppercase
 * \returns a copy of str in uppercase
 */
inline string StringToUpperCase(const string & str)
{
    string upp_str(str);
    std::transform(upp_str.begin(), upp_str.end(), upp_str.begin(), ::toupper);
    return upp_str;
}

/*!
 * \brief Boolean answers
 */
enum ANSWER
{
    NONE = 0,
    NO = 0,    /*!< \brief Boolean definition of no. */
    YES = 1	/*!< \brief Boolean definition of yes. */
};

/*!
 * \brief different operations of methods
 */
enum ENUM_METHOD
{
    NO_METHOD = 0,	/*!< \brief Definition of no method. */
    FEM = 1,	/*!< \brief FEM. */
    ESFEM = 2,	/*!< \brief ES-FEM. */
    NSFEM = 3,
    FSFEM = 4,
    ESNSFEM = 5,
    FSNSFEM = 6

};



/* BEGIN_CONFIG_ENUMS */
static const map<string, ENUM_METHOD> Method_Map = CCreateMap<string, ENUM_METHOD>
        ("NONE", NO_METHOD)
        ("FEM", FEM)
        ("ESFEM", ESFEM)
        ("NSFEM", NSFEM)
        ("FSFEM", FSFEM)
        ("ESNSFEM", ESNSFEM)
        ("FSNSFEM", FSNSFEM)
        ;


/*!
 * \brief different post-processor
 */
enum ENUM_POSTPROCESSOR
{
    TECPLOT = 1,	/*!< \brief Default TECPLOT. */
    VTK = 2,	/*!< \brief VTK for paraview. */
};

/* BEGIN_CONFIG_ENUMS */
static const map<string, ENUM_POSTPROCESSOR> PostProcessor_Map = CCreateMap<string, ENUM_POSTPROCESSOR>
        ("TECPLOT", TECPLOT)
        ("VTK", VTK);


/*!
 * \brief different solver types
 */

enum ENUM_SOLVER
{
    NO_SOLVER = 0,
    STATIC = 1,
    IMPLICIT_DYNA = 2,
    EXPLICIT_DYNA =3,
    SEMIIMPLICIT_CBS = 11  //CFD solver will use index from 10.
};
/* BEGIN_CONFIG_ENUMS */
static const map<string, ENUM_SOLVER> Solver_Map = CCreateMap<string, ENUM_SOLVER>
        ("STATIC", STATIC)
        ("IMPLICIT_DYNA", IMPLICIT_DYNA)
        ("EXPLICIT_DYNA", EXPLICIT_DYNA)
        ("SEMIIMPLICIT_CBS", SEMIIMPLICIT_CBS);

/*!
 * \brief different material types
 */

enum ENUM_MATERIAL
{
    LINEAR_ELASTIC = 1,
    ST_VENANT = 2,
    MO_RIV =3,
};
/* BEGIN_CONFIG_ENUMS */
static const map<string, ENUM_MATERIAL> Material_Map = CCreateMap<string, ENUM_MATERIAL>
        ("LINEAR_ELASTIC", LINEAR_ELASTIC)
        ("ST_VENANT", ST_VENANT)
        ("MO_RIV", MO_RIV);

/*!
 * \brief different element types
 */

enum ENUM_ELEM
{
    NO_ELEM = 0,
    T3 = 1,
    T4 = 2,
};
/* BEGIN_CONFIG_ENUMS */
static const map<string, ENUM_ELEM> Elem_Map = CCreateMap<string, ENUM_ELEM>
        ("NONE", NO_ELEM)
        ("T3", T3)
        ("T4", T4);



/*!
 * \brief types of geometric entities based on VTK nomenclature
 */
enum GEO_TYPE
{
    VERTEX = 1,   		/*!< \brief VTK nomenclature for defining a vertex element. */
    LINE = 3,			/*!< \brief VTK nomenclature for defining a line element. */
    TRIANGLE = 5, 		/*!< \brief VTK nomenclature for defining a triangle element. */
    RECTANGLE = 9,		/*!< \brief VTK nomenclature for defining a rectangle element. */
    TETRAHEDRON = 10,     	/*!< \brief VTK nomenclature for defining a tetrahedron element. */
    HEXAHEDRON = 12,      	/*!< \brief VTK nomenclature for defining a hexahedron element. */
    WEDGE = 13,     		/*!< \brief VTK nomenclature for defining a wedge element. */
    PYRAMID = 14  		/*!< \brief VTK nomenclature for defining a pyramid element. */
};





/*!
 * \class CAnyOptionRef
 * \brief provides a means of referencing variables of any type
 * \author J. Hicken
 *
 * In order to build a map that associates option names (strings) with
 * options, we need a way of referencing artbitrary types; otherwise,
 * we would need a separate map for ints, doubles, etc.  This class is
 * an abstract base class designed to accommodate this requirement.
 */
class CAnyOptionRef
{
public:
    virtual ~CAnyOptionRef() = 0;
    virtual void WriteValue() = 0;
    virtual void SetValue(const vector<string> & value) = 0;
};
inline CAnyOptionRef::~CAnyOptionRef() {}

/*!
 * \class COptionRef
 * \brief a typed version of the base class for standard types
 * \tparam T - an arbitary standard type (short, int, double, etc)
 * \author J. Hicken
 *
 * This class can accommodate scalars and arrays of constant length.
 * If your option requies a variable length, use class CListOptionRef.
 * Why not use CListOptionRef for arrays?  You could, but COptionRef
 * lets you set default values for arrays (because it does not do any
 * memory management).
 */
template <class T>
class COptionRef : public CAnyOptionRef
{
private:
    unsigned short ndim_; /*!< \brief number of array dimensions */
    unsigned int* dim_; /*!< \brief length of each array dimension */
    T* ref_; /*!< \brief pointer to the option */

    /*!< \brief function pointer used (if necessary) to set the value of the option */
    void (*user_set_value_)(T* ref, const vector<string> & value);

public:

    /*!
     * \brief constructor for scalar options
     * \param[in] value - variable to create a reference to
     */
    COptionRef(T & value)
    {
        ndim_ = 0;
        dim_ = new unsigned int[ndim_];
        ref_ = &value;
        user_set_value_ = NULL;
    }

    /*!
     * \brief constructor for 1D fixed length array options
     * \param[in] value_ptr - pointer to variable to create a reference to
     * \param[in] size - length of the array option
     */
    COptionRef(T * & value_ptr, const int & size)
    {
        ndim_ = 1;
        dim_ = new unsigned int[ndim_];
        if (size <= 0)
        {
            cerr << "COptionRef::COptionRef(T&, const int &): "
                 << "invalid input: size = " << size << endl;
            throw(-1);
        }
        dim_[0] = size;
        ref_ = value_ptr;
        user_set_value_ = NULL;
    }

    /*!
     * \brief constructor for scalar options that require special parsing
     * \param[in] value - variable to create a reference to
     * \param[in] set_value - function that sets options based on vector of strings
     *
     * If the option requires a special parsing of the configuration
     * file, this constructor can be used to set a pointer to a function
     * that performs the necessary parsing
     */
    COptionRef(T & value, void (*set_value)(T*, const vector<string>&))
    {
        ndim_ = 0;
        dim_ = new unsigned int[ndim_];
        ref_ = &value;
        user_set_value_ = set_value;
    }

    /*!
     * \brief class destructor
     */
    ~COptionRef()
    {
        if (ndim_ > 0)
        {
            delete [] dim_;
        }
        user_set_value_ = NULL;
    }

    /*!
     * \brief sets the value of the referenced option using vector of strings
     * \param[in] value - a set of strings used to define the option
     */
    void SetValue(const vector<string> & value)
    {
        stringstream ss;
        if (user_set_value_ == NULL)
        {
            if (ndim_ == 0)
            {
                ss << value[0];
                ss >> (*ref_);
            }
            else if (ndim_ == 1)
            {
                if (value.size() != dim_[0])
                {
                    cerr << "COptionRef::SetValue(const vector<string>&): "
                         << "number of input values does not match size of option array."
                         << endl;
                    throw(-1);
                }
                for (unsigned int i = 0; i < dim_[0]; i++)
                {
                    ss << value[i] << " ";
                    ss >> (ref_[i]);
                }
            }
            else
            {
                cerr << "COptionRef::SetValue(const vector<string>&): "
                     << "cannot handle 2d arrays yet" << endl;
                throw(-1);
            }
        }
        else
        {
            // this option requires a special function for parsing
            (*user_set_value_)(ref_, value);
        }
    }

    /*!
     * \brief write the value of the option to std out (mostly for debugging)
     */
    void WriteValue()
    {
        if (ndim_ == 0)
        {
            cout << *ref_ << endl;
        }
        else if (ndim_ == 1)
        {
            for (unsigned int i = 0; i < dim_[0]; i++)
                cout << ref_[i] << ", ";
            cout << endl;
        }
    }
};

/*!
 * \class CListOptionRef
 * \brief for options of variable array length
 * \author J. Hicken
 */
template <class T>
class CListOptionRef : public CAnyOptionRef
{
private:
    T** ref_; /*!< \brief pointer to the memory holding the list option */
    unsigned short* ref_size_; /*!< \brief number of items in list */

public:

    /*!
     * \brief constructor for list-type options
     * \param[in] value - variable we want to create a reference to
     * \param[in] size - number of elements the list WILL have
     */
    CListOptionRef(unsigned short & size, T* & value)
    {
        ref_ = &value;
        *ref_ = NULL;
        size = 0;
        ref_size_ = &size;
    }

    /*!
     * \brief sets the value of the referenced convection option using vector of strings
     * \param[in] value - a set of strings used to define the option
     */
    void SetValue(const vector<string> & value)
    {
        stringstream ss;
        *ref_size_ = static_cast<unsigned short>(value.size());
        if (*ref_ != NULL)
        {
            cerr << "Error in CListOptionRef(SetValue): "
                 << "list option has already been allocated."
                 << endl;
            throw(-1);
        }
        (*ref_) = new T[*ref_size_];
        for (unsigned short i = 0; i < *ref_size_; i++)
        {
            ss << value[i] << " ";
            ss >> (*ref_)[i];
        }
    }

    /*!
     * \brief write the value of the option to std out (mostly for debugging)
     */
    void WriteValue()
    {
        for (unsigned short i = 0; i < *ref_size_; i++)
            cout << (*ref_)[i] << ", ";
        cout << endl;
    }
};

/*!
 * \class CEnumOptionRef
 * \brief a typed version of the base class for options associated with enumerations
 * \tparam T - the type of option (usually unsigned short)
 * \tparam Tenum - an enumeration assocatied with T
 * \author J. Hicken
 */
template <class T, class Tenum>
class CEnumOptionRef : public CAnyOptionRef
{
private:
    T** ref_; /*!< \brief pointer to memory for the option */
    unsigned short* ref_dim_; /*!< \brief number of elemets in ref_ for array enums*/
    const map<string, Tenum> * Tmap_; /*!< \brief map between strings and enums */

public:

    /*!
     * \brief constructor for scalar enum options
     * \param[in] value - variable to create a reference to
     * \param[in] Tmap - map between strings and enums
     */
    CEnumOptionRef(T & value, const map<string, Tenum> & Tmap)
    {
        ref_ = new T*;
        *ref_ = &value;
        ref_dim_ = NULL;
        Tmap_ = &Tmap;
    }

    /*!
     * \brief constructor for list enum options
     * \param[in] value - variable to create a reference to
     * \param[in] Tmap - map between strings and enums
     * \param[in] size - length of the array option
     */
    CEnumOptionRef(unsigned short & size, T* & value, const map<string, Tenum> & Tmap)
    {
        ref_ = &value;
        ref_dim_ = &size;
        Tmap_ = &Tmap;
    }

    /*!
     * \brief write the value of the option to std out (mostly for debugging)
     */
    void WriteValue()
    {
        if (ref_dim_ == NULL)
        {
            cout << *(*ref_) << endl;
        }
        else
        {
            for (unsigned int i = 0; i < *ref_dim_; i++)
                cout << (*ref_)[i] << ", ";
            cout << endl;
        }
    }

    /*!
     * \brief sets the value of the referenced enum option using vector of strings
     * \param[in] value - a set of strings used to define the option
     */
    void SetValue(const vector<string> & value)
    {

//    int rank = MASTER_NODE;
//#ifndef NO_MPI
//    rank = MPI::COMM_WORLD.Get_rank();
//#endif

        typename map<string,Tenum>::const_iterator it;
        if (ref_dim_ == NULL)
        {
            // this is a scalar enum option
            it = Tmap_->find(StringToUpperCase(value[0]));
            if (it == Tmap_->end())
            {
//        if (rank == MASTER_NODE) {
                cerr << "ERROR: Cannot find value " << value[0] << " in given map." << endl;
                cerr << "Please check the name of the variable in the config file." << endl;
//        }
//#ifdef NO_MPI
//        exit(1);
//#else
//        MPI::COMM_WORLD.Abort(1);
//        MPI::Finalize();
//#endif
            }
            *(*ref_) = it->second;
        }
        else
        {
            // this is an array enum option
            (*ref_dim_) = static_cast<unsigned short>(value.size());
            (*ref_) = new T[*ref_dim_];
            for (unsigned short i = 0; i < *ref_dim_; i++)
            {
                it = Tmap_->find(StringToUpperCase(value[i]));
                if (it == Tmap_->end())
                {
//          if (rank == MASTER_NODE) {
                    cerr << "ERROR: Cannot find value " << value[i] << " in given map." << endl;
                    cerr << "Please check the name of the variable in the config file." << endl;
//          }
//#ifdef NO_MPI
//          exit(1);
//#else
//          MPI::COMM_WORLD.Abort(1);
//          MPI::Finalize();
//#endif
                }
                (*ref_)[i] = it->second;
            }
        }
    }

};


/*!
 * \class CMarkerInletRef_
 * \brief Specialized option for inlet boundary markers (no flow direction)
 * \author J. Hicken
 */
class CMarkerInletRef_ : public CAnyOptionRef
{
private:
    unsigned short* nMarker_Inlet_; /*!< \brief the number of inlet marker boundaries */
    string** Marker_Inlet_;         /*!< \brief string names of inlet boundaries */
    double** Ttotal_;               /*!< \brief specified total temperatures for inlet boundaries */
    double** Ptotal_;               /*!< \brief specified total pressures for inlet boundaries */

public:

    /*!
     * \brief constructor for inlet marker option
     * \param[in] nMarker_Inlet - the number of inlet marker boundaries
     * \param[in] Marker_Inlet - string names of inlet boundaries
     * \param[in] Ttotal - specified total temperatures for inlet boundaries
     * \param[in] Ptotal - specified total pressures for inlet boundaries
     * \param[in] FlowDir - specified flow direction vector (unit vector) for inlet boundaries
     */
    CMarkerInletRef_(unsigned short & nMarker_Inlet, string* & Marker_Inlet, double* & Ttotal,
                     double* & Ptotal)
    {
        nMarker_Inlet_ = &nMarker_Inlet;
        Marker_Inlet_ = &Marker_Inlet;
        *Marker_Inlet_ = NULL;
        Ttotal_ = &Ttotal;
        *Ttotal_ = NULL;
        Ptotal_ = &Ptotal;
        *Ptotal_ = NULL;
    }

    /*!
     * \brief sets the value of the inlet parameters given the vector of strings
     * \param[in] value - a set of strings used to define the option
     */
    void SetValue(const vector<string> & value)
    {
        if ( (*Marker_Inlet_ != NULL) || (*Ttotal_ != NULL) || (*Ptotal_ != NULL) )
        {
            cerr << "Error in CMarkerInletRef_::SetValue(): "
                 << "one or more inlet-marker option arrays have already been allocated."
                 << endl;
            throw(-1);
        }
        if (static_cast<int>(value.size()) % 3 != 0)
        {
            if (value[0].compare("NONE") == 0)
            {
                *nMarker_Inlet_ = 0;
                return;
            }
            cerr << "Error in CMarkerInletRef_::SetValue(): "
                 << "incorrect number of MARKER_INLET parameters in the configuration file."
                 << endl;
            throw(-1);
        }
        *nMarker_Inlet_ = static_cast<unsigned short>(value.size())/3;
        (*Marker_Inlet_) = new string[*nMarker_Inlet_];
        (*Ttotal_)       = new double[*nMarker_Inlet_];
        (*Ptotal_)       = new double[*nMarker_Inlet_];

        stringstream ss;
        unsigned short i = 0;
        for (unsigned short iMarker = 0; iMarker < *nMarker_Inlet_; iMarker++)
        {
            ss << value[i++] << " ";
            ss >> (*Marker_Inlet_)[iMarker];
            ss << value[i++] << " ";
            ss >> (*Ttotal_)[iMarker];
            ss << value[i++] << " ";
            ss >> (*Ptotal_)[iMarker];
        }
    }

    /*!
     * \brief write the value of the option to std out (mostly for debugging)
     */
    void WriteValue()
    {
        cout << "Inlet markers (" << (*nMarker_Inlet_) << ")" << endl;
        for (unsigned short iMarker = 0; iMarker < *nMarker_Inlet_; iMarker++)
            cout << "name = " << (*Marker_Inlet_)[iMarker]
                 << ": temp. = " << (*Ttotal_)[iMarker]
                 << ": pressure. = " << (*Ptotal_)[iMarker] << endl;
    }

};



/*!
 * \class CMarkerDisplacementRef
 * \brief Specialized option for Displacement boundary markers in FEM
 * \author C. Jiang
 * \2015-05-25-13.04
 */
class CMarkerDisplacementRef : public CAnyOptionRef
{
private:
    unsigned short* nMarker_Displacement_; /*!< \brief the number of Displacement marker boundaries */
    string** Marker_Displacement_;         /*!< \brief string names of Displacement boundaries */
    unsigned short** Displx_idx_;  /*!< \brief indicator for free or constrain the displacement in x-direction for Displacement boundaries */
    unsigned short** Disply_idx_;
    unsigned short** Displz_idx_;
    double** Displx_;              /*!< \brief specified Displacement in x-direction for Displacement boundaries */
    double** Disply_;
    double** Displz_;

public:

    /*!
     * \brief constructor for outlet marker option
     * \param[in] nMarker_Displacement - the number of Displacement marker boundaries
     * \param[in] Marker_Displacement - string names of Displacement boundaries
     * \param[in] Displ - specified back Displacement for Displacement boundaries
     */
    CMarkerDisplacementRef(unsigned short & nMarker_Displacement, string* & Marker_Displacement,
                           unsigned short* & Displx_idx, unsigned short* & Disply_idx, unsigned short* & Displz_idx,
                           double* & Displx, double* &Disply, double* &Displz)
    {
        nMarker_Displacement_ = &nMarker_Displacement;
        Marker_Displacement_ = &Marker_Displacement;
        *Marker_Displacement_ = NULL;
        Displx_idx_ = &Displx_idx;
        *Displx_idx_ = NULL;
        Disply_idx_ = &Disply_idx;
        *Disply_idx_ = NULL;
        Displz_idx_ = &Displz_idx;
        *Displz_idx_ = NULL;
        Displx_ = &Displx;
        *Displx_ = NULL;
        Disply_ = &Disply;
        *Disply_ = NULL;
        Displz_ = &Displz;
        *Displz_ = NULL;
    }

    /*!
     * \brief sets the value of the Displacement parameters given the vector of strings
     * \param[in] value - a set of strings used to define the option
     */
    void SetValue(const vector<string> & value)
    {
        if ( (*Marker_Displacement_ != NULL) || (*Displx_ != NULL)|| (*Disply_ != NULL) || (*Displz_ != NULL) )
        {
            cerr << "Error in CMarkerDisplacementRef::SetValue(): "
                 << "one or more Displacement-marker option arrays have already been allocated."
                 << endl;
            throw(-1);
        }
        //Below 7 control how many parameters in your marker option. By JC 2015-05-25-15.48
        if (static_cast<int>(value.size()) % 7 != 0)
        {
            if (value[0].compare("NONE") == 0)
            {
                *nMarker_Displacement_ = 0;
                return;
            }
            cerr << "Error in CMarkerDisplacementRef::SetValue(): "
                 << "incorrect number of MARKER_Displacement parameters in the configuration file."
                 << endl;
            throw(-1);
        }
        *nMarker_Displacement_  = static_cast<unsigned short>(value.size())/7;
        (*Marker_Displacement_) = new string[*nMarker_Displacement_];
        (*Displx_idx_)  = new unsigned short [*nMarker_Displacement_];
        (*Disply_idx_)  = new unsigned short [*nMarker_Displacement_];
        (*Displz_idx_)  = new unsigned short [*nMarker_Displacement_];
        (*Displx_)      = new double[*nMarker_Displacement_];
        (*Disply_)      = new double[*nMarker_Displacement_];
        (*Displz_)      = new double[*nMarker_Displacement_];

        stringstream ss;
        unsigned short i = 0;
        for (unsigned short iMarker = 0; iMarker < *nMarker_Displacement_; iMarker++)
        {
            ss << value[i++] << " ";
            ss >> (*Marker_Displacement_)[iMarker];
            ss << value[i++] << " ";
            ss >> (*Displx_idx_)[iMarker];
            ss << value[i++] << " ";
            ss >> (*Disply_idx_)[iMarker];
            ss << value[i++] << " ";
            ss >> (*Displz_idx_)[iMarker];
            ss << value[i++] << " ";
            ss >> (*Displx_)[iMarker];
            ss << value[i++] << " ";
            ss >> (*Disply_)[iMarker];
            ss << value[i++] << " ";
            ss >> (*Displz_)[iMarker];
        }
    }

    /*!
     * \brief write the value of the option to std out (mostly for debugging)
     */
    void WriteValue()
    {
        cout << "Displacement markers (" << (*nMarker_Displacement_) << ")" << endl;
        for (unsigned short iMarker = 0; iMarker < *nMarker_Displacement_; iMarker++)
            cout << "name = " << (*Marker_Displacement_)[iMarker]
                 << ": displx_indicator = " << (*Displx_idx_)[iMarker]
                 << ": disply_indicator = " << (*Disply_idx_)[iMarker]
                 << ": displz_indicator = " << (*Displz_idx_)[iMarker]
                 << ": displx. = " << (*Displx_)[iMarker]
                 << ": disply. = " << (*Disply_)[iMarker]
                 << ": displz. = " << (*Displz_)[iMarker] << endl;
    }

};



/*!
 * \class CMarkerVelocityRef
 * \brief Specialized option for Velocity boundary markers
 * \author F. Palacios
 */
class CMarkerVelocityRef : public CAnyOptionRef
{
private:
    unsigned short* nMarker_Velocity_; /*!< \brief the number of Velocity marker boundaries */
    string** Marker_Velocity_;         /*!< \brief string names of Velocity boundaries */
    unsigned short** Veloc_X_Idx_;
    unsigned short** Veloc_Y_Idx_;
    unsigned short** Veloc_Z_Idx_;
    double** Veloc_X_;              /*!< \brief specified Velocity for Velocity boundaries */
    double** Veloc_Y_;
    double** Veloc_Z_;

public:

    /*!
     * \brief constructor for outlet marker option
     * \param[in] nMarker_Displacement - the number of Displacement marker boundaries
     * \param[in] Marker_Displacement - string names of Displacement boundaries
     * \param[in] Displ - specified back Displacement for Displacement boundaries
     */
    CMarkerVelocityRef(unsigned short & nMarker_Velocity, string* & Marker_Velocity,
                       unsigned short* & Veloc_X_Idx, unsigned short* & Veloc_Y_Idx, unsigned short* & Veloc_Z_Idx,
                       double* & Veloc_X, double* & Veloc_Y, double* &Veloc_Z)
    {
        nMarker_Velocity_ = &nMarker_Velocity;
        Marker_Velocity_ = &Marker_Velocity;
        *Marker_Velocity_ = NULL;
        Veloc_X_Idx_ = &Veloc_X_Idx;
        *Veloc_X_Idx_ = NULL;
        Veloc_Y_Idx_ = &Veloc_Y_Idx;
        *Veloc_Y_Idx_ = NULL;
        Veloc_Z_Idx_ = &Veloc_Z_Idx;
        *Veloc_Z_Idx_ = NULL;
        Veloc_X_ = &Veloc_X;
        *Veloc_X_ = NULL;
        Veloc_Y_ = &Veloc_Y;
        *Veloc_Y_ = NULL;
        Veloc_Z_ = &Veloc_Z;
        *Veloc_Z_ = NULL;
    }

    /*!
     * \brief sets the value of the Displacement parameters given the vector of strings
     * \param[in] value - a set of strings used to define the option
     */
    void SetValue(const vector<string> & value)
    {
        if ( (*Marker_Velocity_ != NULL) )
        {
            cerr << "Error in CMarkerVelocityRef::SetValue(): "
                 << "one or more Velocity-marker option arrays have already been allocated."
                 << endl;
            throw(-1);
        }
        if (static_cast<int>(value.size()) % 7 != 0)
        {
            if (value[0].compare("NONE") == 0)
            {
                *nMarker_Velocity_ = 0;
                return;
            }
            cerr << "Error in CMarkerVelocityRef::SetValue(): "
                 << "incorrect number of MARKER_Velocity parameters in the configuration file."
                 << endl;
            throw(-1);
        }
        *nMarker_Velocity_  = static_cast<unsigned short>(value.size())/7;
        (*Marker_Velocity_) = new string[*nMarker_Velocity_];
        (*Veloc_X_Idx_)     = new unsigned short[*nMarker_Velocity_];
        (*Veloc_Y_Idx_)     = new unsigned short[*nMarker_Velocity_];
        (*Veloc_Z_Idx_)     = new unsigned short[*nMarker_Velocity_];
        (*Veloc_X_)      = new double[*nMarker_Velocity_];
        (*Veloc_Y_)      = new double[*nMarker_Velocity_];
        (*Veloc_Z_)      = new double[*nMarker_Velocity_];
        stringstream ss;
        unsigned short i = 0;
        for (unsigned short iMarker = 0; iMarker < *nMarker_Velocity_; iMarker++)
        {
            ss << value[i++] << " ";
            ss >> (*Marker_Velocity_)[iMarker];
            ss << value[i++] << " ";
            ss >> (*Veloc_X_Idx_)[iMarker];
            ss << value[i++] << " ";
            ss >> (*Veloc_Y_Idx_)[iMarker];
            ss << value[i++] << " ";
            ss >> (*Veloc_Z_Idx_)[iMarker];
            ss << value[i++] << " ";
            ss >> (*Veloc_X_)[iMarker];
            ss << value[i++] << " ";
            ss >> (*Veloc_Y_)[iMarker];
            ss << value[i++] << " ";
            ss >> (*Veloc_Z_)[iMarker];
        }
    }

    /*!
     * \brief write the value of the option to std out (mostly for debugging)
     */
    void WriteValue()
    {
        cout << "Velocity markers (" << (*nMarker_Velocity_) << ")" << endl;
        for (unsigned short iMarker = 0; iMarker < *nMarker_Velocity_; iMarker++)
            cout << "name = " << (*Marker_Velocity_)[iMarker]
                 << ": veloc_x_indicator. = " << (*Veloc_X_Idx_)[iMarker]
                 << ": veloc_y_indicator. = " << (*Veloc_Y_Idx_)[iMarker]
                 << ": veloc_z_indicator. = " << (*Veloc_Z_Idx_)[iMarker]
                 << ": veloc_x. = " << (*Veloc_X_)[iMarker]
                 << ": veloc_y. = " << (*Veloc_Y_)[iMarker]
                 << ": veloc_z. = " << (*Veloc_Z_)[iMarker]
                 << endl;
    }

};

/*!
 * \class CMarkerPressureRef
 * \brief Specialized option for Velocity boundary markers
 * \author F. Palacios
 */
class CMarkerPressureRef : public CAnyOptionRef
{
private:
    unsigned short* nMarker_Pressure_; /*!< \brief the number of pressure marker boundaries */
    string** Marker_Pressure_;         /*!< \brief string names of pressure boundaries */
    double** Pressure_;              /*!< \brief specified pressure for pressure boundaries */

public:

    /*!
     * \brief constructor for pressure marker option
     * \param[in] nMarker_Pressure - the number of Pressure marker boundaries
     * \param[in] Marker_Pressure - string names of Pressure boundaries
     * \param[in] Pressure- specified back Pressure for Pressure boundaries
     */
    CMarkerPressureRef(unsigned short & nMarker_Pressure, string* & Marker_Pressure, double* & Pressure)
    {
        nMarker_Pressure_ = &nMarker_Pressure;
        Marker_Pressure_ = &Marker_Pressure;
        *Marker_Pressure_ = NULL;
        Pressure_ = &Pressure;
        *Pressure_ = NULL;
    }

    /*!
     * \brief sets the value of the Pressure parameters given the vector of strings
     * \param[in] value - a set of strings used to define the option
     */
    void SetValue(const vector<string> & value)
    {
        if ( (*Marker_Pressure_ != NULL) || (*Pressure_ != NULL) )
        {
            cerr << "Error in CMarkerPressureRef::SetValue(): "
                 << "one or more Pressure-marker option arrays have already been allocated."
                 << endl;
            throw(-1);
        }
        if (static_cast<int>(value.size()) % 2 != 0)
        {
            if (value[0].compare("NONE") == 0)
            {
                *nMarker_Pressure_ = 0;
                return;
            }
            cerr << "Error in CMarkerPressureRef::SetValue(): "
                 << "incorrect number of MARKER_Pressure parameters in the configuration file."
                 << endl;
            throw(-1);
        }
        *nMarker_Pressure_  = static_cast<unsigned short>(value.size())/2;
        (*Marker_Pressure_) = new string[*nMarker_Pressure_];
        (*Pressure_)      = new double[*nMarker_Pressure_];
        stringstream ss;
        unsigned short i = 0;
        for (unsigned short iMarker = 0; iMarker < *nMarker_Pressure_; iMarker++)
        {
            ss << value[i++] << " ";
            ss >> (*Marker_Pressure_)[iMarker];
            ss << value[i++] << " ";
            ss >> (*Pressure_)[iMarker];
        }
    }

    /*!
     * \brief write the value of the option to std out (mostly for debugging)
     */
    void WriteValue()
    {
        cout << "Pressure markers (" << (*nMarker_Pressure_) << ")" << endl;
        for (unsigned short iMarker = 0; iMarker < *nMarker_Pressure_; iMarker++)
            cout << "name = " << (*Marker_Pressure_)[iMarker]
                 << ": pressure. = " << (*Pressure_)[iMarker] << endl;
    }

};

/*!
 * \class CMarkerOptionRef
 * \brief a typed version of the base class for marker options
 * \author J. Hicken
 */
class CMarkerOptionRef : public CAnyOptionRef
{
private:
    string** marker_ref_; /*!< \brief pointer to the memory for the marker option */
    unsigned short* num_marker_; /*!< \brief number of markers */

public:

    /*!
     * \brief constructor for marker options
     * \param[in] value - variable to create a reference to
     * \param[in] size - variable refering to the number of markers
     */
    CMarkerOptionRef(string* & value, unsigned short & size)
    {
        marker_ref_ = &value;
        *marker_ref_ = NULL;
        num_marker_ = &size;
    }

    /*!
     * \brief sets the value of the referenced marker option using vector of strings
     * \param[in] value - a set of strings used to define the option
     */
    void SetValue(const vector<string> & value)
    {
        if (value.size() == 0)
        {
            cerr << "Error in CMarkerOptionRef::SetValue(): "
                 << "marker option in config file with no value" << endl;
            cerr << "Use NONE for no markers." << endl;
            throw(-1);
        }
        if (marker_ref_ != NULL) delete [] (*marker_ref_);
        (*marker_ref_) = new string[value.size()];
        for (unsigned int i = 0; i < value.size(); i++)
            (*marker_ref_)[i] = value[i];
        if ( (value.size() == 1) && ((*marker_ref_)[0] == "NONE") )
        {
            *num_marker_ = 0;
        }
        else
        {
            *num_marker_ = value.size();
        }
    }

    /*!
     * \brief write the value of the option to std out (mostly for debugging)
     */
    void WriteValue()
    {
        for (unsigned int i = 0; i < *num_marker_; i++)
            cout << (*marker_ref_)[i] << ", ";
        cout << endl;
    }
};



