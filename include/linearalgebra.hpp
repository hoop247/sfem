#ifndef CLINEARALGEBRA_H
#define CLINEARALGEBRA_H

/** \brief  CSolver class is the base class for wrapping linear equations solver.
 *  \date   2015-09-09-10.39
 *  \author Chen Jiang, GR Lab
 *
 */

 #include <vector>
 #include <iostream>
 #include <algorithm>
 #include <Eigen/Eigen>
 #include <Eigen/Sparse>

 using namespace std;
 using namespace Eigen;

class CLinearAlgebra
{
    public:
        /** Default constructor */
        CLinearAlgebra();
        /** Default destructor */
        virtual ~CLinearAlgebra();

    protected:
        bool direct_solver;
        bool iterative_solver;
        bool MKL_solver;
    private:
};

#endif // CLINEARALGEBRA_H
