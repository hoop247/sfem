#ifndef COUTPUT_H
#define COUTPUT_H

#include "config.hpp"
#include "geometry.hpp"
#include "option.hpp"
#include "elem.hpp"
#include <armadillo>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;
using namespace arma;

/** \class  COutput
 *  \brief  Base class which output any data needed for post-processing.
 *  \author Chen Jiang, GR Lab
 *  \date   2015-12-16-21.32
 */

class COutput
{
    public:
        /** Default constructor */
        COutput();
        /** Default destructor */
        virtual ~COutput();

        /** Member functions */
        virtual void WriteSolutionSolid
            (CConfig *config, CGeometry *geoemtry, vector<double> &u, vector<double> &v, vector<vector<double> > &S, unsigned long n_t, double t) {};

        virtual void WriteSolutionFluid
            (CConfig *config, CGeometry *geometry, vector<double> &v, vector<double> &p, unsigned long n_t, double t) {};

        virtual void WriteConvergeHistorySolid
            (CConfig *config, double r_u, double r_v, unsigned long n) {};

        virtual void WriteConvergeHistoryFluid
            (CConfig *config, double r_v, double r_p, unsigned long n) {};
    protected:

    private:
};

/** \class  COutput
 *  \brief  Base class which output any data needed for post-processing.
 *  \author Chen Jiang, GR Lab
 *  \date   2015-12-16-21.32
 */
class COutputFluid : public COutput
{
    public:
        /** Default constructor */
        COutputFluid();
        /** Default destructor */
        virtual ~COutputFluid();

        /** Member functions */
        virtual void WriteSolutionFluid
            (CConfig *config, CGeometry *geometry, vector<double> &v, vector<double> &p, unsigned long n_t, double t);
        virtual void WriteConvergeHistoryFluid
            (CConfig *config, double r_v, double r_p, unsigned long n);
    protected:
        void WriteSolutionTecplot(CConfig *config, CGeometry *geometry, vector<double> &v, vector<double> &p, unsigned long n_t, double t);
        //TODO (JC) : add in future
        void WriteSolutionVTK(CConfig *config, CGeometry *geometry, vector<double> &v, vector<double> &p, unsigned long n_t, double t) {};
    private:
        vector<unsigned long> ConHisotry_n;
        vector<double> ConHistory_rv;
        vector<double> ConHistory_rp;
};

/** \class  COutputSolid
 *  \brief  class which output any data of solid solver needed for post-processing.
 *  \author Chen Jiang, GR Lab
 *  \date   2016-03-08-14.34
 */
class COutputSolid : public COutput
{
    public:
        /** Default constructor */
        COutputSolid();
        /** Default destructor */
        virtual ~COutputSolid();

        /** Member functions */
        virtual void WriteSolutionSolid
            (CConfig *config, CGeometry *geometry, vector<double> &u, vector<double> &v, vector<vector<double> > &S,
            unsigned long n_t, double t);
        virtual void WriteConvergeHistorySolid
            (CConfig *config, double r_u, double r_v, unsigned long n);
    protected:
        void WriteSolutionTecplot(CConfig *config, CGeometry *geometry, vector<double> &u, vector<double> &v, vector<vector<double> > &S,
            unsigned long n_t, double t);
        //TODO (JC) : add in future
        void WriteSolutionVTK(CConfig *config, CGeometry *geometry, vector<double> &u, vector<double> &v, vector<vector<double> > &S,
            unsigned long n_t, double t) {};
    private:
        vector<unsigned long> ConHisotry_n;
        vector<double> ConHistory_ru;
        vector<double> ConHistory_rv;
};

#endif // COUTPUT_H
