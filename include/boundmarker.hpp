#ifndef CBOUNDMARKER_H
#define CBOUNDMARKER_H

/*!
 * \class CBoundMarker
 * \brief Class to define the boundary marker.
 * \author Chen Jiang.
 * \date 2015-09-20-13.50
 */

#include <vector>
#include <string>
#include <algorithm>
#include "elem.hpp"
#include "option.hpp"

using namespace std;
using namespace arma;

class CBoundMarker
{
    public:
        /** Default constructor */
        CBoundMarker();
        /** Overloaded constructor */

        /** Default destructor */
        virtual ~CBoundMarker();

        /** public member funcs*/
        void SetNodes(void);
        void SetBoundElemsNormalsAll(vector<vector<double> > &node_coord);
        void SetBoundElemsCGsAll(vector<vector<double> > &node_coord);
        void SetBoundElemsVolAll(vector<vector<double> > &node_coord);
        void Display();
        /** inline member funcs*/
        inline void SetTag(string val_tag) {Tag=val_tag;}
        inline void SetnBoundElem(unsigned long val_nboundele) {nBoundElems = val_nboundele;}
        inline void SetBoundElem(CElem* boundele) { BoundElems.push_back(boundele);}
        inline CElem* GetBoundElem(unsigned long iele) { return BoundElems[iele];}
        inline unsigned long GetnBoundElem(void) {return nBoundElems;}
        inline unsigned long GetnNode(void) {return nNodes;}
        inline unsigned long GetNode(unsigned long inode) {return Nodes[inode];}

        inline vector<vector<double> > GetBoundElemsNormalsAll(void){
            return BoundElemsNormals;}
        inline vector<double> GetBoundElemNormal(unsigned long iboundele){
            return BoundElemsNormals[iboundele];}

        inline vector<vector<double> > GetBoundElemCGsAll(void){return BoundElemsCGs;}
        inline vector<double> GetBoundElemCG(unsigned long iboundele) {return BoundElemsCGs[iboundele];}

        inline vector<double> GetBoundElemVolAll(void) { return BoundElemsVols;}
        inline double GetBoundElemVol(unsigned long iboundele) { return BoundElemsVols[iboundele];}

        inline string GetTag(void) {return Tag;}


    protected:
        string Tag;
        vector<CElem*> BoundElems;
        unsigned long nBoundElems;
        vector<unsigned long> Nodes;
        unsigned long nNodes; // uniqued number of nodes.
        vector<vector<double> > BoundElemsNormals;
        vector<vector<double> > BoundElemsCGs;
        vector<double> BoundElemsVols;

    private:
        vector<double> GetCG_TRI(vector<double> &p0_xyz, vector<double> &p1_xyz, vector<double> &p2_xyz);
        vector<double> GetCG_LINE(vector<double> &p0_xyz, vector<double> &p1_xyz);

        vector<double> GetNormal_TRI(vector<double> &p0_xyz, vector<double> &p1_xyz, vector<double> &p2_xyz);
        vector<double> GetNormal_LINE(vector<double> &p0_xyz, vector<double> &p1_xyz);

        double GetArea_TRI(vector<double> &p0_xyz, vector<double> &p1_xyz, vector<double> &p2_xyz);
        double GetLength_LINE(vector<double> &p0_xyz, vector<double> &p1_xyz);


};




#endif // CBOUNDMARKER_H
