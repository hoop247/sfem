#ifndef CONFIG_H
#define CONFIG_H



#pragma once

#include <iostream>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <string.h>
#include <string>
#include <vector>
#include <stdlib.h>
#include <cmath>
#include <map>

#include "./option.hpp"

using namespace std;

/*!
 * \class CConfig
 * \brief Main class for defining the problem; basically this class reads the configuration file, and
 *        stores all the information.
 * \author Chen Jiang
 * \version 0.0.0
 */
 class CConfig{
private:

    unsigned short Kind_Method, /*!< \brief Kind of solid mechancis solver, FEM or SFEM. */
    Kind_Material, /*!< \brief Kind of solid materials, Linear elastic or else. */
    Kind_Elem, /*!< \brief Kind of element. */
    Kind_Solver,
    Kind_PostProcessor; /*!< \brief Kind of solid problem type, static, implicit or explicit. */

    bool  Mat_Incomp, /*!< \brief Flag to know if it is a incompressible problem. */
    Geo_NL, /*!< \brief Flag to know if it is a geometrical nonlinear problem. */
    Restart; /*!< \brief Flag to know if it is a restart problem. */

    string Mesh_FileName, /*!< \brief Input structural mesh file name. */
    Output_FileName, /*!< \brief output structural displacement file name. */
    Restart_SD_FileName,
    Restart_Data_FileName,
    FSI_Load_FileName,
    FSI_Displacement_FileName,
    FSI_Mesh_FileName,
    History_FileName;


    unsigned short Kind_SFEM;  /*!< \brief Kind of S-FEM, ES, FS or NS */

    unsigned short nMarker_Displacement, /*!< \brief Number  of marker for displacement*/
    nMarker_Velocity, /*!< \brief Number  of marker for velocity*/
    nMarker_Pressure, /*!< \brief Number  of marker for pressure*/
    nMarker_Config, /*!< \brief Number  of marker for pressure*/
    nMarker_All, /*!< \brief Number  of all markers*/
    nMarker_FSI, /*!< \brief Number  of FSI markers*/
    nMarker_Inlet;

    string *Marker_Displacement,  /*!< \brief Displacement boundary surface markers*/
    *Marker_Velocity, /*!< \brief Velocity boundary surface markers*/
    *Marker_Pressure,  /*!< \brief Pressure loads surface markers*/
    *Marker_All_Tag,  /*!< \brief Global index for markers using grid information. */
    *Marker_FSI, /*!< \brief  FSI markers*/
    *Marker_Inlet;

    double *Displ_X;
    double *Displ_Y;
    double *Displ_Z;
    unsigned short *Displ_X_Indicator;
    unsigned short *Displ_Y_Indicator;
    unsigned short *Displ_Z_Indicator;

    double *Velocity_X;
    double *Velocity_Y;
    double *Velocity_Z;
    unsigned short *Velocity_X_Indicator;
    unsigned short *Velocity_Y_Indicator;
    unsigned short *Velocity_Z_Indicator;
    double *Pressure;

    double *Ttotal;
    double *Ptotal;

   //{  Parameters for analysis
    unsigned long nExtIter;			/*!< \brief Number of external iterations. */
	unsigned long ExtIter;			/*!< \brief Current external iteration number. */
	unsigned long IntIter;			/*!< \brief Current internal iteration number. */
	unsigned long nIntIter;			/*!< \brief Number of internal iterations (Dual time Method). */
    double DeltaTime;
    double TotalTime;
    double CurrentTime;

    double *Mat_Para;
    double *Mat_Para_Fluid;
    double Damp_Coef;
    double *Gravity;

    unsigned long Wrt_Sol_Freq,	/*!< \brief Writing solution frequency. */
	Wrt_Con_Freq,				/*!< \brief Writing convergence history frequency (on screen). */
	Wrt_Con_Freq_DualTime;				/*!< \brief Writing convergence history frequency (on screen). */


    unsigned long CurrentFSIIter;

    double Residual_min;
   //}

    map<string, CAnyOptionRef*> param; /*!< \brief associates option names (strings) with options */

public:
	/*!
	 * \brief Constructor of the class which reads the input file.
	 */
    CConfig(char case_filename[200]);

	/*!
	 * \brief Destructor of the class.
	 */
	~CConfig(void);

	/*!
	 * \brief add a scalar option to the param map and set its default value
	 * \param[in] name - name of the option as it appears in the .cfg file
	 * \param[in,out] option - the option to associate with name
	 * \param[in] default_value - option is set to default_value
	 * \tparam T - an arbitary type (int, double, enum, etc)
	 */
	template <class T, class T_default>
	void AddScalarOption(const string & name, T & option, const T_default & default_value) {
		//cout << "Adding Scalar option " << name << endl;
		option = static_cast<T>(default_value);
		CAnyOptionRef* option_ref = new COptionRef<T>(option);
		param.insert( pair<string, CAnyOptionRef*>(string(name), option_ref) );
	}

    /*!
	 * \brief add an enum-based option to the param map and set its default value
	 * \param[in] name - name of the option as it appears in the .cfg file
	 * \param[in,out] option - the option to associate with name
	 * \param[in] Tmap - a map from strings to type Tenum
	 * \tparam T - the option type (usually unsigned short)
	 * \tparam Tenum - an enumeration assocaited with T
	 * \param[in] default_value - option is set to default_value
	 */
	template <class T, class Tenum>
	void AddEnumOption(const string & name, T & option, const map<string, Tenum> & Tmap,
			const string & default_value) {
		//cout << "Adding Enum option " << name << endl;
		typename map<string,Tenum>::const_iterator it;
		it = Tmap.find(default_value);
		if (it == Tmap.end()) {
			cerr << "Error in CConfig::AddEnumOption(string, T&, const map<string, Tenum> &, const string): "
					<< "cannot find " << default_value << " in given map."
					<< endl;
			throw(-1);
		}
		option = it->second;
		CAnyOptionRef* option_ref = new CEnumOptionRef<T,Tenum>(option, Tmap);
		param.insert( pair<string, CAnyOptionRef*>(name, option_ref) );
	}

	/*!
	 * \brief add an array option to the param map and set its default value
	 * \param[in] name - name of the option as it appears in the .cfg file
	 * \param[in] size - length of option and default_value arrays
	 * \param[in,out] option - the option to associate with name
	 * \param[in] default_value - option is set to default_value
	 * \param[in] update - set to true if the option has already been initialized
	 * \tparam T - an arbitary type (int, double, enum, etc)
	 *
	 * The option array is allocated in this function.  If memory for
	 * the option array is already allocated, this memory is first released.
	 */
	template <class T>
	void AddArrayOption(const string & name, const int & size, T* & option,
			const T* default_value, const bool & update = false) {
		//cout << "Adding Array option " << name << endl;
		if (update && option != NULL) delete [] option;
		option = new T[size];
		for (int i = 0; i < size; i++)
			option[i] = default_value[i];
		CAnyOptionRef* option_ref = new COptionRef<T>(option, size);
		param.insert( pair<string, CAnyOptionRef*>(name, option_ref) );
	}

	/*!
	 * \brief add a special option to the param map and set its default value
	 * \param[in] name - string name of the option as it appears in the .cfg file
	 * \param[in,out] option - the option to associate with name
	 * \param[in] set_value - function to parse string vectors and set option
	 * \param[in] default_value - option is set to default_value
	 * \tparam T - an arbitrary type (int, double, bool, enum, etc)
	 */
	template <class T>
	void AddSpecialOption(const string & name, T & option,
			void (*set_value)(T*, const vector<string>&),
			const T & default_value) {
		//cout << "Adding Special option " << name << endl;
		option = default_value;
		CAnyOptionRef* option_ref = new COptionRef<T>(option, set_value);
		param.insert( pair<string, CAnyOptionRef*>(name, option_ref) );
	}

	/*!
	 * \brief add a marker-type option to the param map
	 * \param[in] name - name of the marker option as it appears in the .cfg file
	 * \param[in,out] num_marker - number of boundary markers
	 * \param[in,out] marker - an array of boundary marker names
	 */
	void AddMarkerOption(const string & name, unsigned short & num_marker, string* & marker);

	/*!
	 * \brief adds an displacement marker option to the param map
	 * \param[in] name - name of the displacement marker option in the config file
	 * \param[in] nMarker_Displacement - the number of displacement marker boundaries
	 * \param[in] Marker_Displacement - string names of displacement boundaries
	 * \param[in] Displ_Value - Specified displacement for displacement boundaries
	 */
	void AddMarkerDisplacement(const string & name, unsigned short & nMarker_Displacement,
			string* & Marker_Displacement, unsigned short* & Displx_idx, unsigned short* & Disply_idx, unsigned short* & Displz_idx,
			double* & Displx_Value, double* & Disply_Value, double* & Displz_Value);

    /*!
	 * \brief adds an velocity marker option to the param map
	 * \param[in] name - name of the velocity marker option in the config file
	 * \param[in] nMarker_Velocity - the number of velocity marker boundaries
	 * \param[in] Marker_Velocity - string names of velocity boundaries
	 * \param[in] Veloc_X_Idx - indicator for free or constrained in this direction.
	 * \param[in] Veloc_Value - Specified velocity for velocity boundaries
	 */
	void AddMarkerVelocity(const string & name, unsigned short & nMarker_Velocity,string* & Marker_Velocity,
						unsigned short* & V_X_Idx, unsigned short* & V_Y_Idx, unsigned short* & V_Z_Idx,
						double* & V_X, double* & V_Y, double* & V_Z);


     /*!
	 * \brief adds an pressure marker option to the param map
	 * \param[in] name - name of the pressure marker option in the config file
	 * \param[in] nMarker_Outlet - the number of pressure marker boundaries
	 * \param[in] Marker_Outlet - string names of pressure boundaries
	 * \param[in] Displ_Value - Specified pressure for pressure boundaries
	 */
	void AddMarkerPressure(const string & name, unsigned short & nMarker_Pressure,
			string* & Marker_Pressure, double* & Pressure);

    void AddMarkerInlet(const string & name, unsigned short & nMarker_Inlet,
			string* & Marker_Inlet, double* & Ttotal, double* & Ptotal);



	/*!
	 * \brief used to set Boolean values based on strings "YES" and "NO"
	 * \param[in] ref - a pointer to the boolean value being assigned
	 * \param[in] value - value[0] is "YES" or "NO" and determines the value of ref
	 */
	static void SetBoolOption(bool* ref, const vector<string> & value);

	/*!
	 * \brief breaks an input line from the config file into a set of tokens
	 * \param[in] str - the input line string
	 * \param[out] option_name - the name of the option found at the beginning of the line
	 * \param[out] option_value - the tokens found after the "=" sign on the line
	 * \returns false if the line is empty or a commment, true otherwise
	 */
	bool TokenizeString(string & str, string & option_name,
			vector<string> & option_value);

	/*!
	 * \brief Get name of the input structural grid.
	 * \return File name of the input structural grid.
	 */
	string GetMesh_FileName(void);

	/*!
	 * \brief Get name of the output structural loads.
	 * \return File name of the output structural loads.
	 */
	string GetOutput_FileName(void);

    /*!
	 * \brief Set the config options.
	 */
	void SetConfig_Options(void);

    /*!
	 * \brief Set the config file parsing.
	 */
    void SetParsing(char case_filename[200]);

    /*!
	 * \brief Get method type.
	 */
    unsigned short GetKind_Method(void);

    /*!
	 * \brief Get material type.
	 */
    unsigned short GetKind_Material(void);

    /*!
	 * \brief Get solver type.
	 */
    unsigned short GetKind_Solver(void);
    /*!
	 * \brief Get Element type.
	 */
    unsigned short GetKind_Elem(void);

    /*!
	 * \brief Get the total number of boundary markers.
	 * \return Total number of boundary markers.
	 */
	unsigned short GetnMarker_All(void);

	/*!
	 * \brief Set the total number of boundary markers.
	 * \return no
	 */
	void SetnMarker_All(unsigned short nMarker_all);

	/*!
	 * \brief Get the total number of displacement boundary markers.
	 * \return Total number of displacement boundary markers.
	 */
	unsigned short GetnMarker_Displacement(void);
	string GetMarkerTag_Displacement(unsigned short iMarker);

    /*!
	 * \brief Get the total number of Velocity boundary markers.
	 * \return Total number of Velocity boundary markers.
	 */
	unsigned short GetnMarker_Velocity(void);
	string GetMarkerTag_Velocity(unsigned short iMarker);
	/*!
	 * \brief Get the total number of pressure boundary markers.
	 * \return Total number of pressure boundary markers.
	 */
	unsigned short GetnMarker_Pressure(void);
	string GetMarkerTag_Pressure(unsigned short iMarker);



	/*!
	 * \brief Get the total number of inlet markers.
	 * \return Total number of inlet markers.
	 */
	unsigned short GetnMarker_Inlet(void);

	/*!
	 * \brief Get the total number of FSI interface markers.
	 * \return Total number of FSI markers.
	 */
	unsigned short GetnMarker_FSI(void);
	string GetMarkerTag_FSI(unsigned short iMarker);

	/*!
	 * \brief Get the  number of external iterations
	 * \return Number of external iterations.
	 */
	unsigned long GetnExtIter(void);

	/*!
	 * \brief Get the  current of external iterations
	 * \return Currentexternal iterations.
	 */
	unsigned long GetExtIter(void);

	/*!
	 * \brief Get the  number of internal iterations
	 * \return Number of internal iterations.
	 */
	unsigned long GetnIntIter(void);


	/*!
	 * \brief Get the current internal iterations
	 * \return Current internal iterations.
	 */
	unsigned long GetIntIter(void);

	/*!
	 * \brief Get the option of geometrical nonlinearity
	 * \return geometrical nonlineary flag, Yes or No.
	 */
	bool GetGeo_NL(void);

	/*!
	 * \brief Get Writing solution frequency
	 * \return Writing solution frequency
	 */
	unsigned long GetWrt_Sol_Freq(void);

	/*!
	 * \brief Get Writing convergence frequency
	 * \return Writing convergence frequency
	 */
	unsigned long GetWrt_Con_Freq(void);

    /*!
	 * \brief Get Writing convergence frequency dual time
	 * \return Writing convergence frequency dual time
	 */
	unsigned long GetWrt_Con_Freq_DualTime(void);


    /*!
	 * \brief Get total time of computation
	 * \return total time
	 */
    double GetTotalTime(void);

	/*!
	 * \brief Get time step of computation
	 * \return time step length
	 */
    double GetDeltaTime(void);

    /*!
	 * \brief Get displacement X of i-th displacement marker
	 * \return  dispalcement X of i-th displacement marker
	 */
    double GetDisplX(string val_index);
    double GetDisplY(string val_index);
    double GetDisplZ(string val_index);

    /*!
	 * \brief Get indicator of displacement X of i-th displacement marker
	 * \return  indicator of dispalcement X of i-th displacement marker
	 */
    unsigned short GetDisplX_Indicator(string val_index);
    unsigned short GetDisplY_Indicator(string val_index);
    unsigned short GetDisplZ_Indicator(string val_index);

    /*!
	 * \brief Get indicator of velcoity X of i-th displacement marker
	 * \return  indicator of velocity X of i-th displacement marker
	 */
	unsigned short GetVelocX_Indicator(string val_index);
	unsigned short GetVelocY_Indicator(string val_index);
	unsigned short GetVelocZ_Indicator(string val_index);


	/*!
	 * \brief Get value of velcoity X of i-th displacement marker
	 * \return  value of velocity X of i-th displacement marker
	 */
	double GetVelocX(string val_index);
	double GetVelocY(string val_index);
	double GetVelocZ(string val_index);
	/*!
	 * \brief Get value of pressure of i-th pressure marker
	 * \return  value of pressure  of i-th pressure marker
	 */
	double GetPressure(string val_index);

    /*!
	 * \brief Get number of markers in configure file.
	 * \return number of markers in configure file.
	 */
    unsigned short GetnMarker_Config(void);

    double* GetMat_Para(void);

    double* GetMat_Para_Fluid(void);

    double GetDamp_Coef(void);

    double* GetGravity(void);

    bool GetRestartOption(void);

    string GetRestartSDFileName(void);

    string GetRestartDATAFileName(void);

    string GetFSILoadFileName(void);

    string GetFSIDisplacementFileName(void);

    unsigned long GetCurrentFSIIter(void);

    string GetFSIMarkerMeshFileName(void);

    double GetDensity(void);

    unsigned short GetKind_PostProcessor(void);

    string GetHistoryFileName(void);

    double GetResidualMin(void);

 };
#include "config.inl"


#endif // CONFIG_H
