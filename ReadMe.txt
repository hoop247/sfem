////////////////////////////////////////////////////////////////////////////
//            SSSSSS  FFFFFF  EEEEEE  MMM   MMM        ++       ++        //
//           SS       FF      EE      MM M M MM       ++       ++         //
//           SSSSSSS  FFFF    EEEE    MM  M  MM   ++++++++ ++++++++       //
//                SS  FF      EE      MM     MM     ++       ++           //
//                SS  FF      EE      MM     MM    ++       ++            //
//           SSSSSS   FF      EEEEEE  MM     MM   ++       ++             //
////////////////////////////////////////////////////////////////////////////

Main Developer : Chen Jiang
Log Date : 2016-11-15-15.31


==========================================
1. Compiling
==========================================
All developments are done under Code::Block 13.12 with Gcc 4.8.1;

The linear algebra library is the Eigen3.2.7-dev, download and install follow

Please open the C++11 option of your gcc complier.



==========================================
2. Log
==========================================

2.1
-----------------------------------------------
mat or field in Armadillo can be inline functions return vars.

2.2
-----------------------------------------------
Although the Arma_mat is column-major index. However, the accessing speed seems
similar when doing row-major accessing.
For 4 million double mat, self adding test.

However, for the bulid-in vector<double>, it has differences when using different accessing methods.

2.3 2015/5/27 19:00:44
----------------------------------------------
In Armadillo, to free the memory used by a mat or vec, just use .reset()!

2.3 2015/5/29 10:56:29
----------------------------------------------
In C++, the unsigned long to int is good when the data of unsigned long doesn't exceed the int range.

2.4 2015/0/2015
----------------------------------------------
In current S-FEM code, the diplacement boundary conditions are applied with bugs.
The rotation of leadingedge has been changed to add as velocity boundary conditions. Then, correct.

2.5 2015-08-25-09.07
----------------------------------------------
Add class elements, T4, T3,etc.


2.6 2016/2/22 14:42:11
----------------------------------------------
Add new features for Python mesh format convertor, now can handle T3 2D elements, line elements as marker.
Also use Mass element in Abaqus as the Vertex marker.

2.6 2016/2/26 11:23:11
----------------------------------------------
2D CBS Semi-Implicit with FEM-T3 is finished.
At Workstation with Dual E5-2670v2 and Tesla K40, a 2D lid-driven case with 2601 nodes and 5000 elements takes about 95s for 20000 steps.


2.7 2016/3/2 11:23:11
----------------------------------------------
2D CBS Semi-Implicit with ES-FEM-T3 is finished.
At Workstation with Dual E5-2620v2 and Tesla K40.


2.7 2016-11-15-07.31
----------------------------------------------
2D and 3D FEM and ES-FEM for geometric nonlinear solid with StVenant Material is done.
And Armadillo is no longer used, only Eigen is used.

==========================================
3. Usage
==========================================
sfem_oo_FSI is only for the FSI calculation.

